package com.zerokol.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

@SuppressLint("ClickableViewAccessibility")
public class JoystickViewModif extends View{
	private final double RAD = 57.2957795;
	
	private int xPosition = 0;
	private int yPosition = 0;
	private double centerX = 0;
	private double centerY = 0;

	private Paint button;
	private int joystickRadius;
	private int buttonRadius;
	private int lastAngle = 0;
	private int lastPower = 0;
	private Rect mRect;
	private GradientDrawable mDrawable;
	int theme_id = 0;

	public JoystickViewModif(Context context) {
		super(context);
	}

	public JoystickViewModif(Context context, AttributeSet attrs) {
		super(context, attrs);
		initJoystickView(context);
	}

	public JoystickViewModif(Context context, AttributeSet attrs,
			int defaultStyle) {
		super(context, attrs, defaultStyle);
		initJoystickView(context);
	}

	@SuppressLint("NewApi")
	protected void initJoystickView(Context context) {

		button = new Paint(Paint.ANTI_ALIAS_FLAG);
		button.setColor(Color.WHITE);
		button.setStyle(Paint.Style.FILL);

		mRect = new Rect(0, 0, 90, 400);
		mDrawable = new GradientDrawable();

		mDrawable.setOrientation(GradientDrawable.Orientation.TL_BR);

		mDrawable.setColor(Color.parseColor("#00000000"));

		mDrawable.setShape(GradientDrawable.RECTANGLE);
		mDrawable.setStroke(6, Color.WHITE);
		mDrawable.setGradientRadius((float) (Math.sqrt(2) * 60));
	}

	static void setCornerRadii(GradientDrawable drawable, float r0, float r1,
			float r2, float r3) {
		drawable.setCornerRadii(new float[] { r0, r0, r1, r1, r2, r2, r3, r3 });
	}

	@SuppressLint("MissingSuperCall")
	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
	}

	@Override
	protected void onSizeChanged(int xNew, int yNew, int xOld, int yOld) {
		super.onSizeChanged(xNew, yNew, xOld, yOld);
		xPosition = (int) getWidth() / 2;
		yPosition = (int) getWidth() * 2;

		buttonRadius = (int) (300 / 2 * 0.25);
		joystickRadius = (int) (435 / 2 * 0.75);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

		setMeasuredDimension(100, 400);

	}

	@Override
	protected void onDraw(Canvas canvas) {

		centerX = (getWidth()) / 2;
		centerY = (getHeight()) / 2;

		mDrawable.setBounds(mRect);

		float r = 100;

		canvas.save();

		mDrawable.setGradientType(GradientDrawable.LINEAR_GRADIENT);
		setCornerRadii(mDrawable, r, r, r, r);
		mDrawable.draw(canvas);
		canvas.drawCircle(45, yPosition, buttonRadius, button);

	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {

		xPosition = (int) centerX;
		yPosition = (int) event.getY();

		double abs = Math.sqrt((xPosition - centerX) * (xPosition - centerX)
				+ (yPosition - centerY) * (yPosition - centerY));
		if (abs > joystickRadius) {
			xPosition = (int) centerX;
			yPosition = (int) ((yPosition - centerY) * joystickRadius / abs + centerY);
		}
		invalidate();
		if (event.getAction() == MotionEvent.ACTION_UP) {
			xPosition = (int) centerX;
			yPosition = (int) centerY;
			
		}
	
		return true;
	}

	public int getAngle() {
		if (xPosition > centerX) {
			if (yPosition < centerY) {
				return lastAngle = (int) (Math.atan((yPosition - centerY)
						/ (xPosition - centerX))
						* RAD + 90);
			} else if (yPosition > centerY) {
				return lastAngle = (int) (Math.atan((yPosition - centerY)
						/ (xPosition - centerX)) * RAD) + 90;
			} else {
				return lastAngle = 90;
			}
		} else if (xPosition < centerX) {
			if (yPosition < centerY) {
				return lastAngle = (int) (Math.atan((yPosition - centerY)
						/ (xPosition - centerX))
						* RAD - 90);
			} else if (yPosition > centerY) {
				return lastAngle = (int) (Math.atan((yPosition - centerY)
						/ (xPosition - centerX)) * RAD) - 90;
			} else {
				return lastAngle = -90;
			}
		} else {
			if (yPosition <= centerY) {
				return lastAngle = 0;
			} else {
				if (lastAngle < 0) {
					return lastAngle = -180;
				} else {
					return lastAngle = 180;
				}
			}
		}
	}

	public int getPower() {
		return (int) (10 * Math.sqrt((xPosition - centerX)
				* (xPosition - centerX) + (yPosition - centerY)
				* (yPosition - centerY)) / joystickRadius);
	}

	public int getDirection() {

		if (getPower() > 2 && getAngle() == 0)
			return 3;

		if (getPower() > 2 && getAngle() == 180)
			return 7;

		if ((getPower() < 2 && getAngle() == 180 && getPower() > 1)
				|| (getPower() < 2 && getAngle() == 0 && getPower() > 1))
			return 9;

		if (lastPower == 0 && lastAngle == 0) {
			return 0;
		}

		return 0;
	}

	

}