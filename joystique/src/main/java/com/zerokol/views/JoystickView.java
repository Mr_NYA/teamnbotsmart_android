package com.zerokol.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

@SuppressLint({ "ClickableViewAccessibility", "NewApi" })
public class JoystickView extends View {

	// Variables
	private Bitmap bitmap;
	public Point mRockerPosition;
	public Point mCtrlPoint;
	private int mRudderRadius = 30;// æ‘‡æ�†å�Šå¾„
	public int mmWheelRadius = 100;// æ‘‡æ�†æ´»åŠ¨èŒƒå›´å�Šå¾„
	public int mWheelRadius = 100;
	private float scale;
	public int isHide = 0;
	private Paint mPaint;
	public float pX;
	public float pY;
	public float pXmax;
	public float pYmax;
	public boolean flagClicked = false;
	

	public JoystickView(Context context) {
		super(context);
	}

	public JoystickView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public JoystickView(Context context, AttributeSet attrs, int defaultStyle) {
		super(context, attrs, defaultStyle);
	}

	public void initJoystickView(float scale) {
		this.scale = scale;
		mRudderRadius = dip2px(30);
		mWheelRadius = dip2px(100);
		mmWheelRadius = dip2px(100);
		bitmap = BitmapFactory.decodeResource(getResources(),
				R.drawable.controlpoint);
		bitmap = Bitmap.createScaledBitmap(bitmap, mRudderRadius * 2,
				mRudderRadius * 2, false);
		mCtrlPoint = new Point(mWheelRadius + mRudderRadius, mWheelRadius
				+ mRudderRadius);
		mPaint = new Paint();
		mPaint.setAntiAlias(true);
		mRockerPosition = new Point(mCtrlPoint);
		pX = (float) 0.0;
		pY = (float) 0.0;
		
		
	}

	public int dip2px(float dpValue) {
		return (int) (dpValue * scale + 0.5f);
	}

	public Canvas canvas;

	@Override
	protected void onDraw(Canvas canvas) {
		if (bitmap != null) {
			this.canvas = canvas;
			canvas.drawBitmap(bitmap, mRockerPosition.x - mRudderRadius,
					mRockerPosition.y - mRudderRadius, mPaint);

		}
		super.onDraw(canvas);
	}

	int len;

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		try {
			if (isHide == 0) {
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					flagClicked = true;

					len = getLength(mCtrlPoint.x, mCtrlPoint.y, event.getX(),
							event.getY());

					if (len > mmWheelRadius) {
						return true;
					}
					break;
				case MotionEvent.ACTION_MOVE:
					pYmax=(float) ((0.76/2)*getHeight());
					
					pXmax =(float) ((-0.76/2)*getWidth());

					len = getLength(mCtrlPoint.x, mCtrlPoint.y, event.getX(),
							event.getY());
			
					
					if (len <= mmWheelRadius) {


						mRockerPosition.set((int) event.getX(),
								(int) event.getY());
						pX = mCtrlPoint.x - event.getX();
						pY = mCtrlPoint.y - event.getY();

					} else {

						mRockerPosition = getBorderPoint(mCtrlPoint, new Point(
								(int) event.getX(), (int) event.getY()),
								mmWheelRadius);

						pX = mCtrlPoint.x - mRockerPosition.x;
						pY= mCtrlPoint.y - mRockerPosition.y;

					}

					break;
				case MotionEvent.ACTION_UP:
					flagClicked = false;
					mRockerPosition = new Point(mCtrlPoint);
					pX = (float) 0.0;
					pY = (float) 0.0;
					break;
				}
			} else {
			}
		} catch (Exception e) {

		}
		invalidate();
		return true;
	}

	public static int getLength(float x1, float y1, float x2, float y2) {
		return (int) Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
	}

	public static Point getBorderPoint(Point a, Point b, int cutRadius) {
		float radian = getRadian(a, b);
		return new Point(a.x + (int) (cutRadius * Math.cos(radian)), a.x
				+ (int) (cutRadius * Math.sin(radian)));
	}

	public static float getRadian(Point a, Point b) {
		float lenA = b.x - a.x;
		float lenB = b.y - a.y;
		float lenC = (float) Math.sqrt(lenA * lenA + lenB * lenB);
		float ang = (float) Math.acos(lenA / lenC);
		ang = ang * (b.y < a.y ? -1 : 1);
		return ang;
	}

	double map(double input, double toMin, double toMax, double fromMin,
			double fromMax) {

		return ((input - fromMin) / (fromMax - fromMin)) * (toMax - toMin)
				+ toMin;
	}
	double center(double input, double maxValue) {
		  return map(input,-1,1,0,maxValue);
		}
}
