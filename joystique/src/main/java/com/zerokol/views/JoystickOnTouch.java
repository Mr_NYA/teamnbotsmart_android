package com.zerokol.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

@SuppressLint({ "ClickableViewAccessibility", "NewApi" })
public class JoystickOnTouch extends View {

	// Variables
	public Point mCtrlPoint;
	private float scale;
	public int isHide = 0;
	private Paint mPaint;
	public float pX;
	public float pY;
	public float pXmax;
	public float pYmax;
	public boolean flagClicked = false;

	public JoystickOnTouch(Context context) {
		super(context);
	}

	public JoystickOnTouch(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public JoystickOnTouch(Context context, AttributeSet attrs, int defaultStyle) {
		super(context, attrs, defaultStyle);
	}

	public void initJoystickView(float scale, int x, int y, int statusBarHeight) {
		this.scale = scale;
		this.scale = scale;

		pXmax = (float) -x / 2;
		pYmax = (float) (y - statusBarHeight) / 2;

		mCtrlPoint = new Point(x / 2, y / 2);

		mPaint = new Paint();
		mPaint.setAntiAlias(true);
		pX = (float) 0.0;
		pY = (float) 0.0;

	}

	public int dip2px(float dpValue) {
		return (int) (dpValue * scale + 0.5f);
	}

	public Canvas canvas;

	@Override
	protected void onDraw(Canvas canvas) {

		super.onDraw(canvas);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		try {
			if (isHide == 0) {
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					flagClicked = true;

					break;
				case MotionEvent.ACTION_MOVE:

					pX = mCtrlPoint.x - event.getRawX();
					pY = mCtrlPoint.y - event.getRawY();



					break;
				case MotionEvent.ACTION_UP:
					flagClicked = false;
					pX = (float) 0.0;
					pY = (float) 0.0;
					break;
				}
			} else {
			}
		} catch (Exception e) {

		}
		invalidate();// 更新布局
		return true;
	}

	public static int getLength(float x1, float y1, float x2, float y2) {
		return (int) Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
	}

	public static Point getBorderPoint(Point a, Point b, int cutRadius) {
		float radian = getRadian(a, b);
		return new Point(a.x + (int) (cutRadius * Math.cos(radian)), a.x
				+ (int) (cutRadius * Math.sin(radian)));
	}

	public static float getRadian(Point a, Point b) {
		float lenA = b.x - a.x;
		float lenB = b.y - a.y;
		float lenC = (float) Math.sqrt(lenA * lenA + lenB * lenB);
		float ang = (float) Math.acos(lenA / lenC);
		ang = ang * (b.y < a.y ? -1 : 1);
		return ang;
	}

	double map(double input, double toMin, double toMax, double fromMin,
			double fromMax) {

		return ((input - fromMin) / (fromMax - fromMin)) * (toMax - toMin)
				+ toMin;
	}

	double center(double input, double maxValue) {
		return map(input, -1, 1, 0, maxValue);
	}
}
