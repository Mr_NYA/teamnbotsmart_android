package com.orrobotics.webservice;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import android.os.AsyncTask;
import android.util.Log;

import com.orrobotics.webservice.util.CallbackReceiver;

public class WebService {

	String text = null;


	protected static String getASCIIContentFromEntity(HttpEntity entity)
			throws IllegalStateException, IOException {
		InputStream in = entity.getContent();

		StringBuffer out = new StringBuffer();
		int n = 1;
		while (n > 0) {
			byte[] b = new byte[4096];
			n = in.read(b);

			if (n > 0)
				out.append(new String(b, 0, n));
		}

		return out.toString();
	}


	public String doHttpsRequest(String param){
		URL url= null;
		String text = null;
		while (true){
			try {
				try {
					url = new URL(param);
				} catch (MalformedURLException e) {
					e.printStackTrace();
				}
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Accept", "application/json");
				conn.setDoOutput(true);
				conn.setConnectTimeout(5000);
				conn.setReadTimeout(30000);
				String userAgent = System.getProperty("http.agent");
				Log.i("mbu", "HTTPUrlConnection user-agent=" + userAgent);
				conn.connect();

				int status = conn.getResponseCode();

				if (status != HttpURLConnection.HTTP_OK) {
					Log.e("",
							"Error HttpURLConnection= "
									+ conn.getErrorStream());
					text = "Error " + conn.getErrorStream().toString();

				} else {
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(conn.getInputStream()));
					text = readResponse(reader);


				}
				conn.disconnect();
				break;

			} catch (Exception ex) {
				Log.e("mbu", "Error connect--------");
				ex.printStackTrace();

			}
		}
		return text;
	}
	public String doHttpRequest(String url){
		HttpClient httpClient = new DefaultHttpClient();
		HttpContext localContext = new BasicHttpContext();
		HttpGet httpGet = new HttpGet(url);
		String text = null;

		HttpResponse response;
		httpClient.getParams().setParameter(CoreProtocolPNames.USER_AGENT,
				System.getProperty("http.agent"));


		while (true) {
			try {

				response = httpClient.execute(httpGet, localContext);
				HttpEntity entity = response.getEntity();
				text = getASCIIContentFromEntity(entity);

			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				text = "Error " + e.getMessage().toString();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				text = "Error " + e.getMessage().toString();
			}

			try {
				text = URLDecoder.decode(text, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		}
		return text;
	}
	public abstract class HttpRequest extends AsyncTask<String, Integer, String> implements CallbackReceiver {

		public abstract void receiveData(Object object);
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
			// Mise à jour de la ProgressBar
		}

		@Override
		protected String doInBackground(String... urlString) {
			if(urlString[0].contains("ontomantics"))
				text = doHttpsRequest(urlString[0]);
			else
				text= doHttpRequest(urlString[0]);

			return text;
		}

		@Override
		protected void onPostExecute(String result) {
			text = result;
			if(result!=null)
			{
				receiveData(result);
			}
		}
	}

	private String readResponse(BufferedReader reader) {
		StringBuilder sb = new StringBuilder();
		String NL = System.getProperty("line.separator");
		try {
			for (;;) {
				String line = reader.readLine();
				if (line == null) {
					break;
				}
				sb.append(line).append(NL);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return sb.toString();
	}
}
