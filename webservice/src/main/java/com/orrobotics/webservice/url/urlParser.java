package com.orrobotics.webservice.url;


import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class urlParser {

	public static String getURL_authentification(String url , String email , String password,String type, String pseudoSkype ,String ip, String mac, String android_version,  String application ){
		if(url.contains("ontomantics"))
			return url+"/auth?mail="+email+"&pasword="+password+"&type="+type+"&pseudoSkype="+pseudoSkype+"&ip="+ip+"&mac="+mac+"&version="+android_version+"&application="+application;
		else
			return url+"/auth?mail="+email+"&password="+password+"&type="+type+"&pseudoSkype="+pseudoSkype+"&ip="+ip+"&mac="+mac+"&version="+android_version+"&application="+application;

	}

	public static String getURL_Creation_Compte(String url , String email , String password, String civilite, String nom , String prenom, String naissance){

		if(url.contains("ontomantics")) {
			naissance = changeDateFormat(naissance);
			Log.i("mbu","---naissance="+naissance);

			return url + "/crea?civilite=" + civilite + "&nom=" + nom + "&prenom=" + prenom + "&naissance=" + naissance + "&mail=" + email + "&pasword=" + password;
		}
		else
			return url+"/crea?civilite="+civilite+"&nom="+nom+"&prenom="+prenom+"&naissance="+naissance+"&mail="+email+"&password="+password;

	}

	private static String changeDateFormat(String date) {
		SimpleDateFormat spf=new SimpleDateFormat("dd/MM/yyyy");
		Date newDate=null;
		try {
			newDate=spf.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		spf= new SimpleDateFormat("yyyy-MM-dd");
		return spf.format(newDate);
	}

	public static String getURL_Upadate_Robot(String url , String mac , String etat){

		return url+"/uprobot?mac="+mac+"&etat="+etat;

	}

	public static String getURL_Creation_Compte1(String url , String email , String password, String civilite, String nom , String prenom, String naissance,String pays, String ville){
		if(url.contains("ontomantics"))
			return url+"/crea?civilite="+civilite+"&nom="+nom+"&prenom="+prenom+"&naissance="+naissance+"&mail="+email+"&pasword="+password+"&pays="+pays+"&ville="+ville;
		else
			return url+"/crea?civilite="+civilite+"&nom="+nom+"&prenom="+prenom+"&naissance="+naissance+"&mail="+email+"&password="+password+"&pays="+pays+"&ville="+ville;

	}

	public static String getURL_Modification_Compte(String url , String email , String password, String civilite, String nom , String prenom, String naissance){
		if(url.contains("ontomantics")) {
			naissance = changeDateFormat(naissance);
			return url + "/modi?civilite=" + civilite + "&nom=" + nom + "&prenom=" + prenom + "&naissance=" + naissance + "&mail=" + email + "&pasword=" + password;
		}
		else
			return url+"/modi?civilite="+civilite+"&nom="+nom+"&prenom="+prenom+"&naissance="+naissance+"&mail="+email+"&password="+password;

	}
	public static String getURL_AddRobot(String url , String email , String skype, String nom , int type){

		return url+"/rbtCrea?mail="+email+"&skype="+skype+"&nom="+nom+"&type="+type;

	}

	public static String getURL_SuppRobot(String url , String email , String nom){

		return url+"/rbtRemo?mail="+email+"&nom="+nom;

	}
	public static String getURL_Password(String url , String email){

		return url+"/passforg?mail="+email;

	}

	public static String getURL_UpdaRobot(String url , String email, String skype, String nom, int type, String newNom ){

		return url+"/rbtModi?mail="+email+"&nom="+nom+"&skype="+skype+"&type="+type+"&newNom="+newNom;

	}

	public static String getURL_UpdaRobot(String url , String email, String skype, String nom, int type, String mac, String serie){

		return url+"/rbtModi?mail="+email+"&nom="+nom+"&skype="+skype+"&type="+type+"&mac="+mac+"&serie="+serie;

	}

	public static String getListRobot(String url , String email){

		return url+"/rbtList?mail="+email;

	}

	public static String getSnapList(String url , String email){

		return url+"/snapList?mail="+email;

	}
	public static String getSnapListPublic(String url){

		return url+"/snapListPublic";

	}
}
