package com.orrobotics.webservice.json;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonParser {
	static String uniObject;
	static JSONArray uniObjectArray;
    static boolean uniObjecttt;
	
public static String getAuth(String JsonObject){
	
	JSONObject mainObject;
	try {
		mainObject = new JSONObject(JsonObject);
		 uniObject = mainObject.getString("auth");
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		uniObject = "Parser Json error";
	}
	
	
	return uniObject;	
}

public static String getMessageForPassword(String JsonObject){
	
	JSONObject mainObject;
	try {
		mainObject = new JSONObject(JsonObject);
		 uniObject = mainObject.getString("message");
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		uniObject = "Parser Json error";
	}
	
	
	return uniObject;	
}
public static String getMessage(String JsonObject){
	
	JSONObject mainObject;
	try {
		mainObject = new JSONObject(JsonObject);
		 uniObject = mainObject.getString("nom");
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		uniObject = "Parser Json error";
	}
	
	
	return uniObject;	
}

public static String getMessageCrea(String JsonObject){
	
	JSONObject mainObject;
	try {
		mainObject = new JSONObject(JsonObject);
		 uniObject = mainObject.getString("message");
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		uniObject = "Parser Json error";
	}
	
	
	return uniObject;	
}

public static String getPseuoOpenFire(String JsonObject){
	
	JSONObject mainObject;
	try {
		mainObject = new JSONObject(JsonObject);
		 uniObject = mainObject.getString("pseudoOpenFire");
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		uniObject = "Parser Json error";
	}
	
	
	return uniObject;	
}

public static String getPasswordOpenFire(String JsonObject){
	
	JSONObject mainObject;
	try {
		mainObject = new JSONObject(JsonObject);
		 uniObject = mainObject.getString("passwordOpenFire");
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		uniObject = "Parser Json error";
	}
	
	
	return uniObject;	
}
public static String getPseuoSIP(String JsonObject){
	
	JSONObject mainObject;
	try {
		mainObject = new JSONObject(JsonObject);
		 uniObject = mainObject.getString("pseudoSIP");
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		uniObject = "Parser Json error";
	}
	
	
	return uniObject;	
}

public static String getPasswordSIP(String JsonObject){
	
	JSONObject mainObject;
	try {
		mainObject = new JSONObject(JsonObject);
		 uniObject = mainObject.getString("passwordSIP");
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		uniObject = "Parser Json error";
	}
	
	
	return uniObject;	
}

public static String getCode_Crea(String JsonObject){
	
	JSONObject mainObject;
	try {
		mainObject = new JSONObject(JsonObject);
		 uniObject = Integer.toString(mainObject.getInt("code"));
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		uniObject = "Parser Json error";
	}
	
	
	return uniObject;	
}

public static JSONArray getListRobot(String JsonObject){

	JSONObject mainObject;
	
	try {
		mainObject = new JSONObject(JsonObject);
		uniObjectArray = mainObject.getJSONArray("listRobot");
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		
	}
	
	
	return uniObjectArray;	
}



public static String getTypeRobot(String JsonObject){
	
	JSONObject mainObject;
	try {
		mainObject = new JSONObject(JsonObject);
		 uniObject = mainObject.getString("typeRobot");
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		uniObject = "Parser Json error";
	}
	
	
	return uniObject;	
}
public static String getIdRobot(String JsonObject){
	
	JSONObject mainObject;
	try {
		mainObject = new JSONObject(JsonObject);
		 uniObject = mainObject.getString("id");
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		uniObject = "Parser Json error";
	}
	
	
	return uniObject;	
}

public static String getPseudoSkype(String JsonObject){
	
	JSONObject mainObject;
	try {
		mainObject = new JSONObject(JsonObject);
		 uniObject = mainObject.getString("pseudoSkype");
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		uniObject = "Parser Json error";
	}
	
	
	return uniObject;	
}

public static String getNom(String JsonObject){
	
	JSONObject mainObject;
	try {
		mainObject = new JSONObject(JsonObject);
		 uniObject = mainObject.getString("nom");
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		uniObject = "Parser Json error";
	}
	
	
	return uniObject;	
}

public static String getPrenom(String JsonObject){
	
	JSONObject mainObject;
	try {
		mainObject = new JSONObject(JsonObject);
		 uniObject = mainObject.getString("prenom");
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		uniObject = "Parser Json error";
	}
	
	
	return uniObject;	
}

public static String getEmail(String JsonObject){
	
	JSONObject mainObject;
	try {
		mainObject = new JSONObject(JsonObject);
		 uniObject = mainObject.getString("mail");
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		uniObject = "Parser Json error";
	}
	
	
	return uniObject;	
}

public static String getMacAdresse(String JsonObject){
	
	JSONObject mainObject;
	try {
		mainObject = new JSONObject(JsonObject);
		 uniObject = mainObject.getString("adresseMAC");
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		uniObject = "Parser Json error";
	}
	
	
	return uniObject;	
}


public static String getSerie(String JsonObject){
	
	JSONObject mainObject;
	try {
		mainObject = new JSONObject(JsonObject);
		 uniObject = mainObject.getString("numeroSerie");
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		uniObject = "Parser Json error";
	}
	
	
	return uniObject;	
}

public static Boolean getValidate(String JsonObject){
	
	JSONObject mainObject;
	try {
		mainObject = new JSONObject(JsonObject);
		 uniObjecttt = mainObject.getBoolean("valide");
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		uniObject = "Parser Json error";
	}
	
	
	return uniObjecttt;	
}

public static String getXML(String JsonObject) {

	JSONObject mainObject;
	try {
		mainObject = new JSONObject(JsonObject);
		uniObject = mainObject.getString("xml");
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		uniObject = "Parser Json error";
	}
	return uniObject;
}


public static String getid(String JsonObject) {

	JSONObject mainObject;
	try {
		mainObject = new JSONObject(JsonObject);
		uniObject = Integer.toString(mainObject.getInt("id"));
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		uniObject = "Parser Json error";
	}
	return uniObject;
}

public static Boolean IsPrivate(String JsonObject) {

	JSONObject mainObject;
	try {
		mainObject = new JSONObject(JsonObject);
		uniObjecttt = mainObject.getBoolean("prive");
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		uniObject = "Parser Json error";
	}

	return uniObjecttt;
}

}
