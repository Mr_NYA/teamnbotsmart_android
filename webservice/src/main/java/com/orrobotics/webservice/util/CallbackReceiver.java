package com.orrobotics.webservice.util;

/**
 * Created by MBU on 25/12/2018.
 */

public interface CallbackReceiver {
    public void receiveData(Object result);

}
