package com.orrobotics.webservice.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import android.util.Log;

public class ConfigurationFile {

	public static CustomProperties props = new CustomProperties();
	public static CustomProperties uboProps = new CustomProperties();
	public static CustomProperties sanbotProps = new CustomProperties();
	public static InputStream is = null;
	public static int commentCmpt = 1;
	private static int defaltCommNbr = 6;

	public static void creerFichier(File file1, String fichier) {

		if (file1.exists() && file1.isDirectory()) {
			Log.v("etat repertoire", "Existe");
		} else {
			file1.mkdir();
			Log.v("etat repertoire", "Existe pas");
		}

	}

	public static CustomProperties loadproperties(File file, String fichier,
												  CustomProperties properties) {

		// First try loading from the current directory
		try {
			File f = new File(file, fichier);
			is = new FileInputStream(f);

			if (is == null) {
				// Try loading from classpath
				ClassLoader loader = Thread.currentThread()
						.getContextClassLoader();
				is = loader.getResourceAsStream(fichier);
			}
			// Try loading properties from the file (if found)
			if (is != null) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(is, "UTF-8"));
				properties.load(reader);
			}

		} catch (Exception e1) {
			Log.d("INFO", "Erreur loading file");
		} finally {
			try {
				if (is != null) {
					is.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return properties;
	}

	public static void WritePropertiesFile(File file1, File file2) {

		Log.v("", "------WritePropertiesFile-------");
		try {

			props = new CustomProperties();

			// paramètres pour SmartControl
			if (file1.getAbsolutePath().contains("SmartControl")) {

				/**************** Paramètres généraux ************/

				setProperty("param",
						"https://teamnet.ontomantics.net/webservices/esapce_client", props,
						"Paramètres généraux", false);
				setProperty("server", "35.197.203.96", props, null, false);
				setProperty("transport", "tcp", props, null, false);
				setProperty("linphone.videoCodec", "VP8", props, null, false);
				setProperty("call", "linphone", props, null, false);
				setProperty("theme", "blue", props, null, false);
				setProperty("logo.fileName", "null", props, null, false);
				setProperty("delai", "20", props, null, false);
				setProperty(
						"boutons.position",
						"haut/bas(horizontal,droite);gauche/droite(horizontal,gauche)",
						props, null, false);
				setProperty("message.lancerApplication.fr",
						"Ok Je vais ouvrir le programme", props, null, false);

				setProperty("message.lancerApplication.en",
						"Ok I will open the program", props, null, false);

				setProperty("message.arreterApplication.fr",
						"Je vais arréter le programme", props, null, false);
				setProperty("message.arreterApplication.en",
						"I will stop the program", props, null, false);

				setProperty("message.connexion.fr",
						"Bonjour la télé présence est démarré", props, null,
						false);
				setProperty("message.connexion.en",
						"Hello telepresence is started", props, null, false);

				setProperty("debit.download.min", "500 Kbps", props, null, false);
				setProperty("debit.upload.min", "500 Kbps", props, null, false);
				setProperty("debit.test.duration", "5 s", props, null, false);
				setProperty("autoConnexion.server", "5 s", props, null, false);
				setProperty("debit.indicator", "icone", props, null, false);
				setProperty("endCall.timeout", "5 s", props, null, false);
				setProperty("debit.upload.max", "10 Mbps", props, null, false);
				setProperty("debit.download.max", "100 Mbps", props, null, false);

				/******************** Propriétées du robot *******************************/

				setProperty("commande.frequence", "3 envoies/seconde", props,
						"Propriétées du robot", false);
				setProperty("commande.frequence.head", "1000 ms", props, null,
						false);
				setProperty("mindwave.avancer",
						"concentration>60;clind'oiel=1", props, null, false);
				setProperty("mindwave.reculer", "meditation>60;clind'oiel=1",
						props, null, false);
				setProperty("mindwave.tourner-a-gauche",
						"concentration>60;clind'oiel>=2", props, null, false);
				setProperty("mindwave.tourner-a-droite",
						"meditation>60;clind'oiel>=2", props, null, false);
				setProperty("mindwave.priority", "concentration", props, null,
						false);

				/******************** Commandes vocales *******************************/
				setProperty("commande.timout", "5 s", props,
						"Commandes vocales", false);
				setProperty("Frequence.declenchement", "60", props, null, false); // minute
				setProperty("head.up", "4", props, null, false);
				setProperty("head.down", "4", props, null, false);
				setProperty("head.left", "4", props, null, false);
				setProperty("head.right", "4", props, null, false);
				setProperty("vocale.mode.3.keywordNumber", "1", props, null,
						false);
				setProperty("delai.affichage", "2 s", props, null, false);
				setProperty("processus.sequence", "3", props, null, false);

				setProperty("Message.invitation.fr",
						"Que puis-je faire pour vous ?", props, null, false);
				setProperty("Message.invitation.en", "What can I do for you ?",
						props, null, false);
				setProperty("Commande.rienEntendue.fr",
						"Je n’ai pas entendu de commande !", props, null, false);
				setProperty("Commande.rienEntendue.en",
						"I didn't hear any order!", props, null, false);
				setProperty("Commande.inconnue.fr",
						"Je n’ai pas compris votre commande !", props, null,
						false);
				setProperty("Commande.inconnue.en",
						"I didn't understand your order !", props, null, false);

			}
			// paramètres pour SmartRobot
			if (file1.getAbsolutePath().contains("SmartRobot")) {

				/**************** Paramètres généraux ************/
				setProperty("param",
						"https://teamnet.ontomantics.net/webservices/esapce_client", props,
						"Paramètres généraux", false);
				setProperty("server", "35.197.203.96", props, null, false);
				setProperty("kurento", "149.202.194.24:3021", props, null, false);
				setProperty("snap",
						"http://149.202.194.24:20080/snap/robotics_player/",
						props, null, false);
				setProperty("transport", "tcp", props, null, false);
				setProperty("linphone.videoCodec", "H264", props, null, false);
				setProperty("call", "webrtc", props, null, false);

				setProperty("theme", "blue", props, null, false);
				setProperty("logo.fileName", "null", props, null, false);
				setProperty("control.fond", "camera", props, null, false);
				setProperty(
						"boutons.position",
						"haut/bas(horizontal,droite);gauche/droite(horizontal,gauche)",
						props, null, false);

				setProperty("robot.connexion.type", "nordic", props, null, false);
				setProperty("robot.bluetooth.id", "89,32,32,86,97,108,101,116,116,45,100,101,118;76,0,2,21,226,197,109,181,223,251,72,210,176,96,208,245,167,16,150,224,0,0,0,0,197", props, null, false);
				setProperty("telecomande.mode", "on", props, null, false);
				setProperty("autoScan.duration", "5 s", props, null, false);
				setProperty("autoScan.period", "6 s", props, null, false);
				setProperty("autoConnexion.robot", "20 s", props, null, false);
				setProperty("autoConnexion.server", "10 s", props, null, false);
				setProperty("commande.timout", "2 s", props, null, false);
				setProperty("frequence", "2 envois/seconde", props, null, false);
				setProperty("debit.download.min", "500 Kbps", props, null, false);
				setProperty("debit.upload.min", "500 Kbps", props, null, false);
				setProperty("debit.test.duration", "5 s", props, null, false);
				setProperty("debit.indicator", "icone", props, null, false);
				setProperty("endCall.timeout", "5 s", props, null, false);
				setProperty("debit.upload.max", "10 Mbps", props, null, false);
				setProperty("debit.download.max", "100 Mbps", props, null, false);
				setProperty("screen.lock", "off", props, null, false);

				setProperty("stop_call.position", "bas", props, null, false);

				/******************** Suivi *******************************/
				setProperty("suivi.isactive", "1", props,
						"Suivi de visage/silhouette", false);
				setProperty("suivi.fondecran.isactive", "0", props,
						"null", false);
				setProperty("rectangle.isvisible", "1", props,
						"null", false);
				setProperty("silhouette.isactive", "0", props,
						"null", false);
				setProperty("zoom.isactive", "0", props,
						"null", false);
				setProperty("cadre.taille", "5 cm", props, null, false);
				setProperty("suivi.delai", "100 ms", props, null, false);
				setProperty("suivi.CoeffA ", "0.5", props,
						"null", false);
				setProperty("suivi.CoeffL1", "0.5", props,
						"null", false);
				setProperty("suivi.CoeffL2", "2", props,
						"null", false);
				setProperty("time.unzoom", "1", props,
						"null", false);

				/******************** Commandes vocales *******************************/

				setProperty("Frequence.declenchement", "60", props,
						"Commandes vocales", false); // minute
				setProperty("Defilement ", "1", props,
						null, false);
				setProperty("SizeAvailableThershold ", "500", props,
						null, false);
				setProperty("delaiPourDisparaitre ", "10", props,
						null, false);
				setProperty("head.up", "4", props, null, false);
				setProperty("head.down", "4", props, null, false);
				setProperty("vocale.mode.3.keyword", "ubo,alice,bonjour alice,bonjour alexa,alexa,ok google", props, null, false);

				setProperty("vocale.mode.3.isenable", "1", props, null, false);
				setProperty("msgVocal.taille", "25", props, null, false);
				setProperty("msgVocal.police", "serif", props, null, false);
				setProperty("msgVocal.color.question", "blanc", props, null, false);
				setProperty("msgVocal.color.response", "jaune", props, null, false);
				setProperty("vocal.control", "1", props, null, false);
				setProperty("delai.affichage", "2 s", props, null, false);
				setProperty("type.declenchement", "android", props, null, false);
				setProperty("time.speech", "5 s", props, null, false);
				setProperty("response.timout", "1 s", props, null, false);
				setProperty("processus-ecoute.nbr-sequence", "3", props, null, false);
				setProperty("gif.size.height", "250", props, null, false);
				setProperty("gif.size.width", "250", props, null, false);
				setProperty("scanBlueST.period", "5 s", props, null, false);
				/******************** Commandes vocales/ détection de Silence *******************************/

				setProperty("nbrSecSil", "2", props, null, false);
				setProperty("seuil_intensity", "0.0", props, null, false);
				setProperty("agressivity", "2", props, null, false);
				setProperty("timeNoSpeech", "6", props, null, false);

				/*******************************************************************************************/
				setProperty("googleNews.url", "newsapi.org", props, null, false);
				setProperty("googleNews.apiKey",
						"dda5f00494754412b047b617eaeb833f", props, null, false);
				setProperty("googleNews.newsNumber", "2", props, null, false);
				setProperty("wiki.url", "wikipedia.org", props, null, false);
				setProperty("music.filesNumber", "1", props, null, false);

				setProperty("Message.invitation.fr",
						"Que puis-je faire pour vous ?", props, null, false);
				setProperty("Message.invitation.en", "What can I do for you ?",
						props, null, false);
				setProperty("Commande.rienEntendue.fr",
						"Je n’ai pas entendu de commande !", props, null, false);
				setProperty("Commande.rienEntendue.en",
						"I didn't hear any order!", props, null, false);
				setProperty("Commande.inconnue.fr",
						"Je n’ai pas compris votre commande !", props, null,
						false);
				setProperty("Commande.inconnue.en",
						"I didn't understand your order !", props, null, false);
				setProperty("errorMusic.notFound.fr",
						"Je ne trouve pas de musique correspondante", props,
						null, false);
				setProperty("errorMusic.notFound.en",
						"No matching music found", props, null, false);
				setProperty("errorGoogleNews.notFound.fr",
						"Je n’arrive pas à lire les nouvelles de google",
						props, null, false);
				setProperty("errorGoogleNews.notFound.en",
						"I can not read the news from google", props, null,
						false);

				setProperty("errorWiki.fr", "Je n’arrive pas à lire Wikipédia",
						props, null, false);
				setProperty("errorWiki.en", "I can not read in Wiki", props,
						null, false);
				/*********************** Appel vocal ******************************/

				setProperty("call.error.fr",
						"Désolé, votre correspondant n'est pas connecté",
						props, "Appel vocal", false);
				setProperty("call.error.en",
						"Sorry, your correspondent is not logged in", props,
						null, false);
				// setNewProperty("call1", "call.error.en", "1111111", props,
				// null);

				/*********************** Objets connectés ***********************************/
				setProperty("device.owner", "Albert", props,
						"Objets connectés", false);

				setProperty(
						"device.infos",
						"(withings,withing,,1);(hexiwear,montre,00:10:40:0B:00:1A,1);(hexiwear,hexiwear,00:3F:40:08:00:15,1)",
						props, null, false);
				setProperty("withings.url", "https://wbsapi.withings.net",
						props, null, false);
				setProperty(
						"withings.ConsumerKey",
						"e1b33153371e2192676339e49f9e9d59ee8653fdf517c0f8c0493e78db7f5",
						props, null, false);
				setProperty(
						"withings.TokenKey",
						"586c113bf366ce667803b7989c3493fb63bbc76e2d5ec4c1ddebdc36",
						props, null, false);

				setProperty("Hexiwear.failedConnexion.fr",
						"n’est pas à proximité", props, null, false);

				setProperty("Hexiwear.failedConnexion.en", "Isn't nearby",
						props, null, false);
				setProperty(
						"errorDevice.noInfo.fr",
						"les informations associées à votre appareil ne correspondent pas à votre demande",
						props, null, false);
				setProperty(
						"errorDevice.noInfo.en",
						"The information associated with your device does not match your request",
						props, null, false);

				setProperty("errorDevice.noResult.fr",
						"Aucune information disponible", props, null, false);
				setProperty("errorDevice.noResult.en",
						"No information available", props, null, false);

				setProperty(
						"errorDevice.noInternet.fr",
						"Il n’y a pas de connexion internet pour récupérer les informations du boitier",
						props, null, false);
				setProperty(
						"errorDevice.noInternet.en",
						"There is no Internet connection to retrieve information from the box",
						props, null, false);

				setProperty(
						"errorDevice.noDeviceActif.fr",
						"Il n'y a aucun appareil actif sur votre fichier de configuration",
						props, null, false);
				setProperty("errorDevice.noDeviceActif.en",
						"There is no active device on your configuration file",
						props, null, false);
				/*********************** Chat bots ***********************************/
				setProperty("chatbot.type", "unitext", props, "chat bots",
						false);
				setProperty("chatbot.actif", "1", props, null, false);
				setProperty("botLibre.userName", "Robotics", props, null, false);
				setProperty("botLibre.userPassword", "robotics", props, null,
						false);
				setProperty("botLibre.appId", "1277957418266273210", props,
						null, false);
				setProperty("botLibre.frBot", "frBot", props, null, false);
				setProperty("botLibre.enBot", "en_bot", props, null, false);
				setProperty("pandorabot.frBot", "dbf74f507e3458e4", props,
						null, false);
				setProperty("pandorabot.enBot", "d282bc3f4e3458f0", props,
						null, false);
				setProperty("chatbot.url", "https://teamnet.ml/voicebot-api", props,
						null, false);

				/***************** logs ***********************/
				setProperty("LogFrequencyPerDay ", "1", props,
						null, false);
				setProperty("FileSize  ", "10", props,
						null, false);
				setProperty("Monitoring", "1", props,
						null, false);
				setProperty("LogsArchivage ", "local", props,
						null, false);



			}


			/**********************************************************/

			creerFichier(file1, "Configuration.properties");

			FileOutputStream fileOut = new FileOutputStream(file2);

			props.store(fileOut, "Configuration");

			fileOut.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static void setProperty(String key, String value,
								   CustomProperties properties, String comment, boolean updateVal) {
		// TODO Auto-generated method stub
		Log.v("", "------setProperty----");

		Log.v("", "------containsKey=" + properties.containsKey(key));

		Log.v("", "---comment=" + comment);
		if (comment != null && !properties.containsValue(comment)
				&& commentCmpt <= defaltCommNbr) {
			Log.v("", "---commentCmpt=" + commentCmpt);
			properties.setProperty("//comment" + commentCmpt, comment);
			commentCmpt++;
		}

		if (!properties.containsKey(key)) {
			Log.v("", "------not containsKey----");

			properties.setProperty(key, value);
		}

	}

	public static void editProperty(String key, String value,
									CustomProperties properties, String comment, boolean updateVal,
									File file) throws IOException {
		// TODO Auto-generated method stub
		Log.v("", "------editProperty----");

		Log.v("", "---comment=" + comment);
		if (comment != null && !properties.containsValue(comment)
				&& commentCmpt <= defaltCommNbr) {
			Log.v("", "---commentCmpt=" + commentCmpt);
			properties.setProperty("//comment" + commentCmpt, comment);
			commentCmpt++;
		}

		properties.setProperty(key, value);
		FileOutputStream fileOut = new FileOutputStream(file);

		properties.store(fileOut, "Configuration");

		fileOut.close();

	}

	public static void setNewProperty(String key, String prevkey, String value,
									  CustomProperties properties, String comment, File file)
			throws IOException {
		// TODO Auto-generated method stub
		Log.v("", "------setNewProperty----");
		Log.v("", "------containsKey=" + properties.containsKey(key));
		if (comment != null && !properties.containsValue(comment)
				&& commentCmpt <= defaltCommNbr) {
			Log.v("", "---commentCmpt=" + commentCmpt);
			properties.setProperty("//comment" + commentCmpt, comment);
			commentCmpt++;
		}
		if (!properties.containsKey(key)) {
			Log.v("", "------not containsKey----");

			properties.setNewProperty(key, prevkey, value);
		}

		FileOutputStream fileOut = new FileOutputStream(file);

		properties.store(fileOut, "Configuration");

		fileOut.close();
	}

	public static void deleteProperty(File file, String fichier,
									  CustomProperties properties, String param) throws IOException {
		loadproperties(file, fichier, properties);
		properties.deleteProperty(param, file, fichier);
		FileOutputStream fileOut = new FileOutputStream(file);

		properties.store(fileOut, "Configuration");

		fileOut.close();
	}

	public static void WritePropertiesFile_ubo(File file1, File file2) {

		try {
			setProperty("frequence", "10 fois par seconde", uboProps, null,
					false);
			setProperty("temps.propagation", "1 ms", uboProps, null, false);
			setProperty("imu.send", "non", uboProps, null, false);
			setProperty("commande.mouve", "MOVE", uboProps, null, false);
			setProperty("commande.adresse", "ADDRESS", uboProps, null, false);
			setProperty("commande.sensor", "SENSOR", uboProps, null, false);
			setProperty("commande.stop", "STOP", uboProps, null, false);
			setProperty("commande.head", "HEAD", uboProps, null, false);
			setProperty("commande.batterie", "BATTERIE", uboProps, null, false);
			setProperty("ScenarioName", "scenario scenario1 scenario2",
					uboProps, null, false);
			setProperty("ScenarioVisibility", "1", uboProps, null, false);
			setProperty("baudRate", "115200", uboProps, null, false);
			creerFichier(file1, "teambotSmart.properties");
			setProperty("serail.databits", "8", uboProps, null, false);
			creerFichier(file1, "teambotSmart.properties");
			setProperty("serial.parite", "0", uboProps, null, false);
			creerFichier(file1, "teambotSmart.properties");
			setProperty("serial.stopbits", "1", uboProps, null, false);
			creerFichier(file1, "teambotSmart.properties");
			setProperty("serial.flowcontrol", "0", uboProps, null, false);
			creerFichier(file1, "teambotSmart.properties");

			FileOutputStream output = new FileOutputStream(file2);
			uboProps.store(output,
					"smartTeambot");

			output.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static void WritePropertiesFile_teambot(File file1, File file2) {

		try {

			setProperty("duration", "1500 ms", sanbotProps, null, false);
			creerFichier(file1, "sanbotSmart.properties");
			FileOutputStream output = new FileOutputStream(file2);
			sanbotProps.store(output, "smartSanbot");

			output.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static void WritePropertiesFile_atti(File file1, File file2) {

		try {
			Properties prop = new Properties();
			prop.setProperty("temps.propagation", "1 ms");

			creerFichier(file1, "smartATTI.properties");

			FileOutputStream output = new FileOutputStream(file2);
			prop.store(output, "smartATTI");

			output.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
