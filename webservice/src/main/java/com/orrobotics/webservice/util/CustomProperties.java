package com.orrobotics.webservice.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import android.util.Log;

public class CustomProperties extends Properties {

	private static final long serialVersionUID = 1L;

	private Map<Object, Object> linkMap = new LinkedHashMap<Object, Object>();
	private Map<Object, Object> newLinkMap = new LinkedHashMap<Object, Object>();

	@Override
	public synchronized Object put(Object key, Object value) {
		return linkMap.put(key, value);
	}

	public synchronized void deleteProperty(Object key, File file,
			String fichier) throws IOException {
		linkMap.remove(key);
		FileOutputStream fileOut = new FileOutputStream(file);
		store(fileOut, "Configuration");
		fileOut.close();

	}

	public synchronized void setNewProperty(String key, String prevkey,
			String value) {
		for (Iterator<java.util.Map.Entry<Object, Object>> e = linkMap
				.entrySet().iterator(); e.hasNext();) {
			java.util.Map.Entry<Object, Object> entry = (java.util.Map.Entry<Object, Object>) e
					.next();
			newLinkMap.put(entry.getKey(), entry.getValue());
			if (prevkey.equals((String) entry.getKey())) {
				Log.v("", "-----prevkey---=" + prevkey);
				newLinkMap.put(key, value);
			}

		}
		linkMap.clear();
		linkMap.putAll(newLinkMap);
		newLinkMap.clear();

	}

	@Override
	public synchronized boolean contains(Object value) {
		return linkMap.containsValue(value);
	}

	@Override
	public boolean containsValue(Object value) {
		return linkMap.containsValue(value);
	}

	@Override
	public synchronized Enumeration<Object> elements() {
		throw new UnsupportedOperationException(
				"Enumerations are so old-school, don't use them, "
						+ "use keySet() or entrySet() instead");
	}

	@Override
	public Set<java.util.Map.Entry<Object, Object>> entrySet() {
		return linkMap.entrySet();
	}

	@Override
	public synchronized void clear() {
		linkMap.clear();
	}

	@Override
	public synchronized boolean containsKey(Object key) {
		return linkMap.containsKey(key);
	}

	@Override
	public synchronized Object setProperty(String key, String value) {
		return put(key, value);
	}

	@Override
	public synchronized String getProperty(String key) {
		String val = null;
		for (Iterator<java.util.Map.Entry<Object, Object>> e = linkMap
				.entrySet().iterator(); e.hasNext();) {
			java.util.Map.Entry<Object, Object> entry = (java.util.Map.Entry<Object, Object>) e
					.next();

			if (key.equals((String) entry.getKey())) {
				val = (String) entry.getValue();
				break;
			}
		}
		return val;
	}

	public synchronized void store(OutputStream out, String comments)
			throws IOException {
		BufferedWriter awriter;
		awriter = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));
		if (comments != null)
			writeln(awriter, "#" + comments);
		writeln(awriter, "#" + new Date().toString());
		for (Iterator<java.util.Map.Entry<Object, Object>> e = linkMap
				.entrySet().iterator(); e.hasNext();) {
			java.util.Map.Entry<Object, Object> entry = (java.util.Map.Entry<Object, Object>) e
					.next();

			String key = (String) entry.getKey();
			String val = (String) entry.getValue();

			if (!val.contains("#") && !key.contains("#")) {

				writeln(awriter, key + "=" + val);
			}
		}
		awriter.flush();
		awriter.close();
	}

	private static void writeln(BufferedWriter bw, String s) throws IOException {
		bw.write(s);
		bw.newLine();
	}

}