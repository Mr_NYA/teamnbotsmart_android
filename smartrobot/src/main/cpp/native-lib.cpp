#include <jni.h>
#include <string>
#include <stdio.h>
#include <assert.h>
#include <android/log.h>
#include "webrtc/common_audio/vad/include/webrtc_vad.h"
//#define  LOG_TAG    "webrtc_1cad"
//#define  LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)

extern "C" JNIEXPORT jstring

JNICALL
Java_com_orrobotics_orsmartrobot_view_ControlWithVoice_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    int ret ;
    int i = 0;
    int kRates[] = { 8000, 12000, 16000, 24000, 32000, 48000 };
    int kMaxFrameLength = 16;
    int kFrameLengths[] = { 80, 120, 160, 240, 320, 480, 640, 960 };
    short zeros[80] = {0};
    short speech[80] = {0};

    //for (i = 0; i < 160; i++) {
     //   speech[i] = (i * i);
    // }
    VadInst* handle;
    int ret_state = 0;
    ret_state = WebRtcVad_Create(&handle);
    //VadInst* handle = WebRtcVad_Create();
    WebRtcVad_Init(handle);
    WebRtcVad_set_mode(handle,3);

    ret = WebRtcVad_Process(handle, kRates[0], speech, kFrameLengths[0]);
    //LOGD("ret1 = %d\n",ret);
    std::string t;
    //ret = WebRtcVad_Process(handle, kRates[2], zeros, kFrameLengths[2]);
    if (ret == 1){
        t="true";
    } else{
        t="false";
    }
    //LOGD("ret2 = %d\n",ret);
    WebRtcVad_Free(handle);
    std::string hello = "Hello from C++ and :"+t;
    return env->NewStringUTF(hello.c_str());
}

extern "C" jint
JNICALL
Java_com_orrobotics_orsmartrobot_view_ControlWithVoice_isSpeechJNI(
        JNIEnv *env, jobject,jshortArray buffer,jint offseetInshort,jint readSize,jint sampleRate,jint vadFrame, jint aggressivity, jdouble vad_seuil){

/*static jboolean jni_calcDecibelLevel(JNIEnv *env, jobject,jshortArray buffer,jint offseetInshort,jint readSize)
*/
    //int ret ;
    //int i = 0;
    //int kRates[] = { 8000, 12000, 16000, 24000, 32000, 48000 };
    //int kMaxFrameLength = 16;
    //int kFrameLengths[] = { 80, 120, 160, 240, 320, 480, 640, 960 };
    VadInst* handle;
    int result;
    int ret_state = 0;
    ret_state = WebRtcVad_Create(&handle);
    WebRtcVad_Init(handle);
    WebRtcVad_set_mode(handle,aggressivity);
    //jshort *pcm_data = env->GetShortArrayElements(buffer, JNI_FALSE);
    //ret = WebRtcVad_Process(handle, kRates[2], pcm_data, kFrameLengths[2]);
    //short speech[160] = {0};
    int index = readSize/vadFrame;
    jshort *pcm_data = env->GetShortArrayElements(buffer, JNI_FALSE);

    //int vad = WebRtcVad_Process(handle, sampleRate, pcm_data, vadFrame);
    //LOGD("vad = %d\n",vad);
    int vad = 0;
    int sum = 0;
    float vadrate;
    for (int i = 0; i < index; ++i) {
        int vad = WebRtcVad_Process(handle, sampleRate, pcm_data+offseetInshort+i*vadFrame, vadFrame);
        sum+=vad;
        //LOGD("vad = %d\n",vad);
    }
    vadrate = (float)sum/index;
    if(vadrate > vad_seuil){
        //b = JNI_TRUE;
        result = 1;

    } else{
        //b = JNI_FALSE;
        result = 0;
    }

    //int vad = WebRtcVad_Process(handle, 16000, pcm_data+offseetInshort+i*160, 160);

    env->ReleaseShortArrayElements(buffer,pcm_data,JNI_ABORT);
    WebRtcVad_Free(handle);
    return result;}

extern "C" jint
JNICALL
Java_com_orrobotics_orsmartrobot_voicerecognition_VoiceRecorder_isSpeechJNI(
        JNIEnv *env, jobject,jshortArray buffer,jint offseetInshort,jint readSize,jint sampleRate,jint vadFrame, jint aggressivity, jdouble vad_seuil){

/*static jboolean jni_calcDecibelLevel(JNIEnv *env, jobject,jshortArray buffer,jint offseetInshort,jint readSize)
*/
//int ret ;
//int i = 0;
//int kRates[] = { 8000, 12000, 16000, 24000, 32000, 48000 };
//int kMaxFrameLength = 16;
//int kFrameLengths[] = { 80, 120, 160, 240, 320, 480, 640, 960 };
VadInst* handle;
int result;
int ret_state = 0;
ret_state = WebRtcVad_Create(&handle);
WebRtcVad_Init(handle);
WebRtcVad_set_mode(handle,aggressivity);
//jshort *pcm_data = env->GetShortArrayElements(buffer, JNI_FALSE);
//ret = WebRtcVad_Process(handle, kRates[2], pcm_data, kFrameLengths[2]);
//short speech[160] = {0};
int index = readSize/vadFrame;
jshort *pcm_data = env->GetShortArrayElements(buffer, JNI_FALSE);

//int vad = WebRtcVad_Process(handle, sampleRate, pcm_data, vadFrame);
//LOGD("vad = %d\n",vad);
int vad = 0;
int sum = 0;
float vadrate;
for (int i = 0; i < index; ++i) {
int vad = WebRtcVad_Process(handle, sampleRate, pcm_data+offseetInshort+i*vadFrame, vadFrame);
sum+=vad;
//LOGD("vad = %d\n",vad);
}
vadrate = (float)sum/index;
if(vadrate > vad_seuil){
//b = JNI_TRUE;
result = 1;

} else{
//b = JNI_FALSE;
result = 0;
}

//int vad = WebRtcVad_Process(handle, 16000, pcm_data+offseetInshort+i*160, 160);

env->ReleaseShortArrayElements(buffer,pcm_data,JNI_ABORT);
WebRtcVad_Free(handle);
return result;}
