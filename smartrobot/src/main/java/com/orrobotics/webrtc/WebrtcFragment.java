package com.orrobotics.webrtc;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Movie;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.SystemClock;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.hannesdorfmann.mosby.mvp.MvpFragment;
import com.nhancv.webrtcpeer.rtc_plugins.ProxyRenderer;
import com.orrobotics.orsmartrobot.R;
import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.observer.OrSmarRobotObserver;
import com.orrobotics.orsmartrobot.ui.PlayGifView;
import com.orrobotics.orsmartrobot.util.RemoteControlUtil;
import com.orrobotics.webrtc.util.ImageUtils;
import com.tzutalin.dlib.Constants;
import com.tzutalin.dlib.FaceDet;
import com.tzutalin.dlib.PedestrianDet;
import com.tzutalin.dlib.VisionDetRet;

import org.webrtc.Camera1Enumerator;
import org.webrtc.Camera2Enumerator;
import org.webrtc.CameraEnumerator;
import org.webrtc.EglBase;
import org.webrtc.Logging;
import org.webrtc.RendererCommon;
//import org.webrtc.SurfaceViewRenderer;
import com.orrobotics.webrtc.util.SurfaceViewRenderer;
import org.webrtc.VideoCapturer;
import org.webrtc.VideoRenderer;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static com.orrobotics.webrtc.util.Constants.LOCAL_HEIGHT_CONNECTED;
import static com.orrobotics.webrtc.util.Constants.LOCAL_WIDTH_CONNECTED;
import static com.orrobotics.webrtc.util.Constants.LOCAL_X_CONNECTED;
import static com.orrobotics.webrtc.util.Constants.LOCAL_Y_CONNECTED;

@SuppressLint("ValidFragment")
public class WebrtcFragment extends MvpFragment<One2OneView, One2OnePresenter>
        implements One2OneView, OrSmarRobotObserver, SurfaceHolder.Callback {

    SmartRobotApplication application;
    private static final String TAG = WebrtcFragment.class.getSimpleName();
    String fileName = "";
    SurfaceHolder holder,holderTransparent;
    SurfaceView  transparentView;
    SurfaceView restsurfaceview;
    private Boolean isProcessing = false;
    private Paint mFaceLandmardkPaint;
    private long lastProcessingTimeMs;
    private FaceDet mFaceDet;
    private PedestrianDet mPersonDet;
    int  deviceHeight,deviceWidth;
    private Handler handler;
    private HandlerThread handlerThread;
    private static final boolean SAVE_BITMAP = false;
    private Bitmap croppedBitmap = null;
    private static final int INPUT_SIZE = 224;
    PlayGifView backGif;
    int width;
    int height;
    MediaPlayer mediaplayer;
    protected org.webrtc.SurfaceViewRenderer vGLSurfaceViewCallFull;
    protected SurfaceViewRenderer vGLSurfaceViewCallPip;
    private EglBase rootEglBase;
    private ProxyRenderer localProxyRenderer;
    private ProxyRenderer remoteProxyRenderer;
    private boolean isSwappedFeeds;
    private View view1;
    Activity act;
    private Toast logToast;
    public VideoView backgroundVideoView;
    int silo_det;
    double CoeffA;
    double CoeffL1;
    int CoeffL2;
    int delay=500;
    int screenWidth;
    int screenHight;
    int rec_view;
    int suivi_with_fond;
    MediaController mediaC;
    ImageView img;
    @SuppressLint("ValidFragment")
    public WebrtcFragment(Activity act) {
        this.act=act;
    }

    @Override
    public One2OnePresenter createPresenter() {
        new Handler().postDelayed(run, 500);

        return new One2OnePresenter(act.getApplication());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //CoeffA = Integer.parseInt(SmartRobotApplication.getParamFromFile("suivi.CoeffA",
        //        "Configuration.properties", null));
        //CoeffL1 = Integer.parseInt(SmartRobotApplication.getParamFromFile("suivi.CoeffL1",
        //       "Configuration.properties", null));
        CoeffL2 = Integer.parseInt(SmartRobotApplication.getParamFromFile("suivi.CoeffL2",
                "Configuration.properties", null));
        delay = Integer.parseInt(application.getParamFromFile("suivi.delai",
                "Configuration.propertie", null).split(" ")[0]);
        CoeffA = Double.parseDouble(SmartRobotApplication.getParamFromFile("suivi.CoeffA",
                "Configuration.properties", null));
        CoeffL1 = Double.parseDouble(SmartRobotApplication.getParamFromFile("suivi.CoeffL1",
                "Configuration.properties", null));

        silo_det = Integer.parseInt(application.getParamFromFile("silhouette.isactive", "Configuration.properties", null));
        rec_view = Integer.parseInt(application.getParamFromFile("rectangle.isvisible", "Configuration.properties", null));
        suivi_with_fond = Integer.parseInt(application.getParamFromFile("suivi.fondecran.isactive", "Configuration.properties", null));

        view1 = inflater.inflate(R.layout.videowebrtc, container, false);

        application = (SmartRobotApplication) act.getApplicationContext();
        application.registerObserver(this);
        String email = application.getEmail();
        String pseudo = email.replace("@","_");
        application.setPseudoOpenFireSender(pseudo+""+pseudo);
        application.setEditor(RemoteControlUtil.EXTRAS_PSEUDO_OPENFIRE_CONTROL,pseudo+""+pseudo);
        CreateVue();
        return view1;
    }
    public static int getScreenWidth() {

        return Resources.getSystem().getDisplayMetrics().widthPixels;

    }

    public static int getScreenHeight() {

        return Resources.getSystem().getDisplayMetrics().heightPixels;

    }

    public void CreateVue(){

        vGLSurfaceViewCallFull =  view1.findViewById(R.id.vGLSurfaceViewCallFull);

        vGLSurfaceViewCallPip = (SurfaceViewRenderer) view1.findViewById(R.id.vGLSurfaceViewCallPip);
        //config peer
        localProxyRenderer = new ProxyRenderer();
        remoteProxyRenderer = new ProxyRenderer();
        rootEglBase = EglBase.create();

        vGLSurfaceViewCallFull.init(rootEglBase.getEglBaseContext(), null);
        vGLSurfaceViewCallFull.setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_FILL);
        vGLSurfaceViewCallFull.setEnableHardwareScaler(true);
        vGLSurfaceViewCallFull.setMirror(true);

        vGLSurfaceViewCallPip.init(rootEglBase.getEglBaseContext(), null);
        vGLSurfaceViewCallPip.setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_FILL);
        vGLSurfaceViewCallPip.setEnableHardwareScaler(true);
        vGLSurfaceViewCallPip.setMirror(true);
        vGLSurfaceViewCallPip.setSecure(true);
        vGLSurfaceViewCallPip.setZOrderMediaOverlay(true);

        vGLSurfaceViewCallFull.setVisibility(View.VISIBLE);
        vGLSurfaceViewCallPip.setVisibility(View.VISIBLE);


        holder = vGLSurfaceViewCallPip.getHolder();
        holder.addCallback((SurfaceHolder.Callback) this);
        holder.setFormat(PixelFormat.TRANSLUCENT);

        transparentView = (SurfaceView) view1.findViewById(R.id.TransparentView);
        restsurfaceview = (SurfaceView) view1.findViewById(R.id.surfaceview3);
        holderTransparent = transparentView.getHolder();
        //holderlocal = local_video_view.getHolder();
        holderTransparent.addCallback((SurfaceHolder.Callback) this);
        // holderlocal.addCallback((SurfaceHolder.Callback) this);
        transparentView.setVisibility(View.VISIBLE);
        holderTransparent.setFormat(PixelFormat.TRANSLUCENT);
        transparentView.setZOrderMediaOverlay(true);
        backgroundVideoView = (VideoView) view1.findViewById(R.id.backgroundVideoSurface);

        fileName = SmartRobotApplication.getParamFromFile("control.fond",
                "Configuration.properties", null);
        final String pathName = Environment.getExternalStorageDirectory().getAbsolutePath() + "/SmartRobot/"+fileName;

        application.getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                if (application.getBackgroundType().equals("img") && application.isTelepresence() == false ) {
                    Log.i("img" ,"image backgrounddddddddddddddddd: ");


                    Resources res = getResources();
                    Bitmap bmap = BitmapFactory.decodeFile(pathName);
                    BitmapDrawable background = new BitmapDrawable(res, bmap);
                    restsurfaceview.setVisibility(View.VISIBLE);
                    restsurfaceview.setZOrderMediaOverlay(true);

                    restsurfaceview.setBackgroundDrawable(background);

                }else if (application.getBackgroundType().equals("gif") && application.isTelepresence() == false  ) {

                    img=(ImageView)view1.findViewById(R.id.img);
                    img.setVisibility(View.VISIBLE);

                    Glide.with(application.getApplicationContext()).load(pathName).asGif().into(img);

                    //backGif = (PlayGifView) view1.findViewById(R.id.backGif);
                    //Movie movie = Movie.decodeFile(pathName);
                    if (backgroundVideoView != null)
                        backgroundVideoView.setVisibility(View.GONE);

                }else if (application.getBackgroundType().equals("video") && application.isTelepresence() == false  ) {
                    if (backGif != null)
                        backGif.setVisibility(View.GONE);

                    String fullPath = Environment.getExternalStorageDirectory() + "/SmartRobot/"+fileName;
                    File file = new File(fullPath);
                    Uri videoUri = Uri.fromFile(file);
                    backgroundVideoView.setVisibility(View.VISIBLE);
                    backgroundVideoView.setZOrderMediaOverlay(true);
                    mediaC = new MediaController(application.getApplicationContext());
                    backgroundVideoView.setVideoURI(videoUri);
                    backgroundVideoView.setMediaController(mediaC);
                    mediaC.setAnchorView(backgroundVideoView);
                    backgroundVideoView.start();
                    backgroundVideoView.setOnCompletionListener ( new MediaPlayer.OnCompletionListener() {

                        @Override
                        public void onCompletion(MediaPlayer mediaPlayer) {

                            backgroundVideoView.start();
                        }
                    });
                }else if ((!application.getBackgroundType().equals("video") &&
                           !application.getBackgroundType().equals("gif") &&
                           !application.getBackgroundType().equals("img") &&
                           !application.getBackgroundType().equals("camera")) &&
                            application.isTelepresence() == false  ) {

                    restsurfaceview.setVisibility(View.VISIBLE);
                    restsurfaceview.setZOrderMediaOverlay(true);
                    restsurfaceview.setBackgroundColor(Color.BLACK);


                }


            }
        });


        deviceWidth=getScreenWidth();

        deviceHeight=getScreenHeight();


        mFaceDet = new FaceDet(Constants.getFaceShapeModelPath());
        mPersonDet = new PedestrianDet();

        mFaceLandmardkPaint = new Paint();
        mFaceLandmardkPaint.setColor(Color.GREEN);
        mFaceLandmardkPaint.setStrokeWidth(3);
        mFaceLandmardkPaint.setStyle(Paint.Style.STROKE);
        iniiBitmap();

        Log.i("face" ,"CcccccccccccccccccccreateVuet: ");

        vGLSurfaceViewCallPip.addFrameListener(bitmap -> {
            //L.e(this, "localId: "+ Thread.currentThread().getId());
//            CallActivity.saveBitmap(bitmap, "local.png");
            Log.i("faceSuivi", " "+application.getFaceSuivi());
            if (application.getFaceSuivi()== "true" && application.isTelepresence() == true){
                Log.i("face" ,"aaaaaaaaaaaaaaaaaaddFrameListener: ");

                processAndRecognize(bitmap);
            }

        }, 0.5f);


        // Swap feeds on pip view click.
        // vGLSurfaceViewCallPip.setOnClickListener(view -> setSwappedFeeds(!isSwappedFeeds));

        //setSwappedFeeds(true);

    }
    protected synchronized void runInBackground(final Runnable r) {
        if (handler != null) {
            handler.post(r);
        }
    }
    private void processAndRecognize(Bitmap srcBitmap) {
//        L.w(getClass(), "PID: " + Thread.currentThread().getId());
        //if (isProcessing) {
        //  return;
        //}
        application.getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                Log.i("imag","imag processing");
                if (restsurfaceview!=null ){
                    Log.i("imag","imag processing restsurfaceview--------");
                    restsurfaceview.setVisibility(View.INVISIBLE);
                    restsurfaceview = null;
                }
                if ( img!=null){
                    Log.i("imag","imag processing img--------");

                    img.setVisibility(View.INVISIBLE);
                    img = null;

                }
                if ( backgroundVideoView!=null){
                    Log.i("imag","imag processing backgroundVideoView--------");

                    backgroundVideoView.setVisibility(View.INVISIBLE);
                    backgroundVideoView = null;

                }
            }
        });

        if (application.getFaceSuivi()== "true") {
            //isProcessing = true;
            croppedBitmap = Bitmap.createBitmap  (INPUT_SIZE, INPUT_SIZE, Bitmap.Config.ARGB_8888);
            Log.i("face", "bbbbbbbbbbbbbbbbbbAndRecognize: ");


            //isProcessing = true;

            Bitmap dstBitmap;
            if (srcBitmap.getWidth() >= srcBitmap.getHeight()) {
                dstBitmap = Bitmap.createBitmap(srcBitmap, srcBitmap.getWidth() / 2 - srcBitmap.getHeight() / 2, 0,
                        srcBitmap.getHeight(), srcBitmap.getHeight()
                );

            } else {
                dstBitmap = Bitmap.createBitmap(srcBitmap, 0, srcBitmap.getHeight() / 2 - srcBitmap.getWidth() / 2,
                        srcBitmap.getWidth(), srcBitmap.getWidth()
                );
            }

            Matrix frameToCropTransform = ImageUtils.getTransformationMatrix(dstBitmap.getWidth(), dstBitmap.getHeight(),
                    INPUT_SIZE, INPUT_SIZE, 0, true);

            Matrix cropToFrameTransform = new Matrix();
            frameToCropTransform.invert(cropToFrameTransform);

            final Canvas canvas = new Canvas(croppedBitmap);
            //final Canvas canvas = holderTransparent.lockCanvas(null);
            //final Canvas canvas = transparentView.getHolder().lockCanvas();

            canvas.drawBitmap(dstBitmap, frameToCropTransform, null);

            //TODO: enable this for analyzing the bitmaps, but it may degrade the performance
            //if (SAVE_BITMAP) {
            //    ImageUtils.saveBitmap(srcBitmap, "remote_raw.png");
            //   ImageUtils.saveBitmap(dstBitmap, "remote_rawCrop.png");
            //   ImageUtils.saveBitmap(croppedBitmap, "remote_crop.png");
            //}
            Log.i("face", "cccccccccccccccccccaddFrameListener: ");
            //runInBackground(() -> {
            //TODO your background code
            //Toast.makeText(act.getApplicationContext(), "getScreenWidth"+deviceWidth+"; deviceHeight size: ("+deviceHeight+";croppedBitmap.getWidth()"+croppedBitmap.getWidth()+";croppedBitmap.getHeight()"+croppedBitmap.getHeight()+")",
            //      Toast.LENGTH_LONG).show();

            //pass bitmap to TFLite model
            final long startTime = SystemClock.uptimeMillis();
            //final List<Classifier.Recognition> results = classifier.recognizeImage(croppedBitmap);
            final List<VisionDetRet> results = mFaceDet.detect(croppedBitmap);
            final List<VisionDetRet> results1 = mPersonDet.detect(croppedBitmap);
            /***if (silo_det == 0){
             results = mFaceDet.detect(croppedBitmap);

             }else{
             results = mPersonDet.detect(croppedBitmap);
             }**/


            Log.i("face", "Result fffffffffffffffffffffffffffffffffffffffffface det: " + results.size());
            lastProcessingTimeMs = SystemClock.uptimeMillis() - startTime;
//            L.w("(TFLite) Detect: %s, elapsed: %d", results, lastProcessingTimeMs);
            if (canvas != null) {
                //canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);

                //Canvas canvas2 = holderTransparent.lockCanvas(null);
                //canvas2.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
                if ((results != null && results.size() > 0)) {
                    Canvas canvas1 = holderTransparent.lockCanvas(null);
                    canvas1.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);

                    for (final VisionDetRet ret : results) {
                        float resizeRatio = 5.0f;
                        //float resizeRatioW = (float)canvas.getWidth() - (float)canvas.getHeight()/INPUT_SIZE;
                        //float resizeRatioH = (float)canvas.getHeight()/INPUT_SIZE;

                        Rect bounds = new Rect();
                        Rect bounds1 = new Rect();

                        bounds1.left = (int) (ret.getLeft() + (canvas1.getWidth() - vGLSurfaceViewCallPip.getWidth()));//5.3f
                        bounds1.top = (int) (ret.getTop());//7.5f
                        bounds1.right = (int) (ret.getRight() + (canvas1.getWidth() - vGLSurfaceViewCallPip.getWidth()));//5.3f
                        bounds1.bottom = (int) (ret.getBottom());//7.0f

                        bounds.left = (int) (ret.getLeft() * (float) canvas1.getWidth() / INPUT_SIZE);//5.3f
                        bounds.top = (int) (ret.getTop() * canvas1.getHeight() / INPUT_SIZE);//7.5f
                        bounds.right = (int) (ret.getRight() * (float) canvas1.getWidth() / INPUT_SIZE);//5.3f
                        bounds.bottom = (int) (ret.getBottom() * canvas1.getHeight() / INPUT_SIZE);//7.0f
                        //canvas = new Canvas(croppedBitmap);
                        if (rec_view == 1) {
                            //canvas1.drawRect(bounds, mFaceLandmardkPaint);
                            canvas1.drawRect(bounds1, mFaceLandmardkPaint);
                        }
                        holderTransparent.unlockCanvasAndPost(canvas1);
                        Log.i("face", " canvas.drawRect doneeeeeeeeeeeeeeeeeeeeeeee " + results);


                        if (application.isTelepresence() == true) {

                            int canvasW = application.getActivity().getWindowManager()
                                    .getDefaultDisplay().getWidth();
                            int canvasH = application.getActivity().getWindowManager()
                                    .getDefaultDisplay().getHeight();
                            Point centerOfscreen = new Point(canvasW / 2, canvasH / 2);

                            int centreX = (int) (bounds.left + (bounds.right - bounds.left) / 2);
                            int centreY = (int) (bounds.top + (bounds.bottom - bounds.top) / 2);

                            int VA = (int) (CoeffA * ((int) centreX / 100 - centerOfscreen.x / 100));
                            int VL = (int) (CoeffL1 * ((int) centreY / 100 - centerOfscreen.y / 100));//+ CoeffL2 * (heightPre - heightAct ));
                            Log.i("vitesse", " centreX/100 :" + centreX / 100);
                            Log.i("vitesse", " centerOfscreen.x/100 :" + centerOfscreen.x / 100);
                            if ((VA != 0 || VL != 0)) {

                                //application.setZommTest(false);
                                application.notifyObservers("MOVE;" + VL + ";" + VA);
                            }

                        }

                    }

                } else if (results1 != null && results1.size() > 0 && silo_det == 1) {
                    // (results1 != null && results1.size()>0) {
                    Canvas canvas1 = holderTransparent.lockCanvas(null);
                    canvas1.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
                    //if (results1 != null){
                    for (final VisionDetRet ret : results1) {
                        float resizeRatio = 5.0f;
                        //float resizeRatioW = (float)canvas.getWidth() - (float)canvas.getHeight()/INPUT_SIZE;
                        //float resizeRatioH = (float)canvas.getHeight()/INPUT_SIZE;

                        Rect bounds = new Rect();
                        Rect bounds1 = new Rect();

                        bounds1.left = (int) (ret.getLeft() + (canvas1.getWidth() - vGLSurfaceViewCallPip.getWidth()));//5.3f
                        bounds1.top = (int) (ret.getTop());//7.5f
                        bounds1.right = (int) (ret.getRight() + (canvas1.getWidth() - vGLSurfaceViewCallPip.getWidth()));//5.3f
                        bounds1.bottom = (int) (ret.getBottom());//7.0f

                        bounds.left = (int) (ret.getLeft() * (float) canvas1.getWidth() / INPUT_SIZE);//5.3f
                        bounds.top = (int) (ret.getTop() * canvas1.getHeight() / INPUT_SIZE);//7.5f
                        bounds.right = (int) (ret.getRight() * (float) canvas1.getWidth() / INPUT_SIZE);//5.3f
                        bounds.bottom = (int) (ret.getBottom() * canvas1.getHeight() / INPUT_SIZE);//7.0f
                        //canvas = new Canvas(croppedBitmap);
                        if (rec_view == 1) {
                            canvas1.drawRect(bounds, mFaceLandmardkPaint);
                        }
                        //canvas1.drawRect(bounds1, mFaceLandmardkPaint);
                        holderTransparent.unlockCanvasAndPost(canvas1);
                        Log.i("face", " canvas.drawRect doneeeeeeeeeeeeeeeeeeeeeeee " + results1);

                        if (application.isTelepresence() == true) {

                            int canvasW = application.getActivity().getWindowManager()
                                    .getDefaultDisplay().getWidth();
                            int canvasH = application.getActivity().getWindowManager()
                                    .getDefaultDisplay().getHeight();
                            Point centerOfscreen = new Point(canvasW / 2, canvasH / 2);

                            int centreX = (int) (bounds.left + (bounds.right - bounds.left) / 2);
                            int centreY = (int) (bounds.top + (bounds.bottom - bounds.top) / 2);

                            int VA = (int) (CoeffA * ((int) centreX / 100 - centerOfscreen.x / 100));
                            int VL = (int) (CoeffL1 * ((int) centreY / 100 - centerOfscreen.y / 100));//+ CoeffL2 * (heightPre - heightAct ));
                            Log.i("vitesse", " centreX/100 :" + centreX / 100);
                            Log.i("vitesse", " centerOfscreen.x/100 :" + centerOfscreen.x / 100);
                            if ((VA != 0 || VL != 0)) {

                                //application.setZommTest(false);
                                application.notifyObservers("MOVE;" + VL + ";" + VA);
                            }

                        }

                    }
                    //
                } else {
                    Canvas canvas1 = holderTransparent.lockCanvas(null);
                    canvas1.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
                    holderTransparent.unlockCanvasAndPost(canvas1);
                }

            }
        }
    }
    private void iniiBitmap() {

        if (croppedBitmap == null){
            //display.getSize(size);
            width = getScreenWidth();
            height = getScreenHeight();
            croppedBitmap = Bitmap.createBitmap(INPUT_SIZE, INPUT_SIZE, Bitmap.Config.ARGB_8888);
        }

    }
    private void Draw(List<VisionDetRet> results, List<VisionDetRet> results1) {


        Log.i("face" ,"inside drawwwwwwwwwwwwwwwwwwwwwwwwww: " + results);
        if (results != null || results1 != null) {


            Canvas canvas = holderTransparent.lockCanvas(null);
            //canvas.drawColor(Color.BLACK);
            canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
            //if (results1 != null){
            for (final VisionDetRet ret : results) {
                float resizeRatio = 5.0f;
                //float resizeRatioW = (float)canvas.getWidth() - (float)canvas.getHeight()/INPUT_SIZE;
                //float resizeRatioH = (float)canvas.getHeight()/INPUT_SIZE;

                Rect bounds = new Rect();
                bounds.left = (int) (ret.getLeft() * (float) canvas.getWidth() / INPUT_SIZE);//5.3f
                bounds.top = (int) (ret.getTop() * canvas.getHeight() / INPUT_SIZE);//7.5f
                bounds.right = (int) (ret.getRight() * (float) canvas.getWidth() / INPUT_SIZE);//5.3f
                bounds.bottom = (int) (ret.getBottom() * canvas.getHeight() / INPUT_SIZE);//7.0f
                //canvas = new Canvas(croppedBitmap);
                canvas.drawRect(bounds, mFaceLandmardkPaint);
                holderTransparent.unlockCanvasAndPost(canvas);
                Log.i("face" ," canvas.drawRect doneeeeeeeeeeeeeeeeeeeeeeee " + results);

            }
        }
    }
    @Override
    public void surfaceCreated(SurfaceHolder holderTransparent) {

        try {

            synchronized (holderTransparent)

            {//Draw();
            }   //call a draw method

            //camera = Camera.open(); //open a camera

        }

        catch (Exception e) {

            Log.i("Exception", e.toString());

            return;

        }

    }
    @Override
    public void surfaceChanged(SurfaceHolder holderTransparent, int format, int width, int height) {



        //refresh(); //call method for refress camera

    }
    public void refresh() {

        if (holderTransparent.getSurface() == null) {

            return;

        }

    }


    @Override

    public void surfaceDestroyed(SurfaceHolder holderTransparent) {

        //camera.release(); //for release a camera

    }
    Runnable run = new Runnable() {
        @Override
        public void run() {

            presenter.connectServer();

        }
    };



    @Override
    public void logAndToast(String msg) {
        if (logToast != null) {
            logToast.cancel();
        }

        if(msg.equals("closed")){

            presenter.connectServer();
        }else if(msg.contains("offer")){
            logToast = Toast.makeText(act, act.getResources().getString(R.string.callwebrtc), Toast.LENGTH_SHORT);
            logToast.show();
        }
    }

    @Override
    public void disconnect() {
        application.setEditor(RemoteControlUtil.CONNECTED_TO_SERVER,"false");
        application.setEditor(RemoteControlUtil.CONNECTED_TO_SIP_SERVER,"false");
        application.notifyObservers("serverlogout");
        application.notifyObservers("siplogout");
        application.setTelepresence(false);
        application.notifyObservers("endtelepresence");
    }

    @Override
    public VideoCapturer createVideoCapturer() {
        VideoCapturer videoCapturer;
        if (useCamera2()) {
            if (!captureToTexture()) {
                return null;
            }
            videoCapturer = createCameraCapturer(new Camera2Enumerator(act.getApplicationContext()));
        } else {
            videoCapturer = createCameraCapturer(new Camera1Enumerator(captureToTexture()));
        }
        if (videoCapturer == null) {
            return null;
        }
        return videoCapturer;
    }

    private VideoCapturer createCameraCapturer(CameraEnumerator enumerator) {
        final String[] deviceNames = enumerator.getDeviceNames();
        // First, try to find front facing camera
        for (String deviceName : deviceNames) {

            if (enumerator.isFrontFacing(deviceName)) {
                VideoCapturer videoCapturer = enumerator.createCapturer(deviceName, null);

                if (videoCapturer != null) {
                    return videoCapturer;
                }
            }
        }

        // Front facing camera not found, try something else
        for (String deviceName : deviceNames) {
            if (!enumerator.isFrontFacing(deviceName)) {
                VideoCapturer videoCapturer = enumerator.createCapturer(deviceName, null);

                if (videoCapturer != null) {
                    return videoCapturer;
                }
            }
        }

        return null;
    }

    private boolean captureToTexture() {
        return presenter.getDefaultConfig().isCaptureToTexture();
    }


    private boolean useCamera2() {
        return Camera2Enumerator.isSupported(getContext()) && presenter.getDefaultConfig().isUseCamera2();
    }

    @Override
    public EglBase.Context getEglBaseContext() {
        return rootEglBase.getEglBaseContext();    }

    @Override
    public VideoRenderer.Callbacks getLocalProxyRenderer() {
        return localProxyRenderer;
    }

    @Override
    public VideoRenderer.Callbacks getRemoteProxyRenderer() {
        return remoteProxyRenderer;
    }

    @Override
    public void setSwappedFeeds(boolean swappedFeed) {
        Logging.d(TAG, "setSwappedFeeds: " + isSwappedFeeds);
        this.isSwappedFeeds = isSwappedFeeds;
        localProxyRenderer.setTarget(isSwappedFeeds ? vGLSurfaceViewCallFull : vGLSurfaceViewCallPip);
        remoteProxyRenderer.setTarget(isSwappedFeeds ? vGLSurfaceViewCallPip : vGLSurfaceViewCallFull);
        vGLSurfaceViewCallFull.setMirror(isSwappedFeeds);
        vGLSurfaceViewCallPip.setMirror(!isSwappedFeeds);
    }

    @Override
    public void socketConnect(boolean success) {
        presenter.register(application.getPseudoOpenFire() + "" + application.getPseudoOpenFire());

    }

    @Override
    public void registerStatus(boolean success) {
        if(success==true){
            application.setEditor(RemoteControlUtil.CONNECTED_TO_SERVER,"true");
            application.setEditor(RemoteControlUtil.CONNECTED_TO_SIP_SERVER,"true");
            application.notifyObservers("serviceConnected");
            application.notifyObservers("siplogin");
        }

    }

    @Override
    public void transactionToCalling(String fromPeer, String toPeer, boolean isHost) {
        application.setTelepresence(true);
        application.notifyObservers("istelepresence");
        application.notifyObservers("displayLinphone");
        presenter.initPeerConfig(fromPeer, toPeer, isHost);
        presenter.startCall();
    }

    @Override
    public void incomingCalling(String fromPeer) {
        transactionToCalling(fromPeer, application.getPseudoOpenFire()+""+application.getPseudoOpenFire(), false);

    }

    @Override
    public void stopCalling() {
        application.setTelepresence(false);
        application.getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
        end();


    }


    @Override
    public void startCallIng() {
        application.notifyObservers("startCalling");
    }

    @Override
    public void move(String message) {

        application.notifyObservers(message);
    }

    @Override
    public void update(String message) throws IOException {
        if(message!=null){
            if(message.equals("deconnexion")){
                application.removeObserver(this);
                presenter.disconnect();
            }
            else if(message.equals("stopcalling")){
                end();

            }
            else if(message.contains("test")){
                if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                    application.getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                } else  application.getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                if(message.split(" ").length>1){
                    presenter.sendMessage(message);
                }

            }else if(message.contains("botMic")){
                presenter.sendMessage(message);

            }else if(message.contains("orientationChanged")&&application.isTelepresence()==false){
                end();

            }else if(message.contains("webrtcsend")){
                presenter.sendMessage(message.split(":")[1]);

            }
        }
    }

    private void end() {
        vGLSurfaceViewCallFull.setVisibility(View.GONE);
        vGLSurfaceViewCallPip.setVisibility(View.GONE);
        WebrtcFragment fragment = (WebrtcFragment)
                getFragmentManager().findFragmentById(R.id.fragmentContainer1);

        getFragmentManager().beginTransaction()
                .detach(fragment)
                .attach(fragment)
                .commit();
        presenter.disconnect();

    }
}
