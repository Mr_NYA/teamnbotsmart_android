package com.orrobotics.linphone;

import org.linphone.core.LinphoneAddress;
import org.linphone.core.LinphoneCall;
import org.linphone.core.LinphoneCallParams;
import org.linphone.core.LinphoneCore;
import org.linphone.core.LinphoneCoreException;

import android.util.Log;

public class CallManager {

	private static CallManager instance;

	private CallManager() {
	}

	public static final synchronized CallManager getInstance() {
		if (instance == null)
			instance = new CallManager();
		return instance;
	}

	private static BandwidthManager bm() {
		return BandwidthManager.getInstance();
	}

	/**
	 * Lancer un appel vers une addresse donnée
	 * @param lAddress
	 * @param videoEnabled
	 * @param lowBandwidth
	 * @throws LinphoneCoreException
	 */
	public static void inviteAddress(LinphoneAddress lAddress,
			boolean videoEnabled, boolean lowBandwidth)
			throws LinphoneCoreException {
		if (LinphoneRegistration.lc != null){
		LinphoneCore lc = LinphoneRegistration.lc;

		LinphoneCallParams params = lc.createCallParams(null);
		bm().updateWithProfileSettings(lc, params);

		if (videoEnabled && params.getVideoEnabled()) {
			params.setVideoEnabled(true);
		} else {
			params.setVideoEnabled(false);
		}

		if (lowBandwidth) {
			params.enableLowBandwidth(true);
			Log.v("", "basse bande passante activ� dans params de l'appel");
		}

		lc.inviteAddressWithParams(lAddress, params);
		}}

	/**
	 * Ajout de video a l'appel en cours Aucune invitation de video n'est envoyé
	 * si l'appel en cours est un appel video ou si la bande passante est tros
	 * basse.
	 * 
	 * @return if updateCall called
	 */
	boolean reinviteWithVideo() {
		if (LinphoneRegistration.lc != null){
		LinphoneCore lc = LinphoneRegistration.lc;
		LinphoneCall lCall = lc.getCurrentCall();
		if (lCall == null) {
			Log.e("",
					"Tentative de relancer un appel video quand appel termin�: ne rien faire");
			return false;
		}
		LinphoneCallParams params = lCall.getCurrentParamsCopy();

		if (params.getVideoEnabled())
			return false;

		// Verification s'il est possible d'ouvrir une session video selon les
		// limitations de bande passante
		bm().updateWithProfileSettings(lc, params);

		// Terminaison s'il n'y a pas assez de bande passante
		if (!params.getVideoEnabled()) {
			return false;
		}

		// pas encore en appel video : tentative de réinviter avec video
		lc.updateCall(lCall, params);
		}
		return true;
	}

	void reinvite() {
		if (LinphoneRegistration.lc != null){
		LinphoneCore lc = LinphoneRegistration.lc;
		LinphoneCall lCall = lc.getCurrentCall();
		if (lCall == null) {

			Log.e("",
					"Tentative de reinvite quand on est plus en cours d'appel : ne rien faire");
			return;
		}
		LinphoneCallParams params = lCall.getCurrentParamsCopy();
		bm().updateWithProfileSettings(lc, params);
		lc.updateCall(lCall, params);
		}
	}

	/**
	 * Mise à jour de l'appel en cours après modification d'un paramètrage (ex :
	 * modification de camera)
	 */
	public void updateCall() {
		if (LinphoneRegistration.lc != null){
		LinphoneCore lc = LinphoneRegistration.lc;
		LinphoneCall lCall = lc.getCurrentCall();
		if (lCall == null) {
			Log.e("",
					"Tentative de reinvite quand on est plus en cours d'appel : ne rien faire");
			return;
		}
		LinphoneCallParams params = lCall.getCurrentParamsCopy();
		bm().updateWithProfileSettings(lc, params);
		lc.updateCall(lCall, null);
		}
	}

}
