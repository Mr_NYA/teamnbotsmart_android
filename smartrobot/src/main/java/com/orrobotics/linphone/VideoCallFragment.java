package com.orrobotics.linphone;

import java.io.IOException;

import org.linphone.core.LinphoneCall;
import org.linphone.core.LinphoneCore;
import org.linphone.mediastream.Version;
import org.linphone.mediastream.video.AndroidVideoWindowImpl;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.app.Fragment;
import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.OrientationEventListener;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;

import com.orrobotics.orsmartrobot.R;
import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.observer.OrSmarRobotObserver;

public class VideoCallFragment extends Fragment implements
		SurfaceHolder.Callback, OnGestureListener, OnDoubleTapListener,
		CompatibilityScaleGestureListener, OrSmarRobotObserver {

	LinphoneCore mlc;

	public SurfaceView mVideoView;
	public SurfaceView mCaptureView;
	private AndroidVideoWindowImpl androidVideoWindowImpl;
	private GestureDetector mGestureDetector;
	private float mZoomFactor = 1.f;
	private float mZoomCenterX, mZoomCenterY;
	private CompatibilityScaleGestureDetector mScaleDetector;
	int videoDeviceId;
	private Context context;
	static SmartRobotApplication application;
	private OrientationEventListener mOrientationHelper;
	private int mAlwaysChangingPhoneAngle = -1;
	int screenWidth;
	int screenHight;
	int length;
	int rotation = 0;
	int degrees = 0;
	View view;

	public VideoCallFragment() {
	}

	@SuppressLint("ValidFragment")
    public VideoCallFragment(Context context) {
		this.context = context;
		LinphoneUtils.loadLinphoneLibraries();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
	}

	@SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
	// Warning useless because value is ignored and automatically set by new
	// APIs.
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		if (context != null)
			startOrientationSensor();
		application = (SmartRobotApplication) context;

		View view = inflater.inflate(R.layout.video, container, false);
		this.view = view;
		screenWidth = application.getActivity().getWindowManager()
				.getDefaultDisplay().getWidth();
		screenHight = application.getActivity().getWindowManager()
				.getDefaultDisplay().getHeight();
		mCaptureView = (SurfaceView) view
				.findViewById(R.id.videoCaptureSurface);
		mVideoView = (SurfaceView) view.findViewById(R.id.videoSurface);

		mCaptureView.getHolder().setType(
				SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS); // Warning useless
		// because value is
		// ignored and
		// automatically set
		// by new APIs.

		application.registerObserver(this);
		fixZOrder(mVideoView, mCaptureView);

		androidVideoWindowImpl = new AndroidVideoWindowImpl(mVideoView,
				mCaptureView, new AndroidVideoWindowImpl.VideoWindowListener() {
			public void onVideoRenderingSurfaceReady(
					AndroidVideoWindowImpl vw, SurfaceView surface) {
				if (LinphoneRegistration.lc != null)
					LinphoneRegistration.lc.setVideoWindow(vw);
				mVideoView = surface;
			}

			public void onVideoRenderingSurfaceDestroyed(
					AndroidVideoWindowImpl vw) {
				if (LinphoneRegistration.lc != null) {
					LinphoneCore lc = LinphoneRegistration.lc;
					if (lc != null) {
						lc.setVideoWindow(null);
					}
				}
			}

			public void onVideoPreviewSurfaceReady(
					AndroidVideoWindowImpl vw, SurfaceView surface) {

				mCaptureView = surface;

				if (LinphoneRegistration.lc != null)
					LinphoneRegistration.lc
							.setPreviewWindow(mCaptureView);
			}

			public void onVideoPreviewSurfaceDestroyed(
					AndroidVideoWindowImpl vw) {

				if (LinphoneRegistration.lc != null)
					LinphoneRegistration.lc.setPreviewWindow(null);

			}
		});

		mVideoView.setOnTouchListener(new OnTouchListener() {
			@SuppressLint("ClickableViewAccessibility")
			public boolean onTouch(View v, MotionEvent event) {
				if (mScaleDetector != null) {
					mScaleDetector.onTouchEvent(event);
				}

				mGestureDetector.onTouchEvent(event);

				return true;
			}
		});

		return view;
	}

	private void fixZOrder(SurfaceView video, SurfaceView preview) {
		video.setZOrderOnTop(false);
		preview.setZOrderOnTop(true);
		preview.setZOrderMediaOverlay(true); // nï¿½cessaire pour l'affichage du
		// layout de control au dessus
	}

	@Override
	public void onResume() {
		super.onResume();
		if (mVideoView != null) {

			((GLSurfaceView) mVideoView).onResume();
		}

		if (mCaptureView != null && LinphoneRegistration.lc != null) {
			if (LinphoneRegistration.lc.getCalls().length > 0)
				mCaptureView.setVisibility(View.VISIBLE);
		}

		if (androidVideoWindowImpl != null) {
			synchronized (androidVideoWindowImpl) {
				if (LinphoneRegistration.lc != null)
					LinphoneRegistration.lc
							.setVideoWindow(androidVideoWindowImpl);
			}
		}
		if (context == null)
			context = application.getApplicationContext();
		mGestureDetector = new GestureDetector(context, this);
		mScaleDetector = getScaleGestureDetector(context, this);

	}

	@Override
	public void onPause() {

		if (androidVideoWindowImpl != null) {
			synchronized (androidVideoWindowImpl) {

				if (LinphoneRegistration.lc != null)
					LinphoneRegistration.lc.setVideoWindow(null);
			}
		}

		if (mVideoView != null) {
			((GLSurfaceView) mVideoView).onPause();
		}

		super.onPause();
	}

	public boolean onScale(CompatibilityScaleGestureDetector detector) {
		LinphoneCall currentCall;
		mZoomFactor *= detector.getScaleFactor();
		// L'objet ne doit pas etre ni trop petit ni trop grand
		float portraitZoomFactor = ((float) mVideoView.getHeight())
				/ (float) ((3 * mVideoView.getWidth()) / 4);
		float landscapeZoomFactor = ((float) mVideoView.getWidth())
				/ (float) ((3 * mVideoView.getHeight()) / 4);
		mZoomFactor = Math.max(
				0.1f,
				Math.min(mZoomFactor,
						Math.max(portraitZoomFactor, landscapeZoomFactor)));
		if (LinphoneRegistration.lc != null) {
			currentCall = LinphoneRegistration.lc.getCurrentCall();
			if (currentCall != null) {
				currentCall.zoomVideo(mZoomFactor, mZoomCenterX, mZoomCenterY);
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
							float distanceY) {
		if (LinphoneRegistration.lc != null
				&& LinphoneRegistration.lc.getCurrentCall() != null)
			if (isCallEstablished(LinphoneRegistration.lc.getCurrentCall())) {
				if (mZoomFactor > 1) {
					if (distanceX > 0 && mZoomCenterX < 1) {
						mZoomCenterX += 0.01;
					} else if (distanceX < 0 && mZoomCenterX > 0) {
						mZoomCenterX -= 0.01;
					}
					if (distanceY < 0 && mZoomCenterY < 1) {
						mZoomCenterY += 0.01;
					} else if (distanceY > 0 && mZoomCenterY > 0) {
						mZoomCenterY -= 0.01;
					}

					if (mZoomCenterX > 1)
						mZoomCenterX = 1;
					if (mZoomCenterX < 0)
						mZoomCenterX = 0;
					if (mZoomCenterY > 1)
						mZoomCenterY = 1;
					if (mZoomCenterY < 0)
						mZoomCenterY = 0;

					if (LinphoneRegistration.lc != null)
						LinphoneRegistration.lc.getCurrentCall().zoomVideo(
								mZoomFactor, mZoomCenterX, mZoomCenterY);
					return true;
				}
			}

		return false;
	}

	@Override
	public boolean onDoubleTap(MotionEvent e) {
		if (LinphoneRegistration.lc != null
				&& isCallEstablished(LinphoneRegistration.lc.getCurrentCall())) {
			if (mZoomFactor == 1.f) {
				application.notifyObservers("dispalyVideoOnly");

				float portraitZoomFactor = ((float) mVideoView.getHeight())
						/ (float) ((3 * mVideoView.getWidth()) / 4);
				float landscapeZoomFactor = ((float) mVideoView.getWidth())
						/ (float) ((3 * mVideoView.getHeight()) / 4);

				mZoomFactor = Math.max(portraitZoomFactor, landscapeZoomFactor);
			} else {
				resetZoom();
				application.notifyObservers("dispalyAll");

			}
			if (LinphoneRegistration.lc != null)
				LinphoneRegistration.lc.getCurrentCall().zoomVideo(mZoomFactor,
						mZoomCenterX, mZoomCenterY);
			return true;
		}

		if (mZoomFactor == 1.f) {
			application.notifyObservers("dispalyVideoOnly");

		} else {
			application.notifyObservers("dispalyAll");

		}
		return false;
	}

	private void resetZoom() {
		mZoomFactor = 1.f;
		mZoomCenterX = mZoomCenterY = 0.5f;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		if (mOrientationHelper != null) {
			mOrientationHelper.disable();
			mOrientationHelper = null;
		}
		mCaptureView = null;
		if (mVideoView != null) {
			mVideoView.setOnTouchListener(null);
			mVideoView = null;
		}
		if (androidVideoWindowImpl != null) {
			// Eviter que linphone crash si le corespondant coupe l'appel quand
			// on fait tourner le device
			androidVideoWindowImpl.release();
			androidVideoWindowImpl = null;
		}
		if (mGestureDetector != null) {
			mGestureDetector.setOnDoubleTapListener(null);
			mGestureDetector = null;
		}
		if (mScaleDetector != null) {
			mScaleDetector.destroy();
			mScaleDetector = null;
		}
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

	}

	@Override
	public boolean onDown(MotionEvent e) {
		return true;
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e) {
		return false;
	}

	@Override
	public boolean onSingleTapConfirmed(MotionEvent e) {
		return false;
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
						   float velocityY) {
		return false;
	}

	@Override
	public void onLongPress(MotionEvent e) {

	}

	@Override
	public void onShowPress(MotionEvent e) {

	}

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		return false;
	}

	public static CompatibilityScaleGestureDetector getScaleGestureDetector(
			Context context, CompatibilityScaleGestureListener listener) {
		if (Version.sdkAboveOrEqual(Version.API08_FROYO_22)) {
			if (context == null)
				context = application.getApplicationContext();
			CompatibilityScaleGestureDetector csgd = new CompatibilityScaleGestureDetector(
					context);

			csgd.setOnScaleListener(listener);
			return csgd;

		}
		return null;
	}

	public static boolean isCallRunning(LinphoneCall call) {
		if (call == null) {
			return false;
		}

		LinphoneCall.State state = call.getState();

		return state == LinphoneCall.State.Connected
				|| state == LinphoneCall.State.CallUpdating
				|| state == LinphoneCall.State.CallUpdatedByRemote
				|| state == LinphoneCall.State.StreamsRunning
				|| state == LinphoneCall.State.Resuming;
	}

	public static boolean isCallEstablished(LinphoneCall call) {
		if (call == null) {
			return false;
		}

		LinphoneCall.State state = call.getState();

		return isCallRunning(call) || state == LinphoneCall.State.Paused
				|| state == LinphoneCall.State.PausedByRemote
				|| state == LinphoneCall.State.Pausing;
	}

	/**
	 * Register a sensor to track phoneOrientation changes
	 */
	private synchronized void startOrientationSensor() {
		try {
			if (mOrientationHelper == null) {

				mOrientationHelper = new LocalOrientationEventListener(
						context);
			}
			mOrientationHelper.enable();
		}catch(Exception e){

		}
	}

	// Orientation listener
	private class LocalOrientationEventListener extends
			OrientationEventListener {
		public LocalOrientationEventListener(Context context) {
			super(context);
		}

		@Override
		public void onOrientationChanged(int orientation) {
			// TODO Auto-generated method stub
			if (orientation == OrientationEventListener.ORIENTATION_UNKNOWN) {
				return;
			}
			degrees = 270;
			//
			if (orientation < 45 || orientation > 315)
				degrees = 0;
			else if (orientation < 135)
				degrees = 90;

			else if (orientation < 225)
				degrees = 180;

			if (mAlwaysChangingPhoneAngle == degrees) {
				return;
			}

			rotation = (360 - degrees) % 360;

			LinphoneCore lc = LinphoneRegistration.lc;
			if (lc != null) {
				lc.setDeviceRotation(rotation);
				LinphoneCall currentCall = lc.getCurrentCall();
				if (currentCall != null && currentCall.cameraEnabled()
						&& currentCall.getCurrentParamsCopy().getVideoEnabled()) {
					mAlwaysChangingPhoneAngle = degrees;
					lc.updateCall(currentCall, null);
				}
			}

		}
	}

	@Override
	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub

	}

	@SuppressLint("NewApi")
	@Override
	public void surfaceCreated(SurfaceHolder arg0) {
		// TODO Auto-generated method stub

	}

	public void hideVideo() {
		application.getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				if (mCaptureView != null)
					mCaptureView.setVisibility(View.GONE);
				else {
					mCaptureView = (SurfaceView) view
							.findViewById(R.id.videoCaptureSurface);
					mCaptureView.setVisibility(View.GONE);
				}
				if (mVideoView != null)
					mVideoView.setVisibility(View.GONE);
				else {
					mVideoView = (SurfaceView) view
							.findViewById(R.id.videoSurface);
					mVideoView.setVisibility(View.GONE);
				}
			}
		});

	}

	public void showVideo() {
		application.getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				if (mCaptureView != null)
					mCaptureView.setVisibility(View.VISIBLE);
				else {
					mCaptureView = (SurfaceView) view
							.findViewById(R.id.videoCaptureSurface);
					mCaptureView.setVisibility(View.VISIBLE);
				}
				if (mVideoView != null)
					mVideoView.setVisibility(View.VISIBLE);
				else {
					mVideoView = (SurfaceView) view
							.findViewById(R.id.videoSurface);
					mVideoView.setVisibility(View.VISIBLE);
				}
			}
		});

	}

	@Override
	public void surfaceDestroyed(SurfaceHolder arg0) {
		// TODO Auto-generated method stub
		// mp.setDisplay(null);
	}

	@Override
	public void update(String message) throws IOException {
		// TODO Auto-generated method stub
		if (message != null) {

			if (message.equals("terminateCall")
					|| message.equals("endtelepresence")) {

				hideVideo();

			}

			if (message.equals("camBotOff")
					|| (message.equals("disableCamera"))) {

				application.getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						if (mCaptureView != null)
							mCaptureView.setVisibility(View.GONE);
						else {
							mCaptureView = (SurfaceView) view
									.findViewById(R.id.videoCaptureSurface);
							mCaptureView.setVisibility(View.GONE);
						}

					}
				});

			}
			if (message.equals("camBotOn") || (message.equals("enableCamera"))) {
				application.getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						if (mCaptureView != null)
							mCaptureView.setVisibility(View.VISIBLE);
						else {
							mCaptureView = (SurfaceView) view
									.findViewById(R.id.videoCaptureSurface);
							mCaptureView.setVisibility(View.VISIBLE);
						}

					}
				});
			}

		}

	}

	public void removeObserver() {
		// TODO Auto-generated method stub
		application.removeObserver(this);
	}

}
