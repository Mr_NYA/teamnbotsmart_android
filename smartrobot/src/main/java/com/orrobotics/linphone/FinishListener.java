package com.orrobotics.linphone;

public interface FinishListener
{
    void onFinishCallback();
}
