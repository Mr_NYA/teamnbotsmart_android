package com.orrobotics.linphone;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import org.linphone.core.LinphoneAddress;
import org.linphone.core.LinphoneAuthInfo;
import org.linphone.core.LinphoneCall;
import org.linphone.core.LinphoneCall.State;
import org.linphone.core.LinphoneCallParams;
import org.linphone.core.LinphoneCallStats;
import org.linphone.core.LinphoneChatMessage;
import org.linphone.core.LinphoneChatRoom;
import org.linphone.core.LinphoneContent;
import org.linphone.core.LinphoneCore;
import org.linphone.core.LinphoneAddress.TransportType;
import org.linphone.core.LinphoneCore.AuthMethod;
import org.linphone.core.LinphoneCore.EcCalibratorStatus;
import org.linphone.core.LinphoneCore.GlobalState;
import org.linphone.core.LinphoneCore.LogCollectionUploadState;
import org.linphone.core.LinphoneCore.RegistrationState;
import org.linphone.core.LinphoneCore.RemoteProvisioningState;
import org.linphone.core.LinphoneCoreException;
import org.linphone.core.LinphoneCoreFactory;
import org.linphone.core.LinphoneCoreListener;
import org.linphone.core.LinphoneEvent;
import org.linphone.core.LinphoneFriend;
import org.linphone.core.LinphoneFriendList;
import org.linphone.core.LinphoneInfoMessage;
import org.linphone.core.LinphoneProxyConfig;
import org.linphone.core.OpenH264DownloadHelperListener;
import org.linphone.core.PayloadType;
import org.linphone.core.PublishState;
import org.linphone.core.SubscriptionState;
import org.linphone.mediastream.Version;
import org.linphone.tools.OpenH264DownloadHelper;

import com.orrobotics.orsmartrobot.R;
import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.service.LinphoneService;
import com.orrobotics.orsmartrobot.service.ServiceRobot;
import com.orrobotics.orsmartrobot.util.GestionDesFichiers;
import com.orrobotics.orsmartrobot.util.RemoteControlUtil;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

public class LinphoneRegistration implements LinphoneCoreListener {
	boolean running;
	public static LinphoneCore lc;
	LinphoneCallParams params = null;
	SmartRobotApplication model;
	Context context;
	LinphoneCall mCall;
	private Handler mHandler = new Handler();
	Boolean echoCanceled = false;
	String basePath;
	private Resources mR;
	String rootCaFile;
	String linphoneConfigFile;
	String transport = "tls";
	Boolean isecho = false;
	String status = "";
	String codec ="VP8";
	OpenH264DownloadHelper mCodecDownloader;
	OpenH264DownloadHelperListener mCodecListener;

	public LinphoneRegistration(Context c) {
		this.context = c;
		model = (SmartRobotApplication) c;
		if (SmartRobotApplication.getParamFromFile("transport",
				"Configuration.properties", null) != null)
			transport = SmartRobotApplication.getParamFromFile("transport",
					"Configuration.properties", null);


		String data = SmartRobotApplication.getParamFromFile("linphone.videoCodec", "Configuration.properties",null);
		if(data!=null)
			codec=data;


		try {
			initRegistration();
		} catch (LinphoneCoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*
	 * Le listener de l'etat de Registration
	 */
	@Override
	public void registrationState(final LinphoneCore lc,
								  final LinphoneProxyConfig cfg, RegistrationState cstate,
								  final String smessage) {
		write(cfg.getIdentity() + " : " + smessage);
		GestionDesFichiers.log("LinphoneRegistration", "State : " + smessage,
				"Log_SmartRobot");

		if (smessage.contains("Refresh")) {

		}

		if (smessage.contains("Forbidden")) {
			model.notifyObservers("siplogout");
			showToast(model.getResources().getString(
					R.string.registrationFailed)
					+ "("
					+ transport
					+ ") : "
					+ model.getResources().getString(
					R.string.registartionForbidden));
		}
		if (smessage.equals("Unregistration done")) {
			model.setEditor(RemoteControlUtil.CONNECTED_TO_SIP_SERVER, "false");
			model.setRegistered(false);
			model.notifyObservers("siplogout");

		}

		if (cstate == RegistrationState.RegistrationOk) {
			String speaker_enable = model.getSpeakerState();
			if (speaker_enable.equals("false") || speaker_enable.equals("")) {
				model.enableSpeaker(false);

			} else
				model.enableSpeaker(true);
			if (model.isRegisted() == false) {
				model.notifyObservers("cancelAutoRegistration");

			}
			model.setEditor(RemoteControlUtil.CONNECTED_TO_SIP_SERVER, "true");
			model.setRegistered(true);
			model.notifyObservers("siplogin");
			Log.v("", "------ echoCanceled =" + echoCanceled);
			if (echoCanceled == false) {

				if (model.getEchoConcellation().equals("true")) {
					Log.v("", "------ EchoConcellation =true");
					lc.enableEchoCancellation(true);
					try {
						lc.startEchoCalibration(this);
					} catch (LinphoneCoreException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					GestionDesFichiers.log("info", "ECHO_CANCELLATION",
							"State : enabled");
					echoCanceled = true;
				} else {
					Log.v("", "------ EchoConcellation =false");

					lc.enableEchoCancellation(false);
					GestionDesFichiers.log("info", "ECHO_CANCELLATION",
							"State : disabled");
				}

			}
		}

		if (cstate == RegistrationState.RegistrationFailed
				|| cstate == RegistrationState.RegistrationNone) {
			model.setEditor(RemoteControlUtil.CONNECTED_TO_SIP_SERVER, "false");
			model.setRegistered(false);
			model.notifyObservers("siplogout");

			if (model.isNetworkUP() && !model.isAutoRegistrationOn())
				LinphoneService.startAutoRegistrationTimer();
			model.setNumfailSIP(model.getNumfailSIP()+1);
		}
	}

	@SuppressLint("ShowToast")
	private void showToast(final String message) {
		// TODO Auto-generated method stub
		mHandler.post(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
			}
		});
	}

	@Override
	public void show(LinphoneCore lc) {
	}

	public void byeReceived(LinphoneCore lc, String from) {

	}

	@Override
	public void authInfoRequested(LinphoneCore lc, String realm,
								  String username, String domain) {
	}

	@Override
	public void displayStatus(LinphoneCore lc, String message) {
		Log.e("", "-------------displayStatus=" + message);

		if (message.contains("Not Found") || message.contains("Call failed")) {
			status = "unvalaible";
			model.notifyObservers("pseudonotfound");
		}

	}

	@Override
	public void displayMessage(LinphoneCore lc, String message) {
	}

	@Override
	public void displayWarning(LinphoneCore lc, String message) {
	}

	@Override
	public void globalState(LinphoneCore lc, GlobalState state, String message) {

	}

	@Override
	public void newSubscriptionRequest(LinphoneCore lc, LinphoneFriend lf,
									   String url) {
	}

	@Override
	public void notifyPresenceReceived(LinphoneCore lc, LinphoneFriend lf) {
	}

	public void textReceived(LinphoneCore lc, LinphoneChatRoom cr,
							 LinphoneAddress from, String message) {
	}

	/*
	 * Le listener de l'etat de l'appel
	 */
	@Override
	public void callState(LinphoneCore lc, LinphoneCall call,
						  LinphoneCall.State cstate, String msg) {

		if (msg.equals("Call ended")) {

			// model.notifyObservers("endtelepresence");
			model.setTelepresence(false);
			ServiceRobot.sendMessage(
					model.getPseudoOpenFireSender(),
					"badBandWith"
			);
		}
		if (msg.equals("Call terminated") || msg.equals("Call released")) {
			if (!status.equals("unvalaible")) {
				model.notifyObservers("endtelepresence");
			} else {
				status = "";
			}
			model.setTelepresence(false);
			ServiceRobot.sendMessage(
					model.getPseudoOpenFireSender(),
					"badBandWith"
			);
		}


		if (cstate == LinphoneCall.State.IncomingReceived) {

			model.setTelepresence(true);
			mCall = call;
			answer(mCall);

		}
		if (cstate == LinphoneCall.State.CallEnd) {

			model.setTelepresence(false);
			ServiceRobot.sendMessage(
					model.getPseudoOpenFireSender(),
					"badBandWith"
			);
			model.notifyObservers("callEnded");
		}
		if (cstate == LinphoneCall.State.Connected) {

			model.setVolume();
			model.setEditor(RemoteControlUtil.CALL_TYPE, "linphone");
			model.setCallType();
			model.notifyObservers("displayLinphone");
		}
		if (cstate == LinphoneCall.State.CallReleased) {
		}
		if (cstate == LinphoneCall.State.CallUpdatedByRemote) {

			try {
				if (LinphoneRegistration.lc != null)
					LinphoneRegistration.lc.acceptCallUpdate(mCall,
							mCall.getRemoteParams());

			} catch (LinphoneCoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public void callStatsUpdated(LinphoneCore lc, LinphoneCall call,
								 LinphoneCallStats stats) {
		try {
			lc.acceptCallUpdate(call, call.getRemoteParams());

		} catch (LinphoneCoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/*
	 * Le listener de l'etat de la calibration d'echo
	 */
	@Override
	public void ecCalibrationStatus(LinphoneCore lc2,
									EcCalibratorStatus status, int delay_ms, Object data) {
		isecho = true;
		if (status == EcCalibratorStatus.DoneNoEcho) {

		} else if (status == EcCalibratorStatus.Done) {

		} else if (status == EcCalibratorStatus.Failed) {

		}

		if(running==false){
			if(lc!=null){
				lc.destroy();
				lc=null;
			}

		}
	}

	@Override
	public void callEncryptionChanged(LinphoneCore lc, LinphoneCall call,
									  boolean encrypted, String authenticationToken) {
	}

	@Override
	public void notifyReceived(LinphoneCore lc, LinphoneCall call,
							   LinphoneAddress from, byte[] event) {
	}

	@Override
	public void dtmfReceived(LinphoneCore lc, LinphoneCall call, int dtmf) {
	}

	/**
	 * Initialisation des paramètres de registration
	 * @throws LinphoneCoreException
	 */
	public void initRegistration()
			throws LinphoneCoreException {
		LinphoneUtils.loadLinphoneLibraries();
		mR = context.getResources();
		basePath = context.getFilesDir().getAbsolutePath();
		rootCaFile = basePath + "/rootca.pem";
		linphoneConfigFile = basePath + "/.linphonerc";
		try {
			copyFile(R.raw.linphonerc_default, linphoneConfigFile);
			copyFile(R.raw.rootca, rootCaFile);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// Premiere instanciation de l'objet core linphone
		try {
			lc = LinphoneCoreFactory.instance().createLinphoneCore(this,
					linphoneConfigFile, null, null, context);
			lc.setContext(context);
			lc.setZrtpSecretsCache(basePath + "/zrtp_secrets");
			lc.setUserAgent("LinphoneAndroid", lc.getVersion());
			int migrationResult = lc.migrateToMultiTransport();
			Log.v("linphone", "Migration to multi transport result = "
					+ migrationResult);

			lc.setRootCA(rootCaFile);

		} catch (LinphoneCoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(codec.contains("H264")){

			mCodecDownloader = LinphoneCoreFactory.instance().createOpenH264DownloadHelper();
			mCodecListener = new OpenH264DownloadHelperListener() {
				ProgressDialog progress;
				@Override
				public void OnProgress(final int current, final int max) {
					mHandler.post(new Runnable() {
						@Override
						public void run() {
							if (progress == null) {
								progress = new ProgressDialog(model.getActivity());
								progress.setCanceledOnTouchOutside(false);
								progress.setCancelable(false);
								progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
							} else if (current <= max) {
								progress.setMessage(model.getActivity().getResources().getString(R.string.down));
								progress.setMax(max);
								progress.setProgress(current);
								progress.show();
							} else {
								progress.dismiss();
								progress = null;
								lc.reloadMsPlugins(null);
								Intent i = model.getActivity().getBaseContext().getPackageManager()
										.getLaunchIntentForPackage( model.getActivity().getBaseContext().getPackageName() );
								i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
								model.getActivity().startActivity(i);
								model.getActivity().finish();
							}

						}
					});
				}

				@Override
				public void OnError(final String error) {
					mHandler.post(new Runnable() {
						@Override
						public void run() {
							if (progress != null) progress.dismiss();
							Toast.makeText(context, model.getActivity().getString(R.string.errorRelaod), Toast.LENGTH_SHORT).show();
						}
					});
				}
			};
			mCodecDownloader.setOpenH264HelperListener(mCodecListener);


		}
	}

	/**
	 * Enregistrememt auprès du serveur SIP
	 *
	 * @throws LinphoneCoreException
	 */
	public void Registration(String sipAddress, String password) throws LinphoneCoreException {
		Log.v("linphone", "-------in Registration------");
		final LinphoneCoreFactory lcFactory = LinphoneCoreFactory.instance();
		isecho = false;
		Log.v("linphone", "-------initRegistration----------");
		Log.v("linphone", "-------sipAddress=" + sipAddress);
		Log.v("linphone", "-------password=" + password);
		// Preparation de l'address identité
		LinphoneAddress address = lcFactory.createLinphoneAddress(sipAddress);
		String username = address.getUserName();
		String domain = address.getDomain();

		if (password != null) {
			// creation de la structure d'authentification depuis l'identité
			// et l'ajouter a linphone
			lc.addAuthInfo(lcFactory.createAuthInfo(username, password, null,
					domain));
		}
		// creation du proxy config
		LinphoneProxyConfig proxyCfg = lc.createProxyConfig(sipAddress, domain,
				null, true);
		PayloadType h264 = null;
		PayloadType vp8 = null;
		for (PayloadType pt : lc.getVideoCodecs()) {
			if(codec.contains("H264")) {
				if(lc.openH264Enabled()) {
					if (pt.getMime().equals("H264")) {
						h264 = pt;

						if (Version.getCpuAbis().contains("armeabi-v7a") && !Version.getCpuAbis().contains("x86")
								&& !mCodecDownloader.isCodecFound()) {

							mCodecDownloader.setOpenH264HelperListener(mCodecListener);
							mCodecDownloader.setUserData(0, context);
							mCodecDownloader.setUserData(1, codec);
							mCodecDownloader.downloadCodec();
							lc.enablePayloadType(h264, true);
						}

					}
				}

			}
			if (pt.getMime().equals("VP8")) {
				vp8 = pt;
				lc.enablePayloadType(vp8, true);
			}
		}
		lc.addProxyConfig(proxyCfg); // ajout du proxy config a linphone
		lc.setDefaultProxyConfig(proxyCfg);
		lc.getDefaultProxyConfig().setExpires(20000);
		lc.setVideoPolicy(true, true);
		lc.enableVideo(true, true);

		if (transport.equals("tls")) {
			setAccountTransport(1, "tls", proxyCfg);
		} else if (transport.equals("tcp")) {
			setAccountTransport(1, "tcp", proxyCfg);
		} else if (transport.equals("udp")) {
			setAccountTransport(1, "udp", proxyCfg);
		}

		/*
		 * loop principale pour recevoir les notifications et réaliser les
		 * taches d'arriere plan de linphonecore
		 */
		running = true;
		while (running) {
			lc.iterate(); // Premiere itération pour initier la registration
			sleep(50);
		}

		// Déregistration
		if (running && lc.getDefaultProxyConfig() != null) {
			lc.getDefaultProxyConfig().edit();
			lc.getDefaultProxyConfig().enableRegister(false);
			lc.getDefaultProxyConfig().done();
			while (running
					&& lc.getDefaultProxyConfig() != null
					&& lc.getDefaultProxyConfig().getState() != RegistrationState.RegistrationCleared) {
				lc.iterate();
				sleep(50);
			}
		}

	}

	/**
	 * Déconnection du serveur SIP
	 */
	public static void unRegistration() {

		if (lc != null) {
			// Déregistration
			lc.getDefaultProxyConfig().edit();
			lc.getDefaultProxyConfig().enableRegister(false);
			lc.getDefaultProxyConfig().done();
		}
	}

	/**
	 * Mettre le thread en attente
	 *
	 * @param ms
	 */
	private void sleep(int ms) {
		try {
			Thread.sleep(ms);
		} catch (InterruptedException ie) {
			write("Interrempu!\n Términaison");
			return;
		}
	}

	/**
	 * Arreter le thread
	 */
	public void stopMainLoop() {
		running = false;
		Log.v("", "----in stop MainLoop()------");
		if(isecho==true){
			if(lc!=null){
				lc.destroy();
				lc=null;
			}
		}
	}

	private void write(String s) {
		System.out.println(s);
	}

	/**
	 * Répondre à un appel avec des paramètres initiales
	 *
	 * @param call
	 * @param params
	 * @return
	 */
	public static boolean acceptCallWithParams(LinphoneCall call,
											   LinphoneCallParams params) {
		try {
			lc.acceptCallWithParams(call, params);
			return true;
		} catch (LinphoneCoreException e) {
			Log.v("info", "Accept de l'appel échoué" + e);
		}
		return false;
	}

	/**
	 * Répondre à un appel reçue
	 */
	private void answer(LinphoneCall call) {
		params = LinphoneRegistration.lc.createCallParams(null);
		if (model.getCamId().equals("0")) {
			LinphoneRegistration.lc.setVideoDevice(0);
		} else {
			LinphoneRegistration.lc.setVideoDevice(1);
		}
		params.setVideoEnabled(true);
		if (model.getCamState().equals("off")) {

			call.enableCamera(false);
		} else {

			call.enableCamera(true);
		}


		if (model.getMicState().equals("false")) {
			lc.muteMic(true);
		} else {

			lc.muteMic(false);
		}
		if (model.getSpeakerState().equals("false")) {
			model.enableSpeaker(false);
		} else {

			model.enableSpeaker(true);
		}
		if (LinphoneRegistration.acceptCallWithParams(call, params)) {
			model.notifyObservers("istelepresence");
			model.unlockScreen();
			model.notifyObservers("launchControl");

		}

	}

	public static boolean isHighBandwidthConnection(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = cm.getActiveNetworkInfo();
		return (info != null && info.isConnected() && isConnectionFast(
				info.getType(), info.getSubtype()));
	}

	private static boolean isConnectionFast(int type, int subType) {
		if (type == ConnectivityManager.TYPE_MOBILE) {
			switch (subType) {
				case TelephonyManager.NETWORK_TYPE_EDGE:
				case TelephonyManager.NETWORK_TYPE_GPRS:
				case TelephonyManager.NETWORK_TYPE_IDEN:
					return false;
			}
		}
		// in doubt, assume connection is good.
		return true;
	}

	/**
	 * Modifier le protocol de transport utilisé
	 *
	 * @param n
	 * @param transport
	 * @param proxyConfig
	 */
	public void setAccountTransport(int n, String transport,
									final LinphoneProxyConfig proxyConfig) {
		if (proxyConfig != null && transport != null) {
			LinphoneAddress proxyAddr;

			try {
				proxyAddr = LinphoneCoreFactory.instance()
						.createLinphoneAddress(proxyConfig.getProxy());
				if (transport.equals("udp")) {
					proxyAddr.setTransport(TransportType.LinphoneTransportUdp);
				} else if (transport.equals("tcp")) {
					proxyAddr.setTransport(TransportType.LinphoneTransportTcp);
				} else if (transport.equals("tls")) {
					proxyAddr.setTransport(TransportType.LinphoneTransportTls);
				}
				proxyConfig.edit();

				proxyConfig.setProxy(proxyAddr.asStringUriOnly());

				proxyConfig.done();

			} catch (LinphoneCoreException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Afficher le protocol de transport utilisé
	 *
	 * @param n
	 * @param proxyConfig
	 * @return
	 */
	public TransportType getAccountTransport(int n,
											 LinphoneProxyConfig proxyConfig) {
		TransportType transport = null;

		if (proxyConfig != null) {
			LinphoneAddress proxyAddr;
			try {
				proxyAddr = LinphoneCoreFactory.instance()
						.createLinphoneAddress(proxyConfig.getProxy());
				transport = proxyAddr.getTransport();
			} catch (LinphoneCoreException e) {
				e.printStackTrace();
			}
		}

		return transport;
	}

	/**
	 * Copier un fichier depuis le dossier "res"
	 *
	 * @param ressourceId
	 * @param target
	 * @throws IOException
	 */
	public void copyFile(int ressourceId, String target) throws IOException {

		File lFileToCopy = new File(target);
		copyFromPackage(ressourceId, lFileToCopy.getName());

	}

	/**
	 * Copier le contenu d'un fichier dpuis "res" vers stockage interne de
	 * l'appareil
	 *
	 * @param ressourceId
	 * @param target
	 * @throws IOException
	 */
	public void copyFromPackage(int ressourceId, String target)
			throws IOException {
		FileOutputStream lOutputStream = context.openFileOutput(target, 0);

		InputStream lInputStream = mR.openRawResource(ressourceId);

		int readByte;
		byte[] buff = new byte[8048];
		while ((readByte = lInputStream.read(buff)) != -1) {
			lOutputStream.write(buff, 0, readByte);
		}
		lOutputStream.flush();
		lOutputStream.close();
		lInputStream.close();
	}

	@Override
	public void authenticationRequested(LinphoneCore lc,
										LinphoneAuthInfo authInfo, AuthMethod method) {
		// TODO Auto-generated method stub

	}

	@Override
	public void transferState(LinphoneCore lc, LinphoneCall call,
							  State new_call_state) {
		// TODO Auto-generated method stub

	}

	@Override
	public void infoReceived(LinphoneCore lc, LinphoneCall call,
							 LinphoneInfoMessage info) {
		// TODO Auto-generated method stub

	}

	@Override
	public void subscriptionStateChanged(LinphoneCore lc, LinphoneEvent ev,
										 SubscriptionState state) {
		// TODO Auto-generated method stub

	}

	@Override
	public void publishStateChanged(LinphoneCore lc, LinphoneEvent ev,
									PublishState state) {
		// TODO Auto-generated method stub

	}

	@Override
	public void fileTransferProgressIndication(LinphoneCore lc,
											   LinphoneChatMessage message, LinphoneContent content, int progress) {
		// TODO Auto-generated method stub

	}

	@Override
	public void fileTransferRecv(LinphoneCore lc, LinphoneChatMessage message,
								 LinphoneContent content, byte[] buffer, int size) {
		// TODO Auto-generated method stub

	}

	@Override
	public int fileTransferSend(LinphoneCore lc, LinphoneChatMessage message,
								LinphoneContent content, ByteBuffer buffer, int size) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void configuringStatus(LinphoneCore lc,
								  RemoteProvisioningState state, String message) {
		// TODO Auto-generated method stub

	}

	@Override
	public void messageReceived(LinphoneCore lc, LinphoneChatRoom cr,
								LinphoneChatMessage message) {
		// TODO Auto-generated method stub

	}


	@Override
	public void notifyReceived(LinphoneCore lc, LinphoneEvent ev,
							   String eventName, LinphoneContent content) {
		// TODO Auto-generated method stub

	}

	@Override
	public void isComposingReceived(LinphoneCore lc, LinphoneChatRoom cr) {
		// TODO Auto-generated method stub

	}

	@Override
	public void uploadProgressIndication(LinphoneCore lc, int offset, int total) {
		// TODO Auto-generated method stub

	}

	@Override
	public void uploadStateChanged(LinphoneCore lc,
								   LogCollectionUploadState state, String info) {
		// TODO Auto-generated method stub

	}

	@Override
	public void friendListCreated(LinphoneCore lc, LinphoneFriendList list) {
		// TODO Auto-generated method stub

	}

	@Override
	public void friendListRemoved(LinphoneCore lc, LinphoneFriendList list) {
		// TODO Auto-generated method stub

	}



	public static void end() {

	}



	public void launchCall(LinphoneAddress destinationSipAddress)
			throws LinphoneCoreException {

		try {
			LinphoneCallParams params = null;
			// Envoyer un message invite a la destination SIP address
			if (lc != null) {
				params = lc.createCallParams(null);
				params.setVideoEnabled(true);
				params.enableLowBandwidth(false);

				Log.v("videoSize",
						"getPreferredVideoSize() = "
								+ lc.getPreferredVideoSize());

				lc.setVideoPolicy(true, true);

				bm().updateWithProfileSettings(lc, params);
				LinphoneCall call = lc.inviteAddressWithParams(
						destinationSipAddress, params);

				if (call == null) {
					write("Could not place call to " + destinationSipAddress);
					write("Aborting");
					return;
				} else {
					if (model.getCamState().equals("off"))
						call.enableCamera(false);
					else
						call.enableCamera(true);

					model.notifyObservers("istelepresence");
					model.notifyObservers("launchControl");
				}
				write("Call to " + destinationSipAddress + " is in progress...");

				/*
				 * loop principale pour recevoir les notifications et réaliser
				 * les taches d'arriere plan de linphonecore
				 */
				running = true;
				while (running) {
					if (lc != null)
						lc.iterate();

				}

				if (!State.CallEnd.equals(call.getState())) {
					write("Terminating the call");
					lc.terminateCall(call);
				}
			}

		} finally {
			write("Exiting call thread...");

		}
	}

	private static BandwidthManager bm() {
		return BandwidthManager.getInstance();
	}
}
