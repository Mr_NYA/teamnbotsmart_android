package com.orrobotics.linphone;


public interface CompatibilityScaleGestureListener {
	public boolean onScale(CompatibilityScaleGestureDetector detector);
}