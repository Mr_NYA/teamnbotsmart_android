package com.orrobotics.linphone;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import android.graphics.PixelFormat;
import android.net.Uri;
import  android.util.DisplayMetrics;

import com.bumptech.glide.Glide;
import com.orrobotics.orsmartrobot.R;
import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.observer.OrSmarRobotObserver;
import com.orrobotics.orsmartrobot.ui.PlayGifView;
import com.orrobotics.orsmartrobot.ui.FaceOverlayView;
import com.orrobotics.orsmartrobot.util.CameraErrorCallback;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Movie;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.Camera;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.VideoView;
import android.os.Handler;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.graphics.RectF;
import com.orrobotics.orsmartrobot.modelImpl.FaceResult;
import com.orrobotics.orsmartrobot.util.GestionDesFichiers;
import com.orrobotics.orsmartrobot.util.ImageUtils;
import com.orrobotics.orsmartrobot.util.RectangleView;
import com.orrobotics.orsmartrobot.util.Util;
import com.tzutalin.dlib.Constants;
import com.tzutalin.dlib.FaceDet;
import com.tzutalin.dlib.PedestrianDet;
import com.tzutalin.dlib.VisionDetRet;

public class BackgroundFragment extends Fragment implements
		SurfaceHolder.Callback , Camera.PreviewCallback , OrSmarRobotObserver{

	public VideoView backgroundVideoView;
	static SmartRobotApplication application;
	String fileName = "";
	PlayGifView backGif;
	int Volume = 0;
	int screenWidth;
	int screenHight;
	int length = 0;
	int rotation = 0;
	int degrees = 0;
	View view;
	MediaPlayer mediaplayer;
	SurfaceHolder holder;
	Boolean hasActiveHolder = false;
	Context con;
	private int numberOfCameras;
	int prevSettingHeight;
    int rec_view;
	private SurfaceView mView;
	private SurfaceView mView2;
	long startTime = 0;
	int time_unzoom = 0;
	int zoomIsActive = 0;
	int silo_det = 0;
	public static final String TAG = BackgroundFragment.class.getSimpleName();

	private Camera mCamera;
	private int cameraId = 1;

	int bmapW;
	int bmapH;
	// Let's keep track of the display rotation and orientation also:
	private int mDisplayRotation;
	private int mDisplayOrientation;

	private int previewWidth;
	private int previewHeight;

	int  currentZoomLevel = 0;
	FaceOverlayView mFaceView;
	int suivi_with_fond;
	// Draw rectangles and other fancy stuff:
	private RectangleView mrectangleView;

	private final CameraErrorCallback mErrorCallback = new CameraErrorCallback();
	SurfaceView restsurfaceview;

	private static final int MAX_FACE = 10;
	private boolean isThreadWorking = false;
	private Handler handler;
	private FaceDetectThread detectThread = null;
	private int prevSettingWidth;
	private android.media.FaceDetector fdet;
	private FaceResult faces[];
	private FaceResult faces_previous[];

	SurfaceView mView1;
	private FaceDet mFaceDet;
	private PedestrianDet mPersonDet;
	List<VisionDetRet> resultsPD;
	private int Id = 0;
	int px;
	private String BUNDLE_CAMERA_ID = "camera";
	RelativeLayout frame_video;
	Boolean flagStart = true;
	int unzoom_time = 0;
	long duration;
	MediaController mediaC;
	//RecylerView face image
	private HashMap<Integer, Integer> facesCount = new HashMap<>();
	boolean stopThread = false;

	public BackgroundFragment() {

	}

	@SuppressLint("ValidFragment")
	public BackgroundFragment(Context context) {
		application = (SmartRobotApplication) context;
		con= context;
		mFaceDet = new FaceDet(Constants.getFaceShapeModelPath());
		if ( silo_det == 1 ) {
			mPersonDet = new PedestrianDet();

		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
	}

	private WindowManager.LayoutParams initWindowParameter() {

		return new WindowManager.LayoutParams(1, 1, WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, PixelFormat.TRANSLUCENT);

	}
	@SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
	// Warning useless because value is ignored and automatically set by new
	// APIs.
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		GestionDesFichiers
				.log("INFO",
						"Le fragment de caméra est recrée",
						"Log_SmartRobot");
		View view = inflater.inflate(R.layout.video_back, container, false);
		//View view1 = inflater.inflate(R.layout.videowebrtc, container, false);
		//View view = inflater.inflate(R.layout.video_back, container, false);
		this.view = view;
		time_unzoom = Integer.parseInt(application.getParamFromFile("time.unzoom", "Configuration.properties", null));
		zoomIsActive = Integer.parseInt(application.getParamFromFile("zoom.isactive", "Configuration.properties", null));
		silo_det = Integer.parseInt(application.getParamFromFile("silhouette.isactive", "Configuration.properties", null));

		if ( silo_det == 1 ) {
			mPersonDet = new PedestrianDet();

		}
		//ec_view = Integer.parseInt(application.getParamFromFile("rect.view", "Configuration.properties", null));

		suivi_with_fond = Integer.parseInt(application.getParamFromFile("suivi.fondecran.isactive", "Configuration.properties", null));
		//view.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,    RelativeLayout.LayoutParams.MATCH_PARENT));

		if(application.isTelepresence()==false) {

			screenWidth = application.getActivity().getWindowManager()
					.getDefaultDisplay().getWidth();
			screenHight = application.getActivity().getWindowManager()
					.getDefaultDisplay().getHeight();
			application.registerObserver(this);
			if (application.fileExist(fileName)) {
				Log.i("fileExist" ,"fileExist fileExist: ");

			}

			if (application.getBackgroundType().equals("camera") || suivi_with_fond==1) {

				mView = (SurfaceView) view.findViewById(R.id.surfaceview);
				mView.setVisibility(View.VISIBLE);
				//mView2 = (SurfaceView) view.findViewById(R.id.surfaceview2);

				//mView.setZOrderOnTop(true);
				//mView.getHolder().setFormat(PixelFormat.TRANSLUCENT);
				//mView2.setVisibility(View.VISIBLE);
				//mView2.setZOrderOnTop(true);
				//mView.getHolder().setFormat(PixelFormat.TRANSLUCENT);
				mFaceView = new FaceOverlayView(con);
				application.getActivity().addContentView(mFaceView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
				//application.getActivity().addContentView(mFaceView, initWindowParameter());

				handler = new Handler();
				faces = new FaceResult[MAX_FACE];
				faces_previous = new FaceResult[MAX_FACE];
				for (int i = 0; i < MAX_FACE; i++) {
					faces[i] = new FaceResult();
					faces_previous[i] = new FaceResult();
				}

				SurfaceHolder holder = mView.getHolder();
				//holder.setFormat(PixelFormat.TRANSPARENT);
				holder.addCallback(this);
				holder.setFormat(ImageFormat.NV21);

				if (application.getParamFromFile("suivi.isactive",
						"Configuration.properties", null).equals("1")) {

					application.setEnableSuivi(true);
					int size = Integer.parseInt(application.getParamFromFile("cadre.taille",
							"Configuration.properties", null).split(" ")[0]) * 10;
					px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_MM, size, getResources().getDisplayMetrics());
					application.setPx(px);
					mrectangleView = new RectangleView(con);
					application.getActivity().addContentView(mrectangleView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));


				}

			}

		}
		return view;
	}

	@Override
	public void onResume() {
		super.onResume();

			if((application.getBackgroundType().equals("camera") || suivi_with_fond==1) && application.isTelepresence()==false){
				stopThread=false;
				isThreadWorking= false;

				flagStart=true;

				if( application.getParamFromFile("suivi.isactive",
						"Configuration.properties", null).equals("1")){

					int size = Integer.parseInt(application.getParamFromFile("cadre.taille",
							"Configuration.properties", null).split(" ")[0])*10;
					px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_MM, size,
							getResources().getDisplayMetrics());
					application.setPx(px);
					mrectangleView = new RectangleView(con);
					application.getActivity().addContentView(mrectangleView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));


				}

			}
		}


	@Override
	public void onPause() {

		super.onPause();
		stopThread=true;

	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		if (application.getBackgroundType().equals("video")) {
			//mediaplayer.stop();
			//mediaplayer.release();
			//backgroundVideoView.getHolder().removeCallback(this);
		}
		//if (application.getBackgroundType().equals("camera") && application.isTelepresence()==false) {
		if (application.isTelepresence()==false) {

			application.setStartSuivi(false);
			application.setIdSuivi(-1);
			application.notifyObservers("drawerOuvert");
			flagStart=false;
			if(mFaceView!=null)
				mFaceView.setFaces(null);

		}

		stopThread=true;
		GestionDesFichiers
				.log("INFO",
						"Le fragment de caméra est supprimé",
						"Log_SmartRobot");
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

	}

	private void changeSurfaceVideo() {
		// TODO Auto-generated method stub

		if (application.getBackgroundType().equals("video")) {

			playBackgroundVideo(fileName);

		} else if (application.getBackgroundType().equals("img")) {
			showImageBackground();
		} else if (application.getBackgroundType().equals("gif")) {
			showGifBackground();

		}


	}

	@SuppressLint("NewApi")
	public void showImageBackground() {
		// TODO Auto-generated method stub
		final String pathName = Environment.getExternalStorageDirectory()
				.getAbsolutePath() + "/SmartRobot/" + fileName;


		application.getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				backgroundVideoView.setVisibility(View.VISIBLE);
				//backgroundVideoView.setZOrderOnTop(true);
				Resources res = getResources();
				Bitmap bitmap = BitmapFactory.decodeFile(pathName);
				BitmapDrawable background = new BitmapDrawable(res, bitmap);
				if (backgroundVideoView != null) {
					backgroundVideoView.setBackgroundDrawable(background);
				}
			}
		});

	}

	public void showGifBackground() {
		// TODO Auto-generated method stub
		final String pathName = Environment.getExternalStorageDirectory()
				.getAbsolutePath() + "/SmartRobot/" + fileName;

		application.getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				Movie movie = Movie.decodeFile(pathName);
				if (backgroundVideoView != null)
					backgroundVideoView.setVisibility(View.GONE);
				if (backGif != null) {
					backGif.setImageResourceFromFile(movie, screenWidth,
							screenHight);
					backGif.setVisibility(View.VISIBLE);
					//backGif.setZOrderOnTop(true);

				}
			}
		});

	}

	public void hideGifBackground() {
		application.getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if (backGif != null)
					backGif.setVisibility(View.GONE);
			}
		});

	}

	public void hideImageBackground() {
		application.getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if (backgroundVideoView != null)
					backgroundVideoView.setVisibility(View.GONE);
			}
		});

	}

	public void resumeAutoPlayVideo() {
		// TODO Auto-generated method stub

		playBackgroundVideo(fileName);
	}

	public void playBackgroundVideo(String fileName) {
		// TODO Auto-generated method stub
		String video = Environment.getExternalStorageDirectory()
				.getAbsolutePath() + "/SmartRobot/" + fileName;
		try {
			mediaplayer.setDataSource(video);
			mediaplayer.setLooping(true);

			mediaplayer.prepare();

			mediaplayer.start();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@SuppressLint("NewApi")
	public void cancelAutoPlayVideo() {
		// TODO Auto-generated method stub

		mediaplayer.pause();

		application.getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if (backgroundVideoView != null)
					backgroundVideoView.setVisibility(View.GONE);
			}
		});
	}

	public void hideControlBackground() {
		String fileName = SmartRobotApplication.getParamFromFile(
				"control.fond", "Configuration.properties", null);

		if (application.fileExist(fileName)) {
			if (application.getBackgroundType().equals("video")) {
				cancelAutoPlayVideo();
			} else if (application.getBackgroundType().equals("gif")) {
				hideGifBackground();
			} else if (application.getBackgroundType().equals("img")) {
				hideImageBackground();
			}
		}
	}

	public void showControlBackground() {
		if (application.fileExist(fileName)) {
			if (application.getBackgroundType().equals("video")) {
				resumeAutoPlayVideo();
			} else if (application.getBackgroundType().equals("gif")) {
				showGifBackground();
			} else if (application.getBackgroundType().equals("img")) {
				showImageBackground();
			}
		}

	}

	@Override
	public void surfaceChanged(SurfaceHolder surfaceHolder, int format, int width, int height) {
		// TODO Auto-generated method stub
		if (application.getBackgroundType().equals("camera") || suivi_with_fond==1){
			if (surfaceHolder.getSurface() == null) {
				return;
			}
			// Try to stop the current preview:
			try {
				mCamera.stopPreview();
			} catch (Exception e) {
				// Ignore...
			}

			configureCamera(width, height);
			setDisplayOrientation();
			setErrorCallback();

			// Create media.FaceDetector
			float aspect = (float) previewHeight / (float) previewWidth;
			fdet = new android.media.FaceDetector(prevSettingWidth, (int) (prevSettingWidth * aspect), MAX_FACE);


			// Everything is configured! Finally start the camera preview again:
			startPreview();







		}

	}

	private void setErrorCallback() {
		mCamera.setErrorCallback(mErrorCallback);
	}

	private void setDisplayOrientation() {
		// Now set the display orientation:
		mDisplayRotation = Util.getDisplayRotation(application.getActivity());
		mDisplayOrientation = Util.getDisplayOrientation(mDisplayRotation, cameraId);

		mCamera.setDisplayOrientation(mDisplayOrientation);

		if (mFaceView != null) {
			mFaceView.setDisplayOrientation(mDisplayOrientation);
		}
	}

	private void configureCamera(int width, int height) {
		Camera.Parameters parameters = mCamera.getParameters();
		// Set the PreviewSize and AutoFocus:
		setOptimalPreviewSize(parameters, width, height);
		setAutoFocus(parameters);
		// And set the parameters:
		mCamera.setParameters(parameters);
	}

	private void setAutoFocus(Camera.Parameters cameraParameters) {
		List<String> focusModes = cameraParameters.getSupportedFocusModes();
		if (focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE))
			cameraParameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
	}

	private void startPreview() {
		if (mCamera != null) {
			isThreadWorking = false;
			mCamera.startPreview();
			mCamera.setPreviewCallback(this);
			counter = 0;
		}
	}

	private void setOptimalPreviewSize(Camera.Parameters cameraParameters, int width, int height) {
		List<Camera.Size> previewSizes = cameraParameters.getSupportedPreviewSizes();
		float targetRatio = (float) width / height;
		Camera.Size previewSize = Util.getOptimalPreviewSize(application.getActivity(), previewSizes, targetRatio);
		previewWidth = previewSize.width;
		previewHeight = previewSize.height;

		Log.e(TAG, "previewWidth" + previewWidth);
		Log.e(TAG, "previewHeight" + previewHeight);

		/**
		 * Calculate size to scale full frame bitmap to smaller bitmap
		 * Detect face in scaled bitmap have high performance than full bitmap.
		 * The smaller image size -> detect faster, but distance to detect face shorter,
		 * so calculate the size follow your purpose
		 */
		if (previewWidth / 4 > 360) {
			prevSettingWidth = 360;
			prevSettingHeight = 270;
		} else if (previewWidth / 4 > 320) {
			prevSettingWidth = 320;
			prevSettingHeight = 240;
		} else if (previewWidth / 4 > 240) {
			prevSettingWidth = 240;
			prevSettingHeight = 160;
		} else {
			prevSettingWidth = 160;
			prevSettingHeight = 120;
		}

		cameraParameters.setPreviewSize(previewSize.width, previewSize.height);

		mFaceView.setPreviewWidth(previewWidth);
		mFaceView.setPreviewHeight(previewHeight);
	}

	@Override
	public void surfaceCreated(SurfaceHolder arg0) {
		// TODO Auto-generated method stub

		if (application.getBackgroundType().equals("camera") || suivi_with_fond==1){

			//Find the total number of cameras available
			numberOfCameras = Camera.getNumberOfCameras();
			Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
			for (int i = 0; i < Camera.getNumberOfCameras(); i++) {
				Camera.getCameraInfo(i, cameraInfo);
				if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
					if (cameraId == 0) cameraId = i;
				}
			}
			String idCam = application.getCamId();
			if(idCam!=null) {
				if (!idCam.equals(""))
					cameraId = Integer.parseInt(idCam);
			}
			mCamera = Camera.open(cameraId);

			application.setCameraObject(mCamera);

			Camera.getCameraInfo(cameraId, cameraInfo);
			if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
				mFaceView.setFront(true);
			}

			try {
				mCamera.setPreviewDisplay(mView.getHolder());
			} catch (Exception e) {
				Log.e(TAG, "Could not preview the image.", e);
			}
		}
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder arg0) {
		// TODO Auto-generated method stub
		application.removeObserver(this);

		if (application.getBackgroundType().equals("camera") || suivi_with_fond==1 ) {
			flagStart=false;
			if(mrectangleView!=null)
				mrectangleView.setInvisible();
			if(mFaceView!=null)
				mFaceView.setFaces(null);
			mCamera.setPreviewCallbackWithBuffer(null);
			mCamera.setErrorCallback(null);
			mCamera.release();
			mCamera = null;
		}
	}

	@Override
	public void onPreviewFrame(byte[] data, Camera camera) {

		//if(flagStart==true) {

		Start(data, camera);

//	}

	}

	public void Start(byte[] data, Camera camera){
		if (!isThreadWorking) {
			if (counter == 0)
				start = System.currentTimeMillis();

			isThreadWorking = true;
			waitForFdetThreadComplete();
			detectThread = new FaceDetectThread(handler, con);
			detectThread.setData(data);
			detectThread.start();
		}
	}

	private void waitForFdetThreadComplete() {
		if (detectThread == null) {
			return;
		}

		if (detectThread.isAlive()) {
			try {
				detectThread.join();
				detectThread = null;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}


	// fps detect face (not FPS of camera)
	long start, end;
	int counter = 0;
	double fps;

	@Override
	public void update(String message) throws IOException {
		if(message!=null){
			if(message.equals("drawerFermer") ){
				if(application.isTelepresence()==false) {
					flagStart = true;
				}
			}else if(message.equals("drawerOuvert")  ){
				if(application.isTelepresence()==false) {
					flagStart = false;
					if (mFaceView != null)
						mFaceView.setFaces(null);
				}

			}
		}

	}

	/**
	 * option Zoom
	 */

	public void zoom(int currentZoomLevel)
	{
		Camera.Parameters params=mCamera.getParameters();
		//params.setZoom(params.getMaxZoom());
		params.setZoom(currentZoomLevel);
		// params.getMaxZoom();
		mCamera.setParameters(params);
	}

	public void unzoom()
	{
		Camera.Parameters params=mCamera.getParameters();
		params.setZoom(0);
		mCamera.setParameters(params);
	}

	/**
	 * Do face detect in thread
	 */
	private class FaceDetectThread extends Thread {
		private Handler handler;
		private byte[] data = null;
		private Context ctx;
		private Bitmap faceCroped;

		public FaceDetectThread(Handler handler, Context ctx) {
			this.ctx = ctx;
			this.handler = handler;
		}




		public void setData(byte[] data) {
			this.data = data;
		}

		public void run() {
			//startTime = System.nanoTime();

			/** ajouté par YBE **/
			if (!stopThread) {
				//application.setIncrementZoom(currentZoomLevel);
				if (application.getFlagSuivi()) {
					unzoom_time = 0;
				} else {
					unzoom_time++;
					if (unzoom_time == time_unzoom * 2) {
						currentZoomLevel = 0;
						unzoom();
						application.setIncrementZoom(currentZoomLevel);

					}
				}

				int H = application.getHeightScreen();
				// = application.getZommInd();
				boolean test_zoom = application.getZommTest();
				int h_condi = (int) application.getHeight();
				if (test_zoom && application.getFlagSuivi() && zoomIsActive == 1) {

					if (h_condi < H / 2 && zoomIsActive == 1 && currentZoomLevel < 99) {

						currentZoomLevel = currentZoomLevel + 2;

						zoom(currentZoomLevel);
						application.setIncrementZoom(currentZoomLevel);
					}

				}


				/** FIN **/

//            Log.i("FaceDetectThread", "running");

				float aspect = (float) previewHeight / (float) previewWidth;
				int w = prevSettingWidth;
				int h = (int) (prevSettingWidth * aspect);

				Bitmap bitmap = Bitmap.createBitmap(previewWidth, previewHeight, Bitmap.Config.RGB_565);
				// face detection: first convert the image from NV21 to RGB_565
				YuvImage yuv = new YuvImage(data, ImageFormat.NV21,
						bitmap.getWidth(), bitmap.getHeight(), null);
				// TODO: make rect a member and use it for width and height values above
				Rect rectImage = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

				// TODO: use a threaded option or a circular buffer for converting streams?
				//see http://ostermiller.org/convert_java_outputstream_inputstream.html
				ByteArrayOutputStream baout = new ByteArrayOutputStream();
				if (!yuv.compressToJpeg(rectImage, 100, baout)) {
					Log.e("CreateBitmap", "compressToJpeg failed");
				}

				BitmapFactory.Options bfo = new BitmapFactory.Options();
				bfo.inPreferredConfig = Bitmap.Config.RGB_565;
				bitmap = BitmapFactory.decodeStream(
						new ByteArrayInputStream(baout.toByteArray()), null, bfo);

				Bitmap bmp = Bitmap.createScaledBitmap(bitmap, w, h, false);


				float xScale = (float) previewWidth / (float) prevSettingWidth;
				float yScale = (float) previewHeight / (float) h;

				Camera.CameraInfo info = new Camera.CameraInfo();
				Camera.getCameraInfo(cameraId, info);

				int rotate = mDisplayOrientation;
				if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT && mDisplayRotation % 180 == 0) {
					if (rotate + 180 > 360) {
						rotate = rotate - 180;
					} else
						rotate = rotate + 180;
				}

				switch (rotate) {
					case 90:
						bmp = ImageUtils.rotate(bmp, 90);
						xScale = (float) previewHeight / bmp.getWidth();
						yScale = (float) previewWidth / bmp.getHeight();
						break;
					case 180:
						bmp = ImageUtils.rotate(bmp, 180);
						break;
					case 270:
						bmp = ImageUtils.rotate(bmp, 270);
						xScale = (float) previewHeight / (float) h;
						yScale = (float) previewWidth / (float) prevSettingWidth;
						break;
				}

				if (silo_det == 1) {
					resultsPD = mPersonDet.detect(bmp);
					bmapW = bmp.getWidth();
					bmapH = bmp.getHeight();
					mFaceView.setPedist(resultsPD, bmapW, bmapH);
					//application.setBmpSize;
				}


				fdet = new android.media.FaceDetector(bmp.getWidth(), bmp.getHeight(), MAX_FACE);

				android.media.FaceDetector.Face[] fullResults = new android.media.FaceDetector.Face[MAX_FACE];
				fdet.findFaces(bmp, fullResults);


				for (int i = 0; i < MAX_FACE; i++) {
					if (fullResults[i] == null) {
						faces[i].clear();
					} else {
						PointF mid = new PointF();
						fullResults[i].getMidPoint(mid);

						mid.x *= xScale;
						mid.y *= yScale;

						float eyesDis = fullResults[i].eyesDistance() * xScale;
						float confidence = fullResults[i].confidence();
						float pose = fullResults[i].pose(android.media.FaceDetector.Face.EULER_Y);
						int idFace = Id;

						Rect rect = new Rect(
								(int) (mid.x - eyesDis * 1.20f),
								(int) (mid.y - eyesDis * 0.55f),
								(int) (mid.x + eyesDis * 1.20f),
								(int) (mid.y + eyesDis * 1.85f));

						/**
						 * Only detect face size > 100x100
						 */

						if (rect.height() * rect.width() > 100 * 100) {
							for (int j = 0; j < MAX_FACE; j++) {
								float eyesDisPre = faces_previous[j].eyesDistance();
								PointF midPre = new PointF();
								faces_previous[j].getMidPoint(midPre);

								RectF rectCheck = new RectF(
										(midPre.x - eyesDisPre * 1.5f),
										(midPre.y - eyesDisPre * 1.15f),
										(midPre.x + eyesDisPre * 1.5f),
										(midPre.y + eyesDisPre * 1.85f));

								if (rectCheck.contains(mid.x, mid.y) && (System.currentTimeMillis() - faces_previous[j].getTime()) < 1000) {
									idFace = faces_previous[j].getId();
									break;
								}
							}

							if (idFace == Id) Id++;

							faces[i].setFace(idFace, mid, eyesDis, confidence, pose, System.currentTimeMillis());


							faces_previous[i].set(faces[i].getId(), faces[i].getMidEye(), faces[i].eyesDistance(), faces[i].getConfidence(), faces[i].getPose(), faces[i].getTime());

							//
							// if focus in a face 5 frame -> take picture face display in RecyclerView
							// because of some first frame have low quality
							//
							if (facesCount.get(idFace) == null) {
								facesCount.put(idFace, 0);
							} else {
								int count = facesCount.get(idFace) + 1;
								if (count <= 5)
									facesCount.put(idFace, count);

								//
								// Crop Face to display in RecylerView
								//
								if (count == 5) {
									faceCroped = ImageUtils.cropFace(faces[i], bitmap, rotate);
									if (faceCroped != null) {
										handler.post(new Runnable() {
											public void run() {

											}
										});
									}
								}
							}
						}

					}
				}

				handler.post(new Runnable() {
					public void run() {
						//send face to FaceView to draw rect
						mFaceView.setFaces(faces);
						//mFaceView.setPedist(resultsPD, bmapW,bmapH);
						//mFaceView.setPedist(resultsPD, bmapW,bmapH);
						//calculate FPS
						end = System.currentTimeMillis();
						counter++;
						double time = (double) (end - start) / 1000;
						if (time != 0)
							fps = counter / time;

						mFaceView.setFPS(fps);

						if (counter == (Integer.MAX_VALUE - 1000))
							counter = 0;

						isThreadWorking = false;


					}
				});

				duration = System.nanoTime(); //divide by 1000000 to get milliseconds.
				startTime = duration;
			}
		}
	}




}
