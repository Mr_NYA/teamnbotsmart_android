package com.orrobotics.linphone;

import java.io.File;
import java.io.IOException;

import com.orrobotics.orsmartrobot.R;
import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.observer.OrSmarRobotObserver;
import com.orrobotics.orsmartrobot.ui.PlayGifView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Movie;
import android.graphics.drawable.BitmapDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.VideoView;

public class VideoSnapFragment extends Fragment implements OrSmarRobotObserver {

	public VideoView backgroundVideoView;
	public ImageView imageBack;
	private static MediaPlayer mp = null;

	private Context context;
	static SmartRobotApplication application;
	String fileName = "";
	PlayGifView backGif;

	int screenWidth;
	int screenHight;
	Boolean isGif = false;
	Boolean isImage = false;
	Boolean isAudio = false;
	Boolean isVideo = false;
	Boolean isbackResumed = false;
	int length = 0;
	int tempo = 0;
	Handler mHandler = new Handler();

	public VideoSnapFragment() {
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
	}

	@SuppressLint("ValidFragment")
	public VideoSnapFragment(Context context) {
		this.context = context;
		application = (SmartRobotApplication) context;
	}

	@SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
	// Warning useless because value is ignored and automatically set by new
	// APIs.
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.video_snap, container, false);

		application.registerObserver(this);
		screenWidth = application.getActivity().getWindowManager()
				.getDefaultDisplay().getWidth();
		screenHight = application.getActivity().getWindowManager()
				.getDefaultDisplay().getHeight();
		mp = new MediaPlayer();
		backGif = (PlayGifView) view.findViewById(R.id.backGifSnap);
		backgroundVideoView = (VideoView) view
				.findViewById(R.id.backgroundVideoSurfaceSnap);
		imageBack = (ImageView) view.findViewById(R.id.image);

		return view;
	}

	@Override
	public void onResume() {
		super.onResume();

		if (context == null)
			context = application.getApplicationContext();

	}

	@Override
	public void onPause() {

		super.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		mp.release();
		application.removeObserver(this);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

	}

	@SuppressLint("NewApi")
	public void showImageBackground(String fileName) {
		// TODO Auto-generated method stub
		isImage = true;
		isbackResumed = false;
		backgroundVideoView.setVisibility(View.GONE);
		backGif.setVisibility(View.GONE);
		imageBack.setVisibility(View.VISIBLE);
		String pathName = Environment.getExternalStorageDirectory()
				.getAbsolutePath()
				+ "/SmartRobot/SmartTeambotScenarios/"
				+ fileName;
		Resources res = getResources();
		Bitmap bitmap = BitmapFactory.decodeFile(pathName);
		BitmapDrawable background = new BitmapDrawable(res, bitmap);
		imageBack.setBackground(background);
		mHandler.postDelayed(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				imageBack.setVisibility(View.GONE);
				isImage = false;
			}
		}, tempo);

	}

	public void showGifBackground(String fileName) {
		// TODO Auto-generated method stub
		isGif = true;
		isbackResumed = false;
		String pathName = Environment.getExternalStorageDirectory()
				.getAbsolutePath()
				+ "/SmartRobot/SmartTeambotScenarios/"
				+ fileName;
		backgroundVideoView.setVisibility(View.GONE);
		imageBack.setVisibility(View.GONE);
		Movie movie = Movie.decodeFile(pathName);
		backGif.setVisibility(View.VISIBLE);

		backGif.setImageResourceFromFile(movie, screenWidth, screenHight);

		mHandler.postDelayed(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				backGif.setVisibility(View.GONE);
				isGif = false;
			}
		}, tempo);

	}

	public void audioPlayer(String fileName) {
		// set up MediaPlayer
		isAudio = true;
		isbackResumed = false;
		mp.setAudioStreamType(AudioManager.STREAM_DTMF);

		Uri video = Uri.parse(Environment.getExternalStorageDirectory()
				.getAbsolutePath()
				+ "/SmartRobot/SmartTeambotScenarios/"
				+ fileName);

		try {
			mp.setDataSource(context, video);
			mp.prepare();
			mp.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
		mHandler.postDelayed(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				if (mp != null) {
					if (mp.isPlaying()) {
						mp.stop();
					}
				}
				isAudio = false;
			}
		}, tempo);
	}

	public void playBackgroundVideo(String fileName) {
		// TODO Auto-generated method stub
		isVideo = true;
		isbackResumed = false;

		Uri video = Uri.parse(Environment.getExternalStorageDirectory()
				.getAbsolutePath()
				+ "/SmartRobot/SmartTeambotScenarios/"
				+ fileName);
		backgroundVideoView.setVisibility(View.VISIBLE);
		backgroundVideoView.setVideoURI(video);
		backgroundVideoView.setOnPreparedListener(new OnPreparedListener() {

			@Override
			public void onPrepared(MediaPlayer mediaPlayer) {
				//mediaPlayer.setAudioStreamType(AudioManager.STREAM_DTMF);
				backgroundVideoView.start();

			}

		});
		mHandler.postDelayed(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				backgroundVideoView.stopPlayback();
				backgroundVideoView.setVisibility(View.GONE);
				isVideo = false;

			}
		}, tempo);

	}

	@SuppressLint("NewApi")
	public void cancelAutoPlayVideo() {
		// TODO Auto-generated method stub
		length = backgroundVideoView.getCurrentPosition();
		backgroundVideoView.pause();
		application.getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if (backgroundVideoView != null)
					backgroundVideoView.setVisibility(View.GONE);
			}
		});
	}

	public void cancelAutoPlayAudio() {
		// TODO Auto-generated method stub
		if (mp.isPlaying()) {
			length = mp.getCurrentPosition();
			mp.pause();

		}
	}

	public void resumeAutoPlayAudio() {
		// TODO Auto-generated method stub
		if (!mp.isPlaying()) {
			mp.seekTo(length);
			mp.start();
		}

		mHandler.postDelayed(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				if (mp.isPlaying()) {
					mp.stop();
					isAudio = false;
					isbackResumed = true;

				}
			}
		}, tempo - length);

	}

	public void resumeAutoPlayVideo() {
		// TODO Auto-generated method stub
		application.getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				backgroundVideoView.setVisibility(View.VISIBLE);
			}
		});

		backgroundVideoView.seekTo(length);
		backgroundVideoView.resume();

		mHandler.postDelayed(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				backgroundVideoView.stopPlayback();
				backgroundVideoView.setVisibility(View.GONE);
				isVideo = false;
				isbackResumed = true;

			}
		}, tempo - length);

	}

	public void openFileSnap(final String fileName) {
		this.fileName = fileName;
		if (fileName.endsWith("mp4")) {

			playBackgroundVideo(fileName);

		} else if (fileName.endsWith("mp3")) {
			audioPlayer(fileName);

		} else if (fileName.endsWith("gif")) {
			showGifBackground(fileName);
		} else
			showImageBackground(fileName);

	}

	public void hideBackgroundSnap() {
		if (fileName.endsWith("mp4")) {
			application.getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					cancelAutoPlayVideo();
					backgroundVideoView.setVisibility(View.GONE);
				}
			});

		} else if (fileName.endsWith("mp3")) {
			application.getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					cancelAutoPlayAudio();
				}
			});

		} else if (fileName.endsWith("gif")) {
			application.getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					backGif.setVisibility(View.GONE);
				}
			});

		} else {
			application.getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					imageBack.setVisibility(View.GONE);
				}
			});

		}

	}

	public void showBackgroundSnap() {
		if (fileName.endsWith("mp4")) {
			if (!isbackResumed)
				application.getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						resumeAutoPlayVideo();
						backgroundVideoView.setVisibility(View.VISIBLE);
					}
				});

		} else if (fileName.endsWith("mp3")) {
			if (!isbackResumed)
				application.getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						resumeAutoPlayAudio();
					}
				});

		} else if (fileName.endsWith("gif")) {
			if (!isbackResumed)
				application.getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						backGif.setVisibility(View.VISIBLE);
						mHandler.postDelayed(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub
								backGif.setVisibility(View.GONE);
								isGif = false;
								isbackResumed = true;

							}
						}, tempo - length);
					}
				});

		} else {
			if (!isbackResumed)
				application.getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						imageBack.setVisibility(View.VISIBLE);
						mHandler.postDelayed(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub
								imageBack.setVisibility(View.GONE);
								isImage = false;
								isbackResumed = true;

							}
						}, tempo - length);
					}
				});

		}

	}

	@Override
	public void update(String message) throws IOException {
		// TODO Auto-generated method stub
		if (message.contains("openSnapFile")) {
			tempo = Integer.parseInt(message.split(";")[2]);
			openFileSnap(message.split(";")[1]);

		}
		if (message.equals("launchControl")) {
			hideBackgroundSnap();
		}
		if (message.equals("terminateCall")) {
			showBackgroundSnap();
		}
		if (message.contains("visible;")) {
			try {
				if (mp.isPlaying()) {
					if (message.split(";")[1].equals("oui")) {
						if (isVideo == true) {

							backgroundVideoView.setVisibility(View.VISIBLE);
						}

						if (isImage == true) {
							imageBack.setVisibility(View.VISIBLE);

						}
						if (isGif == true) {
							backGif.setVisibility(View.VISIBLE);

						}
					} else {
						if (isVideo == true) {

							backgroundVideoView.setVisibility(View.INVISIBLE);
						}

						if (isImage == true) {
							imageBack.setVisibility(View.INVISIBLE);

						}
						if (isGif == true) {
							backGif.setVisibility(View.INVISIBLE);

						}
					}
				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}

	}

	public void removeObserver() {
		// TODO Auto-generated method stub
		application.removeObserver(this);

	}


}
