package com.orrobotics.orsmartrobot.start;

import java.io.File;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.util.GestionDesFichiers;
import com.orrobotics.orsmartrobot.util.RemoteControlUtil;
import com.orrobotics.orsmartrobot.view.Authentification;
import com.orrobotics.orsmartrobot.view.ControlRobot;
import com.orrobotics.orsmartrobot.view.ListRobot;
import com.orrobotics.webservice.WebService;
import com.orrobotics.webservice.json.JsonParser;
import com.orrobotics.webservice.url.urlParser;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

/**
 * @author Hajiba IFRAH La classe Start est la première classe appelée par
 *         l’application, le but de cette classe est de vérifier si
 *         l’utilisateur est déjà connecté à l’espace client si oui
 *         l’application liste les robots sinon l’application démarre
 *         l’interface de l’authentification
 * @version 2.1.0
 * @date 18/05/2015
 */

public class Start extends Activity {
	/**
	 * L'Email sauvegardé dans l'application
	 */
	String email_shared = null;
	Activity acti;

	SmartRobotApplication application;
	static File pathOfMyFile = Environment.getExternalStorageDirectory();
	String ws="";
	boolean hasRobot = false;
	String json = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		acti = this;

		/**
		 * Création de fichier de log pour OrSmartRobot
		 */
		File file = new File(pathOfMyFile, "SmartRobot");
		if (!file.exists()) {
			GestionDesFichiers.creerFichier("Log_SmartRobot");
		}

		/**
		 * Instanciation de l'application SmartRobot
		 */
		application = (SmartRobotApplication) this.getApplicationContext();

		application.stricMode();
		application.setActivity(acti);
		application.setAdresseOpenFire(SmartRobotApplication.getParamFromFile(
				"server", "configuration.properties", null));



	}

	@Override
	protected void onStart() {
		super.onStart();

		application.initInternParams();

		GestionDesFichiers.collectLogs(getApplicationContext());

		/**
		 * Récupération d'email enregistré dans l'application
		 *
		 */

		email_shared = application.getEmail();
		GestionDesFichiers.log("Email, start ", email_shared, "Log_SmartRobot");

		/**
		 * Vérifier si l'utisateur a déjà connecté dans l'espace client
		 *
		 */
		if (email_shared != null ) {
			if (email_shared.equals(""))
			{

				application.StartAct("picture", null, Authentification.class,
						acti);

			} else{
				String url = SmartRobotApplication.getParamFromFile(
						"param", "configuration.properties", null);

				String serverURL = urlParser.getListRobot(url,application.getEmail());
				GestionDesFichiers.log("URL de serveur", serverURL,
						"Log_SmartRobot");

				Log.i("mbu","---json--isConnectedToInternet()="+application.isConnectedToInternet());
				if (application.isConnectedToInternet()) {
					//json = WebService.connection(serverURL);

					WebService ws = new WebService();
					WebService.HttpRequest callbackservice = ws.new HttpRequest() {
						@Override
						public void receiveData(Object object) {
                          json = (String)object;
                          Log.i("mbu","---json="+json);
							Log.i("mbu","---application.getWsUrl()="+application.getWsUrl());
                            GestionDesFichiers.log("Json=", json,
                                    "Log_SmartRobot");
							if(application.getWsUrl().contains("ontomantics")){
								try {
									JSONObject jsonObj = new JSONObject(json);
									JSONArray array = jsonObj.getJSONArray("listRobot");

									json = array.toString();
								} catch (JSONException e) {
									e.printStackTrace();
								}

							}
							jsonTreatment();

						}
					};
					callbackservice.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, serverURL);


				} else {
					json = application.getJsonList();
                    jsonTreatment();
				}


			}
		} else
			application.StartAct("picture", null, Authentification.class, acti);

		finish();

	}

    public void jsonTreatment(){
		JSONArray jr= null;
        try {
			jr = new JSONArray(json);

        } catch (JSONException e) {
            e.printStackTrace();
            json = application.getJsonList();
			try {
				jr = new JSONArray(json);

			} catch (JSONException e1) {
				e.printStackTrace();
			}
        }

		try {
			if(jr.length() >= 1 && application.hasRobot(jr))
                hasRobot= true;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		if(!hasRobot) {
            application.clear();
            application.StartAct("picture", null, Authentification.class, acti);

        }
        else {
            String data = application.getWsUrl();
            ws = application.getParam(RemoteControlUtil.WEBSERVICE);
            if(ws==null){
                application.setEditor(RemoteControlUtil.WEBSERVICE,data);
                application.clear();
                application.StartAct("picture", null, Authentification.class, acti);
            }
            else if(ws.equals("")){
                application.setEditor(RemoteControlUtil.WEBSERVICE,data);
                application.clear();
                application.StartAct("picture", null, Authentification.class, acti);
            } else if(!ws.equals(data)){
                application.setEditor(RemoteControlUtil.WEBSERVICE,data);
                application.clear();
                application.StartAct("picture", null, Authentification.class, acti);
            }
            else {

                application.setData(application.getEmail(),
                        application.getPasswod(), application.getNom(),
                        application.getPrenom(),
                        application.getPseudoOpenFire(),
                        application.getPasswordOpenFire(),
                        application.getPseudoSIP(),
                        application.getPasswordSIP());

                if (application.getDeviceAddress().equals("")) {

                    application
                            .StartAct("picture", null, ListRobot.class, acti);
                } else {


                    if (json.contains(application.getDeviceAddress())) {

                        application.StartAct("picture", null,
                                ControlRobot.class, acti);
                        application.initRobotInternParams(application
                                .getRobotId());
                    } else {
                        application.StartAct("picture", null, ListRobot.class,
                                acti);
                    }
                }
            }

        }
    }
	public static void doRestart(Context c) {
		try {
			// check if the context is given
			if (c != null) {
				// fetch the packagemanager so we can get the default launch
				// activity
				// (you can replace this intent with any other activity if you
				// want
				PackageManager pm = c.getPackageManager();
				// check if we got the PackageManager
				if (pm != null) {
					// create the intent with the default start activity for
					// your application
					Intent mStartActivity = pm.getLaunchIntentForPackage(c
							.getPackageName());
					if (mStartActivity != null) {
						mStartActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						// create a pending intent so the application is
						// restarted after System.exit(0) was called.
						// We use an AlarmManager to call this intent in 100ms
						int mPendingIntentId = 223344;
						PendingIntent mPendingIntent = PendingIntent
								.getActivity(c, mPendingIntentId,
										mStartActivity,
										PendingIntent.FLAG_CANCEL_CURRENT);
						AlarmManager mgr = (AlarmManager) c
								.getSystemService(Context.ALARM_SERVICE);
						mgr.set(AlarmManager.RTC,
								System.currentTimeMillis() + 100,
								mPendingIntent);
						// kill the application
						System.exit(0);
					} else {
						Log.e("INFO",
								"Was not able to restart application, mStartActivity null");
					}
				} else {
					Log.e("INFO",
							"Was not able to restart application, PM null");
				}
			} else {
				Log.e("INFO",
						"Was not able to restart application, Context null");
			}
		} catch (Exception ex) {
			Log.e("INFO", "Was not able to restart application");
		}
	}
}