package com.orrobotics.orsmartrobot.util;

public class PandorabotObject {

	private String reponse;
	private String input;

	public PandorabotObject() {

	}

	public PandorabotObject(String reponse, String input) {
		super();
		this.reponse = reponse;
		this.input = input;
	}

	public String getReponse() {

		return reponse;
	}

	public void setReponse(String reponse) {
		this.reponse = reponse;
	}

	public String getInput() {
		return input;
	}

	public void setInput(String input) {
		this.input = input;
	}

}
