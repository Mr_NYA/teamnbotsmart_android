package com.orrobotics.orsmartrobot.util;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

import android.os.AsyncTask;

public class BandWithTest extends AsyncTask<Void, Integer, Boolean> {

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}

	@Override
	protected void onProgressUpdate(Integer... values) {
		super.onProgressUpdate(values);
		// Mise à jour de la ProgressBar
	}

	@Override
	protected Boolean doInBackground(Void... arg0) {

		try {
			int timeoutMs = 1500;
			Socket sock = new Socket();
			SocketAddress sockaddr = new InetSocketAddress("8.8.8.8", 53);

			sock.connect(sockaddr, timeoutMs);
			sock.close();

			return true;
		} catch (IOException e) {
			return false;
		}
	}

	@Override
	protected void onPostExecute(Boolean result) {

	}
}