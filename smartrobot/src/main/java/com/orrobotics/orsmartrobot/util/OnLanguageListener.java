package com.orrobotics.orsmartrobot.util;

public interface OnLanguageListener
{
    public void onLanguageDetailsReceived(LanguageChecker data);
}
