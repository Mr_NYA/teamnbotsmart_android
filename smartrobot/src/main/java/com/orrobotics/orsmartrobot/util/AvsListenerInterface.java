package com.orrobotics.orsmartrobot.util;

import com.willblaschko.android.alexa.callbacks.AsyncCallback;
import com.willblaschko.android.alexa.interfaces.AvsResponse;

public interface AvsListenerInterface{
    AsyncCallback<AvsResponse, Exception> getRequestCallback();
}
