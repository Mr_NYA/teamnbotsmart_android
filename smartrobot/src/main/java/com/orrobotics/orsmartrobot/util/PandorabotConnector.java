package com.orrobotics.orsmartrobot.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.concurrent.ExecutionException;

import org.json.JSONObject;

import com.orrobotics.orsmartrobot.application.SmartRobotApplication;

import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

public class PandorabotConnector {

	private static final String TAG = "PandorabotsLog";
	public static String baseUrl = SmartRobotApplication.getParamFromFile(
			"chatbot.url", "Configuration.properties", null);

	public static String botId;
	public static String custId = null;
	SmartRobotApplication model;
	String response = "";
	PandorabotObject obj = new PandorabotObject();
	String input1 = "";
	pandorabotRequest pandoraTask;

	public PandorabotConnector(String Bot_id, SmartRobotApplication model) {
		PandorabotConnector.botId = Bot_id;
		this.model = model;
	}

	@SuppressWarnings("deprecation")
	private URL requestUrl(String input) {
		try {
			if (baseUrl == null)
				baseUrl = "www.pandorabots.com";
			String spec = String.format("%s?botid=%s%s&input=%s&format=json",
					"https://" + baseUrl + "/pandora/talk-xml", botId,
					custId == null ? "" : "&custid=" + custId,
					URLEncoder.encode(input));
			Log.i(TAG, spec);
			return new URL(spec);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	private String readResponse(BufferedReader reader) {
		StringBuilder sb = new StringBuilder();
		String NL = System.getProperty("line.separator");
		try {
			for (;;) {
				String line = reader.readLine();
				if (line == null) {
					break;
				}
				sb.append(line).append(NL);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return sb.toString();
	}

	public String doServerRequest(String input) {

		if (!model.isConnectedToInternet()) {
			response = "Network unreachable";
			obj.setReponse(response);
		} else {
			try {
				pandoraTask = new pandorabotRequest();
				response = pandoraTask.executeOnExecutor(
						AsyncTask.THREAD_POOL_EXECUTOR, input).get();
			} catch (InterruptedException | ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				response = "Network unreachable";
				obj.setReponse(response);
			}
		}
		return response;
	}

	public void cancelServerRequest() {
		if (pandoraTask != null)
			if (!pandoraTask.isCancelled())
				pandoraTask.cancel(true);
	}

	private class pandorabotRequest extends AsyncTask<String, Integer, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
			// Mise à jour de la ProgressBar
		}

		@Override
		protected String doInBackground(String... input) {
			String serverResponse = "";
			if (!isCancelled()) {

				try {

					HttpURLConnection conn = (HttpURLConnection) requestUrl(
							input[0]).openConnection();
					conn.setRequestMethod("POST");
					conn.setDoOutput(true);
					conn.setConnectTimeout(5000);
					conn.setReadTimeout(30000);
					String userAgent = System.getProperty("http.agent");
					Log.i(TAG, "HTTPUrlConnection user-agent=" + userAgent);
					conn.connect();

					int status = conn.getResponseCode();

					if (status != HttpURLConnection.HTTP_OK) {
						Log.e("",
								"Error HttpURLConnection= "
										+ conn.getErrorStream());
						obj.setReponse("NoResponseServer");

					} else {
						BufferedReader reader = new BufferedReader(
								new InputStreamReader(conn.getInputStream()));
						String responseContent = readResponse(reader);
						Log.v(TAG, "responseContent=" + responseContent);
						try {
							JSONObject jsonObj = new JSONObject(responseContent);
							if (jsonObj.has("that")) {
								response = jsonObj.getString("that");

							} else if (jsonObj.has("message")) {
								response = jsonObj.getString("message");
							}
							obj.setReponse(response);
							custId = jsonObj.getString("custid");
							input1 = jsonObj.getString("input");
							obj.setInput(input[0]);


						} catch (Exception ex) {
							Log.e(TAG, "Error --------");
							ex.printStackTrace();
						}
					}
					conn.disconnect();
					serverResponse = obj.getReponse();

				} catch (Exception ex) {
					Log.e(TAG, "Error connect--------");

					obj.setReponse("NoResponseServer");
					serverResponse = "NoResponseServer";
					ex.printStackTrace();
				}
			}
			return serverResponse;
		}

		@Override
		protected void onPostExecute(String result) {

		}
	}

}
