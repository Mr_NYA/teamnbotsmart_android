package com.orrobotics.orsmartrobot.util;

import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import android.content.Context;
import android.database.ContentObserver;
import android.media.AudioManager;
import android.os.Handler;

public class SettingsContentObserver extends ContentObserver {
	int previousVolume;
	Context context;
	SmartRobotApplication application;

	public SettingsContentObserver(Context c, Handler handler) {
		super(handler);
		context = c;
		AudioManager audio = (AudioManager) context
				.getSystemService(Context.AUDIO_SERVICE);
		previousVolume = audio.getStreamVolume(AudioManager.STREAM_MUSIC);
		application = (SmartRobotApplication) context;

	}

	@Override
	public boolean deliverSelfNotifications() {
		return super.deliverSelfNotifications();
	}

	@Override
	public void onChange(boolean selfChange) {
		super.onChange(selfChange);

		AudioManager audio = (AudioManager) context
				.getSystemService(Context.AUDIO_SERVICE);
		int currentVolume = audio.getStreamVolume(AudioManager.STREAM_MUSIC);
		if(currentVolume!=0)
			application.setEditor(RemoteControlUtil.VOLUME, Integer.toString(currentVolume));

	}

}
