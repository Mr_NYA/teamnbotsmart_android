package com.orrobotics.orsmartrobot.util;

import java.util.Locale;

import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.service.ServiceRobot;

import android.content.Context;
import android.database.ContentObserver;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;

public class TTS extends ContentObserver implements OnInitListener {
	Context context;
	private TextToSpeech myTTS;
	Locale locale;
	String engine;
	SmartRobotApplication application;

	public TTS(Context c, Handler handler) {
		super(handler);
		context = c;
		application = (SmartRobotApplication) c;
	}

	@Override
	public boolean deliverSelfNotifications() {
		return super.deliverSelfNotifications();
	}

	@Override
	public void onChange(boolean selfChange) {
		super.onChange(selfChange);
		myTTS = new TextToSpeech(context, this);
	}

	@Override
	public void onInit(int initStatus) {

		if (initStatus == TextToSpeech.SUCCESS) {
			engine = myTTS.getDefaultEngine();

			locale = myTTS.getLanguage();

			if (locale != null)

			{

				String langue = locale.getDisplayLanguage();
				application.notifyObservers("texttospeech;" + langue);
				if (application.getChatEnabled() == true
						&& application.getFlagServer() == true)
					ServiceRobot.sendMessage(
							application.getPseudoOpenFireSender(), "tts"
									+ langue);

			}
		} else if (initStatus == TextToSpeech.ERROR) {

		}

	}

}
