package com.orrobotics.orsmartrobot.util;

import com.orrobotics.orsmartrobot.R;
import com.orrobotics.orsmartrobot.application.SmartRobotApplication;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;

/**
 *
 * @author Hajiba IFRAH la classe SkypeUtilises liste l'ensemble des méthodes
 *         qui permet à l'application de téléControl d'intéragir avec Skype
 * @version 2.1.0
 * @date 18/05/2015
 *
 */

public class SkypeUtilities {

	/**
	 *
	 * Cette méthode permet de charger la page de téléchargement de skype dans
	 * play store, "appstore "
	 *
	 * @param context
	 *            est le contexte de l'activité ou la méthode est appelé
	 *
	 */

	public static void goToMarket(Context context, String lien) {
		Uri marketUri = Uri.parse(lien);
		Intent myIntent = new Intent(Intent.ACTION_VIEW, marketUri);
		myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(myIntent);

		return;
	}

	/**
	 *
	 * Cette méthode sert à appeler un compte skype
	 *
	 * @param context
	 *            est le contexte de l'activité
	 * @param user_name
	 *            : le pseudo skype
	 *
	 */

	public static void initier_appel_Skype(Context context, String user_name) {
		Intent skype = new Intent("android.intent.action.VIEW");

		skype.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		skype.setData(Uri.parse("skype:" + user_name + "?call&video=true"));

		context.startActivity(skype);

	}

	/**
	 *
	 * Cette méthode permet de récupérer le pseudo Skype configuré dans
	 * l'appareil
	 *
	 * @param context
	 *            est le contexte de l'activité
	 * @return le pseudo skype
	 *
	 */

	public static Account[] PseudoSkype(Context context) {
		return AccountManager.get(context).getAccountsByType(
				"com.skype.contacts.sync");

	}

	/**
	 *
	 * @author Hajiba IFRAH Cette méthode set à afficher un popup avec deux
	 *         boutons l'un permet d'installer l'application skype et l'autre
	 *         permet annuler le lancement de l'application de télécontrol
	 * @param activity
	 *            : de type Activity
	 *
	 */

	public static void downloadSkype(final Activity activity) {

		activity.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				AlertDialog.Builder builder = new AlertDialog.Builder(activity);
				builder.setMessage(R.string.skypeInstall)
						.setPositiveButton(R.string.skypeToInstall,
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
														int id) {

										SkypeUtilities
												.goToMarket(activity,
														"market://details?id=com.skype.raider");

									}
								})
						.setNegativeButton(R.string.cancel,
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
														int id) {
									}
								});

				builder.create();
				builder.show();
			}
		});

	}

	/**
	 *
	 * @author Hajiba IFRAH Cette méthode set à afficher un popup avec deux
	 *         boutons l'un permet de lancer la page de connexion de
	 *         l'application skype et l'autre permet annuler le lancement de
	 *         l'application de télécontrol
	 * @param activity
	 *            : de type Activity
	 *
	 */
	public static void login_to_Skype(final Activity activity, Context con) {

		final SmartRobotApplication application = (SmartRobotApplication) con;

		AlertDialog.Builder builder = new AlertDialog.Builder(activity);

		builder.setMessage("Connnexion à Skype")
				.setPositiveButton("Se connecter Skype",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {

								application.lancer_app("com.skype.raider");

							}
						})
				.setNegativeButton(R.string.cancel,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {
							}
						});

		builder.create();
		builder.show();

	}

}
