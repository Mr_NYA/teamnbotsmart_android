package com.orrobotics.orsmartrobot.util;

import java.util.ArrayList;

public class ResultVocalObject {

	String action;
	String response;
	String date;
	ArrayList<ParamVocalObject> paramsList;
	ArrayList<MessageVocalObject> messagesList;

	public ResultVocalObject() {

	}

	public ResultVocalObject(String action, String response, String date) {
		this.action = action;
		this.response = response;
		this.date = date;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public ArrayList<ParamVocalObject> getParamsList() {
		return paramsList;
	}

	public void setParamsList(ArrayList<ParamVocalObject> paramsList) {
		this.paramsList = paramsList;
	}

	public ArrayList<MessageVocalObject> getMessagesList() {
		return messagesList;
	}

	public void setMessagesList(ArrayList<MessageVocalObject> messagesList) {
		this.messagesList = messagesList;
	}

}
