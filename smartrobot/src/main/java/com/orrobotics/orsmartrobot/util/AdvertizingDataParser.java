package com.orrobotics.orsmartrobot.util;

import android.util.Log;


public class AdvertizingDataParser {

	static int manufacturer;

	public static byte [] getBleManufacturerData(byte data[]) {
		int pos=0;
		byte [] manData = null;

		int dlen = data.length;
		while((pos+1) < dlen) {
			int bpos = pos;
			int blen = ((int)data[pos]) & 0xFF;
			if( blen == 0 )
				break;
			if( bpos+blen > dlen )
				break;
			++pos;
			int type = ((int)data[pos]) & 0xFF;
			++pos;
			int len = blen - 1;
			switch( type ) {
// Flags
			case 0x01:
				break;

// 16-bit UUIDs
			case 0x02:
			case 0x03:
			case 0x14:
				break;
// 32-bit UUIDs
			case 0x04:
			case 0x05:
				break;

// 128-bit UUIDs
			case 0x06:
			case 0x07:
			case 0x15:
				break;

// Local name (short and long)
			case 0x08:
			case 0x09:
				break;

// TX Power Level
			case 0x0A:
				break;
				
// Various not interpreted indicators (byte dump)
			case 0x0D:
			case 0x0E:
			case 0x0F:
			case 0x10:
				break;

// OOB Flags
			case 0x11:
				break;

// Slave Connection Interval Range
			case 0x12:
				break;

// Service data
			case 0x16:
				break;
// Service data, 32 bit UUID
			case 0x20:
				break;

// Service data, 128 bit UUID
			case 0x21:
				break;

// Public Target Address, Random Target Address
			case 0x17:
			case 0x18:
				break;

// Appearance
			case 0x19:
				break;

// Advertising interval
			case 0x1A:
				break;

// LE Bluetooth Device Address
			case 0x1B:
				break;

			case 0x1C:
				break;

			case 0xFF:
				Log.v("mbu", "--len = "+len);
				Log.v("mbu", "--pos = "+pos);

				int ptr = pos;

				int v = ((int)data[ptr]) & 0xFF;
				ptr++;
				v |= (((int)data[ptr]) & 0xFF ) << 8;
				ptr++;
				manufacturer = v;
				int length = len - 2;

				byte[]b1 = new byte[length];
				System.arraycopy(data, pos, b1, 0, length);
				manData = b1;
				break;
				
			default:
				break;
			}
			pos = bpos + blen+1;
		}
		return manData;
	}

}
