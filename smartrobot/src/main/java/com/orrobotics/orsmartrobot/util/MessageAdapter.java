package com.orrobotics.orsmartrobot.util;
/**
 * @author Hatim CHERKAOUI
 *
 */
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.orrobotics.orsmartrobot.R;

import java.util.ArrayList;
import java.util.List;

import static com.orrobotics.orsmartrobot.view.ControlWithVoice.model;

public class MessageAdapter extends ArrayAdapter<Message> {

    private TextView chatText;
    private List<Message> chatMessageList = new ArrayList<Message>();
    private Context context;

    @Override
    public void add(Message object) {
        chatMessageList.add(object);
        super.add(object);
    }

    public MessageAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
        this.context = context;
    }

    @Override
    public  void clear(){
        chatMessageList.clear();
        super.clear();
    }

    public int getCount() {
        return this.chatMessageList.size();
    }

    public Message getItem(int index) {
        return this.chatMessageList.get(index);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        Message chatMessageObj = getItem(position);
        View row = convertView;
        LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (chatMessageObj.left) {
            row = inflater.inflate(R.layout.left, parent, false);
            chatText = (TextView) row.findViewById(R.id.message_body);
            chatText.setTextColor(model.changeTextColor("question"));
        }else{
            row = inflater.inflate(R.layout.right, parent, false);
            chatText = (TextView) row.findViewById(R.id.message_body);
            chatText.setTextColor(model.changeTextColor("response"));
        }

        chatText.setText(chatMessageObj.message);
        chatText.setTextSize(model.changeTextSize());
        return row;
    }
}