package com.orrobotics.orsmartrobot.util;

import android.os.AsyncTask;
import android.util.Log;

import com.orrobotics.orsmartrobot.application.SmartRobotApplication;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutionException;

public class ChatbotConnector {

	private static final String TAG = "ChatBotLog";
	public static String baseUrl = SmartRobotApplication.getParamFromFile(
			"chatbot.url", "Configuration.properties", null);

	SmartRobotApplication model;
	String response = "";
	PandorabotObject obj = new PandorabotObject();
	String input1 = "";
	chatbotRequest chatbotTask;

	public ChatbotConnector(SmartRobotApplication model) {
		this.model = model;
	}

	private String readResponse(BufferedReader reader) {
		StringBuilder sb = new StringBuilder();
		String NL = System.getProperty("line.separator");
		try {
			for (;;) {
				String line = reader.readLine();
				if (line == null) {
					break;
				}
				sb.append(line).append(NL);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return sb.toString();
	}

	public String doServerRequest(String input) {

		if (!model.isConnectedToInternet()) {
			response = "Network unreachable";
			obj.setReponse(response);
		} else {
			try {
				chatbotTask = new chatbotRequest();
				response = chatbotTask.executeOnExecutor(
						AsyncTask.THREAD_POOL_EXECUTOR, input).get();
			} catch (InterruptedException | ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				response = "Network unreachable";
				obj.setReponse(response);
			}
		}
		return response;
	}

	public void cancelServerRequest() {
		if (chatbotTask != null)
			if (!chatbotTask.isCancelled())
				chatbotTask.cancel(true);
	}

	private class chatbotRequest extends AsyncTask<String, Integer, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
			// Mise à jour de la ProgressBar
		}

		@Override
		protected String doInBackground(String... input) {
			String serverResponse = "";
			URL url=null;
            Log.i("mbu","---input="+input[0]);

			String jsonToSend ="{\"token\":\"cLt9yYygYtVY6rCuQt1F\",\"message\":\""+input[0]+"\"}";

			Log.i("mbu","---jsonToSend="+jsonToSend);
			Log.i("mbu","---baseUrl="+baseUrl);
			if (!isCancelled()) {
				if (baseUrl == null)
					baseUrl = "https://teamnet.ml/voicebot-api";
				try {
					 url = new URL(baseUrl);
				} catch (MalformedURLException e) {
					e.printStackTrace();
				}
				try {

					HttpURLConnection conn = (HttpURLConnection) url.openConnection();

					conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
					conn.setRequestProperty("Accept", "application/json");
					conn.setRequestMethod("POST");
					conn.setConnectTimeout(5000);
					conn.setReadTimeout(30000);
					conn.setDoInput(true);
					conn.setDoOutput(true);

					String userAgent = System.getProperty("http.agent");
					Log.i(TAG, "HTTPUrlConnection user-agent=" + userAgent);

					OutputStreamWriter out = new OutputStreamWriter(conn.getOutputStream());
					out.write(jsonToSend);
					out.flush();
					out.close();

					try {
						conn.connect();
					}
					catch (Exception e){
						e.printStackTrace();
					}


					int status = conn.getResponseCode();

					if (status != HttpURLConnection.HTTP_OK) {

						Log.i("mbu","---nonOk= "
								+ conn.getResponseMessage());

						obj.setReponse("NoResponseServer");

					} else {
						Log.i("mbu","---Ok-----");
						BufferedReader reader = new BufferedReader(
								new InputStreamReader(conn.getInputStream()));
						String responseContent = readResponse(reader);
						Log.v(TAG, "responseContent=" + responseContent);
						try {
							JSONObject jsonObj = new JSONObject(responseContent);
							if (jsonObj.has("that")) {
								response = jsonObj.getString("that");

							} else if (jsonObj.has("message")) {
								response = jsonObj.getString("message");
							}
							obj.setReponse(response);
							input1 = jsonObj.getString("input");
							obj.setInput(input[0]);


						} catch (Exception ex) {
							Log.e(TAG, "Error --------");
							ex.printStackTrace();
						}
					}
					conn.disconnect();
					serverResponse = obj.getReponse();

				} catch (Exception ex) {
					Log.e(TAG, "Error connect--------"+ex.getMessage());
					ex.printStackTrace();
					obj.setReponse("NoResponseServer");
					serverResponse = "NoResponseServer";

				}
			}
			return serverResponse;
		}

		@Override
		protected void onPostExecute(String result) {

		}
	}

}
