package com.orrobotics.orsmartrobot.util;
/**
 * @author Hatim CHERKAOUI
 *
 */
public class Message {
    public boolean left;
    public String message;

    public Message(boolean left, String message) {
        super();
        this.left = left;
        this.message = message;
    }
}
