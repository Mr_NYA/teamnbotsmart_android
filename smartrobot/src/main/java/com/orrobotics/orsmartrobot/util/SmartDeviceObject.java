package com.orrobotics.orsmartrobot.util;

public class SmartDeviceObject {

	private String deviceType;
	private String deviceName;
	private String deviceState;
	private String deviceMac;



	public SmartDeviceObject(String deiveType, String deviceName
			, String deviceMac, String deviceState) {
		super();
		this.deviceType = deiveType;
		this.deviceName = deviceName;
		this.deviceState = deviceState;
		this.deviceMac=deviceMac;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deiveType) {
		this.deviceType = deiveType;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getDeviceState() {
		return deviceState;
	}

	public void setDeviceState(String deviceState) {
		this.deviceState = deviceState;
	}
	
	public String getDeviceMac() {
		return deviceMac;
	}

	public void setDeviceMac(String deviceMac) {
		this.deviceMac = deviceMac;
	}
}
