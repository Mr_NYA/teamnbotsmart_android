package com.orrobotics.orsmartrobot.util;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.hardware.Camera;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;

import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.observer.OrSmarRobotObserver;

import java.io.IOException;
import java.util.logging.Handler;

/**
 * Created by hif on 12/03/2018.
 */

public class RectangleView extends View implements OrSmarRobotObserver {
    private Paint mPaint;
    SmartRobotApplication application;
    float centreX=0.0f;
    float centreY=0.0f;
    Boolean flagStart=true;
    int delay=500;
    android.os.Handler handler = new android.os.Handler();
    Float topPre=0.0f;
    Float leftPre=0.0f;
    Float heightPre=0.0f;
    Float topPreview=0.0f;
    Float leftPreview=0.0f;

    int silo_det;
    float pos_y;
    float pos_x;

    double CoeffA;
    double CoeffL1;
    int CoeffL2;
    //Camera camera;
    // Camera camera = application.getcameraobject();
    //Camera.Parameters parameters = camera.getParameters();

    boolean test_zoom = false;
    int currentZoomLevel = 0;



    public RectangleView(Context context) {
        super(context);
        application = (SmartRobotApplication) context;
        application.registerObserver(this);
        //CoeffA = Integer.parseInt(SmartRobotApplication.getParamFromFile("suivi.CoeffA",
        //        "Configuration.properties", null));
        //CoeffL1 = Integer.parseInt(SmartRobotApplication.getParamFromFile("suivi.CoeffL1",
        //       "Configuration.properties", null));
        CoeffL2 = Integer.parseInt(SmartRobotApplication.getParamFromFile("suivi.CoeffL2",
                "Configuration.properties", null));
        delay = Integer.parseInt(application.getParamFromFile("suivi.delai",
                "Configuration.propertie", null).split(" ")[0]);
        CoeffA = Double.parseDouble(SmartRobotApplication.getParamFromFile("suivi.CoeffA",
                "Configuration.properties", null));
        CoeffL1 = Double.parseDouble(SmartRobotApplication.getParamFromFile("suivi.CoeffL1",
                "Configuration.properties", null));

        silo_det = Integer.parseInt(application.getParamFromFile("silhouette.isactive", "Configuration.properties", null));


        /**if (params.isZoomSupported()) {
         final int maxZoomLevel = params.getMaxZoom();
         Log.i("max ZOOM ", "is " + maxZoomLevel);
         zoomControls.setIsZoomInEnabled(true);
         zoomControls.setIsZoomOutEnabled(true);
         **/
        flagStart=true;
        centreX = 0.0f;
        centreX = 0.0f;
        handler.postDelayed(run,delay);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        if(flagStart==true && application.isTelepresence()==false) {
            DisplayMetrics metrics = getResources().getDisplayMetrics();

            int stroke = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, metrics);
            mPaint = new Paint();
            mPaint.setAntiAlias(true);
            mPaint.setDither(true);
            mPaint.setColor(Color.WHITE);
            mPaint.setStrokeWidth(stroke);
            mPaint.setStyle(Paint.Style.STROKE);
            int canvasW = getWidth();
            int canvasH = getHeight();
            Point centerOfCanvas = new Point(canvasW / 2, canvasH / 2);
            int rectW = application.getPx();
            int rectH = application.getPx();
            if (centreX != 0.0f) {
                float left = centreX - (rectW / 2);
                float top = centreY - (rectH / 2);
                float right = centreX + (rectW / 2);
                float bottom = centreY + (rectH / 2);
                RectF rect = new RectF(left, top, right, bottom);
                //canvas.drawRect(rect, mPaint);

            } else {
                int left = centerOfCanvas.x - (rectW / 2);
                int top = centerOfCanvas.y - (rectH / 2);
                int right = centerOfCanvas.x + (rectW / 2);
                int bottom = centerOfCanvas.y + (rectH / 2);
                RectF rectCenter = new RectF(left, top, right, bottom);
                // canvas.drawRect(rectCenter, mPaint);
            }
        }
    }

    @Override
    public void update(String message) throws IOException {
        if(message!=null){

            if(message.equals("drawerFermer")){

                flagStart = true;
                invalidate();

            }
            if(message.contains("drawerOuvert") ){
                flagStart=false;
                centreX = 0.0f;
                centreY = 0.0f;
                invalidate();
            }
        }
    }


    public void setInvisible() {
        handler.removeCallbacks(run);
        application.removeObserver(this);
        flagStart=false;
        invalidate();
    }

    Runnable run = new Runnable() {
        @Override
        public void run() {
            if(application.isStartSuivi()==true) {

                float topAct ;
                float leftAct ;

                if (silo_det == 1){
                   topAct = application.getSiloTop();
                   leftAct = application.getSiloLeft();
                }else{
                    topAct = application.getTop();
                    leftAct = application.getLeft();
                }

                int height_green_rec = (int) application.getHeight();
                //float right =
                int L = getWidth();
                int H = getHeight();
                application.setHeightScreen(H);

                Point centerOfscreen = new Point(L / 2, H / 2);
                if (Math.abs((topAct - topPreview)) > 20.0f || Math.abs((leftAct - leftPreview)) > 20.0f) {
                    float heightAct = application.getHeight();
                    topAct = topAct / 100;
                    leftAct = leftAct / 100;
                    heightAct = heightAct / 100;

                    //pos_x=L/2-(right+left)/2;
                    //pos_y=H/2-(bottom-height/2);
                    if(centreX !=0.0f || centreY!=0.0f) {
                        test_zoom = false;



                        //int VA = (int) (CoeffA * (leftAct - leftPre));
                        //int VL = (int) (int) (CoeffL1 * (topAct - topPre) + CoeffL2 * (heightPre - heightAct ));
                        //int VA = (int) (2 * ((int)centreX/100 - centerOfscreen.x/100 ));


                        int  VA = (int) (CoeffA * ((int)centreX/100 - centerOfscreen.x/100 ));
                        int  VL = (int) (CoeffL1 * ( (int)centreY/100 - centerOfscreen.y/100 ));//+ CoeffL2 * (heightPre - heightAct ));



                        if ((VA != 0 || VL != 0)){

                            application.setZommTest(false);
                            application.notifyObservers("MOVE;" + VL + ";" + VA);
                        }

                        /**
                         if (Math.abs(VA) > 0  || Math.abs(VL) > 0){
                         application.setZommTest(false);
                         application.notifyObservers("MOVE;" + VL + ";" + VA);
                         }else  application.setZommTest(true);**/

                        if ((VA == 0 || VL == 0)){
                            //currentZoomLevel = 0;
                            //boolean zoo_test_start =  true;
                            application.setZommTest(true);


                        }

                    }else { test_zoom = true;}

                    if (silo_det == 1){
                        centreX = application.getCenterXsilo();
                        centreY = application.getCenterYsilo();
                    }else{
                        centreX = application.getCenterX();
                        centreY = application.getCenterY();
                    }

                    invalidate();
                    topPre = topAct;
                    leftPre = leftAct;
                    heightPre = heightAct;
                }
                if (silo_det == 1){
                    topPreview = application.getSiloTop();
                    leftPreview = application.getSiloLeft();
                }else{
                    topPreview = application.getTop();
                    leftPreview = application.getLeft();
                }

            }else{
                if(centreX !=0.0f || centreY!=0.0f) {
                    flagStart = true;
                    centreX = 0.0f;
                    centreY = 0.0f;
                    invalidate();
                }
            }
            handler.postDelayed(run,delay);
        }
    };
}

