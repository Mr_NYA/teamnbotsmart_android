package com.orrobotics.orsmartrobot.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.concurrent.ExecutionException;

import org.json.JSONObject;

import com.orrobotics.orsmartrobot.application.SmartRobotApplication;

import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

public class WikipediaConnector {

	private static final String TAG = "Wiki";
	String baseUrl = SmartRobotApplication.getParamFromFile("wiki.url",
			"Configuration.properties", null);

	SmartRobotApplication model;
	String response = "";
	Handler hand = new Handler();
	WikiRequest wikiTask;


	public WikipediaConnector(SmartRobotApplication model) {

		this.model = model;
	}

	@SuppressWarnings("deprecation")
	private URL requestUrl(String input) {
		String lang = "en";
		if (model.getLangageTTS().contains("fr"))
			lang = "fr";
		else
			lang = "en";

		try {
			if (baseUrl == null)
				baseUrl = "wikipedia.org";
			// https://fr.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro=&explaintext=&titles=pomme
			String spec = String
					.format("%s&titles=%s&format=json",
							"https://"
									+ lang
									+ "."
									+ baseUrl
									+ "/w/api.php?format=json&action=query&prop=extracts&exintro=&explaintext=",
							URLEncoder.encode(input));
			Log.i(TAG, spec);
			return new URL(spec);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	private String readResponse(BufferedReader reader) {
		StringBuilder sb = new StringBuilder();
		String NL = System.getProperty("line.separator");
		try {
			for (;;) {
				String line = reader.readLine();
				Log.i(TAG, "readResponse read: " + line);
				if (line == null) {
					break;
				}
				sb.append(line).append(NL);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return sb.toString();
	}

	public String doServerRequest(String input) {
		String result = "";
		if (!model.isConnectedToInternet()) {
			result = "Error wiki";
		} else {
			try {
				wikiTask = new WikiRequest();
				result = wikiTask.executeOnExecutor(
						AsyncTask.THREAD_POOL_EXECUTOR, input).get();
			} catch (InterruptedException | ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result;
	}

	public void cancelServerRequest() {
		if (!wikiTask.isCancelled())
			wikiTask.cancel(true);
	}

	private class WikiRequest extends AsyncTask<String, Integer, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
			// Mise à jour de la ProgressBar
		}

		@Override
		protected String doInBackground(String... input) {
			String result = "";
			if (!isCancelled()) {

				try {

					HttpURLConnection conn = (HttpURLConnection) requestUrl(
							input[0]).openConnection();

					conn.setRequestMethod("GET");
					// conn.setDoOutput(true);
					conn.setConnectTimeout(5000);
					conn.setReadTimeout(60000);
					String userAgent = System.getProperty("http.agent");
					Log.i(TAG, "HTTPUrlConnection user-agent=" + userAgent);
					conn.connect();

					int status = conn.getResponseCode();
					Log.v("hh", "----Status=" + status);
					if (status != HttpURLConnection.HTTP_OK) {
						Log.e("Error http",
								"Error HttpURLConnection= "
										+ conn.getErrorStream());
						result = "Error wiki";

					} else {
						BufferedReader reader = new BufferedReader(
								new InputStreamReader(conn.getInputStream()));
						String responseContent = readResponse(reader);
						Log.v(TAG, "responseContent=" + responseContent);
						try {
							JSONObject jsonObj = new JSONObject(responseContent);

							// result = jsonObj.getJSONObject("query")
							// .getJSONObject("pages").getJSONObject("2494890")
							// .getString("extract");
							JSONObject pages = jsonObj.getJSONObject("query")
									.getJSONObject("pages");
							Iterator<?> keys = pages.keys();
							while (keys.hasNext()) {
								String key = (String) keys.next();
								if (pages.get(key) instanceof JSONObject) {
									JSONObject o = (JSONObject) pages.get(key);
									result = o.getString("extract");
								}

							}
							Log.e(TAG, "wikiResult: " + result);

						} catch (Exception ex) {
							Log.e(TAG, "Error --------");
							result = "Error noResult";
							ex.printStackTrace();
						}
					}
					conn.disconnect();

				} catch (Exception ex) {
					Log.e(TAG, "Error connect--------");

					result = "Error wiki";
					ex.printStackTrace();
					cancelServerRequest();
				}
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {

		}
	}

}
