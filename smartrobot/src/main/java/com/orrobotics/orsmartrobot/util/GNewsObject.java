package com.orrobotics.orsmartrobot.util;

import java.util.ArrayList;

public class GNewsObject {

	private ArrayList<String> haeadLines;
	public String message = "";

	public GNewsObject() {

	}

	public GNewsObject(ArrayList<String> haeadLines) {
		super();
		this.haeadLines = haeadLines;
	}

	public ArrayList<String> getReponse() {

		return haeadLines;
	}

	public void setReponse(ArrayList<String> reponse) {
		this.haeadLines = reponse;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
