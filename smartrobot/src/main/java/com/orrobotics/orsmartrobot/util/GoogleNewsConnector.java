package com.orrobotics.orsmartrobot.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import org.json.JSONArray;
import org.json.JSONObject;

import com.orrobotics.orsmartrobot.application.SmartRobotApplication;

import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

public class GoogleNewsConnector {

	private static final String TAG = "GNews";

	SmartRobotApplication model;
	ArrayList<String> headLines = new ArrayList<String>();
	GNewsRequest gNewsTask;
	GNewsObject obj = new GNewsObject();

	public GoogleNewsConnector(SmartRobotApplication model) {

		this.model = model;
	}

	private URL requestUrl(String baseUrl, String apiKey) {
		String sources = "google-news";
		if (model.getLangageTTS().contains("fr"))
			sources = "google-news-fr";
		else
			sources = "google-news";

		try {
			if (baseUrl == null)
				baseUrl = "newsapi.org";
			// https://www.pandorabots.com/pandora/talk-xml?botid=dbf74f507e3458e4&input=bonjour&format=json
			// https://newsapi.org/v2/top-headlines?sources=google-news-fr&apiKey=dda5f00494754412b047b617eaeb833f
			String spec = String
					.format("%s?sources=%s&apiKey=%s&format=json", "https://"
							+ baseUrl + "/v2/top-headlines", sources, apiKey);
			Log.i(TAG, spec);
			return new URL(spec);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	private String readResponse(BufferedReader reader) {
		StringBuilder sb = new StringBuilder();
		String NL = System.getProperty("line.separator");
		try {
			for (;;) {
				String line = reader.readLine();
				Log.i(TAG, "readResponse read: " + line);
				if (line == null) {
					break;
				}
				sb.append(line).append(NL);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return sb.toString();
	}

	public GNewsObject doServerRequest(String url, String apiKey) {

		if (!model.isConnectedToInternet()) {
			obj.setMessage("Error GNews");
			obj.setReponse(headLines);
		} else {
			try {
				gNewsTask = new GNewsRequest(url, apiKey);
				obj = gNewsTask.executeOnExecutor(
						AsyncTask.THREAD_POOL_EXECUTOR).get();
			} catch (InterruptedException | ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return obj;
	}

	public void cancelServerRequest() {
		if (!gNewsTask.isCancelled())
			gNewsTask.cancel(true);
	}

	private class GNewsRequest extends AsyncTask<String, Integer, GNewsObject> {

		String apiKey;
		String url;

		public GNewsRequest(String url, String apiKey) {
			// TODO Auto-generated constructor stub
			this.url = url;
			this.apiKey = apiKey;

		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
			// Mise à jour de la ProgressBar
		}

		@Override
		protected GNewsObject doInBackground(String... input) {
			if (!isCancelled()) {

				try {

					HttpURLConnection conn = (HttpURLConnection) requestUrl(
							url, apiKey).openConnection();
					conn.setRequestMethod("GET");
					// conn.setDoOutput(true);
					conn.setConnectTimeout(5000);
					conn.setReadTimeout(60000);
					String userAgent = System.getProperty("http.agent");
					Log.i(TAG, "HTTPUrlConnection user-agent=" + userAgent);
					conn.connect();

					int status = conn.getResponseCode();
					Log.v("hh", "----Status=" + status);
					if (status != HttpURLConnection.HTTP_OK) {
						Log.e("Error http",
								"Error HttpURLConnection= "
										+ conn.getErrorStream());
						obj.setMessage("Error GNews");
						obj.setReponse(headLines);

					} else {
						BufferedReader reader = new BufferedReader(
								new InputStreamReader(conn.getInputStream()));
						String responseContent = readResponse(reader);
						Log.v(TAG, "responseContent=" + responseContent);
						try {
							JSONObject jsonObj = new JSONObject(responseContent);
							headLines = new ArrayList<String>();
							JSONArray arr = jsonObj.getJSONArray("articles");
							for (int i = 0; i < arr.length(); i++) {
								headLines.add(arr.getJSONObject(i).getString(
										"title"));
							}
							obj.setMessage("Success GNews");
							obj.setReponse(headLines);

							Log.e(TAG, "headLines: " + headLines);

						} catch (Exception ex) {
							Log.e(TAG, "Error --------");
							ex.printStackTrace();
						}
					}
					conn.disconnect();

				} catch (Exception ex) {
					Log.e(TAG, "Error connect--------");
					obj.setMessage("Error");
					obj.setReponse(headLines);

					ex.printStackTrace();
					cancelServerRequest();
				}
			}
			return obj;
		}

		@Override
		protected void onPostExecute(GNewsObject result) {

		}
	}

}
