package com.orrobotics.orsmartrobot.util;

public class RemoteControlUtil {

	// INTERNET USEFUL COSTANTS
	public static final String PSEUDOSKYPE = "pseudoSkype";
	public static final int SERVER_PORT = 5222;
	public static final String PASSWORD = "PASSWORD";
	public static final String EMAIL = "EMAIL";
	public static final String EMAIL1 = "EMAIL1";
	public static final String PASSWORD_OPEN_FIRE = "PASSWORD_OPEN_FIRE";
	public static final String PSEUDO_OPEN_FIRE = "PSEUDO_OPEN_FIRE";
	public static final String PASSWORD_SIP = "PASSWORD_SIP";
	public static final String PSEUDO_SIP = "PSEUDO_SIP";
	public static final String MAC_ADDRESS = "MAC_ADDRESS";
	public static final String SKYPE_PACKAGE = "com.skype.raider";
	public static final String SKYPE_INSTALL_URL = "market://details?id=com.skype.raider";
	public static final String NOM = "NOM";
	public static final String ROBOT_NAME = "ROBOT_NAME";
	public static final String ROBOT_PSEUDO_SKYPE = "ROBOT_PSEUDO_SKYPE";
	// CONSTANTS
	public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
	public static final String EXTRAS_ROBOT_NAME = "ROBOT_NAME";
	public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";
	public static final String EXTRAS_DEVICE_SERIE = "EXTRAS_DEVICE_SERIE";
	// XMPP MESSAGES
	public static final String XMPP_SKYPE = "Skype";
	public static final String XMPP_TEST = "test";
	public static final String XMPP_OK = "OK";
	public static final String XMPP_NOROBOT = "norobot";
	public static final String XMPP_END = "end";
	public static final String XMPP_STOP = "stop";
	public static final String AUTOSTART = "AUTOSTART";
	public static final String ICON = "ICON";
	// mode de controle
	public static final String MODE_CONTROLE = "MODE_CONTROLE";
	public static final String OPT_FACE_TO_FACE = "OPT_FACE_TO_FACE";
	public static final String OPT_CMD_VOCAL = "OPT_CMD_VOCAL";
	public static final String ID = "ID";

	public static final String CONNECTED_TO_APP = "CONNECTED_TO_APP";
	public static final String CONNECTED_TO_DEVICE = "CONNECTED_TO_DEVICE";
	public static final String CONNECTED_TO_SERVER = "CONNECTED_TO_SERVER";
	public static final String CONNECTED_TO_SIP_SERVER = "CONNECTED_TO_SIP_SERVER";
	public static final String EXTRAS_PSEUDO_OPENFIRE_CONTROL = "EXTRAS_PSEUDO_OPENFIRE_CONTROL";
	public static final String MAC_CONNECTED_DEVICE = "MAC_CONNECTED_DEVICE";
	public static final String EXTRAS_ROBOT_TYPE = "ROBOT_TYPE";
	public static final String ECHO_CANCELLATION = "ECHO_CANCELLATION";
	public static final String SPEAKER = "speaker";
	public static final String DATE = "DATE";
	public static final String JSON_LIST = "JSON_LIST";
	public static final String NEW_MAC = "NEW_MAC";
	public static final String NEW_NUM_SERIE = "NEW_NUM_SERIE";
	public static final String SPEED_LINEAIRE = "lineaire";
	public static final String SPEED_LATERALE = "laterale";
	public static final String VOCAL_FREQ_DECLENCH = "VOCAL_FREQ_DECLENCH";
	public static final String TTS_LANGUAGE = "TTS_LANGUAGE";
	public static final String TTS_SPEED_POS = "TTS_SPEED_POS";
	public static final String ISPERMANCE = "ISPERMANCE";
	public static final String VOLUME = "volume";
	public static final String PRENOM = "prenom";
	public static final String APPLICATION_LANGAGE = "APPLICATION_LANGAGE";
	public static final String CALL_TYPE = "CALL_TYPE";
	public static final String REMOTE_CONTROL = "REMOTE_CONTROL";
	public static final String APP_VERSION_NAME = "APP_VERSION_NAME";
	public static final String MIC = "MIC";
	public static final String VIDEO_LENGHT = "VIDEO_LENGHT";
	public static final String CAMERA = "CAMERA";
	public static final String VIDEO_QUALITY = "VIDEO_QUALITY";
	public static final String CAMERA_ID = "CAMERA_ID";

	public static final String MAC_ADDRESS_HEX_BOUND = "MAC_ADDRESS_HEX_BOUND";

    public static final String BATTERIE_DELAY = "BATTERIE_DELAY";
	public static final String SHOW_BATTERIE = "SHOW_BATTERIE";
	public static final String EDIT_TTS_LANGUE ="EDIT_TTS_LANGUE" ;
	public static final String EDIT_TTS_SPEED = "EDIT_TTS_SPEED";
    public static final String SHOW_ROBOT_APP = "SHOW_ROBOT_APP";
    public static final String TTS_LANG_SELECTION = "TTS_LANG_SELECTION" ;

	public static final String LIGHT = "light";
	public static final String ISCLOUDSPEECH = "ISCLOUDSPEECH";
	public static final String WEBSERVICE = "webservice";
	public static final String ICON_SUIVI = "ICON_SUIVI";
}
