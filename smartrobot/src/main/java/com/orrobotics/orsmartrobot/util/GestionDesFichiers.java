package com.orrobotics.orsmartrobot.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.zip.ZipOutputStream;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.orrobotics.orsmartrobot.application.SmartRobotApplication;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Environment;
import android.util.Log;

/**
 *
 * @author Hajiba IFRAH Cette classe sert à gérer le fichier log Version 2.1.0
 * @version 2.1.0
 * @date 18/05/2015
 *
 */

@SuppressLint("SdCardPath")
public class GestionDesFichiers {
	static File absolutPath = Environment.getExternalStorageDirectory();
	static File pathOfMyFile = Environment
			.getExternalStoragePublicDirectory("/SmartRobot");
	public static SimpleDateFormat simpleFormat = new SimpleDateFormat(
			"dd/MM/yyyy HH:mm:ss", java.util.Locale.getDefault());
	static Calendar c = Calendar.getInstance();
	static String FilePath = pathOfMyFile + "/logcat";
	static FileOutputStream dest;
	static ZipOutputStream out;
	static final int BUFFER = 2048;

	/**
	 * Cette classe permet de céer une fihier dans le dossier log de
	 * OrSmartPresence
	 */

	public static void creerRobotDossier() {
		File file1 = new File(absolutPath, "SmartRobot");
		if (!file1.exists()) {
			file1.mkdir();
		}
	}
	public static void creerControlDossier() {
		File file1 = new File(pathOfMyFile, "AutoControl");
		if (!file1.exists()) {
			file1.mkdir();
		}
	}

	public static void creerCommandVocalDossier() {
		File file1 = new File(pathOfMyFile, "CommandVocal");
		if (!file1.exists()) {
			file1.mkdir();
		}
	}

	public static void creerFichier(String nomFichier) {
		File file1 = new File(absolutPath, "SmartRobot");

		if (file1.exists() && file1.isDirectory()) {
			File file = new File(pathOfMyFile, nomFichier + ".txt");
			if (!file.exists()) {
				try {
					file.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 *
	 * Cette classe permet d'écrire dans le fichier log de OrSmartPresence,
	 *
	 * @param level
	 *            peut être le type d'information à écrire par exemple soit une
	 *            information ou une erreur
	 * @param msg
	 *            le message à écrire
	 *
	 */

	public static void log(String level, String msg, String nomFichier) {
		try {
			File file = new File(pathOfMyFile, nomFichier + ".txt");
			if (!file.exists()) {
				creerFichier(nomFichier);
			}
			FileWriter fw;
			try {
				fw = new FileWriter(file.getAbsoluteFile(), true);
				BufferedWriter bw = new BufferedWriter(fw);

				bw.write("[" + simpleFormat.format(new Date()) + "] " + level
						+ " : " + msg + "\r\n");
				bw.flush();
				bw.close();
			} catch (java.io.FileNotFoundException e) {
			}
		} catch (Exception e) {
			e.printStackTrace();
		} catch (java.lang.StackOverflowError e1) {
		}

	}

	/**
	 *
	 * Cette classe permet d'écrire dans le fichier log de OrSmartPresence
	 *
	 * @param level
	 *            peut être le type d'information à écrire dans plusieurs cas ça
	 *            sera une erreur
	 * @param msg
	 *            le message à écrire
	 * @cause la cause de l'apparition de cette erreur
	 * @ste l'exception
	 *
	 */

	public static void log(String level, String msg, String cause,
						   StackTraceElement[] ste, String nomFichier) {
		File file = new File(pathOfMyFile, nomFichier + ".txt");
		if (!file.exists()) {
			creerFichier(nomFichier);
		}
		FileWriter fw;
		try {
			fw = new FileWriter(file.getAbsoluteFile(), true);
			BufferedWriter bw = new BufferedWriter(fw);

			bw.write("[" + simpleFormat.format(new Date()) + "] " + level
					+ " : " + msg + "\r\n");
			bw.flush();
			bw.write("\tCAUSE : " + cause + "\r\n");
			bw.flush();
			for (int i = 0; i < ste.length; i++) {
				bw.write("\tclasse : " + ste[i].getClassName() + "\r\n");
				bw.write("\tline : " + ste[i].getLineNumber() + "\r\n");
				bw.write("\tmethod : " + ste[i].getMethodName() + "\r\n");
				bw.flush();
			}
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static String lireFichier(String fichier, File path)
			throws IOException {
		String chaine = "";
		String ligne;
		File file = new File(path, fichier);
		FileInputStream fIn = new FileInputStream(file);
		BufferedReader br = new BufferedReader(new InputStreamReader(fIn,
				"UTF-8"));
		while ((ligne = br.readLine()) != null) {

			chaine += ligne + ";";
		}

		br.close();
		return chaine;
	}

	@SuppressLint("SimpleDateFormat")
	public static void collectLogs(Context context) {
		BufferedReader br = null;
		Process p = null;
		StringBuilder sb = new StringBuilder();

		try {
			p = Runtime.getRuntime().exec(
					new String[] { "logcat", "-d", "|", "grep",
							"`adb shell ps | grep com.orrobotics `" });
			br = new BufferedReader(new InputStreamReader(p.getInputStream()),
					2048);

			String line;
			while ((line = br.readLine()) != null) {
				sb.append(line);
				sb.append("\r\n");
			}

			File Log = new File(FilePath);

			SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
			String formattedDate = df.format(c.getTime());
			SmartRobotApplication app = (SmartRobotApplication) context;
			if (!app.getDate().equals(formattedDate)) {
				if (Log.exists()) {
					if (Log.isDirectory()) {
						String[] children = Log.list();
						for (int i = 0; i < children.length; i++) {
							new File(Log, children[i]).delete();
						}
					}
				}

			}
			app.setEditor(RemoteControlUtil.DATE, formattedDate);
			if (!Log.exists()) {
				Log.mkdir();
			}
			String name = "log-"
					+ app.getExtraRobotName()
					+ (simpleFormat.format(new Date()).replace(" ", "-")
					.replace(":", "-")).replace("/", "");
			File file = new File(FilePath, name + ".txt");
			if (!file.exists()) {
				try {
					file.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			FileWriter fw;
			try {
				fw = new FileWriter(file.getAbsoluteFile(), false);
				BufferedWriter bw = new BufferedWriter(fw);

				bw.write(sb.toString());
				bw.flush();
				bw.close();
			} catch (java.io.FileNotFoundException e) {
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static ParamVocalObject channelParser(NodeList listChannel,
												 String channel) {
		// TODO Auto-generated method stub
		String channelUrl = null;
		ParamVocalObject obj = null;
		if (listChannel.getLength() == 0)
			return obj;

		int listChannelLenght = listChannel.getLength();
		for (int h = 0; h < listChannelLenght; h++) {

			String channelName = listChannel.item(h).getAttributes()
					.getNamedItem("name").getNodeValue();

			if (channel.equals(channelName)) {
				channelUrl = listChannel.item(h).getTextContent();
				obj = new ParamVocalObject("channelUrl", channelUrl);
				break;
			}

		}

		return obj;
	}

	public static ArrayList<ParamVocalObject> paramParser(NodeList listParam) {
		// TODO Auto-generated method stub
		ArrayList<ParamVocalObject> resultParams = new ArrayList<ParamVocalObject>();

		if (listParam.getLength() == 0)
			return resultParams;

		int listParamLenght = listParam.getLength();
		for (int h = 0; h < listParamLenght; h++) {

			String paramName = listParam.item(h).getAttributes()
					.getNamedItem("name").getNodeValue();

			String paramValue = listParam.item(h).getTextContent();

			ParamVocalObject obj = new ParamVocalObject(paramName, paramValue);

			resultParams.add(obj);

		}

		return resultParams;
	}

	public static ArrayList<SmartDeviceObject> deviceParser(NodeList listDevice) {
		// TODO Auto-generated method stub
		ArrayList<SmartDeviceObject> devices = new ArrayList<SmartDeviceObject>();

		if (listDevice.getLength() == 0)
			return devices;

		for (int k = 0; k < listDevice.getLength(); k++) {
			Node device = listDevice.item(k);

			if (device.getNodeType() == Node.ELEMENT_NODE) {
				Element deviceElement = (Element) device;

				String type = deviceElement.getAttribute("type");
				String name = "";
				String mac = "";
				String state = "";
				NodeList infosDevice = deviceElement.getChildNodes();
				for (int p = 0; p < infosDevice.getLength(); p++) {
					if (infosDevice.item(p).getNodeName().equals("Name")) {
						name = infosDevice.item(p).getTextContent();
					}
					if (infosDevice.item(p).getNodeName().equals("Mac")) {
						mac = infosDevice.item(p).getTextContent();
					}
					if (infosDevice.item(p).getNodeName().equals("State")) {
						state = infosDevice.item(p).getTextContent();
					}

				}
				SmartDeviceObject obj = new SmartDeviceObject(type, name, mac,
						state);

				devices.add(obj);

			}
		}

		return devices;
	}

	public static ArrayList<MessageVocalObject> msgParser(
			NodeList listMessages, SmartRobotApplication model) {
		// TODO Auto-generated method stub
		ArrayList<MessageVocalObject> resultMsgs = new ArrayList<>();

		if (listMessages.getLength() == 0)
			return resultMsgs;

		int listMsgLenght = listMessages.getLength();
		Log.v("", "---listMsgLenght=" + listMsgLenght);
		for (int h = 0; h < listMsgLenght; h++) {

			String msgType = listMessages.item(h).getAttributes()
					.getNamedItem("type").getNodeValue();
			String msgValue = listMessages.item(h).getTextContent();
			Log.v("", "---type=" + msgType);
			Log.v("", "---value=" + msgValue);

			MessageVocalObject obj = new MessageVocalObject(msgType, msgValue);

			resultMsgs.add(obj);

		}

		return resultMsgs;

	}

	public static String datesParser(NodeList listDates, String msg) {
		// TODO Auto-generated method stub
		String actionDate = "null,today";
		for (int k = 0; k < listDates.getLength(); k++) {
			Node date = listDates.item(k);

			if (date.getNodeType() == Node.ELEMENT_NODE) {
				Element dateElement = (Element) date;
				NodeList requestsList = dateElement
						.getElementsByTagName("Request");
				int inputListLenght = requestsList.getLength();
				String action = dateElement.getElementsByTagName("Action")
						.item(0).getChildNodes().item(0).getNodeValue();

				for (int p = 0; p < inputListLenght; p++) {
					if (msg.contains(requestsList.item(p).getChildNodes()
							.item(0).getNodeValue())) {
						actionDate = requestsList.item(p).getChildNodes()
								.item(0).getNodeValue()
								+ "," + action;
						break;
					}

				}

			}
		}
		return actionDate;
	}

	public static Boolean requestsParser(NodeList listRequest, String msg) {
		// TODO Auto-generated method stub

		Boolean requestFound = false;
		int listRequestLenght = listRequest.getLength();
		Log.v("", "---msg in requestParser=" + msg);
		for (int j = 0; j < listRequestLenght; j++) {

			String request = listRequest.item(j).getChildNodes().item(0)
					.getNodeValue();
			if (request.contains("*")) {
				Log.v("", "---contains***-----");

				int count = request.length()
						- request.replace("*", "").length();
				if (count > 2
						|| (count == 2 && (!request.startsWith("*") || !request
						.endsWith("*")))) {
					request = request.replace("*", " ");
					String[] reqList = request.split(" ");
					String[] msgList = msg.split(" ");
					Log.v("", "---msgComplet=" + msg);
					int indexFound = -1;
					for (int i = 0; i < reqList.length; i++) {
						for (int k = 0; k < msgList.length; k++) {
							Log.v("", "---request=" + reqList[i]);
							Log.v("", "---msg=" + msgList[k]);
							if (msgList[k].equals(reqList[i])) {
								Log.v("", "---k=" + k);
								Log.v("", "---indexFound=" + indexFound);
								if (k > indexFound) {
									requestFound = true;
									indexFound = k;
									break;
								} else
									requestFound = false;
							} else
								requestFound = false;
						}
						if (!requestFound)
							break;
					}
				} else if (request.startsWith("*") && request.endsWith("*")) {
					Log.v("", "---contains-----");
					request = request.replace("*", "");
					if (msg.contains(request))
						requestFound = true;
				} else if (request.startsWith("*")) {
					Log.v("", "---startsWith-----");
					request = request.replace("*", "");
					if (msg.endsWith(request))
						requestFound = true;
				} else if (request.endsWith("*")) {
					Log.v("", "---endsWith-----");
					request = request.replace("*", "");
					if (msg.startsWith(request))
						requestFound = true;
				}
				if (requestFound) {
					Log.v("", "---request=" + request);
					break;
				}

			} else {

				if (msg.equals(request)) {
					requestFound = true;
					break;

				}
			}
		}

		return requestFound;
	}

	public static ResultVocalObject cmdParser(NodeList listCmd, String type,
											  String msg) {
		// TODO Auto-generated method stub
		ResultVocalObject result = null;
		String action = "";
		String response = "";
		String date = "";

		boolean isRequestFound = false;

		for (int i = 0; i < listCmd.getLength(); i++) {
			Node cmd = listCmd.item(i);
			if (cmd.getNodeType() == Node.ELEMENT_NODE) {
				Element elem = (Element) cmd;
				NodeList listRequest = elem.getElementsByTagName("Request");

				action = elem.getElementsByTagName("Action").item(0)
						.getChildNodes().item(0).getNodeValue();
				if (type.equals("withings") || type.equals("hexiwear"))
					response = elem.getElementsByTagName("Response").item(0)
							.getChildNodes().item(0).getNodeValue();

				isRequestFound = GestionDesFichiers.requestsParser(listRequest,
						msg);
				if (isRequestFound) {
					Log.v("", "-----RequestFound------");

					action = action.trim();

					result = new ResultVocalObject(action, response, date);
					if ((action.trim()).isEmpty()) {
						result.setAction(msg);
					}
					break;

				}

				else {
					Log.v("", "-----RequestNotFound------");

					result = new ResultVocalObject();
					if (type.equals("withings") || type.equals("hexiwear")) {
						Log.v("", "------noDeviceInfoFound------");
						result.setAction("noInfo");
					}

					else
						result.setAction(msg);

				}
			}
			if (isRequestFound)
				break;
		}
		return result;
	}

	public static void WriteCmdVocaleFile() {
		Log.v("", "----WriteCmdVocaleFile--------");

		File fr = new File("/sdcard/SmartRobot/CmdVocale_fr.xml");

		if (!fr.exists()) {
			try {

				String d = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
						+ "<VocalCmd>"

						+ "<Messages>"
						+ "<Message type='Message.invitation' lang='fr'>Que puis-je faire pour vous ?</Message>"
						+ "<Message type='Commande.rienEntendue' lang='fr'>Je n’ai pas entendu de commande !</Message>"
						+ "<Message type='Commande.inconnue' lang='fr'>Je n’ai pas compris votre commande !</Message>"
						+ "<Message type='errorDevice.noDeviceActif' lang='fr'>Il n'y a aucun appareil actif sur votre fichier de configuration</Message>"
						+ "<Message type='errorDevice.noInfo' lang='fr'>les informations associées à votre appareil ne correspondent pas à votre demande</Message>"
						+ "<Message type='errorDevice.noResult' lang='fr'>Aucune information disponible</Message>"
						+ "<Message type='errorDevice.noInternet' lang='fr'>Il n’y a pas de connexion internet pour récupérer les informations du boitier</Message>"
						+ "<Message type='Hexiwear.failedConnexion' lang='fr'>n’est pas à proximité</Message>"
						+ "<Message type='error.noRobot' lang='fr'>Votre robot n'est pas connecté</Message>"
						+ "</Messages>"

						+ "<Devices owner='Albert'>"
						+ "<Device type='withings'>"
						+ "<State>1</State>"
						+ "<Name>withings</Name>"
						+ "<Mac></Mac>"
						+ "</Device>"
						+ "<Device type='hexiwear'>"
						+ "<State>1</State>"
						+ "<Name>montre</Name>"
						+ "<Mac>00:10:40:0B:00:1A</Mac>"
						+ "</Device>"
						+ "<Device type='hexiwear'>"
						+ "<State>1</State>"
						+ "<Name>hexiwear</Name>"
						+ "<Mac>00:3F:40:08:00:15</Mac>"
						+ "</Device>"
						+ "</Devices>"

						+ "<RobotCmd>"
						+ "<Cmd>"
						+ "<Request lang='fr'>batterie</Request>"
						+ "<Action>batterie</Action>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='fr'>monter</Request>"
						+ "<Action>HEAD U</Action></Cmd>"
						+ "<Cmd>"
						+ "<Request lang='fr'>baisser</Request>"
						+ "<Action>HEAD D</Action></Cmd>"
						+ "<Cmd>"
						+ "<Request lang='fr'>tourner la tête à gauche</Request>"
						+ "<Action>HEAD L</Action></Cmd>"
						+ "<Cmd>"
						+ "<Request lang='fr'>tourner la tête à droite</Request>"
						+ "<Action>HEAD R</Action></Cmd>"
						+ "<Cmd>"
						+ "<Request lang='fr'>stop</Request>"
						+ "<Action>MOVE;0;0</Action></Cmd>"
						+ "<Cmd>"
						+ "<Request lang='fr'>avancer</Request>"
						+ "<Action>MOVE;VL*50%;0</Action></Cmd>"
						+ "<Cmd>"
						+ "<Request lang='fr'>avancer vite</Request>"
						+ "<Action>MOVE;VL;0</Action></Cmd>"
						+ "<Cmd>"
						+ "<Request lang='fr'>avancer doucement</Request>"
						+ "<Action>MOVE;VL*20%;0</Action></Cmd>"
						+ "<Cmd>"
						+ "<Request lang='fr'>reculer</Request>"
						+ "<Action>MOVE;-VL*50%;0</Action>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='fr'>reculer vite</Request>"
						+ "<Action>MOVE;-VL;0</Action>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='fr'>reculer doucement</Request>"
						+ "<Action>MOVE;-VL*20%;0</Action>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='fr'>gauche</Request>"
						+ "<Action>MOVE;0;VA*50%</Action>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='fr'>gauche vite</Request>"
						+ "<Action>MOVE;0;VA</Action>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='fr'>gauche doucement</Request>"
						+ "<Action>MOVE;0;VA*20%</Action>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='fr'>droite</Request>"
						+ "<Action>MOVE;0;-VA*50%</Action></Cmd>"
						+ "<Cmd>"
						+ "<Request lang='fr'>droite vite</Request>"
						+ "<Action>MOVE;0;-VA</Action>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='fr'>droite doucement</Request>"
						+ "<Action>MOVE;0;-VA*20%</Action>"
						+ "</Cmd>"
						+ "</RobotCmd>"

						+ "<WithingsCmd>"
						+ "<Param name='withings.url'>https://wbsapi.withings.net</Param>"
						+ "<Param name='withings.ConsumerKey'>e1b33153371e2192676339e49f9e9d59ee8653fdf517c0f8c0493e78db7f5</Param>"
						+ "<Param name='withings.TokenKey'>586c113bf366ce667803b7989c3493fb63bbc76e2d5ec4c1ddebdc36</Param>"

						+ "<Cmd>"
						+ "<Request lang='fr'>*pas*</Request>"
						+ "<Action>withings,steps</Action>"
						+ "<Response lang='fr'>a fait % pas</Response>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='fr'>*distance*</Request>"
						+ "<Action>withings,distance</Action>"
						+ "<Response lang='fr'>a parcourue une distance de % mètres</Response>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='fr'>*hauteur*</Request>"
						+ "<Action>withings,elevation</Action>"
						+ "<Response lang='fr'>a escaladé une hauteur de % mètres</Response>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='fr'>*calorie*</Request>"
						+ "<Action>withings,calories</Action>"
						+ "<Response lang='fr' >a dépensé % calories</Response>"
						+ "</Cmd>"
						+ "<Date>"
						+ "<Request lang='fr'>aujourd’hui</Request>"
						+ "<Action>today</Action>"
						+ "</Date>"
						+ "<Date>"
						+ "<Request lang='fr'>hier</Request>"
						+ "<Action>yesterday</Action>"
						+ "</Date>"
						+ "<Date>"
						+ "<Request lang='fr'>avant hier</Request>"
						+ "<Action>before yesterday</Action>"
						+ "</Date>"
						+ "<Date>"
						+ "<Request lang='fr'>lundi</Request>"
						+ "<Action>monday</Action>"
						+ "</Date>"
						+ "<Date>"
						+ "<Request lang='fr'>mardi</Request>"
						+ "<Action>tuesday</Action>"
						+ "</Date>"
						+ "<Date>"
						+ "<Request lang='fr'>mercredi</Request>"
						+ "<Action>wednesday</Action>"
						+ "</Date>"
						+ "<Date>"
						+ "<Request lang='fr'>jeudi</Request>"
						+ "<Action>thursday</Action>"
						+ "</Date>"
						+ "<Date>"
						+ "<Request lang='fr'>vendredi</Request>"
						+ "<Action>friday</Action>"
						+ "</Date>"
						+ "<Date>"
						+ "<Request lang='fr'>samedi</Request>"
						+ "<Action>saturday</Action>"
						+ "</Date>"
						+ "<Date>"
						+ "<Request lang='fr'>dimanche</Request>"
						+ "<Action>sunday</Action>"
						+ "</Date>"
						+ "</WithingsCmd>"

						+ "<HexiwearCmd>"

						+ "<Cmd>"
						+ "<Request lang='fr'>*température*</Request>"
						+ "<Action>hexiwear,temperature</Action>"
						+ "<Response lang='fr'>la température est % </Response>"
						+ "</Cmd>"

						+ "<Cmd>"
						+ "<Request lang='fr'>*humidité*</Request>"
						+ "<Action>hexiwear,humidity</Action>"
						+ "<Response lang='fr'>l’humidité est % </Response>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='fr'>*fréquence cardiaque*</Request>"
						+ "<Action>hexiwear,heartbeat</Action>"
						+ "<Response lang='fr'>la fréquence cardiaque est % </Response>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='fr'>*pression atmosphérique*</Request>"
						+ "<Action>hexiwear,pression</Action>"
						+ "<Response lang='fr'>la pression atmosphérique est % </Response>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='fr'>*position*</Request>"
						+ "<Action>hexiwear,position</Action>"
						+ "<Response lang='fr'>les valeurs du capteur magnétomètre par rapport aux axes x y et z respectivement sont % % et % et les valeurs du gyroscope par rapport aux axes x y et z respectivement sont % % et % </Response>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='fr'>*activité*</Request>"
						+ "<Action>hexiwear,activity</Action>"
						+ "<Response lang='fr'>les valeurs du capteur d’activité par rapport aux axes x y et z respectivement sont % % et % </Response>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='fr'>*pas*</Request>"
						+ "<Action>hexiwear,steps</Action>"
						+ "<Response lang='fr'>a fait % pas</Response>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='fr'>*luminosité*</Request>"
						+ "<Action>hexiwear,light</Action>"
						+ "<Response lang='fr'>la luminosité est %</Response>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='fr'>*calorie*</Request>"
						+ "<Action>hexiwear,calories</Action>"
						+ "<Response lang='fr'>a dépensé % calories</Response>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='fr'>*batterie*</Request>"
						+ "<Action>hexiwear,battery</Action>"
						+ "<Response lang='fr'>La batterie est % </Response>"
						+ "</Cmd>"
						+ "</HexiwearCmd>"

						+ "<Call>"
						+ "<Message type='call.error' lang='fr'>Désolé, votre correspondant n'est pas connecté</Message>"
						+ "<Cmd>"
						+ "<Request lang='fr'>appeler %</Request>"
						+ "<Action>CallSkype(%)</Action>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='fr'>appeler % sur skype</Request>"
						+ "<Action> CallSkype(%) </Action>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='fr'>appeler % sur linphone</Request>"
						+ "<Action>CallLinphone(%)</Action>"
						+ "</Cmd>"
						+ "</Call>"

						+ "<Radio>"
						+ "<Param name='duration'>0</Param>"
						+ "<Message type='accueil' lang='fr'>Je vais lancer la radio *</Message>"
						+ "<Message type='error' lang='fr'>Je ne trouve pas la radio *</Message>"
						+ "<Channel name='france culture'>http://direct.franceculture.fr/live/franceculture-midfi.mp3</Channel>"
						+ "<Channel name='france info'>http://direct.franceinfo.fr/live/franceinfo-midfi.mp3</Channel>"
						+ "<Channel name='france inter'>http://direct.franceinter.fr/live/franceinter-midfi.mp3</Channel>"
						+ "<Cmd>"
						+ "<Request lang='fr'>écouter la radio*</Request>"
						+ "<Action>playRadio</Action>"
						+ "</Cmd>"
						+ "</Radio>"

						+ "<Music>"
						+ "<Param name='nbrMorceau'>1</Param>"
						+ "<Message type='accueil' lang='fr'>Je vais lancer le morceau *</Message>"
						+ "<Message type='error' lang='fr'>Je ne trouve pas de musique correspondante</Message>"
						+ "<Cmd>"
						+ "<Request lang='fr'>jouer*</Request>"
						+ "<Action>playMusic</Action>"
						+ "</Cmd>"
						+ "</Music>"

						+ "<GNews>"
						+ "<Param name='titleNbr'>2</Param>"
						+ "<Param name='GNewsUrl'>newsapi.org</Param>"
						+ "<Param name='GNewsApiKey'>dda5f00494754412b047b617eaeb833f</Param>"
						+ "<Message type='accueil' lang='fr'>Voici les dernières nouvelles de google</Message>"
						+ "<Message type='error' lang='fr'>Je n’arrive pas à lire les nouvelles de google</Message>"
						+ "<Cmd>"
						+ "<Request lang='fr'>quelles sont les nouvelles</Request>"
						+ "<Action>googleNews</Action>"
						+ "</Cmd>"
						+ "</GNews>"

						+ "<Wiki>"
						+ "<Param name='wikiUrl'>wikipedia.org</Param>"
						+ "<Message type='accueil' lang='fr'>Voici le résultat de la recherche Wikipédia pour *</Message>"
						+ "<Message type='error' lang='fr'>Je n’arrive pas à lire Wikipédia</Message>"
						+ "<Message type='errorNoResult' lang='fr'>Je ne trouve pas de résultat sur Wikipédia</Message>"
						+ "<Cmd>"
						+ "<Request lang='fr'>recherche*wiki*</Request>"
						+ "<Action>wikiSearch</Action>" + "</Cmd>" + "</Wiki>"

						+ "</VocalCmd>";

				fr.createNewFile();
				FileOutputStream fos;
				fos = new FileOutputStream(fr);
				fos.write(d.getBytes());
				fos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		File en = new File("/sdcard/SmartRobot/CmdVocale_en.xml");

		if (!en.exists()) {
			try {

				String d = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
						+ "<VocalCmd>"

						+ "<Messages>"
						+ "<Message type='Message.invitation' lang='en'>What can I do for you ?</Message>"
						+ "<Message type='Commande.rienEntendue' lang='en'>I didn't hear any order!</Message>"
						+ "<Message type='alexa.noResponce' lang='en'>No response from alexa server!</Message>"
						+ "<Message type='Commande.inconnue' lang='en'>I didn't understand your order !</Message>"
						+ "<Message type='errorDevice.noDeviceActif' lang='en'>There is no active device on your configuration</Message>"
						+ "<Message type='errorDevice.noInfo' lang='en'>The information associated with your device does not match your request</Message>"
						+ "<Message type='errorDevice.noResult' lang='en'>No information available</Message>"
						+ "<Message type='errorDevice.noInternet' lang='en'>There is no Internet connection to retrieve information from the box</Message>"
						+ "<Message type='Hexiwear.failedConnexion' lang='en'>Isn't nearby</Message>"
						+ "<Message type='error.noRobot' lang='en'>Your robot is not connected</Message>"

						+ "</Messages>"
						+ "<Devices owner='Albert'>"
						+ "<Device type='withings'>"
						+ "<State>1</State>"
						+ "<Name>withings</Name>"
						+ "<Mac></Mac>"
						+ "</Device>"
						+ "<Device type='hexiwear'>"
						+ "<State>1</State>"
						+ "<Name>montre</Name>"
						+ "<Mac>00:10:40:0B:00:1A</Mac>"
						+ "</Device>"
						+ "<Device type='hexiwear'>"
						+ "<State>1</State>"
						+ "<Name>hexiwear</Name>"
						+ "<Mac>00:3F:40:08:00:15</Mac>"
						+ "</Device>"
						+ "</Devices>"

						+ "<RobotCmd>"
						+ "<Cmd>"
						+ "<Request lang='en'>battery</Request>"
						+ "<Action>batterie</Action>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='en'>up</Request>"
						+ "<Action>HEAD U</Action>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='en'>down</Request"
						+ "><Action>HEAD D</Action>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='en'>turn your head to the left</Request>"
						+ "<Action>HEAD L</Action>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='en'>turn your head to the right</Request>"
						+ "<Action>HEAD R</Action>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='en'>stop</Request>"
						+ "<Action>MOVE;0;0</Action>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='en'>front</Request>"
						+ "<Action>MOVE;VL*50%;0</Action>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='en'>front quick</Request>"
						+ "<Action>MOVE;VL;0</Action>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='en'>front slowly</Request>"
						+ "<Action>MOVE;VL*20%;0</Action>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='en'>back</Request>"
						+ "<Action>MOVE;-VL*50%;0</Action>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='en'>back quick</Request>"
						+ "<Action>MOVE;-VL;0</Action>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='en'>back slowly</Request>"
						+ "<Action>MOVE;-VL*20%;0</Action>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='en'>left</Request>"
						+ "<Action>MOVE;0;VA*50%</Action>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='en'>left quick</Request>"
						+ "<Action>MOVE;0;VA</Action>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='fr'>gauche doucement</Request>"
						+ "<Request lang='en'>left slowly</Request>"
						+ "<Action>MOVE;0;VA*20%</Action>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='en'>right</Request>"
						+ "<Action>MOVE;0;-VA*50%</Action>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='en'>right quick</Request>"
						+ "<Action>MOVE;0;-VA</Action>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='en'>right slowly</Request>"
						+ "<Action>MOVE;0;-VA*20%</Action>"
						+ "</Cmd>"
						+ "</RobotCmd>"

						+ "<WithingsCmd>"
						+ "<Param name='withings.url'>https://wbsapi.withings.net</Param>"
						+ "<Param name='withings.ConsumerKey'>e1b33153371e2192676339e49f9e9d59ee8653fdf517c0f8c0493e78db7f5</Param>"
						+ "<Param name='withings.TokenKey'>586c113bf366ce667803b7989c3493fb63bbc76e2d5ec4c1ddebdc36</Param>"

						+ "<Cmd>"
						+ "<Request lang='en'>*step*</Request>"
						+ "<Action>withings,steps</Action>"
						+ "<Response lang='en'>did % steps</Response>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='en'>*distance*</Request>"
						+ "<Action>withings,distance</Action>"
						+ "<Response lang='en'>has traveled a distance of % metres</Response>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='en'>*elevation*</Request>"
						+ "<Action>withings,elevation</Action>"
						+ "<Response lang='en'>has climbed a hight of % metres</Response>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='en'>*calorie*</Request>"
						+ "<Action>withings,calories</Action>"
						+ "<Response lang='en' >has spent % calories</Response>"
						+ "</Cmd>"
						+ "<Date>"
						+ "<Request lang='en'>today</Request>"
						+ "<Action>today</Action>"
						+ "</Date>"
						+ "<Date>"
						+ "<Request lang='en'>yesterday</Request>"
						+ "<Action>yesterday</Action>"
						+ "</Date>"
						+ "<Date>"
						+ "<Request lang='en'>before yesterday</Request>"
						+ "<Action>before yesterday</Action>"
						+ "</Date>"
						+ "<Date>"
						+ "<Request lang='en'>monday</Request>"
						+ "<Action>monday</Action>"
						+ "</Date>"
						+ "<Date>"
						+ "<Request lang='en'>tuesday</Request>"
						+ "<Action>tuesday</Action>"
						+ "</Date>"
						+ "<Date>"
						+ "<Request lang='en'>wednesday</Request>"
						+ "<Action>wednesday</Action>"
						+ "</Date>"
						+ "<Date>"
						+ "<Request lang='en'>thursday</Request>"
						+ "<Action>thursday</Action>"
						+ "</Date>"
						+ "<Date>"
						+ "<Request lang='en'>friday</Request>"
						+ "<Action>friday</Action>"
						+ "</Date>"
						+ "<Date>"
						+ "<Request lang='en'>saturday</Request>"
						+ "<Action>saturday</Action>"
						+ "</Date>"
						+ "<Date>"
						+ "<Request lang='en'>sunday</Request>"
						+ "<Action>sunday</Action>"
						+ "</Date>"
						+ "</WithingsCmd>"

						+ "<HexiwearCmd>"
						+ "<Cmd>"
						+ "<Request lang='en'>*temperature*</Request>"
						+ "<Action>hexiwear,temperature</Action>"
						+ "<Response lang='en'>the temperature is  %</Response>"
						+ "</Cmd>"

						+ "<Cmd>"
						+ "<Request lang='en'>*humidity*</Request>"
						+ "<Action>hexiwear,humidity</Action>"
						+ "<Response lang='en'>the Humidity is % </Response>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='en'>*cardiac frequency*</Request>"
						+ "<Action>hexiwear,heartbeat</Action>"
						+ "<Response lang='en'>the cardiac frequency is % </Response>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='en'>*atmospheric pressure*</Request>"
						+ "<Action>hexiwear,pression</Action>"
						+ "<Response lang='en'>the atmospheric pressure is % </Response>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='en'>*position*</Request>"
						+ "<Action>hexiwear,position</Action>"
						+ "<Response lang='en'>the values of the magnetometer sensor with respect to the x y and z axes respectively are % % and % and the values of the gyro with respect to the x y and z axes respectively are % % and%</Response>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='en'>*activity*</Request>"
						+ "<Action>hexiwear,activity</Action>"
						+ "<Response lang='en'>the values of the activity sensor with respect to the axes x y and z respectively are % % and%</Response>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='en'>*steps*</Request>"
						+ "<Action>hexiwear,steps</Action>"
						+ "<Response lang='en'>did % steps</Response>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='en'>*brightness*</Request>"
						+ "<Action>hexiwear,light</Action>"
						+ "<Response lang='en'>the brightness is %</Response>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='en'>*calorie*</Request>"
						+ "<Action>hexiwear,calories</Action>"
						+ "<Response lang='en'>has spent % calories</Response>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='en'>*battery*</Request>"
						+ "<Action>hexiwear,battery</Action>"
						+ "<Response lang='en'>The battery is % </Response>"
						+ "</Cmd>"
						+ "</HexiwearCmd >"

						+ "<Call>"
						+ "<Message type='call.error' lang='en'>Sorry, your correspondent is not logged in</Message>"
						+ "<Cmd>"
						+ "<Request lang='en'>call %</Request>"
						+ "<Action>CallSkype(%)</Action>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='en'>call % on skype</Request>"
						+ "<Action> CallSkype(%) </Action>"
						+ "</Cmd>"
						+ "<Cmd>"
						+ "<Request lang='en'>call % on linphone</Request>"
						+ "<Action>CallLinphone(%)</Action>"
						+ "</Cmd>"
						+ "</Call>"

						+ "<Radio>"
						+ "<Param name='duration'>0</Param>"
						+ "<Message type='accueil' lang='en'>I am starting the radio</Message>"
						+ "<Message type='error' lang='en'>I can not find radio *</Message>"
						+ "<Channel name='france culture'>http://direct.franceculture.fr/live/franceculture-midfi.mp3</Channel>"
						+ "<Channel name='france info'>http://direct.franceinfo.fr/live/franceinfo-midfi.mp3</Channel>"
						+ "<Channel name='france inter'>http://direct.franceinter.fr/live/franceinter-midfi.mp3</Channel>"
						+ "<Cmd>"
						+ "<Request lang='en'>listen to radio*</Request>"
						+ "<Action>playRadio</Action>"
						+ "</Cmd>"
						+ "</Radio>"

						+ "<Music>"
						+ "<Param name='nbrMorceau'>1</Param>"
						+ "<Message type='accueil' lang='en'>I am starting music file *</Message>"
						+ "<Message type='error' lang='en'>No matching music found</Message>"
						+ "<Cmd>"
						+ "<Request lang='en'>play*</Request>"
						+ "<Action>playMusic</Action>"
						+ "</Cmd>"
						+ "</Music>"

						+ "<GNews>"
						+ "<Param name='titleNbr'>2</Param>"
						+ "<Param name='GNewsUrl'>newsapi.org</Param>"
						+ "<Param name='GNewsApiKey'>dda5f00494754412b047b617eaeb833f</Param>"
						+ "<Message type='accueil' lang='en'>Here are the latest news from google</Message>"
						+ "<Message type='error' lang='en'>I can not read the news from google</Message>"
						+ "<Cmd>"
						+ "<Request lang='en'>what are the news</Request>"
						+ "<Action>googleNews</Action>"
						+ "</Cmd>"
						+ "</GNews>"

						+ "<Wiki>"
						+ "<Param name='wikiUrl'>wikipedia.org</Param>"
						+ "<Message type='accueil' lang='en'>Here are the result in wikipedia for *</Message>"
						+ "<Message type='error' lang='en'>I can not read result from wikipedia</Message>"
						+ "<Message type='errorNoResult' lang='fr'>No result found in wikipedia</Message>"
						+ "<Cmd>" + "<Request lang='en'>search*wiki*</Request>"
						+ "<Action>wikiSearch</Action>" + "</Cmd>" + "</Wiki>"

						+ "</VocalCmd>";

				en.createNewFile();
				FileOutputStream fos;
				fos = new FileOutputStream(en);
				fos.write(d.getBytes());
				fos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

	}

	public static String translate(String text) {
		String translated = null;
		try {
			String query = URLEncoder.encode(text, "UTF-8");
			String langpair = URLEncoder.encode(Locale.FRENCH + "|"
					+ Locale.ENGLISH, "UTF-8");
			String url = "http://mymemory.translated.net/api/get?q=" + query
					+ "&langpair=" + langpair;
			HttpClient hc = new DefaultHttpClient();
			HttpGet hg = new HttpGet(url);
			HttpResponse hr = hc.execute(hg);
			if (hr.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				JSONObject response = new JSONObject(EntityUtils.toString(hr
						.getEntity()));
				translated = response.getJSONObject("responseData").getString(
						"translatedText");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return translated;
	}

	public static String getDevicesOwner(Node device) {
		// TODO Auto-generated method stub
		return device.getAttributes().getNamedItem("owner").getNodeValue();
	}

	public static void creerCommandRobotDossier() {
		File file1 = new File(pathOfMyFile, "CommandeRobot");
		if (!file1.exists()) {
			file1.mkdir();
		}
	}
}
