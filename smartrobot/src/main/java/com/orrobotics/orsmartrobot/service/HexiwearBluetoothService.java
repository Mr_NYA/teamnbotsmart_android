package com.orrobotics.orsmartrobot.service;

import android.annotation.SuppressLint;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.TimeZone;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.hexiwear.Characteristic;
import com.orrobotics.orsmartrobot.hexiwear.DataConverter;
import com.orrobotics.orsmartrobot.hexiwear.ManufacturerInfo;
import com.orrobotics.orsmartrobot.hexiwear.Mode;
import com.orrobotics.orsmartrobot.util.RemoteControlUtil;

@SuppressLint("NewApi")
public class HexiwearBluetoothService extends Service {

	private static final String TAG = HexiwearBluetoothService.class
			.getSimpleName();

	private final IBinder mBinder = new LocalBinder();

	// Alert In commands
	private static final byte WRITE_NOTIFICATION = 1;
	private static final byte WRITE_TIME = 3;

	private static final Map<String, BluetoothGattCharacteristic> readableCharacteristics = new HashMap<String, BluetoothGattCharacteristic>();
	private static final ManufacturerInfo manufacturerInfo = new ManufacturerInfo();
	private static final Queue<String> readingQueue = new ArrayBlockingQueue<String>(
			12);
	private static final Queue<byte[]> notificationsQueue = new LinkedBlockingDeque<byte[]>();

	private volatile boolean shouldUpdateTime;
	private volatile boolean isConnected;
	private BluetoothDevice bluetoothDevice;
	private BluetoothGattCharacteristic alertIn;
	private BluetoothGatt bluetoothGatt;
	private Mode mode;
	Boolean stop = false;
	SmartRobotApplication app;
	public static final String DATA_AVAILABLE = "dataAvailable";
	public static final String CONNECTED = "connected";
	public static final String READING_TYPE = "readingType";
	public static final String STRING_DATA = "stringData";
	BluetoothAdapter mBluetoothAdapter;
	Boolean flagScaning = false;
	Handler handler = new Handler();
	String address = null;

	public class LocalBinder extends Binder {
		public HexiwearBluetoothService getService() {
			return HexiwearBluetoothService.this;
		}
	}

	public void initialize() {
		stop = false;
		app = (SmartRobotApplication) getApplicationContext();
		app.setEditor(RemoteControlUtil.MAC_ADDRESS_HEX_BOUND, "");
		app.setEditor(RemoteControlUtil.MAC_ADDRESS_HEX_BOUND,
				"00:10:40:0B:00:1A;00:3F:40:08:00:15");
		address = app.getMacAdressHexiwear();
		registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
		final BluetoothManager btManager = (BluetoothManager) getApplicationContext()
				.getSystemService(Context.BLUETOOTH_SERVICE);
		mBluetoothAdapter = btManager.getAdapter();
		if (isBluetoothLeSupported()
				&& (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled())) {
			mBluetoothAdapter.enable();
		}
		flagScaning = true;
		mBluetoothAdapter.startLeScan(mLeScanCallback);

		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				if (flagScaning == true)
					app.notifyObservers("hexiwearError");
				mBluetoothAdapter.stopLeScan(mLeScanCallback);
			}
		}, 20000);

	}

	public BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
		@Override
		public void onLeScan(final BluetoothDevice device, int rssi,
							 byte[] scanRecord) {
			Log.i(TAG, "device " + device.getAddress());
			if (device.getAddress().toString().equals(address)) {
				flagScaning = false;
				mBluetoothAdapter.stopLeScan(this);
				if (app.getMacHexBound().contains(device.getAddress())) {
					createGATT(device);
				} else {
					device.createBond();
				}

			}
		}
	};

	public boolean isBluetoothLeSupported() {
		return getApplicationContext().getPackageManager().hasSystemFeature(
				PackageManager.FEATURE_BLUETOOTH_LE);
	}

	private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			final BluetoothDevice device = intent
					.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
			final int bondState = intent.getIntExtra(
					BluetoothDevice.EXTRA_BOND_STATE, -1);
			final int previousBondState = intent.getIntExtra(
					BluetoothDevice.EXTRA_PREVIOUS_BOND_STATE, -1);
			final String action = intent.getAction();
			Log.d(TAG, "Bond state changed for: " + device.getAddress()
					+ " new state: " + bondState + " previous: "
					+ previousBondState + " action: " + action);

			if (bondState == BluetoothDevice.BOND_BONDED) {
				Log.i(TAG, "Bonded");
				String address = app.getMacHexBound() + ";"
						+ device.getAddress();
				app.setEditor(RemoteControlUtil.MAC_ADDRESS_HEX_BOUND, address);
				createGATT(device);

			} else if (bondState == BluetoothDevice.BOND_NONE) {
				Log.i(TAG, "BOND_NONEBOND_NONEBOND_NONEBOND_NONE");
				device.createBond();
			}
		}
	};

	private static IntentFilter makeGattUpdateIntentFilter() {
		final IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
		return intentFilter;
	}

	void onStopCommand() {
		Log.i(TAG, "Stop command received.");
		stopForeground(true);
		stopSelf();
	}

	@Override
	public void onDestroy() {
		stop();

	}

	public void stop() {
		Log.i(TAG, "Stopping service...");
		if (stop == false) {
			if (bluetoothGatt != null) {
				bluetoothGatt.close();

			}
			if (flagScaning == true)
				mBluetoothAdapter.stopLeScan(mLeScanCallback);
			unregisterReceiver(mGattUpdateReceiver);
			Log.d(TAG, "onDestroy: sending intent that bt service stopped");
			stopSelf();
			stop = true;
		}
	}

	public void createGATT(final BluetoothDevice device) {
		bluetoothGatt = device.connectGatt(this, true,
				new BluetoothGattCallback() {
					@Override
					public void onConnectionStateChange(BluetoothGatt gatt,
														int status, int newState) {

						bluetoothDevice = device;
						isConnected = BluetoothProfile.STATE_CONNECTED == newState;
						if (isConnected) {
							Log.i(TAG, "GATT connected.");
							final Intent connected = new Intent(CONNECTED);

							sendBroadcast(connected);
							app.notifyObservers("start");
							gatt.discoverServices();
						} else {
							Log.i(TAG, "GATT disconnected.");
							gatt.connect();
						}

					}

					@Override
					public void onServicesDiscovered(BluetoothGatt gatt,
													 int status) {
						Log.i(TAG, "Services discovered.");
						if (status == BluetoothGatt.GATT_INSUFFICIENT_AUTHENTICATION) {
							handleAuthenticationError(gatt);
							return;
						}

						discoverCharacteristics(gatt);
					}

					@Override
					public void onCharacteristicWrite(BluetoothGatt gatt,
													  BluetoothGattCharacteristic characteristic,
													  int status) {
						// Log.i(TAG, "Characteristic written: " + status);

						if (status == BluetoothGatt.GATT_INSUFFICIENT_AUTHENTICATION) {
							handleAuthenticationError(gatt);
							return;
						}

						final byte command = characteristic.getValue()[0];
						switch (command) {
							case WRITE_TIME:
								Log.i(TAG, "Time written.");

								final BluetoothGattCharacteristic batteryCharacteristic = readableCharacteristics
										.get(Characteristic.BATTERY.getUuid());
								gatt.setCharacteristicNotification(
										batteryCharacteristic, true);

								for (BluetoothGattDescriptor descriptor : batteryCharacteristic
										.getDescriptors()) {

									if (descriptor.getUuid().toString()
											.startsWith("00002904")) {

										descriptor
												.setValue(BluetoothGattDescriptor.ENABLE_INDICATION_VALUE);
										gatt.writeDescriptor(descriptor);
									}
								}
								break;
							case WRITE_NOTIFICATION:

								Log.i(TAG, "Notification sent.");
								if (notificationsQueue.isEmpty()) {
									Log.i(TAG, "Reading characteristics...");
									readNextCharacteristics(gatt);
								} else {
									Log.i(TAG, "writing next notification...");
									alertIn.setValue(notificationsQueue.poll());
									gatt.writeCharacteristic(alertIn);
								}
								break;
							default:
								Log.w(TAG, "No such ALERT IN command: " + command);
								break;
						}
					}

					@Override
					public void onDescriptorWrite(BluetoothGatt gatt,
												  BluetoothGattDescriptor descriptor, int status) {
						readCharacteristic(gatt, Characteristic.MANUFACTURER);
					}

					@Override
					public void onCharacteristicRead(
							BluetoothGatt gatt,
							final BluetoothGattCharacteristic gattCharacteristic,
							int status) {
						if (status == BluetoothGatt.GATT_INSUFFICIENT_AUTHENTICATION) {
							handleAuthenticationError(gatt);
							return;
						}

						final String characteristicUuid = gattCharacteristic
								.getUuid().toString();
						final Characteristic characteristic = Characteristic
								.byUuid(characteristicUuid);
						switch (characteristic) {
							case MANUFACTURER:
								manufacturerInfo.manufacturer = gattCharacteristic
										.getStringValue(0);
								readCharacteristic(gatt, Characteristic.FW_REVISION);
								break;
							case FW_REVISION:
								manufacturerInfo.firmwareRevision = gattCharacteristic
										.getStringValue(0);
								readCharacteristic(gatt, Characteristic.MODE);
								break;
							default:

								if (characteristic == Characteristic.MODE) {
									final Mode newMode = Mode
											.bySymbol(gattCharacteristic.getValue()[0]);
									if (mode != newMode) {
										onModeChanged(newMode);
									}
								} else {
									onBluetoothDataReceived(characteristic,
											gattCharacteristic.getValue());
								}
								if (shouldUpdateTime) {
									updateTime();
								}

								if (notificationsQueue.isEmpty()) {
									readNextCharacteristics(gatt);
								} else {
									alertIn.setValue(notificationsQueue.poll());
									gatt.writeCharacteristic(alertIn);
								}

								break;
						}
					}

					@Override
					public void onCharacteristicChanged(BluetoothGatt gatt,
														BluetoothGattCharacteristic gattCharacteristic) {
						final String characteristicUuid = gattCharacteristic
								.getUuid().toString();
						final Characteristic characteristic = Characteristic
								.byUuid(characteristicUuid);
						Log.d(TAG, "Characteristic changed: " + characteristic);

						if (characteristic == Characteristic.BATTERY) {
							onBluetoothDataReceived(Characteristic.BATTERY,
									gattCharacteristic.getValue());
						}
					}
				});
	}

	private void onModeChanged(final Mode newMode) {
		Log.i(TAG, "Mode changed. New mode is: " + mode);
		mode = newMode;
		setReadingQueue();

	}

	private void setReadingQueue() {
		readingQueue.clear();
		readingQueue.add(Characteristic.MODE.name());
		final List<String> enabledPreferences = getEnabledPreferences(bluetoothDevice
				.getAddress());
		for (String characteristic : enabledPreferences) {
			if (mode.hasCharacteristic(characteristic)) {
				readingQueue.add(characteristic);
			}
		}
	}

	public Map<String, Boolean> getDisplayPreferences(String deviceAddress) {
		final Map<String, Boolean> displayPrefs = new HashMap<>();

		final List<Characteristic> readings = Characteristic.getReadings();
		for (Characteristic reading : readings) {
			displayPrefs.put(reading.name(), true);
		}

		return displayPrefs;
	}

	public List<String> getEnabledPreferences(String deviceAddress) {
		final List<String> enabledPrefs = new ArrayList<>();

		final Map<String, Boolean> displayPreferences = getDisplayPreferences(deviceAddress);
		for (Map.Entry<String, Boolean> entry : displayPreferences.entrySet()) {
			if (entry.getValue()) {
				enabledPrefs.add(entry.getKey());
			}
		}

		return enabledPrefs;
	}

	private void onBluetoothDataReceived(final Characteristic type,
										 final byte[] data) {
		if (type != Characteristic.BATTERY) {
			DataConverter.formatForPublushing(type, data);

		}

		final Intent dataRead = new Intent(DATA_AVAILABLE);
		dataRead.putExtra(READING_TYPE, type.getUuid());
		dataRead.putExtra(STRING_DATA,
				DataConverter.parseBluetoothData(type, data));
		DataConverter.parseBluetoothData(type, data);
		sendBroadcast(dataRead);
	}

	void readNextCharacteristics(final BluetoothGatt gatt) {
		final String characteristicUuid = readingQueue.poll();
		readingQueue.add(characteristicUuid);
		readCharacteristic(gatt, Characteristic.valueOf(characteristicUuid));
	}

	private void readCharacteristic(final BluetoothGatt gatt,
									final Characteristic characteristic) {
		if (!isConnected) {
			return;
		}

		final BluetoothGattCharacteristic gattCharacteristic = readableCharacteristics
				.get(characteristic.getUuid());
		if (gattCharacteristic != null) {
			gatt.readCharacteristic(gattCharacteristic);
		}
	}

	private void discoverCharacteristics(final BluetoothGatt gatt) {
		if (gatt.getServices().size() == 0) {
			Log.i(TAG, "No services found.");
		}

		for (BluetoothGattService gattService : gatt.getServices()) {
			storeCharacteristicsFromService(gattService);
		}

	}

	private void storeCharacteristicsFromService(
			BluetoothGattService gattService) {
		for (BluetoothGattCharacteristic gattCharacteristic : gattService
				.getCharacteristics()) {
			final String characteristicUuid = gattCharacteristic.getUuid()
					.toString();

			final Characteristic characteristic = Characteristic
					.byUuid(characteristicUuid);

			if (characteristic == Characteristic.ALERT_IN) {
				Log.d(TAG, "ALERT_IN DISCOVERED");
				alertIn = gattCharacteristic;
				setTime();
				updateTime();
			} else if (characteristic != null) {
				Log.v(TAG,
						characteristic.getType() + ": " + characteristic.name()
								+ " " + gattCharacteristic);
				readableCharacteristics.put(characteristicUuid,
						gattCharacteristic);
			} else {
				Log.v(TAG, "UNKNOWN: " + characteristicUuid);
			}
		}
	}

	public void setTime() {
		Log.d(TAG, "Setting time...");
		if (!isConnected || alertIn == null) {
			Log.w(TAG, "Time not set.");
			return;
		}

		shouldUpdateTime = true;
	}

	void updateTime() {
		shouldUpdateTime = false;

		final byte[] time = new byte[20];
		final long currentTime = System.currentTimeMillis();
		final long currentTimeWithTimeZoneOffset = (currentTime + TimeZone
				.getDefault().getOffset(currentTime)) / 1000;

		final ByteBuffer buffer = ByteBuffer.allocate(8);
		buffer.order(ByteOrder.LITTLE_ENDIAN).asLongBuffer()
				.put(currentTimeWithTimeZoneOffset);
		final byte[] utcBytes = buffer.array();

		final byte length = 0x04;

		time[0] = WRITE_TIME;
		time[1] = length;
		time[2] = utcBytes[0];
		time[3] = utcBytes[1];
		time[4] = utcBytes[2];
		time[5] = utcBytes[3];

		alertIn.setValue(time);
		alertIn.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
		bluetoothGatt.writeCharacteristic(alertIn);

	}

	public boolean isConnected() {
		return isConnected;
	}

	public Mode getCurrentMode() {
		return mode;
	}

	public BluetoothDevice getCurrentDevice() {
		return bluetoothDevice;
	}

	private void handleAuthenticationError(final BluetoothGatt gatt) {
		gatt.close();
		gatt.getDevice().createBond();
	}

	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}

	public ManufacturerInfo getManufacturerInfo() {
		return manufacturerInfo;
	}

	@Override
	public boolean onUnbind(Intent intent) {
		stop();
		return super.onUnbind(intent);
	}

}
