package com.orrobotics.orsmartrobot.service;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import com.orrobotics.linphone.LinphoneRegistration;
import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.observer.OrSmarRobotObserver;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

/**
 * 
 * @author Hajiba IFRAH
 * @version 2.1.0
 * @date 18/05/2015
 * 
 */

public class LinphoneService extends Service {

	static SmartRobotApplication smartrobot;
	private static Timer mAutoConnTimer;
	private static Handler hand = new Handler();
	static Context context;

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressLint("NewApi")
	@Override
	public void onCreate() {
		super.onCreate();
		context = this.getApplicationContext();
		smartrobot = (SmartRobotApplication) context;
		if (android.os.Build.VERSION.SDK_INT > 9) {
			smartrobot.stricMode();
		}

		if (smartrobot.isConnectedToInternet()) {
			smartrobot.initRegistration();
			smartrobot.registration();
		}
	}

	public static void startAutoRegistrationTimer() {
		// TODO Auto-generated method stub
		int conn_period = Integer.parseInt(SmartRobotApplication
				.getParamFromFile("autoConnexion.server",
						"Configuration.properties",null).split(" ")[0]);
		if (mAutoConnTimer == null) {
			mAutoConnTimer = new Timer();
		} else {
			mAutoConnTimer.cancel();
			mAutoConnTimer = null;
			mAutoConnTimer = new Timer();
		}
		mAutoConnTimer.scheduleAtFixedRate(new TimerTask() {

			@Override
			public void run() {
				// TODO Auto-generated method stub

				hand.post(new Runnable() {

					@Override
					public void run() {
						smartrobot.setAutoRegistartionOn(true);
						// TODO Auto-generated method stub

						if (smartrobot.isRegisted() == false && smartrobot.isConnectedToInternet() && smartrobot.isEnoughBandWith()) {

							if (LinphoneRegistration.lc != null)
								LinphoneRegistration.lc.refreshRegisters();
							else {
								smartrobot.initRegistration();
								smartrobot.registration();
							}

						}
					}
				});

			}
		}, 1000, conn_period * 1000);
	}

	public void cancelAutoRegistration() {
		// TODO Auto-generated method stub
		if (mAutoConnTimer != null) {
			mAutoConnTimer.cancel();
			mAutoConnTimer = null;
		}
		smartrobot.setAutoRegistartionOn(false);

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		cancelAutoRegistration();
		// Deconnexion du serveur sip
		smartrobot.stopMainLoop();
	}

}