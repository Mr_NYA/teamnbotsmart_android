package com.orrobotics.orsmartrobot.service;

import java.io.IOException;
import java.util.Collection;
import java.util.Timer;
import java.util.TimerTask;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManager;
import org.jivesoftware.smack.ChatManagerListener;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.ConnectionConfiguration.SecurityMode;
import org.jivesoftware.smack.filter.MessageTypeFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smackx.MessageEventManager;
import org.jivesoftware.smackx.MessageEventRequestListener;
import org.jivesoftware.smackx.provider.MessageEventProvider;
import org.jivesoftware.smackx.receipts.DeliveryReceipt;
import org.jivesoftware.smack.XMPPException;

import com.orrobotics.linphone.LinphoneRegistration;
import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.observer.OrSmarRobotObserver;
import com.orrobotics.orsmartrobot.util.GestionDesFichiers;
import com.orrobotics.orsmartrobot.util.RemoteControlUtil;
import com.orrobotics.orsmartrobot.view.ControlRobot;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

/**
 *
 * @author Hajiba IFRAH
 * @version 2.1.0
 * @date 18/05/2015
 *
 */

public class ServiceRobot extends Service implements OrSmarRobotObserver {

	SmartRobotApplication smartrobot;

	XMPPConnection connection;
	public static Roster roster;
	static ChatManager chatManager;

	String message;
	String controler;

	Boolean flagClose = false;
	String messagetoSend = null;
	Timer remoteTimer;
	Boolean isconnected1 = false;
	Boolean islogin = false;
	Boolean isFirstTry = false;
	int conn_period=10000;
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressLint("NewApi")
	@Override
	public void onCreate() {
		super.onCreate();
		smartrobot = (SmartRobotApplication) this.getApplicationContext();
		smartrobot.registerObserver(this);
		if (android.os.Build.VERSION.SDK_INT > 9) {
			smartrobot.stricMode();
		}
		flagClose = false;
		String data =SmartRobotApplication
				.getParamFromFile("autoConnexion.server",
						"Configuration.properties", null).split(" ")[0];

		if(data!=null){
			conn_period = Integer.parseInt(data);
		}

		if (smartrobot.isConnectedToInternet()) {
			remoteTimer=new Timer();
			remoteTimer.scheduleAtFixedRate(new OpenFireRemote(), 1,
					1000);


		}
	}
	private class OpenFireRemote extends TimerTask {

		public OpenFireRemote() {
			// TODO Auto-generated constructor stub
		}

		@Override
		public void run() {
			if(isFirstTry) {
				connectToOpenFire();
				isFirstTry=true;
			}
			else {
				if (smartrobot.isConnectedToInternet() && smartrobot.isEnoughBandWith())
					connectToOpenFire();

			}
		}
	}

	public void connectToOpenFire(){
		if (isconnected1 == false) {

			isconnected1 = init();
			GestionDesFichiers.log(
					"connexion avec le serveur open fire init()", ""
							+ isconnected1, "Log_SmartRobot");

		}
		else if (islogin == false) {


			init();
			islogin = login();
			GestionDesFichiers.log(
					"connexion avec le serveur open fire login()", ""
							+ islogin, "Log_SmartRobot");

		}else{
			smartrobot.setEditor(RemoteControlUtil.CONNECTED_TO_SERVER,
					"true");
			smartrobot.setOpenFireConnected(true);

			onAccountCreationSuccess();
			if(remoteTimer!=null)
				remoteTimer.cancel();
		}
	}

	/**
	 * Connexion avec le serveur open fire
	 */
	public boolean init() {
		// TODO Auto-generated method stub

		ConnectionConfiguration config;
		config = new ConnectionConfiguration(
				SmartRobotApplication.getParamFromFile("server",
						"configuration.properties", null),
				RemoteControlUtil.SERVER_PORT);
		config.setSASLAuthenticationEnabled(false);
		config.setRosterLoadedAtLogin(false);
		config.setSecurityMode(SecurityMode.disabled);
		config.setDebuggerEnabled(true);

		connection = new XMPPConnection(config);
		try {
			connection.connect();

		} catch (XMPPException e) {
			GestionDesFichiers.log(
					"erreur connexion avec le serveur open fire login()", ""
							+ e.getMessage().toString(), "Log_SmartRobot");

			e.printStackTrace();
			return false;
		} catch (NullPointerException e1) {
			e1.printStackTrace();
			GestionDesFichiers.log(
					"erreur connexion avec le serveur open fire init()", "",
					"Log_SmartRobot");

			return false;

		} catch (Exception e2) {
			return false;
		}

		if (connection != null)
			return connection.isConnected();
		else
			return false;
	}

	/**
	 * Se conencter avec open fire avec un compte
	 */
	public boolean login() {
		// TODO Auto-generated method stub
		try {
			String pseudo = smartrobot.getPseudoOpenFire();
			String password = smartrobot.getPasswordOpenFire();

			connection.login(pseudo, password);

			Presence presence = new Presence(Presence.Type.available);
			connection.sendPacket(presence);
			roster = connection.getRoster();
			roster.setSubscriptionMode(Roster.SubscriptionMode.accept_all);
			Roster.setDefaultSubscriptionMode(Roster.SubscriptionMode.manual);

			String jid = pseudo + "@" + smartrobot.getAdresseOpenFire();
			roster.createEntry(jid, jid, null);
			Presence pres = new Presence(Presence.Type.subscribe);
			pres.setFrom(jid);
			connection.sendPacket(pres);

		} catch (XMPPException e) {
			e.printStackTrace();

			GestionDesFichiers.log("ErrorOpenfireLogin", e.getMessage()
					.toString(), "Log_SmartRobot");
			return false;

		} catch (NullPointerException e1) {
			e1.printStackTrace();

			return false;

		}catch (java.lang.IllegalStateException e2) {
			GestionDesFichiers.log("ErrorOpenfireLogin", e2.getMessage()
					.toString(), "Log_SmartRobot");
			return false;

		} catch (Exception e5) {

			return false;

		}

		try {
			return connection.isAuthenticated();
		} catch (Exception e5) {
			return false;

		}

	}

	/**
	 * Traitement dans le cas d'une connexion réussi
	 */
	private void onAccountCreationSuccess() {
		// TODO Auto-generated method stub
		Intent linphoneService = new Intent(smartrobot.getApplicationContext(),
				LinphoneService.class);
		if (smartrobot
				.isServiceRunning("com.orrobotics.orsmartrobot.service.LinphoneService")) {

			stopService(linphoneService);
		}
		startService(linphoneService);
		ProviderManager pm = ProviderManager.getInstance();
		pm.addExtensionProvider("x", "jabber:x:event",
				new MessageEventProvider());
		if (connection != null) {
			chatManager = connection.getChatManager();
			roster = connection.getRoster();
			roster.setSubscriptionMode(Roster.SubscriptionMode.accept_all);

			PacketFilter filter = new MessageTypeFilter(Message.Type.chat);

			connection.addPacketListener(new PacketListener() {
				@Override
				public void processPacket(Packet packet) {
					Packet received = new Message();

					received.addExtension(new DeliveryReceipt(packet
							.getPacketID()));

					received.setTo(packet.getFrom());

					connection.sendPacket(received);
				}
			}, filter);
			MessageEventManager eventManager = new MessageEventManager(
					connection);
			eventManager
					.addMessageEventRequestListener(new MessageEventRequestListener() {
						@Override
						public void offlineNotificationRequested(String from,
																 String packetID,
																 MessageEventManager messageEventManager) {

						}

						@Override
						public void displayedNotificationRequested(String from,
																   String packetID,
																   MessageEventManager messageEventManager) {
							Log.d("INFO ",
									"EventRequest displayedNotificationRequested1==> "
											+ from + packetID);

						}

						@Override
						public void deliveredNotificationRequested(String from,
																   String packetID,
																   MessageEventManager messageEventManager) {

							messageEventManager.sendDeliveredNotification(from,
									packetID);

						}

						@Override
						public void composingNotificationRequested(String from,
																   String packetID,
																   MessageEventManager messageEventManager) {

						}
					});
			connection.addConnectionListener(new ConnectionListener() {
                @Override
                public void connectionClosed() {
                    GestionDesFichiers.log("presenceChanged", "----xmpp connectionClosed----",
                            "Log_SmartRobot");
                    xmppConnexionClosed();
                }

                @Override
                public void connectionClosedOnError(Exception e) {
                    GestionDesFichiers.log("presenceChanged", "----xmpp connectionClosed on error ="+e.getMessage(),
                            "Log_SmartRobot");
                    GestionDesFichiers.log("presenceChanged", "----xmpp connectionClosed on error----flagClose="+flagClose+"--isConnnectedToInternet="+smartrobot.isConnectedToInternet(),
                            "Log_SmartRobot");
                    xmppConnexionClosed();
                }

                @Override
                public void reconnectingIn(int i) {

                }

                @Override
                public void reconnectionSuccessful() {

                }

                @Override
                public void reconnectionFailed(Exception e) {

                }
            });

			roster.addRosterListener(new RosterListener() {
				@Override
				public void presenceChanged(Presence from) {

					if (!from.isAvailable()) {
						GestionDesFichiers.log("presenceChanged", from
										.getFrom().toString() + " " + from,
								"Log_SmartRobot");

						if (from.getFrom().split("@")[0].equals(smartrobot
								.getPseudoOpenFire())) {

							xmppConnexionClosed();

						}
					}
				}
				@Override
				public void entriesAdded(Collection<String> arg0) {
					// TODO Auto-generated method stub

				}

				@Override
				public void entriesDeleted(Collection<String> arg0) {
					// TODO Auto-generated method stub

				}

				@Override
				public void entriesUpdated(Collection<String> arg0) {
					// TODO Auto-generated method stub

				}

			});

			chatManager.addChatListener(new ChatManagerListener() {
				@Override
				public void chatCreated(Chat arg0, boolean arg1) {
					arg0.addMessageListener(new MessageListener() {
						@Override
						public void processMessage(Chat arg0, Message arg1) {
							message = arg1.getBody();
							controler = arg1.getFrom();
							GestionDesFichiers.log("INFO", message + " "
									+ controler, "Log_SmartRobot");
							GestionDesFichiers.log("INFO", "Message reçu ="
											+ message + " From =" + controler,
									"Log_MessageXMPP");
							if (message != null) {
								if (controler != null)
									smartrobot
											.setPseudoOpenFireSender(controler);

								smartrobot.setChatEnabled(true);

								if (RemoteControlUtil.XMPP_SKYPE
										.compareTo(message) == 0) {

									smartrobot.setEditor(
											RemoteControlUtil.CALL_TYPE,
											"skype");
									smartrobot.setCallType();
									smartrobot.notifyObservers("displaySkype");
									smartrobot.lancer_app("com.skype.raider");
								}else if(message.equals("istelepresence")){
									if(smartrobot.isEnoughBandWith())
										messagetoSend = "startCall;goodBandWith";
									else
										messagetoSend = "startCall;badBandWith";

									sendMessage(controler, messagetoSend);

								} else if (message.contains("test")) {
									String[] data = message.split(";");

									if (message.contains("true")) {
										smartrobot
												.notifyObservers("back-end");
										if (SmartRobotApplication
												.isControlVisible())
											smartrobot
													.notifyObservers("istelepresence");


										smartrobot.setTelepresenceControl(true);

									}

									else
										smartrobot
												.setTelepresenceControl(false);

									smartrobot.setEditor(
											RemoteControlUtil.REMOTE_CONTROL,
											"ON");

									smartrobot.notifyObservers("test");
									controler = arg1.getFrom();

									if (LinphoneRegistration.lc != null
											&& !smartrobot
											.isAutoRegistrationOn()) {
										LinphoneService
												.startAutoRegistrationTimer();
									}
									else{

										if (LinphoneRegistration.lc != null)
											LinphoneRegistration.lc.refreshRegisters();
									}

									if (!smartrobot.getRobotConnexion().equals(
											"true")) {

										if(smartrobot.isEnoughBandWith())
											messagetoSend = "test norobot goodBandWith";
										else
											messagetoSend = "test norobot badBandWith";
									} else {
										if(smartrobot.isEnoughBandWith())
											messagetoSend = "test OKCONTROL goodBandWith";
										else
											messagetoSend = "test OKCONTROL badBandWith";
									}

									smartrobot.unlockScreen();
									sendMessage(controler, messagetoSend);

								} else if (arg1.getBody().equals(
										"controlViewStarted")) {
									smartrobot
											.notifyObservers("controlViewStarted");
								}

								else if (arg1.getBody().equals(
										RemoteControlUtil.XMPP_END)) {
									smartrobot.setEditor(
											RemoteControlUtil.REMOTE_CONTROL,
											"OFF");
									smartrobot.setChatEnabled(false);
									smartrobot.setTelepresenceControl(false);
									smartrobot.notifyObservers("MOVE;0;0");
									smartrobot.notifyObservers("screenOff");
									controler = null;
									smartrobot
											.setPseudoOpenFireSender(controler);

								}

								else if (arg1.getBody().contains(
										"robotConnexion")) {

									if (smartrobot.getRobotConnexion().equals(
											"true")) {

										sendMessage(controler, "robot;true;"
												+ smartrobot.getPseudoSkype());
									} else {
										sendMessage(controler, "robot;false;"
												+ smartrobot.getPseudoSkype());
									}

								} else if (arg1.getBody().contains(
										"connexionRobot")) {
									if (smartrobot.getRobotConnexion().equals(
											"false"))
										smartrobot
												.notifyObservers("testConnectionLognIn");

								} else if (arg1.getBody().contains(
										"launchControl")) {

									if (!SmartRobotApplication
											.isControlVisible())

										smartrobot.StartAct(null, null,
												ControlRobot.class,
												getApplicationContext());
								} else if (arg1.getBody().contains(
										"disableCamera")) {
									smartrobot.notifyObservers("disableCamera");

								} else if (arg1.getBody().contains(
										"enableCamera")) {
									smartrobot.notifyObservers("enableCamera");

								}

								else if (arg1.getBody()
										.contains("switchCamera")) {
									smartrobot.notifyObservers("switchCamera");

								} else if (arg1.getBody().contains(
										"robotVideoSize")
										|| arg1.getBody().contains(
										"echoCancelation")
										|| arg1.getBody().contains("volume")) {
									smartrobot.notifyObservers(arg1.getBody());

								}

								else if (arg1.getBody()
										.contains("disableMicro")) {
									smartrobot.notifyObservers("disableMicro");

								} else if (arg1.getBody().contains(
										"enableMicro")) {
									smartrobot.notifyObservers("enableMicro");

								} else if (arg1.getBody().contains(
										"terminateCall")) {

									smartrobot.notifyObservers("terminateCall");

								} else {
									smartrobot.notifyObservers(message);
								}
							}
						}
					});
				}
			});
		} else {

			if (smartrobot.isConnectedToInternet()){
				remoteTimer=new Timer();
				remoteTimer.scheduleAtFixedRate(new OpenFireRemote(), 1,
						conn_period);
			}

		}

		smartrobot.notifyObservers("serviceConnected");
	}

    private void xmppConnexionClosed() {
		if(smartrobot.getConnectedToOpenFire().equals("true")) {
			smartrobot.setEditor(
					RemoteControlUtil.CONNECTED_TO_SERVER,
					"false");
			smartrobot.notifyObservers("serverlogout");
			smartrobot.setNumfailOpenFire(smartrobot.getNumfailOpenFire() + 1);

			if (smartrobot.isConnectedToInternet()
					&& flagClose == false) {
				isconnected1 = false;
				islogin = false;
				isFirstTry = false;
				remoteTimer = new Timer();
				remoteTimer.scheduleAtFixedRate(new OpenFireRemote(), 1,
						conn_period);
			}
			smartrobot.notifyObservers("stop");
		}
    }


    /**
	 * Envoyer un message au contrôle
	 *
	 * @fromId le pseudo open fire du contrôle
	 * @param message
	 *            : le message à envoyer
	 */
	public static void sendMessage(String fromId, String message) {

if(SmartRobotApplication.call.equals("linphone")) {
	if (fromId != null) {
		Chat chat = chatManager.createChat(fromId, null);
		GestionDesFichiers.log("INFO", "Message =" + message + " Sent",
				"Log_MessageXMPP");
		try {
			chat.sendMessage(message);
		} catch (XMPPException e) {
			e.printStackTrace();
		} catch (java.lang.IllegalStateException e) {
			e.printStackTrace();
		}
	}
}
	}



	@Override
	public void onDestroy() {
		super.onDestroy();
		if(SmartRobotApplication.call.equals("linphone")) {

			smartrobot.notifyObservers("serverlogout");
			flagClose = true;
			if (remoteTimer != null) {
				remoteTimer.cancel();
				remoteTimer = null;
			}
			if (connection != null) {
				controler = null;
				connection.disconnect();
				connection = null;
			}
			smartrobot.removeObserver(this);
		}
	}

	@Override
	public void update(String message) throws IOException {
		// TODO Auto-generated method stub
		if (message.equals("cancelAutoRegistration")
				&& !smartrobot.isOpenFireConnected()) {
			remoteTimer=new Timer();
			remoteTimer.scheduleAtFixedRate(new OpenFireRemote(), 1,
					conn_period);
		}
	}

}
