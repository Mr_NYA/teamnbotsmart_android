package com.orrobotics.orsmartrobot.service;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.util.GestionDesFichiers;
import com.orrobotics.orsmartrobot.util.GetSpeedTestHostsHandler;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import fr.bmartel.speedtest.SpeedTestReport;
import fr.bmartel.speedtest.SpeedTestSocket;
import fr.bmartel.speedtest.inter.ISpeedTestListener;
import fr.bmartel.speedtest.model.SpeedTestError;

/**
 *
 * @author Manar BOUKHARI
 *
 *
 *
 */

public class TestDebitService extends Service  {

    static SmartRobotApplication smartrobot;
    static Context context;
    static String serverTestUp="";
    static String serverTestDown="";
    public static double valueUp =0;
    public static double valueDown =0;
    int speedMinUp=500;
    int speedMinDown=500;

    public static double valueUpComplet =0;
    public static double valueDownComplet =0;
    Handler hand = new Handler();
    int endCall_timeout = 5;
    int testDebitDuration=5000;
    boolean stopThread = false;
    FindClosetServerTask task;
    GetSpeedTestHostsHandler getSpeedTestHostsHandler = null;
    HashSet<String> tempBlackList;
    SpeedTestTask taskDown;
    SpeedTestTaskUP taskUp;

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    @SuppressLint("NewApi")
    @Override
    public void onCreate() {
        super.onCreate();
        GestionDesFichiers
                .log("INFO",
                        "Le service TestDebit est démarré",
                        "Log_SmartRobot");
        context = this.getApplicationContext();
        smartrobot = (SmartRobotApplication) context;

        speedMinUp = Integer.parseInt(SmartRobotApplication.getParamFromFile("debit.upload.min", "Configuration.properties",null).split(" ")[0]);
        speedMinDown = Integer.parseInt(SmartRobotApplication.getParamFromFile("debit.download.min", "Configuration.properties",null).split(" ")[0]);


        String endCall = SmartRobotApplication.getParamFromFile("debit.download.min", "Configuration.properties",null);

        String duration = SmartRobotApplication.getParamFromFile("debit.test.duration", "Configuration.properties",null).split(" ")[0];
        if(duration!=null)
            testDebitDuration =Integer.parseInt(duration) *1000;

        if(endCall != null)
            endCall_timeout = Integer.parseInt(endCall.split(" ")[0]);



        task = new FindClosetServerTask();
        task.execute();

        taskDown= new SpeedTestTask();
        taskUp = new SpeedTestTaskUP();


    }


    public class FindClosetServerTask extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {
            findClosetServer();

            return null;
        }

        @Override
        protected void onPostExecute(String data) {
            try {

                final Thread thr = new Thread(new Runnable() {
                    public void run() {
                        taskDown.execute();
                    }
                });
                thr.start();
                final Thread thrUP = new Thread(new Runnable() {
                    public void run() {
                        taskUp.execute();
                    }
                });
                thrUP.start();
            } catch (Exception e) {

            }
        }
    }

    public class SpeedTestTask extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {
            if(!stopThread) {
                final SpeedTestSocket speedTestSocket = new SpeedTestSocket();

                // add a listener to wait for speedtest completion and progress
                speedTestSocket.addSpeedTestListener(new ISpeedTestListener() {

                    @Override
                    public void onCompletion(SpeedTestReport report) {
                        valueDownComplet = report.getTransferRateBit().doubleValue() * 0.0000009537;
                        Log.i("debit", "---onCompletionDown=" + report.getTransferRateBit().doubleValue() * 0.0000009537);
                        valueDown = report.getTransferRateBit().doubleValue() * 0.0009765625;
                        if (valueDown < speedMinDown) {
                            hand.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if (valueDown < speedMinDown) {
                                        smartrobot.setIsEnoughBandWith(false);
                                        smartrobot.setIsEnoughBandWithForXmpp(false);
                                        if (smartrobot.isTelepresence()) {
                                            ServiceRobot.sendMessage(
                                                    smartrobot.getPseudoOpenFireSender(),
                                                    "badBandWith"
                                            );
                                        }
                                    }
                                }
                            }, endCall_timeout * 1000);
                        } else {
                            smartrobot.setIsEnoughBandWith(true);
                            smartrobot.setIsEnoughBandWithForXmpp(true);
                        }
                        if (speedTestSocket != null && !stopThread)
                            speedTestSocket.startDownload(serverTestDown);


                    }

                    @Override
                    public void onError(SpeedTestError speedTestError, String errorMessage) {
                        valueDown = 0;
                        valueDownComplet = 0;
                        smartrobot.setIsEnoughBandWithForXmpp(false);

                        taskDown.cancel(true);
                        taskDown = null;
                        taskDown =new SpeedTestTask();
                        final Thread thr = new Thread(new Runnable() {
                            public void run() {
                                taskDown.execute();
                            }
                        });
                        thr.start();
                    }

                    @Override
                    public void onProgress(float percent, SpeedTestReport report) {
                        valueDown = report.getTransferRateBit().doubleValue() * 0.0009765625;

                        if (valueDown < speedMinDown) {
                            hand.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if (valueDown < speedMinDown) {
                                        smartrobot.setIsEnoughBandWith(false);
                                        if (smartrobot.isTelepresence()) {
                                            ServiceRobot.sendMessage(
                                                    smartrobot.getPseudoOpenFireSender(),
                                                    "badBandWith"
                                            );
                                        }
                                    }
                                }
                            }, endCall_timeout * 1000);

                        } else {
                            smartrobot.setIsEnoughBandWith(true);
                        }
                    }
                });
                if (speedTestSocket != null && !stopThread)
                    speedTestSocket.startDownload(serverTestDown);

            }
            return null;
        }
    }

    public class SpeedTestTaskUP extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {
            if(!stopThread) {
                final SpeedTestSocket speedTestSocket = new SpeedTestSocket();

                // add a listener to wait for speedtest completion and progress
                speedTestSocket.addSpeedTestListener(new ISpeedTestListener() {

                    @Override
                    public void onCompletion(SpeedTestReport report) {
                        valueUp = report.getTransferRateBit().doubleValue() * 0.0009765625;
                        valueUpComplet = report.getTransferRateBit().doubleValue() * 0.0000009537;
                        Log.i("debit", "---onCompletionUp=" + report.getTransferRateBit().doubleValue() * 0.0000009537);

                        if (valueUp < speedMinUp) {
                            hand.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if (valueUp < speedMinUp) {
                                        smartrobot.setIsEnoughBandWith(false);
                                        smartrobot.setIsEnoughBandWithForXmpp(false);
                                        if (smartrobot.isTelepresence()) {
                                            ServiceRobot.sendMessage(
                                                    smartrobot.getPseudoOpenFireSender(),
                                                    "badBandWith"
                                            );
                                        }
                                    }
                                }
                            }, endCall_timeout * 1000);
                        } else {
                            smartrobot.setIsEnoughBandWith(true);
                            smartrobot.setIsEnoughBandWithForXmpp(true);
                        }
                        if(speedTestSocket != null && !stopThread)
                        speedTestSocket.startUpload(serverTestUp, 1000000);


                    }

                    @Override
                    public void onError(SpeedTestError speedTestError, String errorMessage) {
                        valueUp = 0;
                        valueUpComplet = 0;
                        smartrobot.setIsEnoughBandWithForXmpp(false);
                        taskUp.cancel(true);
                        taskUp = null;
                        taskUp =new SpeedTestTaskUP();
                        final Thread thr = new Thread(new Runnable() {
                            public void run() {
                                taskUp.execute();
                            }
                        });
                        thr.start();


                    }

                    @Override
                    public void onProgress(float percent, SpeedTestReport report) {

                        valueUp = report.getTransferRateBit().doubleValue() * 0.0009765625;

                        if (valueUp < speedMinUp) {
                            hand.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if (valueUp < speedMinUp) {
                                        smartrobot.setIsEnoughBandWith(false);
                                        if (smartrobot.isTelepresence()) {
                                            ServiceRobot.sendMessage(
                                                    smartrobot.getPseudoOpenFireSender(),
                                                    "badBandWith"
                                            );
                                        }
                                    }
                                }
                            }, endCall_timeout * 1000);

                        } else {
                            smartrobot.setIsEnoughBandWith(true);
                        }

                    }
                });
                if (speedTestSocket != null && !stopThread)
                    speedTestSocket.startUpload(serverTestUp, 1000000);

            }
            return null;
        }
    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        stopThread = true;
        task.cancel(true);
        taskUp.cancel(true);
        taskDown.cancel(true);
        GestionDesFichiers
                .log("INFO",
                        "Le service TestDebit est arrêté",
                        "Log_SmartRobot");

    }

    private void findClosetServer() {
        //Find closest server

        tempBlackList = new HashSet<>();

        getSpeedTestHostsHandler = new GetSpeedTestHostsHandler();
        getSpeedTestHostsHandler.start();
        //Get egcodes.speedtest hosts
        int timeCount = 600; //1min
        while (!getSpeedTestHostsHandler.isFinished()) {
            timeCount--;
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
            }
            if (timeCount <= 0) {

                getSpeedTestHostsHandler = null;
                return;
            }
        }
        HashMap<Integer, String> mapKey = getSpeedTestHostsHandler.getMapKey();
        HashMap<Integer, List<String>> mapValue = getSpeedTestHostsHandler.getMapValue();
        double selfLat = getSpeedTestHostsHandler.getSelfLat();
        double selfLon = getSpeedTestHostsHandler.getSelfLon();
        double tmp = 19349458;
        double dist = 0.0;
        int findServerIndex = 0;

        for (int index : mapKey.keySet()) {
            if (tempBlackList.contains(mapValue.get(index).get(5))) {
                continue;
            }

            Location source = new Location("Source");
            source.setLatitude(selfLat);
            source.setLongitude(selfLon);

            List<String> ls = mapValue.get(index);
            Location dest = new Location("Dest");
            dest.setLatitude(Double.parseDouble(ls.get(0)));
            dest.setLongitude(Double.parseDouble(ls.get(1)));

            double distance = source.distanceTo(dest);
            if (tmp > distance) {
                tmp = distance;
                dist = distance;
                findServerIndex = index;
            }
        }
        String uploadAddr = mapKey.get(findServerIndex);
        final List<String> info = mapValue.get(findServerIndex);
        final double distance = dist;


        if (uploadAddr == null) {
            serverTestUp = SmartRobotApplication.getParamFromFile("debit.upload.server", "Configuration.properties",
                    null);
            serverTestDown = SmartRobotApplication.getParamFromFile("debit.download.server", "Configuration.properties",
                    null);
        }
        else{
            serverTestUp = uploadAddr;
            serverTestDown = uploadAddr.replace(uploadAddr.split("/")[uploadAddr.split("/").length - 1], "")+"random4000x4000.jpg";

        }
        GestionDesFichiers.log(
                "débit", "--serverTestUp="+serverTestUp+"---serverTestDown"+serverTestDown,
                "Log_SmartRobot");

    }




}