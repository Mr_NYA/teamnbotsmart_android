package com.orrobotics.orsmartrobot.service;

import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.voicerecognition.SpeechRecognition;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

public class SpeechRecognitionService extends Service {
	private static final String TAG = SpeechRecognitionService.class
			.getSimpleName();

	private SpeechRecognition mSpeechRecognition;
	private final Handler mHandler = new Handler();
	SmartRobotApplication model;
	int i=0;
	Boolean flagSkype = false;

	public class LocalBinder extends Binder {
		public SpeechRecognitionService getService() {
			return SpeechRecognitionService.this;
		}
	}

	private final IBinder mBinder = new LocalBinder();

	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		Log.d(TAG, "onCreate");
		mSpeechRecognition = new SpeechRecognition(this);
		model = (SmartRobotApplication) this.getApplicationContext();

	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d(TAG, "onStartCommand");
		return START_STICKY;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d(TAG, "onDestroy");
		stopSelf();
		mSpeechRecognition.destroy();

	}

	public void startRecognition(SpeechRecognition.ResultCallback callback, Boolean isEnabled) {

		mSpeechRecognition.destroy();
		mSpeechRecognition = new SpeechRecognition(this);
		mSpeechRecognition.start(callback, isEnabled);

	}

	public void destroyVoiceRecognition() {
		mSpeechRecognition.destroy();
	}

	public void startHandler() {
		i=0;
		model.setTelepresence(true);
		model.notifyObservers("istelepresence");
		mHandler.postDelayed(getRunningService, 5000);
	}
	public void stopHandler() {
		i=0;
		model.setTelepresence(false);
		model.notifyObservers("endtelepresence");
		mHandler.removeCallbacks(getRunningService);
	}

	Runnable getRunningService = new Runnable() {
		@Override
		public void run() {
			ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
			for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
				String serv = service.service.getClassName();
				if(serv.equals("com.skype.android.app.calling.CallForegroundService")){
					i=0;
					flagSkype=true;
					break;
				}else{
					flagSkype=false;
				}
				if(serv.equals("com.skype.android.app.wear.CommandHandlerService")){

					flagSkype=false;


				}else
					mHandler.postDelayed(this, 5000);

			}

			if(flagSkype==false)
				i=i+1;
			if(i==100 && flagSkype==false) {
				model.notifyObservers("launchControl");
				stopHandler();
			}else
				mHandler.postDelayed(this, 5000);

		}
	};




}
