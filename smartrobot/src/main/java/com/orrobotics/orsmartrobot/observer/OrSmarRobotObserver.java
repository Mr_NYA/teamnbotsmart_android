package com.orrobotics.orsmartrobot.observer;

import java.io.IOException;

public interface OrSmarRobotObserver {

	public void update(String message) throws IOException;

}
