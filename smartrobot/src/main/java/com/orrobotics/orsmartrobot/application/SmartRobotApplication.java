package com.orrobotics.orsmartrobot.application;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.linphone.core.LinphoneAddress;
import org.linphone.core.LinphoneCall;
import org.linphone.core.LinphoneCoreException;
import org.linphone.core.LinphoneCoreFactory;

import com.orrobotics.linphone.LinphoneRegistration;
import com.orrobotics.orsmartrobot.R;
import com.orrobotics.orsmartrobot.controller.BleScanController;
import com.orrobotics.orsmartrobot.controllerImpl.BleScanControllerImpl;
import com.orrobotics.orsmartrobot.modelImpl.BleScanModelImpl;
import com.orrobotics.orsmartrobot.observer.OrSmarRobotObserver;
import com.orrobotics.orsmartrobot.service.ServiceRobot;
import com.orrobotics.orsmartrobot.service.TestDebitService;
import com.orrobotics.orsmartrobot.util.ChatbotConnector;
import com.orrobotics.orsmartrobot.util.GNewsObject;
import com.orrobotics.orsmartrobot.util.GestionDesFichiers;
import com.orrobotics.orsmartrobot.util.GoogleNewsConnector;
import com.orrobotics.orsmartrobot.util.PandorabotConnector;
import com.orrobotics.orsmartrobot.util.RemoteControlUtil;
import com.orrobotics.orsmartrobot.util.SkypeUtilities;
import com.orrobotics.orsmartrobot.util.WikipediaConnector;
import com.orrobotics.orsmartrobot.view.ControlRobot;
import com.orrobotics.webservice.util.ConfigurationFile;
import com.orrobotics.webservice.util.CustomProperties;

import android.accounts.Account;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.hardware.Camera;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.PowerManager;
import android.os.StatFs;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.botlibre.sdk.BOTlibreCredentials;
import org.botlibre.sdk.SDKConnection;
import org.botlibre.sdk.SDKException;
import org.botlibre.sdk.config.BrowseConfig;
import org.botlibre.sdk.config.ChatConfig;
import org.botlibre.sdk.config.ChatResponse;
import org.botlibre.sdk.config.UserConfig;
import org.botlibre.sdk.config.WebMediumConfig;

import static com.orrobotics.orsmartrobot.util.GestionDesFichiers.simpleFormat;
import static java.lang.Math.round;
import android.net.NetworkInfo;

/**
 * @author Hajiba IFRAH Cette classe sert à stocker les données de l'application
 *         qu'on est besoin le long de l'exécution de l'application, cette
 *         classe permet aussi de définir Les méthodes qu'on utilise dans plus
 *         d'une seul classe
 * @version 2.1.0
 * @date 18/05/2015
 */

@SuppressLint({ "NewApi" })
public class SmartRobotApplication extends Application {
	SharedPreferences sharedPref;
	SharedPreferences.Editor editor;
	SharedPreferences sharedPrefRobot;
	SharedPreferences.Editor editorRobot;
	Boolean startBle = false;
	Surface surface;
	String deviceMac = "";
	String read_data = "";
	int type = 3;
	public static String call;
	ChatbotConnector chatbotConnector;

	boolean flagLock = false;
	boolean enable = false;
	boolean isControl = false;
	boolean orientation = false;
	boolean setting = false;
	boolean settingControl = false;
	boolean stop = false;
	boolean flagTest = false;
	Boolean flagServer = false;
	Boolean flagCloseControl = false;
	Boolean end = false;
	private static boolean controlVisible;
	Boolean flagStop = true;
	boolean isRegitred = false;
	boolean isOpenFireConncted = false;
	boolean istelepresence = false;
	boolean networkUP = true;
	boolean autoRegistrationOn = false;
	BleScanController bleScanController;
	boolean istelepresenceControl = false;
	Handler mHandler = new Handler();
	String pseudoLinphone;
	LaunchingThread launchthread;
	SurfaceHolder holder;
	String version = null;
	//int volumeMax = 100;
	Account ppseudoSkype[];
	Activity activity = null;
	String adresseOpenfire;
	private boolean flagisEnoughBandWithForXmpp=true;

	byte[] bmp = null;
	Intent service;
	Intent serviceDebit;
	float floatValue;
	// paramètres gyroscope
	private float[] accel = new float[3];
    String b = "" ;
	public static final float EPSILON = 0.000000001f;
	private static final float NS2S = 1.0f / 1000000000.0f;
	int TIME_CONSTANT = 30;
	private long timestamp;
	private float[] gyroMatrix = new float[9];
	/**
	 * orientation des angles à partir de la matrics gyro
	 */
	private float[] gyroOrientation = new float[3];
	/**
	 * la vitesse angulaire à  partir de la gyro
	 */
	private float[] gyro = new float[3];
	/**
	 * matrice de la rotation d'accélèrométre
	 */
	private float[] rotationMatrix = new float[9];
	private float[] accMagOrientation = new float[3];
	/**
	 * vecteur d'orientation
	 */
	private float[] magnet = new float[3];
	private boolean initState = true;
	/**
	 * orientation finale
	 */
	private float[] fusedOrientation = new float[3];

	public static final float FILTER_COEFFICIENT = 0.98f;
	Timer fuseTimer = new Timer();
	/**
	 * Le chemin du dossier SmartRobot dans sdcard
	 *
	 */

	static File pathOfMyFile = Environment
			.getExternalStoragePublicDirectory("SmartRobot");
	static File path1 = Environment.getExternalStorageDirectory();
	/**
	 * La liste des classes observateurs
	 *
	 */
	private List<OrSmarRobotObserver> _observers = new ArrayList<OrSmarRobotObserver>();

	private LinphoneRegistration LinRegistration;
	RegistrationThread thread;

	public final static int THEME_DEFAULT = 0;
	public final static int THEME_GREEN = 1;
	public final static int THEME_BLACK = 2;
	static SDKConnection connection;

	PandorabotConnector connector;
	GoogleNewsConnector newsConnector;
	WikipediaConnector wikiConnector;
	MediaPlayer mpMusic;
	MediaPlayer mpRadio;
	int nbrMorceau;
	ArrayList<String> filePath;
	int currentSongIndex = 0;
	String email;
	String password;
	String nom;
	String prenom;
	String pseudoOpenFire;
	String passwordOpenFire;
	String pseudoSIP;
	String passwordSIP;
	String macAdresse;
	String numeroSerie;
	String pseudoSkype = "";
	String robotPseudoSkype = "";
	String pseudoOpenFireSender = "";
	String langTTS = "";
	String speedTTS = "";
	String langApp = "fr";
	String callType = "";
	String connectedToRobot = "";
	String camState = "";
	String camId = "";
	String micState = "";
	String speakerState = "";
	String echoState = "";
	String videoLengh = "";
	String videoQuality = "";
	String speedLinear = "";
	String speedLateral = "";
	String vocalFrqDeclench = "";
	String isPermanant = "";
	String volumeMedia = "";
	String modeControl = "";
	String optCmdVocal = "";
	String optFaceToFace = "";
	String connectedToOpenFire = "";
	int NumfailConnection = 0;
	int NumfailSIP = 0;
	int NumfailOpenFire = 0;
	String connectedToSip = "";
	String date = "";
	String deviceAddress = "";
	String deviceName = "";
	String deviceSerie = "";
	String pseudoOpenFireControl = "";
	String robotName = "";
	String extraRobotName = "";
	String robotType = "";
	String robotId = "";
	String robotIcone = "";
	String jsonList = "";
	String macHexBound = "";
	String macConnectedDevice = "";
	String remoteControl = "";
	String autoStart = "";
	String email1 = "";
	int px;
	private boolean enableSuivi=false;
	private boolean startSuivi =false;
	Boolean isBatteryRequest = false;
	int idSuivi =-1;
	float height = 0.0f;
	float left = 0.0f;
	float boutom = 0.0f;
	float right = 0.0f;
	float top = 0.0f;
	float centerX = 0.0f;
	float centerY = 0.0f;

	float silo_centerX;
	float silo_centerY;

	TextToSpeech myTTS;
	HashMap<String, String> ttsList = new HashMap<>();
	String batterieDelay="";
	String showBatterie="";
	String showRobotApp="";
	String editTtsLang="";
	String editTtsSpeed="";
	String ttsLangSelection="";
	KeyguardManager keyguardManager ;
	KeyguardManager.KeyguardLock keyguardLock ;
	Boolean isEnoughBand=true;
	String isCloudSpeech = "";
	String wsUrl;
	int debit_down_max;
	int debit_upload_max;

	Camera mcamera;
	int hscreen;
	boolean test_zoom;
	boolean flagSuivi;
	int incZoom = 0;
	String mfaces = null;

	float silo_top;
	float silo_left;

	public String getIsCloudSpeech(){
		return isCloudSpeech;
	}

	public void setIsCloudSpeech(){ isCloudSpeech = getRobotParam(RemoteControlUtil.ISCLOUDSPEECH);}

	public void setBatteryDelay() {
		batterieDelay=getRobotParam(RemoteControlUtil.BATTERIE_DELAY);
	}
	public String getBatteryDelay() {
		return batterieDelay;
	}

	public void setShowRobotApp() {
		showRobotApp=getRobotParam(RemoteControlUtil.SHOW_ROBOT_APP);
	}
	public String getShowRobotApp() {
		return showRobotApp;
	}
	public void setShowBatterie() {
		showBatterie=getRobotParam(RemoteControlUtil.SHOW_BATTERIE);
	}
	public String getShowBatterie() {
		return showBatterie;
	}
	public void setEditTtsLang() {
		editTtsLang=getRobotParam(RemoteControlUtil.EDIT_TTS_LANGUE);
	}
	public String getEditTtsLang() {
		return editTtsLang;
	}
	public void setEditTtsSpeed() {
		editTtsSpeed=getRobotParam(RemoteControlUtil.EDIT_TTS_SPEED);
	}
	public String getEditTtsSpeed() {
		return editTtsSpeed;
	}
	public Boolean getIsBatteryRequest() {
		return isBatteryRequest;
	}
	public void setTtsLangSelection() {
		ttsLangSelection=getRobotParam(RemoteControlUtil.TTS_LANG_SELECTION);
	}
	public void setIsBatteryRequest(Boolean isBatteryRequest) {
		this.isBatteryRequest = isBatteryRequest;
	}



	@Override
	public void onCreate() {
		super.onCreate();

//		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
//				.detectDiskReads()
//				.detectDiskWrites()
//				.detectNetwork()   // or .detectAll() for all detectable problems
//				.penaltyLog()
//				.build());
//		StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
//				.detectLeakedSqlLiteObjects()
//				.detectLeakedClosableObjects()
//				.penaltyLog()
//				.penaltyDeath()
//				.build());


		PackageManager manager = getPackageManager();
		PackageInfo info = null;
		try {
			info = manager.getPackageInfo(getPackageName(), 0);
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		version = info.versionName;


		/**
		 * Instanciation de l'application SmartRobot
		 */

		service = new Intent(this, ServiceRobot.class);
		serviceDebit = new Intent(this, TestDebitService.class);
		registerReceiver(mControllerReceiver, makeControllerIntentFilter());

		keyguardManager = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);
		keyguardLock = keyguardManager.newKeyguardLock("keyguard");

		call = getParamFromFile("call", "Configuration.properties",null);

		if(call==null){
			call ="webrtc";

		}
		wsUrl = getParamFromFile("param", "Configuration.properties",null);
		if(wsUrl== null)
			wsUrl = "http://35.197.203.96:8081/plugin-ws-1.2.4/";

		setMaxWifiDebit();
	}

	public String getWsUrl() {
		return wsUrl;
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		setLocal();
	}

	/**
	 * Getter et les setter
	 *
	 */

	public String getEmail1() {
		return email1;
	}

	public void setEmail1() {
		email1 = getParam(RemoteControlUtil.EMAIL1);
	}

	public void setEmail(String email) {
		this.email = email;

	}

	public String getEmail() {
		if (email == null)
			email = getParam(RemoteControlUtil.EMAIL);
		return email;
	}

	public String getAdresseOpenFire() {
		return adresseOpenfire;

	}

	public void setAdresseOpenFire(String adresseOpenfire) {
		this.adresseOpenfire = adresseOpenfire;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswod() {
		if (password == null)
			password = getParam(RemoteControlUtil.PASSWORD);
		return password;
	}

	public void setPseudoOpenFire(String pseudoOpenFire) {
		this.pseudoOpenFire = pseudoOpenFire;
	}

	public String getPseudoOpenFire() {
		if (pseudoOpenFire == null)
			pseudoOpenFire = getParam(RemoteControlUtil.PSEUDO_OPEN_FIRE);
		return pseudoOpenFire;
	}

	public void setPasswordOpenFire(String passwordOpenFire) {
		this.passwordOpenFire = passwordOpenFire;
	}

	public String getPasswordOpenFire() {
		if (passwordOpenFire == null)
			passwordOpenFire = getParam(RemoteControlUtil.PASSWORD_OPEN_FIRE);
		return passwordOpenFire;
	}

	public void setPseudoSIP(String pseudoSIP) {
		this.pseudoSIP = pseudoSIP;
	}

	public String getPseudoSIP() {
		if (pseudoSIP == null)
			pseudoSIP = getParam(RemoteControlUtil.PSEUDO_SIP);
		return pseudoSIP;
	}

	public String getPasswordSIP() {
		if (passwordSIP == null)
			passwordSIP = getParam(RemoteControlUtil.PASSWORD_SIP);
		return passwordSIP;
	}

	public void setPasswordSIP(String passwordSIP) {
		this.passwordSIP = passwordSIP;
	}

	public boolean isSetting() {
		return setting;
	}

	public void setSettingControl(boolean settingControl) {
		// TODO Auto-generated method stub
		this.settingControl = settingControl;
	}

	public boolean isSettingControl() {
		return settingControl;
	}

	public void setSetting(boolean setting) {
		// TODO Auto-generated method stub
		this.setting = setting;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getNom() {
		if (nom == null)
			nom = getParam(RemoteControlUtil.NOM);
		return nom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getPrenom() {
		if (prenom == null)
			prenom = getParam(RemoteControlUtil.PRENOM);
		return prenom;
	}

	public void setAdresseMAC(String macAdresse) {
		this.macAdresse = macAdresse;
	}

	public String getAdresseMAC() {
		return macAdresse;
	}

	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	public String getNumeroSerie() {
		return numeroSerie;
	}

	public void setPseudoSkype() {
		pseudoSkype = getParam(RemoteControlUtil.PSEUDOSKYPE);
	}

	public String getPseudoSkype() {

		return pseudoSkype;
	}

	public void setRobotPseudoSkype() {
		// TODO Auto-generated method stub
		robotPseudoSkype = getParam(RemoteControlUtil.ROBOT_PSEUDO_SKYPE);
	}

	public String getRobotPseudoSkype() {

		return robotPseudoSkype;

	}

	public void setFlagLock(boolean flagLock) {
		// TODO Auto-generated method stub
		this.flagLock = flagLock;
	}

	public boolean getFlagLock() {
		// TODO Auto-generated method stub
		return flagLock;
	}

	public void setPseudoOpenFireSender(String pseudoOpenFireSender) {

		this.pseudoOpenFireSender = pseudoOpenFireSender;
	}

	public String getPseudoOpenFireSender() {
		// TODO Auto-generated method stub
		if (pseudoOpenFireSender == null)
			pseudoOpenFireSender = getParam(RemoteControlUtil.EXTRAS_PSEUDO_OPENFIRE_CONTROL);
		return pseudoOpenFireSender;
	}

	public void setChatEnabled(boolean enable) {
		// TODO Auto-generated method stub
		this.enable = enable;
	}

	public Boolean getChatEnabled() {
		// TODO Auto-generated method stub
		return enable;
	}

	public boolean isOrientation() {
		return orientation;
	}

	public void setOrientation(boolean orientation) {
		this.orientation = orientation;
	}

	public void setflagStop(boolean flag) {
		// TODO Auto-generated method stub
		this.flagStop = flag;
	}

	public Boolean getFlagStop() {
		return flagStop;
	}

	public boolean isStop() {
		return stop;
	}

	public void setStop(boolean stop) {
		this.stop = stop;
	}

	public String getDeviceAddress() {
		return deviceAddress;
	}

	public void setDeviceAddress() {
		deviceAddress = getParam(RemoteControlUtil.EXTRAS_DEVICE_ADDRESS);
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName() {
		this.deviceName = getParam(RemoteControlUtil.EXTRAS_DEVICE_NAME);
	}

	public String getDeviceSerie() {
		return deviceSerie;
	}

	public void setDeviceSerie() {
		deviceSerie = getParam(RemoteControlUtil.EXTRAS_DEVICE_SERIE);
	}

	public String getPseudoOpenFireControl() {
		return pseudoOpenFireControl;
	}

	public void setPseudoOpenFireControl() {
		pseudoOpenFireControl = getParam(RemoteControlUtil.EXTRAS_PSEUDO_OPENFIRE_CONTROL);
	}

	public String getRobotId() {
		return robotId;
	}

	public void setRobotId() {
		robotId = getParam(RemoteControlUtil.ID);
	}

	public String getRobotIcone() {
		return robotIcone;
	}

	public void setRobotIcone() {
		robotIcone = getParam(RemoteControlUtil.ICON);
	}

	public String getJsonList() {
		return jsonList;
	}

	public void setJsonList() {
		jsonList = getParam(RemoteControlUtil.JSON_LIST);
	}

	public String getMacHexBound() {
		return macHexBound;
	}

	public void setMacHexBound() {
		macHexBound = getParam(RemoteControlUtil.MAC_ADDRESS_HEX_BOUND);
	}

	public String getMacConnectedDevice() {
		return macConnectedDevice;
	}

	public void setMacConnectedDevice() {
		macConnectedDevice = getParam(RemoteControlUtil.MAC_CONNECTED_DEVICE);
	}

	public String getRemoteControl() {
		return remoteControl;
	}

	public void setRemoteControl() {
		remoteControl = getParam(RemoteControlUtil.REMOTE_CONTROL);
	}

	public String getDate() {
		return date;
	}

	public void setDate() {
		date = getParam(RemoteControlUtil.DATE);
	}

	public String getConnectedToOpenFire() {
		return connectedToOpenFire;
	}

	public void setConnectedToOpenFire() {
		connectedToOpenFire = getParam(RemoteControlUtil.CONNECTED_TO_SERVER);
	}
	public int getNumFailConnection() {
		return NumfailConnection;
	}

	public void setNumFailConnection(int num) {
		NumfailConnection = num;
	}

	public int getNumfailSIP() {
		return NumfailSIP;
	}

	public void setNumfailSIP(int numfailSIP) {
		NumfailSIP = numfailSIP;
	}

	public int getNumfailOpenFire() {
		return NumfailOpenFire;
	}

	public void setNumfailOpenFire(int numfailOpenFire) {
		NumfailOpenFire = numfailOpenFire;
	}

	public String getConnectedToSip() {
		return connectedToSip;
	}

	public void setConnectedToSip() {
		connectedToSip = getParam(RemoteControlUtil.CONNECTED_TO_SIP_SERVER);
	}

	public String getSpeedTTS() {
		return speedTTS;
	}

	public void setSpeedTTS() {
		speedTTS = getRobotParam(RemoteControlUtil.TTS_SPEED_POS);
	}

	public String getCamState() {
		return camState;
	}

	public void setCamState() {
		camState = getRobotParam(RemoteControlUtil.CAMERA);
	}

	public String getCamId() {
		return camId;
	}

	public void setCamId() {
		camId = getRobotParam(RemoteControlUtil.CAMERA_ID);
	}

	public String getMicState() {
		return micState;
	}

	public void setMicState() {
		micState = getRobotParam(RemoteControlUtil.MIC);
	}

	public String getSpeakerState() {
		return speakerState;
	}

	public void setSpeakerState() {
		speakerState = getRobotParam(RemoteControlUtil.SPEAKER);
	}

	public String getEchoConcellation() {
		return echoState;
	}

	public void setEchoConcellation() {
		echoState = getRobotParam(RemoteControlUtil.ECHO_CANCELLATION);
	}

	public void setVideoLengh() {
		videoLengh = getParam(RemoteControlUtil.VIDEO_LENGHT);
	}

	public String getVideoQuality() {
		return videoQuality;
	}

	public void setVideoQuality() {
		videoQuality = getRobotParam(RemoteControlUtil.VIDEO_QUALITY);
	}

	public String getSpeedLinear() {
		return speedLinear;
	}

	public void setSpeedLinear() {
		speedLinear = getRobotParam(RemoteControlUtil.SPEED_LINEAIRE);
	}

	public String getSpeedLateral() {
		return speedLateral;
	}

	public void setSpeedLateral() {
		speedLateral = getRobotParam(RemoteControlUtil.SPEED_LATERALE);
	}

	public String getVocalFrqDeclench() {
		return vocalFrqDeclench;
	}

	public void setVocalFrqDeclench() {
		vocalFrqDeclench = getRobotParam(RemoteControlUtil.VOCAL_FREQ_DECLENCH);
	}

	public String getIsPermanant() {
		return isPermanant;
	}

	public void setIsPermanant() {
		isPermanant = getRobotParam(RemoteControlUtil.ISPERMANCE);
	}

	public String getVolumeMedia() {
		return volumeMedia;
	}

	public void setVolumeMedia() {
		volumeMedia = getParam(RemoteControlUtil.VOLUME);
	}

	public String getModeControl() {
		return modeControl;
	}

	public void setModeControl() {
		modeControl = getRobotParam(RemoteControlUtil.MODE_CONTROLE);
	}

	public String getAutoStart() {
		return autoStart;
	}

	public void setAutoStart() {
		autoStart = getParam(RemoteControlUtil.AUTOSTART);
	}

	public String getOptCmdVocal() {
		return optCmdVocal;
	}

	public void setOptCmdVocal() {
		optCmdVocal = getRobotParam(RemoteControlUtil.OPT_CMD_VOCAL);
	}

	public String getOptFaceToFace() {
		return optFaceToFace;
	}

	public void setOptFaceToFace() {
		optFaceToFace = getRobotParam(RemoteControlUtil.OPT_FACE_TO_FACE);
	}

	public String getRobotConnexion() {
		return connectedToRobot;
	}

	public void setRobotConnexion() {

		connectedToRobot = getParam(RemoteControlUtil.CONNECTED_TO_APP);
	}

	public String getCallType() {
		return callType;
	}

	public void setCallType() {

		callType = getParam(RemoteControlUtil.CALL_TYPE);
	}

	public void setFilePath(ArrayList<String> filePath) {
		this.filePath = filePath;
	}

	public void setNbrMorceau(int nbrMorceau) {
		this.nbrMorceau = nbrMorceau;
	}

	public static boolean isControlVisible() {
		return controlVisible;
	}

	public static void ControlResumed() {
		controlVisible = true;
	}

	public static void ControlPaused() {
		controlVisible = false;
	}


	public void setSharedPrefRobot(SharedPreferences sharedPrefRobot) {
		this.sharedPrefRobot = sharedPrefRobot;
	}


	public void setSharedPref(SharedPreferences sharedPref) {
		this.sharedPref = sharedPref;
		// Renommer les fichier de configuration s'ils existe
		renameFiles();

		/**
		 * Création de dossier SmartRobot
		 */
		GestionDesFichiers.creerRobotDossier();

		/**
		 * Création de dossier AutoControl
		 */
		GestionDesFichiers.creerControlDossier();

		GestionDesFichiers.creerCommandVocalDossier();
		GestionDesFichiers.creerCommandRobotDossier();

		/**
		 * Création de fichier de log pour SmartRobot
		 */
		GestionDesFichiers.creerFichier("Log_SmartRobot");
		/**
		 * Création de fichier de Configuration "Configuration.properties"
		 */

		/**
		 * Création de dossier commandVocal
		 */
		GestionDesFichiers.creerCommandVocalDossier();

		createPropertiesFile();
		setEditor(RemoteControlUtil.APP_VERSION_NAME, version);
		adressServer = getParamFromFile("server", "Configuration.properties",
				null);
		setAdresseOpenFire(getParamFromFile("server",
				"Configuration.properties", null));
		connection = new SDKConnection(new BOTlibreCredentials(
				getParamFromFile("botLibre.appId", "Configuration.properties",
						null)));
	}

	public Boolean getFlagServer() {
		return flagServer;
	}

	public void setFlagServer(Boolean flagServer) {
		this.flagServer = flagServer;
	}

	public boolean isFlagTest() {
		return flagTest;
	}

	public void setFlagTest(boolean flagTest) {
		this.flagTest = flagTest;
	}

	@Override
	protected void attachBaseContext(Context base) {
		super.attachBaseContext(base);

		// The following line triggers the initialization of ACRA

	}

	/**
	 * Lire de contenu du paramètre param dans le fichier
	 * configuration.properties
	 *
	 */
	public static String getParamFromFile(String param, String fichier,
										  String newValue) {
		File file1 = new File(path1, "SmartRobot");
		String paramValue = null;
		CustomProperties props = new CustomProperties();
		CustomProperties newProps = new CustomProperties();

		props = ConfigurationFile.props;

		newProps = ConfigurationFile.loadproperties(file1, fichier, props);
		paramValue = newProps.getProperty(param);
		return paramValue;

	}

	/**
	 * Création du fichier de configuration
	 *
	 *
	 */
	public static void createPropertiesFile() {
		File file = new File(pathOfMyFile, "Configuration.properties");

		File file1 = new File(path1, "SmartRobot");
		if (!file.exists()) {

			ConfigurationFile.WritePropertiesFile(file1, file);

		}
	}

	/**
	 * Stockage des données dans un fichier interne
	 *
	 * @param param
	 *            : le nom avec lequel on paramètre la donnée
	 * @param value
	 *            : la valeur à  affecter à  ce paramètre
	 *
	 */

	public void setEditor(String param, String value) {
		// TODO Auto-generated method stub
		if(sharedPref != null) {
			editor = sharedPref.edit();
			editor.putString(param, value);
			editor.commit();
			setParam(param);
		}
	}

	private void setParam(String param) {
		// TODO Auto-generated method stub

		if (param.equals(RemoteControlUtil.ROBOT_NAME)) {
			setRobotName();
		}
		if (param.equals(RemoteControlUtil.CONNECTED_TO_APP)) {
			setRobotConnexion();
		}
		if (param.equals(RemoteControlUtil.CALL_TYPE)) {
			setCallType();
		}

		if (param.equals(RemoteControlUtil.VIDEO_LENGHT)) {
			setVideoLengh();
		}
		if (param.equals(RemoteControlUtil.AUTOSTART)) {
			setAutoStart();
		}
		if (param.equals(RemoteControlUtil.EMAIL1)) {
			setEmail1();
		}
		if (param.equals(RemoteControlUtil.CONNECTED_TO_SERVER)) {
			setConnectedToOpenFire();
		}
		if (param.equals(RemoteControlUtil.CONNECTED_TO_SIP_SERVER)) {
			setConnectedToSip();
		}
		if (param.equals(RemoteControlUtil.DATE)) {
			setDate();
		}
		if (param.equals(RemoteControlUtil.VOLUME)) {
			setVolumeMedia();
		}
		if (param.equals(RemoteControlUtil.EXTRAS_DEVICE_ADDRESS)) {
			setDeviceAddress();
		}
		if (param.equals(RemoteControlUtil.EXTRAS_DEVICE_NAME)) {
			setDeviceName();
		}
		if (param.equals(RemoteControlUtil.EXTRAS_DEVICE_SERIE)) {
			setDeviceSerie();
		}
		if (param.equals(RemoteControlUtil.EXTRAS_PSEUDO_OPENFIRE_CONTROL)) {
			setPseudoOpenFireControl();
		}
		if (param.equals(RemoteControlUtil.EXTRAS_ROBOT_NAME)) {
			setExtraRobotName();
		}
		if (param.equals(RemoteControlUtil.EXTRAS_ROBOT_TYPE)) {
			setRobotType();
		}
		if (param.equals(RemoteControlUtil.ICON)) {
			setRobotIcone();
		}
		if (param.equals(RemoteControlUtil.ID)) {
			setRobotId();
		}
		if (param.equals(RemoteControlUtil.JSON_LIST)) {
			setJsonList();
		}
		if (param.equals(RemoteControlUtil.MAC_ADDRESS_HEX_BOUND)) {
			setMacHexBound();
		}
		if (param.equals(RemoteControlUtil.MAC_CONNECTED_DEVICE)) {
			setMacConnectedDevice();
		}
		if (param.equals(RemoteControlUtil.PSEUDOSKYPE)) {
			setPseudoSkype();
		}
		if (param.equals(RemoteControlUtil.REMOTE_CONTROL)) {
			setRemoteControl();
		}

		if (param.equals(RemoteControlUtil.ROBOT_PSEUDO_SKYPE)) {
			setRobotPseudoSkype();
		}


	}

	private void setParams() {
		// TODO Auto-generated method stub
		setRobotConnexion();
		setCallType();
		setVideoLengh();
		setAutoStart();
		setEmail1();
		setConnectedToOpenFire();
		setConnectedToSip();
		setDate();
		setVolumeMedia();
		setDeviceAddress();
		setDeviceName();
		setDeviceSerie();
		setPseudoOpenFireControl();
		setExtraRobotName();
		setRobotType();
		setRobotIcone();
		setRobotId();
		setJsonList();
		setMacHexBound();
		setMacConnectedDevice();
		setPseudoSkype();
		setRemoteControl();
		setRobotName();
		setRobotPseudoSkype();

	}

	private void setRobotParam(String param) {
		// TODO Auto-generated method stub
		if (param.equals(RemoteControlUtil.CAMERA))
			setCamState();
		if (param.equals(RemoteControlUtil.CAMERA_ID))
			setCamId();
		if (param.equals(RemoteControlUtil.SPEAKER))
			setSpeakerState();
		if (param.equals(RemoteControlUtil.MIC))
			setMicState();
		if (param.equals(RemoteControlUtil.VIDEO_QUALITY))
			setVideoQuality();
		if (param.equals(RemoteControlUtil.ECHO_CANCELLATION))
			setEchoConcellation();
		if (param.equals(RemoteControlUtil.SPEED_LATERALE))
			setSpeedLateral();
		if (param.equals(RemoteControlUtil.SPEED_LINEAIRE))
			setSpeedLinear();
		if (param.equals(RemoteControlUtil.VOCAL_FREQ_DECLENCH))
			setVocalFrqDeclench();
		if (param.equals(RemoteControlUtil.MODE_CONTROLE))
			setModeControl();
		if (param.equals(RemoteControlUtil.OPT_CMD_VOCAL))
			setOptCmdVocal();
		if (param.equals(RemoteControlUtil.OPT_FACE_TO_FACE))
			setOptFaceToFace();
		if (param.equals(RemoteControlUtil.ISPERMANCE))
			setIsPermanant();
		if(param.equals(RemoteControlUtil.SHOW_ROBOT_APP))
			setShowRobotApp();
		if(param.equals(RemoteControlUtil.SHOW_BATTERIE))
			setShowBatterie();
		if(param.equals(RemoteControlUtil.EDIT_TTS_LANGUE))
			setEditTtsLang();
		if(param.equals(RemoteControlUtil.EDIT_TTS_SPEED))
			setEditTtsSpeed();
		if(param.equals(RemoteControlUtil.BATTERIE_DELAY))
			setBatteryDelay();

		if(param.equals(RemoteControlUtil.TTS_LANG_SELECTION))
			setTtsLangSelection();
		if (param.equals(RemoteControlUtil.APPLICATION_LANGAGE)) {
			setLangApp();
		}
		if (param.equals(RemoteControlUtil.TTS_LANGUAGE)) {
			setLangageTTS();
		}
		if (param.equals(RemoteControlUtil.TTS_SPEED_POS)) {
			setSpeedTTS();
		}
		if (param.equals(RemoteControlUtil.ISCLOUDSPEECH))
			setIsCloudSpeech();

	}

	private void setRobotParams() {
		// TODO Auto-generated method stub
		setCamState();
		setCamId();
		setSpeakerState();
		setMicState();
		setVideoQuality();
		setEchoConcellation();
		setSpeedLateral();
		setSpeedLinear();
		setVocalFrqDeclench();
		setModeControl();
		setOptCmdVocal();
		setOptFaceToFace();
		setIsPermanant();
		setShowBatterie();
		setShowRobotApp();
		setBatteryDelay();
		setEditTtsLang();
		setEditTtsSpeed();
		setTtsLangSelection();

		setLangApp();
		setLangageTTS();
		setSpeedTTS();
		setIsCloudSpeech();

	}

	/**
	 * Lire le contenu d'un paramètre stoché dans un fichier interne
	 *
	 * @param param
	 *            : Le paramètre à  lire
	 *
	 */
	public String getParam(String param) {
		// TODO Auto-generated method stub
		if (sharedPref != null)
			return sharedPref.getString(param, "");
		else
			return "";

	}

	public String getRobotParam(String param) {
		// TODO Auto-generated method stub
		if (sharedPrefRobot != null) {
			String data = sharedPrefRobot.getString(param, "");
			return data;
		} else

			return "";
	}

	public void setRobotEditor(String param, String value) {
		// TODO Auto-generated method stub
		editorRobot = sharedPrefRobot.edit();
		editorRobot.putString(param, value);
		editorRobot.commit();
		setRobotParam(param);
	}

	/**
	 * démarrer une autre activité
	 */

	public void StartAct(String param, String value, Class<?> classname,
						 Context context) {

		Intent intent = new Intent(getApplicationContext(), classname);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
				| Intent.FLAG_ACTIVITY_CLEAR_TOP);
		if (param != null && value != null)
			intent.putExtra(param, value);
		getApplicationContext().startActivity(intent);

	}

	/**
	 * Cette méthode permet de détecter les choses que le développeur faire par
	 * accident et les améne à  son attention afin qu'il peut les corriger.
	 *
	 */

	public void stricMode() {
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);
	}

	public static boolean isKindleFire() {
		return android.os.Build.MANUFACTURER.equals("Amazon")
				&& (android.os.Build.MODEL.equals("Kindle Fire") || android.os.Build.MODEL
				.startsWith("KF"));
	}

	/**
	 * Enregistrer un observateur
	 *
	 */
	public void registerObserver(OrSmarRobotObserver observer) {

		_observers.add(observer);
	}

	/**
	 * Supprimer un observateur
	 */

	public void removeObserver(OrSmarRobotObserver observer) {
		_observers.remove(observer);
	}

	public String getBackgroundType() {
		String param = getParamFromFile("control.fond",
				"Configuration.properties", null);
		String type = "";
		if (param.contains("gif"))
			type = "gif";
		else if (param.contains("png") || param.contains("jpg")
				|| param.contains("jpeg"))
			type = "img";
		else if (param.contains("mp4"))
			type = "video";
		else if (param.contains("camera"))
			type = "camera";
		return type;
	}

	public boolean fileExist(String fileName) {

		File file = new File(Environment.getExternalStorageDirectory()
				.getAbsolutePath() + "/SmartRobot/" + fileName);
		if (file.exists())
			return true;
		else
			return false;
	}

	/**
	 * Notifier les observateurs par une mise a jour
	 *
	 * @param message
	 *            : une caractéristique qui sert à  connaitre le traitement à
	 *            démarrer
	 */
	public void notifyObservers(String message) {

		for (int i = 0; i < _observers.size(); i++) {
			try {
				OrSmarRobotObserver ob = _observers.get(i);

				ob.update(message);

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * Stockage des données de l'application
	 */
	public void setData(String emailTxt, String passwordTxt, String nom,
						String prenom, String pseudoOpenFire, String passwordOpenFire,
						String pseudoSIP, String passwordSIP) {

		setEmail(emailTxt);
		setPassword(passwordTxt);
		setPseudoOpenFire(pseudoOpenFire);
		setPasswordOpenFire(passwordOpenFire);
		setPseudoSIP(pseudoSIP);
		setPasswordSIP(passwordSIP);
		setNom(nom);
		setPrenom(prenom);

	}

	/**
	 * Vérifier la valeur d'un retour du web service si il est null
	 *
	 * @param code
	 *            : la valeur récupérée via l'espace client
	 * @param progress
	 *            : sincrémente jusqu'à  200 puis ll'application arrète la
	 *            vérification
	 *
	 */
	public Boolean iSCodeAttribut(String code, int progress) {
		while (code == null) {

			try {
				Thread.sleep(200);
			} catch (InterruptedException ignore) {

			}
			progress += 10;

			if (progress == 200) {
				return false;
			}

		}
		return true;

	}

	/**
	 * Vérification si un élèment de type string existe dans une liste
	 *
	 * @param s
	 *            : le string qu'on vérifie son existance dans la liste
	 * @param myList
	 *            une liste
	 *
	 */
	public boolean checkList(String s, List<String> myList) {

		for (String str : myList) {

			if (str.equalsIgnoreCase(s))
				return true;
		}
		return false;
	}

	/**
	 * Vérifier si le service spécifié est démarré
	 *
	 * @param service
	 *            : le chemin du servie qu'on vérifie si'il est démarré
	 *
	 */
	public boolean isServiceRunning(String service) {
		ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);

		for (RunningServiceInfo serviceInfo : manager
				.getRunningServices(Integer.MAX_VALUE)) {
			if (serviceInfo.service.getClassName().equals(service)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Supprimer le données stocker dans l'application
	 *
	 */
	public void clear() {
		// TODO Auto-generated method stub

		editor = sharedPref.edit();
		editor.remove(RemoteControlUtil.EMAIL);
		editor.remove(RemoteControlUtil.PASSWORD);
		editor.remove(RemoteControlUtil.PASSWORD_OPEN_FIRE);
		editor.remove(RemoteControlUtil.PSEUDO_OPEN_FIRE);
		editor.remove(RemoteControlUtil.PASSWORD_SIP);
		editor.remove(RemoteControlUtil.PSEUDO_SIP);
		editor.remove(RemoteControlUtil.MAC_ADDRESS);
		editor.remove(RemoteControlUtil.EXTRAS_DEVICE_ADDRESS);
		editor.remove(RemoteControlUtil.EXTRAS_DEVICE_NAME);
		editor.remove(RemoteControlUtil.EXTRAS_PSEUDO_OPENFIRE_CONTROL);

		editor.commit();
	}



	/**
	 * Création d'une notification
	 *
	 */
	public void createNotification(String robotName, int icon,
								   Intent intentControle, Context context) {
		// TODO Auto-generated method stub

		String contentTitle = robotName + "Smart";
		intentControle.setAction(Intent.ACTION_MAIN);
		intentControle.addCategory(Intent.CATEGORY_LAUNCHER);
		PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
				intentControle, 0);

		NotificationCompat.Builder b = new NotificationCompat.Builder(context);
		b.setOngoing(true);
		b.setAutoCancel(false).setDefaults(Notification.DEFAULT_ALL)
				.setWhen(System.currentTimeMillis()).setSmallIcon(icon)
				.setTicker(robotName).setContentIntent(contentIntent)
				.setContentTitle(contentTitle);

		NotificationManager notificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.notify(1, b.build());

	}

	public void setActivity(Activity activity) {
		// TODO Auto-generated method stub
		this.activity = activity;
	}

	public Activity getActivity() {
		return activity;

	}

	/**
	 * Convertir un drawable vers un tableau de byte afin d'envoyer ce draweble
	 * entre les activities
	 */
	public byte[] parseDrawable(int drawable) {

		Bitmap bitmap = BitmapFactory.decodeResource(getResources(), drawable);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
		byte[] b = baos.toByteArray();
		return b;

	}

	public void setIsControl(boolean isControl) {
		// TODO Auto-generated method stub
		this.isControl = isControl;
	}

	public boolean getIsControl() {
		return isControl;
	}

	private IntentFilter makeControllerIntentFilter() {
		final IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
		return intentFilter;
	}

	/**
	 * Broadcast pour détecter la découpure de connexion internet
	 *
	 */
	private final BroadcastReceiver mControllerReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {

			final String action = intent.getAction();

			GestionDesFichiers.log("action ", action, "Log_SmartRobot");
			if (ConnectivityManager.CONNECTIVITY_ACTION.equals(action)) {

				notifyObservers("internetChanged");

				if (isConnectedToInternet() == true
						&& !getParam(RemoteControlUtil.NEW_MAC).equals("")) {
					/**
					 * Dans le cas de connexion est établit, metre à jours
					 * l'adresse mac du robot sur l'espace client
					 *
					 */
					if (getActivity() != null)

						new Task_updateMac()
								.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

				}else{
					NumfailConnection++;
				}

				if (isControl == true) {

					if (isConnectedToInternet() == true) {
						GestionDesFichiers
								.log("INFO ",
										"Etat de connexion Internet : connexion établie",
										"Log_SmartRobot");
						networkUP = true;

						/**
						 * Dans le cas de connexion est établit, démarrer le
						 * service
						 *
						 */
						mHandler.post(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub
								Toast.makeText(
										getApplicationContext(),
										getApplicationContext().getResources()
												.getString(
														R.string.connectionOk),
										Toast.LENGTH_SHORT).show();
							}
						});
						if(call.equals("linphone")) {


							if (isServiceRunning("com.orrobotics.orsmartrobot.service.ServiceRobot")) {
								stopService(service);
								startService(service);
							} else
								startService(service);
						}

						if (isServiceRunning("com.orrobotics.orsmartrobot.service.TestDebitService")) {
							stopService(serviceDebit);
							startService(serviceDebit);
						} else
							startService(serviceDebit);


					} else {
						GestionDesFichiers
								.log("INFO ",
										"Etat de connexion Internet : connexion perdue",
										"Log_SmartRobot");
						networkUP = false;
						if (isTelepresence() == true)
							notifyObservers("terminateCall");
						/**
						 * Dans le cas pas de connexion internet, arréter le
						 * service
						 *
						 */
						if(call.equals("linphone")) {

							if (isServiceRunning("com.orrobotics.orsmartrobot.service.ServiceRobot"))
								stopService(service);
						}
						if (isServiceRunning("com.orrobotics.orsmartrobot.service.TestDebitService"))
							stopService(serviceDebit);

						notifyObservers("stop");
						mHandler.post(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub
								Toast.makeText(
										getApplicationContext(),
										getApplicationContext().getResources()
												.getString(
														R.string.connectionNok),
										Toast.LENGTH_SHORT).show();

							}
						});
					}

				}

			}
		}
	};
	public String adressServer;

	/**
	 * Récupérer le pseudo skype installé dans l'appariel
	 *
	 */

	public String getInstalledPseudoSkype() {

		ppseudoSkype = SkypeUtilities.PseudoSkype(getApplicationContext());
		if (ppseudoSkype.length != 0)
			pseudoSkype = ppseudoSkype[0].name;
		return pseudoSkype;

	}

	public void initRegistration() {

		LinRegistration = new LinphoneRegistration(getApplicationContext());
	}

	public void registration() {
		// registration linphone
		thread = new RegistrationThread();
		thread.start();
	}

	public void setRobotId(Integer robotId) {
		this.robotId=Integer.toString(robotId);
	}

	public void setIdSuivi(int idSuivi) {
		this.idSuivi = idSuivi;
	}

	public int getId(){
		return idSuivi;
	}

	public void setTop(float top) {
		this.top = top;
	}

	public void setLeft(float left) {
		this.left = left;
	}

	public void settSiloTop(float silo_top) {
		this.silo_top = silo_top;
	}

	public void setSiloLeft(float  silo_left) {
		this.silo_left = silo_left;
	}


	public float getSiloTop() {
		return this.silo_top ;
	}

	public float getSiloLeft() {
		return this.silo_left;
	}
	public void setBoutom(float boutom) {
		this.boutom = boutom;
	}

	public void setRight(float right) {
		this.right = right;
	}


	public void setHeight(float height) {
		this.height = height;
	}

	public void setCenterX(float centerX) {
		this.centerX = centerX;
	}

	public void setCenterY(float centerY) {
		this.centerY = centerY;
	}

	public void setCenterXsilo(float silo_centerX){this.silo_centerX = silo_centerX;}

	public void setCenterYsilo(float silo_centerY){this.silo_centerY = silo_centerY;}

	public float getCenterXsilo(){return this.silo_centerX;}

	public void setFaceSuivi(){
		b= getRobotParam(RemoteControlUtil.ICON_SUIVI);
	}
	public String getFaceSuivi(){return this.b;}
	public float getCenterYsilo(){return this.silo_centerY;}
	public float getHeight() {
		return height;
	}

	public float getLeft() {
		return left;
	}

	public float getTop() {
		return top;
	}

	public float getBoutom() {
		return boutom;
	}

	public float getRight() {
		return right;
	}


	public void setIncrementZoom(int currentZoomLevel){this.incZoom = currentZoomLevel;}

	public int  getIncrementZoom(){ return this.incZoom;}


	public void setCameraObject(Camera mCamera){
		this.mcamera = mCamera;
	}

	public Camera getcameraobject(){
		return  mcamera;
	}

	public void setHeightScreen(int height){
		this.hscreen = height;
	}

	public int getHeightScreen(){
		return this.hscreen;
	}
	public void set_myfaces(String s ){
		this.mfaces = s;

	}

	public String get_myfaces(){
		return  this.mfaces;

	}
	public  Boolean getZommTest(){
		return this.test_zoom;

	}
	public  void setZommTest(boolean test_zoom){
		this.test_zoom = test_zoom;

	}
	public void setFlagSuivi(boolean flag){
		this.flagSuivi = flag;
	}
	public boolean getFlagSuivi(){
		return  this.flagSuivi;
	}

	public float getCenterX() {
		return centerX;
	}

	public float getCenterY() {
		return centerY;
	}

	public void setIsEnoughBandWithForXmpp(boolean isEnoughBandWithForXmpp) {
		if(isEnoughBandWithForXmpp==false){
            if(getActivity() != null)
			getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {

					Toast.makeText(getApplicationContext(), getString(
							R.string.BadNotEnought), Toast.LENGTH_SHORT).show();
				}
			});

		}
		if(call.equals("linphone")){

			if(flagisEnoughBandWithForXmpp==false && isEnoughBandWithForXmpp==true) {
				Intent serviceXmpp = new Intent(getApplicationContext(),
						ServiceRobot.class);
				if (isServiceRunning("com.orrobotics.orsmartcontrol.service.ServiceXMPP")) {
					stopService(serviceXmpp);
				}
				startService(serviceXmpp);
			}
		}
		this.flagisEnoughBandWithForXmpp = isEnoughBandWithForXmpp;
	}



	private class RegistrationThread extends Thread {
		@Override
		public void run() {
			super.run();
			try {
				String pseudoSip = "sip:"
						+ getPseudoSIP()
						+ "@"
						+ SmartRobotApplication.getParamFromFile("server",
						"Configuration.properties", null);
				LinRegistration.Registration(pseudoSip, getPasswordSIP());
			} catch (LinphoneCoreException e) {
				e.printStackTrace();

			}
		}
	}

	public void stopMainLoop() {
		// LinphoneRegistration.unRegistration();
		if(LinRegistration != null)
			LinRegistration.stopMainLoop();

	}

	public void setOrientationValue(float floatValue) {
		// TODO Auto-generated method stub
		this.floatValue = floatValue;
	}

	public float getOrientationValue() {
		return floatValue;
	}

	public void startGyro() {
		// TODO Auto-generated method stub
		gyroOrientation[0] = 0.0f;
		gyroOrientation[1] = 0.0f;
		gyroOrientation[2] = 0.0f;
		/**
		 * Initialisation de la matrice gyroMatrix
		 */
		gyroMatrix[0] = 1.0f;
		gyroMatrix[1] = 0.0f;
		gyroMatrix[2] = 0.0f;
		gyroMatrix[3] = 0.0f;
		gyroMatrix[4] = 1.0f;
		gyroMatrix[5] = 0.0f;
		gyroMatrix[6] = 0.0f;
		gyroMatrix[7] = 0.0f;
		gyroMatrix[8] = 1.0f;

		/**
		 * attendez une seconde avant de démarrer gyroscope et magnétométre /
		 * accéléromètre
		 */

		fuseTimer.scheduleAtFixedRate(new calculateFusedOrientationTask(),
				1000, TIME_CONSTANT);
	}

	public void calculateAccMagOrientation(float[] values) {
		// TODO Auto-generated method stub

		/**
		 * Copie de nouvelles données de l'accéléromètre dans le tableau accel
		 * et calculer l'orientation
		 *
		 */
		System.arraycopy(values, 0, accel, 0, 3);
		if (SensorManager
				.getRotationMatrix(rotationMatrix, null, accel, magnet)) {
			SensorManager.getOrientation(rotationMatrix, accMagOrientation);
		}
	}

	public void gyroFunction(SensorEvent event) {
		/**
		 * ne commence pas avant première orientation accélèromètre /
		 * magnétomètre a été acquise
		 *
		 */

		if (accMagOrientation == null)
			return;
		/**
		 * Initialisation de la matrice de rotation du gyroscope
		 *
		 */
		if (initState) {
			float[] initMatrix = new float[9];
			initMatrix = getRotationMatrixFromOrientation(accMagOrientation);
			float[] test = new float[3];
			SensorManager.getOrientation(initMatrix, test);
			gyroMatrix = matrixMultiplication(gyroMatrix, initMatrix);
			initState = false;
		}

		/**
		 * copier les nouvelles valeurs gyroscopiques dans le tableau gyro
		 * convertir les données de gyro du gyroscope dans un vecteur de
		 * rotation
		 */

		float[] deltaVector = new float[4];
		if (timestamp != 0) {
			final float dT = (event.timestamp - timestamp) * NS2S;
			System.arraycopy(event.values, 0, gyro, 0, 3);
			getRotationVectorFromGyro(gyro, deltaVector, dT / 2.0f);
		}

		timestamp = event.timestamp;

		/**
		 * contertir vecteru de rotation en une matrice de rotation
		 */

		float[] deltaMatrix = new float[9];
		SensorManager.getRotationMatrixFromVector(deltaMatrix, deltaVector);
		/**
		 * appliquer le nouvel intervalle de rotation à  la matrice de rotation
		 * du gyroscope.
		 */
		gyroMatrix = matrixMultiplication(gyroMatrix, deltaMatrix);
		/**
		 * obtenir l'orientation de gyroscope à  partir de la matrice de
		 * rotation
		 */
		SensorManager.getOrientation(gyroMatrix, gyroOrientation);
	}

	/**
	 * Cette fonction est emprunté depuis le lien
	 * http://developer.android.com/reference
	 * /android/hardware/SensorEvent.html#values Il calcule un vecteur de
	 * rotation à  partir des valeurs de vitesse angulaire du gyroscope
	 */

	private void getRotationVectorFromGyro(float[] gyroValues,
										   float[] deltaRotationVector, float timeFactor) {
		float[] normValues = new float[3];

		/**
		 * Calculer les valeurs de la vitesse angulaire
		 */
		float omegaMagnitude = (float) Math
				.sqrt(gyroValues[0] * gyroValues[0] + gyroValues[1]
						* gyroValues[1] + gyroValues[2] * gyroValues[2]);
		/**
		 * Normaliser le vecteur de rotation si elle est assez grande pour
		 * obtenir l'axe
		 *
		 */
		if (omegaMagnitude > EPSILON) {
			normValues[0] = gyroValues[0] / omegaMagnitude;
			normValues[1] = gyroValues[1] / omegaMagnitude;
			normValues[2] = gyroValues[2] / omegaMagnitude;
		}

		float thetaOverTwo = omegaMagnitude * timeFactor;
		float sinThetaOverTwo = (float) Math.sin(thetaOverTwo);
		float cosThetaOverTwo = (float) Math.cos(thetaOverTwo);
		deltaRotationVector[0] = sinThetaOverTwo * normValues[0];
		deltaRotationVector[1] = sinThetaOverTwo * normValues[1];
		deltaRotationVector[2] = sinThetaOverTwo * normValues[2];
		deltaRotationVector[3] = cosThetaOverTwo;
	}

	private float[] matrixMultiplication(float[] A, float[] B) {
		float[] result = new float[9];

		result[0] = A[0] * B[0] + A[1] * B[3] + A[2] * B[6];
		result[1] = A[0] * B[1] + A[1] * B[4] + A[2] * B[7];
		result[2] = A[0] * B[2] + A[1] * B[5] + A[2] * B[8];

		result[3] = A[3] * B[0] + A[4] * B[3] + A[5] * B[6];
		result[4] = A[3] * B[1] + A[4] * B[4] + A[5] * B[7];
		result[5] = A[3] * B[2] + A[4] * B[5] + A[5] * B[8];

		result[6] = A[6] * B[0] + A[7] * B[3] + A[8] * B[6];
		result[7] = A[6] * B[1] + A[7] * B[4] + A[8] * B[7];
		result[8] = A[6] * B[2] + A[7] * B[5] + A[8] * B[8];

		return result;
	}

	private float[] getRotationMatrixFromOrientation(float[] o) {
		float[] xM = new float[9];
		float[] yM = new float[9];
		float[] zM = new float[9];

		float sinX = (float) Math.sin(o[1]);
		float cosX = (float) Math.cos(o[1]);
		float sinY = (float) Math.sin(o[2]);
		float cosY = (float) Math.cos(o[2]);
		float sinZ = (float) Math.sin(o[0]);
		float cosZ = (float) Math.cos(o[0]);

		// rotation about x-axis (pitch)
		xM[0] = 1.0f;
		xM[1] = 0.0f;
		xM[2] = 0.0f;
		xM[3] = 0.0f;
		xM[4] = cosX;
		xM[5] = sinX;
		xM[6] = 0.0f;
		xM[7] = -sinX;
		xM[8] = cosX;

		// rotation about y-axis (roll)
		yM[0] = cosY;
		yM[1] = 0.0f;
		yM[2] = sinY;
		yM[3] = 0.0f;
		yM[4] = 1.0f;
		yM[5] = 0.0f;
		yM[6] = -sinY;
		yM[7] = 0.0f;
		yM[8] = cosY;

		// rotation about z-axis (azimuth)
		zM[0] = cosZ;
		zM[1] = sinZ;
		zM[2] = 0.0f;
		zM[3] = -sinZ;
		zM[4] = cosZ;
		zM[5] = 0.0f;
		zM[6] = 0.0f;
		zM[7] = 0.0f;
		zM[8] = 1.0f;

		// rotation order is y, x, z (roll, pitch, azimuth)
		float[] resultMatrix = matrixMultiplication(xM, yM);
		resultMatrix = matrixMultiplication(zM, resultMatrix);
		return resultMatrix;
	}

	class calculateFusedOrientationTask extends TimerTask {
		@Override
		public void run() {
			float oneMinusCoeff = 1.0f - FILTER_COEFFICIENT;

			// azimuth y
			if (gyroOrientation[0] < -0.5 * Math.PI
					&& accMagOrientation[0] > 0.0) {
				fusedOrientation[0] = (float) (FILTER_COEFFICIENT
						* (gyroOrientation[0] + 2.0 * Math.PI) + oneMinusCoeff
						* accMagOrientation[0]);
				fusedOrientation[0] -= (fusedOrientation[0] > Math.PI) ? 2.0 * Math.PI
						: 0;
			} else if (accMagOrientation[0] < -0.5 * Math.PI
					&& gyroOrientation[0] > 0.0) {
				fusedOrientation[0] = (float) (FILTER_COEFFICIENT
						* gyroOrientation[0] + oneMinusCoeff
						* (accMagOrientation[0] + 2.0 * Math.PI));
				fusedOrientation[0] -= (fusedOrientation[0] > Math.PI) ? 2.0 * Math.PI
						: 0;
			} else {
				fusedOrientation[0] = FILTER_COEFFICIENT * gyroOrientation[0]
						+ oneMinusCoeff * accMagOrientation[0];
			}

			// pitch x
			if (gyroOrientation[1] < -0.5 * Math.PI
					&& accMagOrientation[1] > 0.0) {
				fusedOrientation[1] = (float) (FILTER_COEFFICIENT
						* (gyroOrientation[1] + 2.0 * Math.PI) + oneMinusCoeff
						* accMagOrientation[1]);
				fusedOrientation[1] -= (fusedOrientation[1] > Math.PI) ? 2.0 * Math.PI
						: 0;
			} else if (accMagOrientation[1] < -0.5 * Math.PI
					&& gyroOrientation[1] > 0.0) {
				fusedOrientation[1] = (float) (FILTER_COEFFICIENT
						* gyroOrientation[1] + oneMinusCoeff
						* (accMagOrientation[1] + 2.0 * Math.PI));
				fusedOrientation[1] -= (fusedOrientation[1] > Math.PI) ? 2.0 * Math.PI
						: 0;
			} else {
				fusedOrientation[1] = FILTER_COEFFICIENT * gyroOrientation[1]
						+ oneMinusCoeff * accMagOrientation[1];
			}

			// roll z
			if (gyroOrientation[2] < -0.5 * Math.PI
					&& accMagOrientation[2] > 0.0) {
				fusedOrientation[2] = (float) (FILTER_COEFFICIENT
						* (gyroOrientation[2] + 2.0 * Math.PI) + oneMinusCoeff
						* accMagOrientation[2]);
				fusedOrientation[2] -= (fusedOrientation[2] > Math.PI) ? 2.0 * Math.PI
						: 0;
			} else if (accMagOrientation[2] < -0.5 * Math.PI
					&& gyroOrientation[2] > 0.0) {
				fusedOrientation[2] = (float) (FILTER_COEFFICIENT
						* gyroOrientation[2] + oneMinusCoeff
						* (accMagOrientation[2] + 2.0 * Math.PI));
				fusedOrientation[2] -= (fusedOrientation[2] > Math.PI) ? 2.0 * Math.PI
						: 0;
			} else {
				fusedOrientation[2] = FILTER_COEFFICIENT * gyroOrientation[2]
						+ oneMinusCoeff * accMagOrientation[2];
			}

			// overwrite gyro matrix and orientation with fused orientation
			// to comensate gyro drift
			gyroMatrix = getRotationMatrixFromOrientation(fusedOrientation);
			System.arraycopy(fusedOrientation, 0, gyroOrientation, 0, 3);
			setOrientationValue(new BigDecimal(gyroOrientation[1] * 180
					/ Math.PI).setScale(3, RoundingMode.HALF_UP).floatValue());
		}
	}

	public void stopFuseTimer() {
		// TODO Auto-generated method stub
		if (fuseTimer != null) {
			fuseTimer.cancel();
			fuseTimer = null;
		}
	}

	public void magnFunction(float[] values) {
		// TODO Auto-generated method stub
		/**
		 * Copie de nouvelles données de l'accélérométre dans le tableau magnet
		 * et calculer l'orientation
		 *
		 */
		System.arraycopy(values, 0, magnet, 0, 3);
	}

	public void setRobotType() {
		type = Integer.parseInt(getParam(RemoteControlUtil.EXTRAS_ROBOT_TYPE));
	}

	public int getRobotType() {
		return type;
	}

	public void setRobotName() {
		robotName = getParam(RemoteControlUtil.ROBOT_NAME);
	}

	public String getRobotName() {

		return robotName;
	}

	public void setExtraRobotName() {
		extraRobotName = getParam(RemoteControlUtil.EXTRAS_ROBOT_NAME);
	}

	public String getExtraRobotName() {

		return extraRobotName;
	}

	/**
	 *
	 * Cette méthode permet de vérifier si une application est installé dans
	 * l'appareil
	 *
	 * @param context
	 *            est le contexte de l'activité
	 * @return true si l'application est installé sinon la méthode retourne
	 *         false
	 *
	 */

	public boolean isAppInstalled(Context context, String app) {
		PackageManager myPackageMgr = context.getPackageManager();
		try {
			myPackageMgr.getPackageInfo(app, PackageManager.GET_ACTIVITIES);
		} catch (PackageManager.NameNotFoundException e) {
			return (false);
		}
		return (true);
	}

	public void quitApplication(String processName) {
		final ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
		am.killBackgroundProcesses(processName);
	}

	/**
	 * Cette méthode permet de savoir si une application est lancé
	 *
	 * @param context
	 *            est le contexte de l'activité
	 *
	 * @param processName
	 *            est le nom du processus de l'application
	 */
	public boolean isAppRunning(Context context, String processName) {
		boolean isRunning = false;
		ActivityManager activityManager = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningAppProcessInfo> procInfos = activityManager
				.getRunningAppProcesses();
		for (int i = 0; i < procInfos.size(); i++) {
			if (procInfos.get(i).processName.equals(processName)) {
				isRunning = true;
			}
		}
		return isRunning;
	}

	/**
	 * Cette méthode permet d'arreter une l'application
	 *
	 * @param processName
	 *            est le nom du processus de l'application
	 *
	 */
	public void quitApplicationAlbert(String processName) {
		ActivityManager activityManager = (ActivityManager) getApplicationContext()
				.getSystemService(Context.ACTIVITY_SERVICE);
		activityManager.killBackgroundProcesses(processName);
	}

	/**
	 * Cette méthode permet lancer une l'application
	 */

	public void lancer_app(String name) {
		PackageManager packageManager = getApplicationContext()
				.getPackageManager();
		Intent i = packageManager.getLaunchIntentForPackage(name);
		getApplicationContext().startActivity(i);

	}

	public boolean getFlagCloseControl() {
		// TODO Auto-generated method stub
		return flagCloseControl;
	}

	public void setFlagCloseControl(boolean flagCloseControl) {
		// TODO Auto-generated method stub
		this.flagCloseControl = flagCloseControl;
	}

	public void onActivityCreateSetTheme(Activity activity) {
		int sTheme = getThemeFromFile();

		switch (sTheme) {

			case THEME_GREEN:
				activity.setTheme(R.style.GreenTheme);
				break;
			case THEME_BLACK:
				activity.setTheme(R.style.BlackTheme);
				break;
			default:
				activity.setTheme(R.style.BlueTheme);
		}
	}

	public static int getThemeFromFile() {
		String theme = getParamFromFile("theme", "Configuration.properties",
				null);
		int theme_id = 0;

		if (theme != null)
			if (theme.equals("vert") || theme.equals("green")) {
				theme_id = 1;
			} else if (theme.equals("bleu") || theme.equals("blue")) {
				theme_id = 0;
			} else if (theme.equals("noir") || theme.equals("black")) {
				theme_id = 2;
			}
		return theme_id;

	}

	public boolean isLogoOptionEnabled() {
		boolean enabled = false;
		if (!getParamFromFile("logo.fileName", "Configuration.properties", null)
				.equals("null"))
			enabled = true;
		return enabled;
	}

	public Bitmap getLogoFromExternalStorage() {
		Bitmap open = null;
		String qrPath = path1.getAbsolutePath()
				+ "/SmartRobot/"
				+ getParamFromFile("logo.fileName", "Configuration.properties",
				null);
		if (fileExist(getParamFromFile("logo.fileName",
				"Configuration.properties", null))) {
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inPreferredConfig = Bitmap.Config.ARGB_8888;
			open = BitmapFactory.decodeFile(qrPath, options);
		}
		return open;
	}

	/**
	 * Supprimer la notification
	 */
	public void cancelNotification(Context context, String robot) {
		NotificationManager notifManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		NotificationManager notificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		notifManager.cancelAll();

		notificationManager.cancel(robot, 1);

	}


	public String getTtsLangSelection() {
		return ttsLangSelection;
	}

	public void setRegistered(boolean isRegitred) {
		// TODO Auto-generated method stub
		this.isRegitred = isRegitred;
	}

	public boolean isRegisted() {
		return isRegitred;

	}

	public void setOpenFireConnected(boolean isRegitred) {
		// TODO Auto-generated method stub
		this.isOpenFireConncted = isRegitred;
	}

	public boolean isOpenFireConnected() {
		return isOpenFireConncted;

	}

	public boolean isNetworkUP() {
		return networkUP;
	}

	public void setNetworkUP(boolean networkUP) {
		this.networkUP = networkUP;
	}

	public boolean isAutoRegistrationOn() {
		return autoRegistrationOn;
	}

	public void setAutoRegistartionOn(boolean autoRegistartionOn) {
		this.autoRegistrationOn = autoRegistartionOn;
	}

	class Task_updateMac extends AsyncTask<String, Integer, Boolean> {

		@Override
		protected void onPreExecute() {

			super.onPreExecute();

		}

		@Override
		protected void onPostExecute(Boolean result) {

			super.onPostExecute(result);
		}

		@Override
		protected Boolean doInBackground(String... params) {

			bleScanController = new BleScanControllerImpl(new BleScanModelImpl(getApplicationContext(), getActivity()));
			bleScanController.updateMacAdresse(getParam(RemoteControlUtil.NEW_MAC),
					getParam(RemoteControlUtil.NEW_NUM_SERIE), false);
			return true;
		}
	}

	public float changeTextSize() {
		// TODO Auto-generated method stub
		float size = Float.parseFloat(getParamFromFile("msgVocal.taille",
				"Configuration.properties", null));
		return size;

	}

	public Typeface changeTextFont() {
		// TODO Auto-generated method stub
		String font = getParamFromFile("msgVocal.police",
				"Configuration.properties", null);
		Typeface tf = Typeface.SERIF;

		if (font.equals("normal")) {
			tf = Typeface.DEFAULT;
		} else if (font.equals("serif")) {
			tf = Typeface.SERIF;
		} else if (font.equals("monospace")) {
			tf = Typeface.MONOSPACE;
		}
		return tf;
	}

	public int changeTextColor(String type) {
		// TODO Auto-generated method stub
		int src;
		String color;
		if(type.equals("question")){
			color = getParamFromFile("msgVocal.color.question",
					"Configuration.properties", null);
			src = getResources().getColor(R.color.white);
		}
		else{
			color = getParamFromFile("msgVocal.color.response",
					"Configuration.properties", null);
			src = getResources().getColor(R.color.yellow);
		}
		if (color == null)
			color= "white";

		if (color.equals("noir") || color.equals("black")) {
			src = getResources().getColor(R.color.black);
		} else if (color.equals("blanc") || color.equals("white")) {
			src = getResources().getColor(R.color.white);
		} else if (color.equals("gris") || color.equals("grey")) {
			src = getResources().getColor(R.color.grey);
		} else if (color.equals("rouge") || color.equals("red")) {
			src = getResources().getColor(R.color.red_btn_bg_color);
		} else if (color.equals("vert") || color.equals("green")) {
			src = getResources().getColor(R.color.green);
		} else if (color.equals("bleu") || color.equals("blue")) {
			src = getResources().getColor(R.color.blue);
		} else if (color.equals("yellow") || color.equals("jaune")) {
			src = getResources().getColor(R.color.yellow);
		}
		return src;
	}

	public static String getWithingsValues(String deviceName,
										   String withingsUrl, String withingsConsumerKey,
										   String withingsTokenKey, String action, String responseFormat,
										   String dateInput, String dateOuput) {
		String jsonResponse = null;
		String JSONvalue = "";
		String response = "";
		JSONObject json = null;

		String request = withingsUrl
				+ "/v2/measure?action=getactivity&date="
				+ setWithingsDate(dateOuput)
				+ "&oauth_consumer_key="
				+ withingsConsumerKey
				+ "&oauth_nonce=4c75357d5220965e1b60f53ab7bef7ea&oauth_signature=awjI%2Fx4RejBot6WtmhYoZFCiadw%3D&oauth_signature_method=HMAC-SHA1&oauth_timestamp=1488185817&oauth_token="
				+ withingsTokenKey + "&oauth_version=1.0";
		try {
			jsonResponse = performRequest(request);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			json = new JSONObject(jsonResponse);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			JSONvalue = json.getJSONObject("body").getString(action);
			if (JSONvalue.contains(".")) {
				JSONvalue = JSONvalue.replace(".", ",");
			}
			if (dateOuput.equals("today")) {

				response = deviceName + " "
						+ responseFormat.replaceAll("%", JSONvalue);
			} else {
				response = deviceName + " "
						+ responseFormat.replaceAll("%", JSONvalue) + " "
						+ dateInput;
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

		return response;
	}

	@SuppressLint("SimpleDateFormat")
	private static String setWithingsDate(String date) {
		// TODO Auto-generated method stub

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance();

		if (date.equals("yesterday")) {
			c.add(Calendar.DATE, -1);

		} else if (date.equals("before yesterday")) {
			c.add(Calendar.DATE, -2);

		} else if (date.equals("monday")) {
			if (c.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) {
				c.add(Calendar.DAY_OF_WEEK, -7);
			} else
				while (c.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
					c.add(Calendar.DAY_OF_WEEK, -1);
				}

		} else if (date.equals("tuesday")) {
			if (c.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY) {
				c.add(Calendar.DAY_OF_WEEK, -7);
			} else
				while (c.get(Calendar.DAY_OF_WEEK) != Calendar.TUESDAY) {
					c.add(Calendar.DAY_OF_WEEK, -1);
				}

		} else if (date.equals("wednesday")) {
			if (c.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY) {
				c.add(Calendar.DAY_OF_WEEK, -7);
			} else
				while (c.get(Calendar.DAY_OF_WEEK) != Calendar.WEDNESDAY) {
					c.add(Calendar.DAY_OF_WEEK, -1);
				}

		} else if (date.equals("thursday")) {
			if (c.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY) {
				c.add(Calendar.DAY_OF_WEEK, -7);
			} else
				while (c.get(Calendar.DAY_OF_WEEK) != Calendar.THURSDAY) {
					c.add(Calendar.DAY_OF_WEEK, -1);
				}

		} else if (date.equals("friday")) {
			if (c.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY) {
				c.add(Calendar.DAY_OF_WEEK, -7);
			} else
				while (c.get(Calendar.DAY_OF_WEEK) != Calendar.FRIDAY) {
					c.add(Calendar.DAY_OF_WEEK, -1);
				}

		} else if (date.equals("saturday")) {
			if (c.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
				c.add(Calendar.DAY_OF_WEEK, -7);
			} else
				while (c.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY) {
					c.add(Calendar.DAY_OF_WEEK, -1);
				}

		} else if (date.equals("sunday")) {
			if (c.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
				c.add(Calendar.DAY_OF_WEEK, -7);
			} else
				while (c.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
					c.add(Calendar.DAY_OF_WEEK, -1);
				}

		}
		return df.format(c.getTime());

	}

	private static String performRequest(String url) throws Exception {
		// Perform the request
		HttpClient client = new DefaultHttpClient();
		HttpGet get = new HttpGet(url);
		HttpResponse response = client.execute(get);

		// Translate the response into a string
		String result = "";
		try {
			InputStream in = response.getEntity().getContent();
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(in));
			StringBuilder str = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				str.append(line + "\n");
			}
			in.close();
			result = str.toString();
		} catch (Exception ex) {
			result = "Error";
		}

		// Decode JSon object
		JSONObject json = new JSONObject(result);
		// Make sure that the request was successful
		if (json.getInt("status") != 0) {
			throw new Exception("Withings error: " + json.getInt("status"));
		}

		return result;
	}

	public void setEndScenario(Boolean end) {
		this.end = end;
	}

	public Boolean getEnd() {
		return end;
	}

	public Boolean connectToBotLibre() {
		Boolean success = true;
		String userId = getParamFromFile("botLibre.userName",
				"Configuration.properties", null);
		String userPassword = getParamFromFile("botLibre.userPassword",
				"Configuration.properties", null);

		UserConfig user = new UserConfig();
		user.user = userId;
		user.password = userPassword;
		try {
			user = connection.connect(user);

		} catch (SDKException e) {
			e.printStackTrace();
			success = false;
		}
		return success;

	}

	@SuppressLint("DefaultLocale")
	public String getLangageTTS() {
		return langTTS;
	}

	public void setLangageTTS() {
		langTTS = getRobotParam(RemoteControlUtil.TTS_LANGUAGE);
	}

	public String pandorabotSendChat(String message) {
		String response = "";

		String lang_pandora = getParamFromFile("pandorabot."
						+ getLangageTTS().split("_")[0] + "Bot",
				"Configuration.properties", null);
		String en_pandora = getParamFromFile("pandorabot.enBot",
				"Configuration.properties", null);

		if (lang_pandora == null) {
			connector = new PandorabotConnector(en_pandora, this);
		} else {
			connector = new PandorabotConnector(lang_pandora, this);
		}
		response = connector.doServerRequest(message);
		if (response == "NoResponseServer")
			connector.cancelServerRequest();
		return response;
	}


	public String botLibreSendChat(String botInstance, String message) {
		String response = "";
		ChatConfig chat = new ChatConfig();
		chat.instance = botInstance;
		chat.message = message;
		ChatResponse chatResponse = connection.chat(chat);
		response = chatResponse.message;
		return response;

	}

	public String selectBotLibre() {

		String lang_bot = getParamFromFile(
				"botLibre." + getLangageTTS().split("_")[0] + "Bot",
				"Configuration.properties", null);
		String en_bot = getParamFromFile("botLibre.enBot",
				"Configuration.properties", null);
		String botName = "";
		String botId = "";
		BrowseConfig browse = new BrowseConfig();

		if (lang_bot == null) {
			botName = en_bot;
		} else {
			botName = lang_bot;
		}

		browse.type = "Bot";
		browse.typeFilter = "Personal";
		List<WebMediumConfig> bots = connection.browse(browse);
		for (int i = 0; i < bots.size(); i++) {
			if (bots.get(i).name.equals(botName))
				botId = bots.get(i).id;

		}

		return botId;
	}

	public String getchatBotType() {

		return getParamFromFile("chatbot.type", "Configuration.properties",
				null);
	}

	public String getLangApp() {

		return langApp;
	}

	public void setLangApp() {

		langApp = getRobotParam(RemoteControlUtil.APPLICATION_LANGAGE);
	}

	public void setLocal() {
		// TODO Auto-generated method stub

		final Resources resources = getResources();
		final Configuration configuration = resources.getConfiguration();
		final Locale locale = getLocale();
		if (!configuration.locale.equals(locale)) {
			configuration.setLocale(locale);
			resources.updateConfiguration(configuration,
					resources.getDisplayMetrics());
		}

	}

	public Locale getLocale() {

		return new Locale(langApp);
	}

	public String getMessage(String string) {
		// TODO Auto-generated method stub
		String msg;
		if (getLangageTTS().contains("en")) {
			msg = getParamFromFile(string + ".en", "Configuration.properties",
					null);

		} else
			msg = getParamFromFile(string + ".fr", "Configuration.properties",
					null);
		return msg;
	}

	public void setTelepresence(boolean istelepresence) {
		// TODO Auto-generated method stub
		this.istelepresence = istelepresence;
	}

	public Boolean isTelepresence() {
		return istelepresence;
	}

	public void setTelepresenceControl(boolean istelepresenceControl) {
		// TODO Auto-generated method stub
		this.istelepresenceControl = istelepresenceControl;
	}

	public Boolean isTelepresenceControl() {
		return istelepresenceControl;
	}

	public void call(String pseudo) {

		pseudoLinphone = pseudo;
		launchthread = new LaunchingThread();
		launchthread.start();

	}

	private class LaunchingThread extends Thread {
		@Override
		public void run() {
			super.run();

			try {
				LinphoneAddress address =LinphoneCoreFactory.instance()
						.createLinphoneAddress("sip:" + pseudoLinphone + "@"
								+ adressServer);
				LinRegistration.launchCall(address);

			} catch (LinphoneCoreException e) {
				e.printStackTrace();
			}
		}

	}

	public void setMacAdressHexiwear(String deviceMac) {
		// TODO Auto-generated method stub
		this.deviceMac = deviceMac;
	}

	public String getMacAdressHexiwear() {
		return deviceMac;
	}

	public void setHexiwearInfo(String read_data) {
		// TODO Auto-generated method stub
		this.read_data = read_data;
	}

	public String getHexiwearInfo() {
		return read_data;
	}

	public void setSurfaceHolder(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		this.holder = holder;
	}

	public SurfaceHolder getSurfaceHolder() {
		return holder;
	}

	public void enableSpeaker(boolean speaker) {
		AudioManager audioManager = (AudioManager) getApplicationContext()
				.getSystemService(Context.AUDIO_SERVICE);

		if (speaker == false && type == 5) {
			if (LinphoneRegistration.lc != null)
				LinphoneRegistration.lc.enableSpeaker(false);

			startBle = true;
			audioManager.setMode(AudioManager.STREAM_VOICE_CALL);
			audioManager.setBluetoothScoOn(true);
			audioManager.startBluetoothSco();
			int maxVolume = audioManager
					.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL);
			audioManager.setStreamVolume(AudioManager.STREAM_VOICE_CALL,
					maxVolume, 0);

		} else {
			if (startBle == true) {
				audioManager.setBluetoothScoOn(false);
				audioManager.stopBluetoothSco();
			}
			startBle = false;
			audioManager.setMode(AudioManager.MODE_NORMAL);
			audioManager.setSpeakerphoneOn(true);

		}

	}

    public boolean isMicMute() {
        // TODO Auto-generated method stub

            AudioManager audioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
            return  audioManager.isMicrophoneMute();

        }

	public void enableOrDisableMic(boolean b) {
		// TODO Auto-generated method stub
		if(call.equals("linphone")){
			if (LinphoneRegistration.lc != null)
				LinphoneRegistration.lc.muteMic(b);

		}else if(call.equals("webrtc")){
			AudioManager audioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
			audioManager.setMicrophoneMute(b);

		}
	}

	public void cancelPandorabotTask() {
		if (connector != null) {
			connector.cancelServerRequest();
		}
	}


	private void renameFiles() {

		if (!version.equals(getParam(RemoteControlUtil.APP_VERSION_NAME))) {

			File robotDossier = new File(
					Environment.getExternalStorageDirectory() + "/OrSmartRobot");
			if (robotDossier.exists()) {

				robotDossier.renameTo(new File(Environment
						.getExternalStorageDirectory() + "/SmartRobot"));
			}

			File paramUbo = new File(Environment.getExternalStorageDirectory()
					+ "/SmartRobot/Parametre_UBO");
			if (paramUbo.exists()) {
				paramUbo.renameTo(new File(Environment
						.getExternalStorageDirectory()
						+ "/SmartRobot/Parametres_TEAMBOT"));
			}
			File paramSanbot = new File(
					Environment.getExternalStorageDirectory()
							+ "/SmartRobot/Parametre_TEAMBOT");
			if (paramSanbot.exists()) {
				paramSanbot.renameTo(new File(Environment
						.getExternalStorageDirectory()
						+ "/SmartRobot/Parametres_SANBOT"));
			}
			File configUbo = new File(Environment.getExternalStorageDirectory()
					+ "/SmartRobot/smartUBO.properties");
			if (configUbo.exists()) {
				configUbo.renameTo(new File(Environment
						.getExternalStorageDirectory()
						+ "/SmartRobot/teambotSmart.properties"));
			}
			File configSanbot = new File(
					Environment.getExternalStorageDirectory()
							+ "/SmartRobot/smartTeambot.properties");
			if (configSanbot.exists()) {
				configSanbot.renameTo(new File(Environment
						.getExternalStorageDirectory()
						+ "/SmartRobot/sanbotSmart.properties"));
			}

			File logUbo = new File(Environment.getExternalStorageDirectory()
					+ "/SmartRobot/Log_OrSmartUbo.txt");
			if (logUbo.exists()) {
				logUbo.renameTo(new File(Environment
						.getExternalStorageDirectory()
						+ "/SmartRobot/Log_teambot.txt"));
			}
			File logSanbot = new File(Environment.getExternalStorageDirectory()
					+ "/SmartRobot/Log_OrSmartTeambot.txt");
			if (logSanbot.exists()) {
				logSanbot.renameTo(new File(Environment
						.getExternalStorageDirectory()
						+ "/SmartRobot/Log_sanbot.txt"));
			}

			File logRobot = new File(Environment.getExternalStorageDirectory()
					+ "/SmartRobot/Log_OrSmartRobot.txt");
			if (logRobot.exists()) {
				logRobot.renameTo(new File(Environment
						.getExternalStorageDirectory()
						+ "/SmartRobot/Log_SmartRobot.txt"));
			}
			File scenarioUbo = new File(
					Environment.getExternalStorageDirectory()
							+ "/SmartRobot/OrSmartUboScenarios");
			if (scenarioUbo.exists()) {
				scenarioUbo.renameTo(new File(Environment
						.getExternalStorageDirectory()
						+ "/SmartRobot/SmartTeambotScenarios"));
			}

		}
	}

	public int getBandWithFromSelectedSize(int selectedVideoSize) {
		// TODO Auto-generated method stub

		int bandwidth = 512;
		switch (selectedVideoSize) {
			case 1:
				bandwidth = 1024 + 128;
				break;
			case 4:
				bandwidth = 380;
				break;
			case 5:
				bandwidth = 256;
				break;
			default:
				break;
		}
		return bandwidth;
	}

	public void enableOrDisableCamera(boolean enable) {
		// TODO Auto-generated method stub
		if (enable)
			setRobotEditor(RemoteControlUtil.CAMERA, "on");
		else
			setRobotEditor(RemoteControlUtil.CAMERA, "off");

		LinphoneCall call = LinphoneRegistration.lc.getCurrentCall();
		if (LinphoneRegistration.lc != null && call != null) {

			call.enableCamera(enable);
			if (enable)
				notifyObservers("camBotOn");
			else
				notifyObservers("camBotOff");
		}

	}

	public void changeVideoSize(int selectedVideoSize) {
		// TODO Auto-generated method stub
		int bandwidth = 512;
		setRobotEditor(RemoteControlUtil.VIDEO_QUALITY, "" + selectedVideoSize);
		if (LinphoneRegistration.lc != null) {
			LinphoneRegistration.lc
					.setPreferredVideoSizeByName(getSelectedVideoSizeName(selectedVideoSize));
			bandwidth = getBandWithFromSelectedSize(selectedVideoSize);
			LinphoneRegistration.lc.setUploadBandwidth(bandwidth);
			LinphoneRegistration.lc.setDownloadBandwidth(bandwidth);
			LinphoneCall call = LinphoneRegistration.lc.getCurrentCall();

			if (call == null) {
				return;
			}

			LinphoneRegistration.lc.updateCall(call, null);

		}
	}

	public String getSelectedVideoSizeName(int selectedVideoSize) {
		// TODO Auto-generated method stub
		String videoSize = null;
		if (selectedVideoSize == 1) {
			videoSize = "720p";

		} else if (selectedVideoSize == 2) {
			videoSize = "vga";

		} else if (selectedVideoSize == 3) {
			videoSize = "cif";

		} else if (selectedVideoSize == 4) {
			videoSize = "qvga";

		} else if (selectedVideoSize == 5) {
			videoSize = "qcif";
		}
		return videoSize;
	}

	public void setVideoSize(String message) {
		// TODO Auto-generated method stub

		int bandwidth = 512;
		if (LinphoneRegistration.lc != null) {
			LinphoneRegistration.lc.setPreferredVideoSizeByName(message
					.split(";")[1]);

			if (message.contains("720p")) {
				bandwidth = 1024 + 128;
			} else if (message.contains("qvga")) {
				bandwidth = 380;
			} else if (message.contains("qcif")) {
				bandwidth = 256;
			}

			LinphoneRegistration.lc.setUploadBandwidth(bandwidth);
			LinphoneRegistration.lc.setDownloadBandwidth(bandwidth);

			if (LinphoneRegistration.lc.getCurrentCall() != null)
				LinphoneRegistration.lc.updateCall(
						LinphoneRegistration.lc.getCurrentCall(), null);

		}
	}

	public void echoCancellation(String message) {
		// TODO Auto-generated method stub
		if (message.contains("false")) {
			if (LinphoneRegistration.lc != null) {
				setRobotEditor(RemoteControlUtil.ECHO_CANCELLATION, "false");
				LinphoneRegistration.lc.enableEchoCancellation(false);
			}
		} else {
			if (LinphoneRegistration.lc != null) {
				setRobotEditor(RemoteControlUtil.ECHO_CANCELLATION, "true");
				LinphoneRegistration.lc.enableEchoCancellation(true);
			}
		}
	}

	public void adjustVolume(int streamType, int progress) {
		// TODO Auto-generated method stub

		AudioManager myAudioManager = (AudioManager) getActivity()
				.getSystemService(Context.AUDIO_SERVICE);

		int k = myAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		float f = (progress * k) / 100;

		int j = (int) Math.abs(f);
		try {
			if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
				switch (streamType) {
					case 3:


						myAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, j,
								AudioManager.STREAM_MUSIC);


						break;
					case 5:
						myAudioManager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, j,
								AudioManager.STREAM_NOTIFICATION);
						break;
					case 1:
						myAudioManager.setStreamVolume(AudioManager.STREAM_SYSTEM, j,
								AudioManager.STREAM_SYSTEM);
						break;

					default:
						break;
				}
			} else {
				myAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, j,
						AudioManager.STREAM_MUSIC);
			}
		}
		catch (Exception e) {
			NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
			if (!mNotificationManager.isNotificationPolicyAccessGranted()) {
				Intent intent = new Intent(android.provider.Settings.ACTION_NOTIFICATION_POLICY_ACCESS_SETTINGS);
				startActivity(intent);
			}
		}
	}

	public int getbatterieSelection(String readXmlFile) {
		// TODO Auto-generated method stub
		int battSelection = 1;
		if (readXmlFile.equals("10 s")) {
			battSelection = 1;

		} else if (readXmlFile.equals("50 s")) {
			battSelection = 2;

		} else if (readXmlFile.equals("1 min")) {
			battSelection = 3;

		} else if (readXmlFile.equals("2 min")) {
			battSelection = 4;

		}
		return battSelection;
	}

	public void initInternParams() {
		// TODO Auto-generated method stub
		setEditor(RemoteControlUtil.CONNECTED_TO_DEVICE, "false");
		if (getRobotType() != 6) {
			if (getParam(RemoteControlUtil.CONNECTED_TO_APP).equals(""))
				setEditor(RemoteControlUtil.CONNECTED_TO_APP, "false");
		}
		setEditor(RemoteControlUtil.CONNECTED_TO_SERVER, "false");
		setEditor(RemoteControlUtil.CONNECTED_TO_SIP_SERVER, "false");

		if (getParam(RemoteControlUtil.CALL_TYPE).equals(""))
			setEditor(RemoteControlUtil.CALL_TYPE, "linphone");

		if (getParam(RemoteControlUtil.REMOTE_CONTROL).equals(""))
			setEditor(RemoteControlUtil.REMOTE_CONTROL, "OFF");

		if (getParam(RemoteControlUtil.VIDEO_LENGHT).equals(""))
			setEditor(RemoteControlUtil.VIDEO_LENGHT, "0");


		setParams();
	}

	public void initRobotInternParams(String robotId) {
		// TODO Auto-generated method stub
		switch (type) {
			case 1:
				setSharedPrefRobot(this.getSharedPreferences("Atti_" + robotId,
						Context.MODE_PRIVATE));
				break;
			case 4:
				setSharedPrefRobot(this.getSharedPreferences("Albert_" + robotId,
						Context.MODE_PRIVATE));
				break;
			case 5:
				setSharedPrefRobot(this.getSharedPreferences("Teambot_" + robotId,
						Context.MODE_PRIVATE));
				break;
			case 6:
				setSharedPrefRobot(this.getSharedPreferences("Sanbot_" + robotId,
						Context.MODE_PRIVATE));
				break;
			default:
				break;
		}

		if (getRobotParam(RemoteControlUtil.ECHO_CANCELLATION).equals(""))
			setRobotEditor(RemoteControlUtil.ECHO_CANCELLATION, ("false"));
		if (getRobotParam(RemoteControlUtil.VIDEO_QUALITY).equals(""))
			setRobotEditor(RemoteControlUtil.VIDEO_QUALITY, "1");
		if (getRobotParam(RemoteControlUtil.CAMERA_ID).equals(""))
			setRobotEditor(RemoteControlUtil.CAMERA_ID, "1");
		if (getRobotParam(RemoteControlUtil.CAMERA).equals(""))
			setRobotEditor(RemoteControlUtil.CAMERA, "on");
		if (getRobotParam(RemoteControlUtil.MIC).equals(""))
			setRobotEditor(RemoteControlUtil.MIC, "true");
		if (getRobotParam(RemoteControlUtil.SPEAKER).equals(""))
			setRobotEditor(RemoteControlUtil.SPEAKER, "false");

		if (getRobotParam(RemoteControlUtil.SPEED_LINEAIRE).equals(""))
			setRobotEditor(RemoteControlUtil.SPEED_LINEAIRE, "1");

		if (getRobotParam(RemoteControlUtil.SPEED_LATERALE).equals(""))
			setRobotEditor(RemoteControlUtil.SPEED_LATERALE, "1");

		if (getRobotParam(RemoteControlUtil.VOCAL_FREQ_DECLENCH).equals(""))
			setRobotEditor(
					RemoteControlUtil.VOCAL_FREQ_DECLENCH,
					getParamFromFile("Frequence.declenchement",
							"Configuration.properties", null));

		if (getRobotParam(RemoteControlUtil.ISPERMANCE).equals(""))
			setRobotEditor(
					RemoteControlUtil.ISPERMANCE,
					getParamFromFile("vocale.mode.3.isenable",
							"Configuration.properties", null));

		if (getRobotParam(RemoteControlUtil.MODE_CONTROLE).equals(""))
			setRobotEditor(RemoteControlUtil.MODE_CONTROLE, "simple");
		if (getRobotParam(RemoteControlUtil.OPT_FACE_TO_FACE).equals(""))
			setRobotEditor(RemoteControlUtil.OPT_FACE_TO_FACE, "true");

		if (getRobotParam(RemoteControlUtil.OPT_CMD_VOCAL).equals(""))
			setRobotEditor(RemoteControlUtil.OPT_CMD_VOCAL, "true");

		if (getRobotParam(RemoteControlUtil.SHOW_ROBOT_APP).equals(""))
			setRobotEditor(RemoteControlUtil.SHOW_ROBOT_APP, "true");

		if (getRobotParam(RemoteControlUtil.SHOW_BATTERIE).equals(""))
			setRobotEditor(RemoteControlUtil.SHOW_BATTERIE, "true");

		if (getRobotParam(RemoteControlUtil.BATTERIE_DELAY).equals(""))
			setRobotEditor(RemoteControlUtil.BATTERIE_DELAY, "10 s");

		if (getRobotParam(RemoteControlUtil.EDIT_TTS_LANGUE).equals(""))
			setRobotEditor(RemoteControlUtil.EDIT_TTS_LANGUE, "true");

		if (getRobotParam(RemoteControlUtil.EDIT_TTS_SPEED).equals(""))
			setRobotEditor(RemoteControlUtil.EDIT_TTS_SPEED, "true");

		if (getRobotParam(RemoteControlUtil.APPLICATION_LANGAGE).equals(""))
			setRobotEditor(RemoteControlUtil.APPLICATION_LANGAGE, "fr");

		if (getRobotParam(RemoteControlUtil.TTS_LANGUAGE).equals(""))
			setRobotEditor(RemoteControlUtil.TTS_LANGUAGE, "en_US");

		if (getRobotParam(RemoteControlUtil.TTS_SPEED_POS).equals(""))
			setRobotEditor(RemoteControlUtil.TTS_SPEED_POS, "2");

		if (getRobotParam(RemoteControlUtil.TTS_LANG_SELECTION).equals(""))
			setRobotEditor(RemoteControlUtil.TTS_LANG_SELECTION, "English(United States)");

		if (getRobotParam(RemoteControlUtil.ISCLOUDSPEECH).equals(""))
			setRobotEditor(RemoteControlUtil.ISCLOUDSPEECH, "0");

		initTts();
		setRobotParams();
	}

	public static void deleteCache(Context context) {
		try {
			File dir = context.getCacheDir();
			deleteDir(dir);
		} catch (Exception e) {
		}
	}

	public static boolean deleteDir(File dir) {
		if (dir != null && dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
			return dir.delete();
		} else if (dir != null && dir.isFile()) {
			return dir.delete();
		} else {
			return false;
		}
	}

	public void setVolume() {
		AudioManager mAudioManager = (AudioManager) getApplicationContext()
				.getSystemService(Context.AUDIO_SERVICE);
		String volumeMedia = getParam(RemoteControlUtil.VOLUME);
		if (volumeMedia.equals("")) {
			mAudioManager
					.setStreamVolume(AudioManager.STREAM_MUSIC, mAudioManager
							.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
		} else {
			int volume = Integer.parseInt(volumeMedia);
			if (volume != 0) {
				mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
						volume, 0);
			} else {
				mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
						mAudioManager
								.getStreamMaxVolume(AudioManager.STREAM_MUSIC),
						0);
			}
		}
	}

	public void setSurface(Surface surface) {
		// TODO Auto-generated method stub
		this.surface = surface;
	}

	public Surface getSurface() {
		return surface;
	}

	public void playMusic(String filePath) {
		// TODO Auto-generated method stub

		mpMusic = new MediaPlayer();

		if (!isMusicPlaying()) {

			mpMusic.setAudioStreamType(AudioManager.STREAM_VOICE_CALL);

			Uri audio = Uri.parse(filePath);

			try {
				mpMusic.setDataSource(getApplicationContext(), audio);
				mpMusic.prepare();
				mpMusic.start();
				currentSongIndex++;
				mpMusic.setOnCompletionListener(onCompletionListener);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	private OnCompletionListener onCompletionListener = new OnCompletionListener() {

		@Override
		public void onCompletion(MediaPlayer mp) {

			// TODO Auto-generated method stub

			mpMusic.release();
			mpMusic = null;
			if (currentSongIndex < nbrMorceau)
				playMusic(filePath.get(currentSongIndex));
			else
				currentSongIndex = 0;

		}

	};

	public void stopMusic() {
		if (mpMusic != null) {
			if (mpMusic.isPlaying()) {
				mpMusic.stop();
				mpMusic.release();
				mpMusic = null;
				currentSongIndex = 0;
			}
		}
	}

	public void stopRadio() {
		if (mpRadio != null) {
			if (mpRadio.isPlaying()) {
				mpRadio.stop();
				mpRadio.release();
				mpRadio = null;
			}
		}
	}

	@SuppressLint("DefaultLocale") public ArrayList<String> fileMusicExist(String fileName) {
		ArrayList<String> filesPath = new ArrayList<String>();
		ContentResolver cr = getActivity().getContentResolver();

		Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
		String selection = MediaStore.Audio.Media.IS_MUSIC + "!= 0";
		String sortOrder = MediaStore.Audio.Media.TITLE + " ASC";
		Cursor cur = cr.query(uri, null, selection, null, sortOrder);
		int count = 0;

		if (cur != null) {
			count = cur.getCount();

			if (count > 0) {
				while (cur.moveToNext()) {
					String data = cur.getString(cur
							.getColumnIndex(MediaStore.Audio.Media.DATA));
					// Add code to get more column here
					if (data.substring(data.lastIndexOf("/")).toLowerCase()
							.contains(fileName)) {
						filesPath.add(data);
					}

				}

			}
		}

		cur.close();

		return filesPath;
	}

	public boolean isMusicPlaying() {
		// TODO Auto-generated method stub
		boolean isplaying = false;
		if (mpMusic != null)
			isplaying = mpMusic.isPlaying();
		return isplaying;
	}

	public boolean isRadioPlaying() {
		// TODO Auto-generated method stub
		boolean isplaying = false;
		if (mpRadio != null)
			isplaying = mpRadio.isPlaying();
		return isplaying;
	}

	public GNewsObject getGoogleNews(String url, String apiKey) {
		// TODO Auto-generated method stub
		GNewsObject response = null;

		newsConnector = new GoogleNewsConnector(this);

		response = newsConnector.doServerRequest(url, apiKey);
		return response;
	}

	public String wikiSearch(String key) {
		// TODO Auto-generated method stub
		String response = "";

		wikiConnector = new WikipediaConnector(this);

		response = wikiConnector.doServerRequest(key);
		return response;
	}

	public void playRadioStation(String station, int duration) {
		mpRadio = new MediaPlayer();
		mpRadio.setAudioStreamType(AudioManager.STREAM_VOICE_CALL);
		try {
			mpRadio.setDataSource(station);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			mpRadio.prepare();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mpRadio.start();
		if (duration != 0)
			new Handler().postDelayed(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					stopRadio();
				}
			}, duration * 1000 * 60);

	}


	public void setPx(int px) {
		this.px = px;
	}

	public int  getPx(){
		return px;
	}

	public void setEnableSuivi(boolean enableSuivi)
	{
		this.enableSuivi = enableSuivi;
	}

	public boolean isEnableSuivi(){
		return enableSuivi;
	}

	public void setStartSuivi(boolean startSuivi)
	{
		this.startSuivi = startSuivi;
	}

	public boolean isStartSuivi(){
		return startSuivi;
	}


	public boolean isEnoughBandWith(){
		return isEnoughBand;
	}

	public void setIsEnoughBandWith(Boolean IsEnoughBand){
		this.isEnoughBand= IsEnoughBand;
	}
	public void setMaxWifiDebit(){
		String debit_down_maxS = getParamFromFile("debit.download.max",
				"Configuration.properties", null);
		if (debit_down_maxS == null)
			debit_down_maxS ="100 Mbps";

		debit_down_max = Integer.parseInt(debit_down_maxS.split(" ")[0]);

		String debit_up_max = getParamFromFile("debit.upload.max",
				"Configuration.properties", null);
		if (debit_up_max == null)
			debit_up_max ="10 Mbps";

		debit_upload_max= Integer.parseInt(debit_up_max.split(" ")[0]);
	}
	public int getMaxWifiDebit(String debit_type){
		if(debit_type.equals("down")){
			return debit_down_max;
		}
		else{
			return debit_upload_max;
		}
	}

	public void shutDownTTS() {
		myTTS.stop();
		myTTS.shutdown();
	}
	public HashMap<String,String> getTtsList(){
		return  ttsList;
	}
	public TextToSpeech getTts(){
		return  myTTS;
	}
	public void initTts(){
		myTTS = new TextToSpeech(getApplicationContext(),
				new TextToSpeech.OnInitListener() {
					@SuppressLint("NewApi")
					@SuppressWarnings("deprecation")
					@Override
					public void onInit(int status) {
						if (status != TextToSpeech.ERROR) {
							myTTS.setEngineByPackageName("com.google.android.tts");

							myTTS.setOnUtteranceProgressListener(new UtteranceProgressListener() {

								@Override
								public void onStart(String utteranceId) {

								}

								@Override
								public void onDone(final String utteranceId) {
									notifyObservers("ttsDone;"
											+ utteranceId);

								}

								@Override
								public void onError(String utteranceId) {

								}

							});
							Locale[] locales = Locale.getAvailableLocales();
							List<Locale> localeList = new ArrayList<Locale>();
							for (Locale locale : locales) {
								String loc = null;

								try {

									loc = locale.getISO3Country();
								} catch (Exception e) {

								}

								if (loc != null) {
									int res = myTTS.isLanguageAvailable(locale);
									if (res == TextToSpeech.LANG_COUNTRY_AVAILABLE) {

										localeList.add(locale);
										if (getLangApp().equals("fr"))
											ttsList.put(
													locale.toString(),
													locale.getDisplayName(Locale.FRENCH));
										else
											ttsList.put(
													locale.toString(),
													locale.getDisplayName(Locale.ENGLISH));
									}

								}
							}
							notifyObservers("ttsRetrieveFinished");
						}
						if (status == TextToSpeech.ERROR) {
						}

					}
				});
	}
	public void unlockScreen() {
		String lockScreen="";
		lockScreen = getParamFromFile("screen.lock",
				"Configuration.properties", null);

		if(lockScreen == null)
			lockScreen ="off";
		if (lockScreen.equals("on")) {
			StartAct(null, null, ControlRobot.class,
					getActivity());

			PowerManager mPowerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
			PowerManager.WakeLock mWakeLock = mPowerManager.newWakeLock((PowerManager.SCREEN_BRIGHT_WAKE_LOCK
					| PowerManager.FULL_WAKE_LOCK
					| PowerManager.ACQUIRE_CAUSES_WAKEUP), "TAG");
			if (mWakeLock.isHeld()) {
				mWakeLock.release();
			}
			mWakeLock.acquire();
			disableKeyGuard();

		}
	}
	public void disableKeyGuard(){
		if (!isKindleFire()) {
			if(keyguardLock!=null)
				keyguardLock.disableKeyguard();
		}
	}
	public void enableKeyGuard(){
		if (!isKindleFire()) {
			if(keyguardLock!=null)
				keyguardLock.reenableKeyguard();
		}
	}

	// Get a MemoryInfo object for the device's current memory status.
	public ActivityManager.MemoryInfo getAvailableMemory() {
		ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
		activityManager.getMemoryInfo(memoryInfo);
		return memoryInfo;
	}

	public static void copyFile(File src, File dst) throws IOException {
		InputStream in = new FileInputStream(src);
		try {
			OutputStream out = new FileOutputStream(dst);
			try {
				// Transfer bytes from in to out
				byte[] buf = new byte[1024];
				int len;
				while ((len = in.read(buf)) > 0) {
					out.write(buf, 0, len);
				}
			} finally {
				out.close();
			}
		} finally {
			in.close();
		}
	}

	public void archifage(String fileName){
		String archivage = "local";
		String data = SmartRobotApplication
				.getParamFromFile("LogsArchivage",
						"Configuration.properties", null);
		if(data!=null)
			archivage=data;
		if (archivage.equals("local")) {
			File myFile = new File(fileName+".txt");
			myFile.renameTo(new File(fileName + now() + "" + ".txt"));
		}
	}

	public String now(){
		Date date=new Date();
		return date.getYear()+""+date.getMonth()+""+date.getDay()+""+date.getHours()+""+date.getMinutes()+""+date.getSeconds()+"";
	}
	public double storageAvailable(){
		StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
		long bytesAvailable;
		if (Build.VERSION.SDK_INT >=
				Build.VERSION_CODES.JELLY_BEAN_MR2) {
			bytesAvailable = stat.getBlockSizeLong() * stat.getAvailableBlocksLong();
		}
		else {
			bytesAvailable = (long)stat.getBlockSize() * (long)stat.getAvailableBlocks();
		}
		double megAvailable = (double)round((bytesAvailable) / (1024 * 1024));
		return (megAvailable);
	}

	public void logMe(String msg, Boolean typeMessage, String fileName, String nomFichierToWrite, File direct) {
		try {

			double available =storageAvailable();
			Log.i("LRA"," available : "+available);
			String x=SmartRobotApplication
					.getParamFromFile("SizeAvailableThershold",
							"Configuration.properties", null);
			if (x==null){
				x="50";
			}
			if(available<=Double.parseDouble(x)){
				Toast.makeText(getApplicationContext(),"l'espace disque est inférieur à : "+available +"(Mb)",Toast.LENGTH_LONG).show();
			}

			Double length = (new Long(new File(fileName+".txt").length()).doubleValue()) / (1024 * 1024);
			Double fileSize = 10d;
			String f = SmartRobotApplication
					.getParamFromFile("FileSize",
							"Configuration.properties", null);
			if(f!=null)
				fileSize= Double.parseDouble(f);
			if (length >= fileSize) {
				archifage(fileName);
			}
			String nomFichier = nomFichierToWrite+".txt";
			File file = new File(direct, nomFichier);
			if (!file.exists()) {
				this.creerFichier(nomFichier, direct);
			}
			FileWriter fw;
			try {
				fw = new FileWriter(file.getAbsoluteFile(), true);
				BufferedWriter bw = new BufferedWriter(fw);
				if(typeMessage != null && typeMessage){
					bw.write("[" + simpleFormat.format(new Date()) + "] " + "REPONSE"
							+ " : " + msg + "\r\n");
				}else if(typeMessage != null && !typeMessage){
					bw.write("[" + simpleFormat.format(new Date()) + "] " + "COMMANDE"
							+ " : " + msg + "\r\n");
				}else{
					bw.write("[" + simpleFormat.format(new Date()) + "] " + "INFO"
							+ " : " + msg + "\r\n");
				}
				bw.flush();
				bw.close();
			} catch (FileNotFoundException e) {
			}
		} catch (Exception e) {
			e.printStackTrace();
		} catch (StackOverflowError e1) {
		}

	}


	public  void creerFichier(String nomFichier, File direct) {
		File file1 = new File(direct, "AutoControl");
		if (file1.exists() && file1.isDirectory()) {

			File file = new File(direct, nomFichier );
			if (!file.exists()) {
				try {
					file.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public boolean isConnectedToInternet() {

		boolean haveConnectedWifi = false;
		boolean haveConnectedMobile = false;

		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] netInfo = cm.getAllNetworkInfo();
		for (NetworkInfo ni : netInfo) {
			if (ni.getTypeName().equalsIgnoreCase("WIFI"))
				if (ni.isConnected())
					haveConnectedWifi = true;
			if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
				if (ni.isConnected())
					haveConnectedMobile = true;
		}
		return haveConnectedWifi || haveConnectedMobile;
	}

	public String chatbotSendChat(String message) {
		String response = "";
		chatbotConnector = new ChatbotConnector(this);

		response = chatbotConnector.doServerRequest(message);
		if (response == "NoResponseServer")
			chatbotConnector.cancelServerRequest();
		return response;
	}

	public boolean hasRobot(JSONArray jr) throws JSONException {
		boolean has = false;
		for (int i = 0; i < jr.length(); i++) {
			JSONObject object = jr.getJSONObject(i);
			int type = object.getInt("typeRobot");
			if (type == getRobotType())
				has = true;
		}
		return has;
	}
	public static byte [] getBleManufacturerData(byte data[]) {
		int pos=0;
		byte [] manData = null;

		int dlen = data.length;
		while((pos+1) < dlen) {
			int bpos = pos;
			int blen = ((int)data[pos]) & 0xFF;
			if( blen == 0 )
				break;
			if( bpos+blen > dlen )
				break;
			++pos;
			int type = ((int)data[pos]) & 0xFF;
			++pos;
			int len = blen - 1;
			switch( type ) {
// Flags
				case 0x01:
					break;

// 16-bit UUIDs
				case 0x02:
				case 0x03:
				case 0x14:
					break;
// 32-bit UUIDs
				case 0x04:
				case 0x05:
					break;

// 128-bit UUIDs
				case 0x06:
				case 0x07:
				case 0x15:
					break;

// Local name (short and long)
				case 0x08:
				case 0x09:
					break;

// TX Power Level
				case 0x0A:
					break;

// Various not interpreted indicators (byte dump)
				case 0x0D:
				case 0x0E:
				case 0x0F:
				case 0x10:
					break;

// OOB Flags
				case 0x11:
					break;

// Slave Connection Interval Range
				case 0x12:
					break;

// Service data
				case 0x16:
					break;
// Service data, 32 bit UUID
				case 0x20:
					break;

// Service data, 128 bit UUID
				case 0x21:
					break;

// Public Target Address, Random Target Address
				case 0x17:
				case 0x18:
					break;

// Appearance
				case 0x19:
					break;

// Advertising interval
				case 0x1A:
					break;

// LE Bluetooth Device Address
				case 0x1B:
					break;

				case 0x1C:
					break;

				case 0xFF:
					Log.v("mbu", "--len = "+len);
					Log.v("mbu", "--pos = "+pos);

					int ptr = pos;

					int v = ((int)data[ptr]) & 0xFF;
					ptr++;
					v |= (((int)data[ptr]) & 0xFF ) << 8;
					ptr++;
					//manufacturer = v;
					//int length = len - 2;

					byte[]b1 = new byte[len];
					System.arraycopy(data, pos, b1, 0, len);
					manData = b1;
					break;

				default:
					break;
			}
			pos = bpos + blen+1;
		}
		return manData;
	}

	public boolean isScreenOn(){
		PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
		Boolean isScreenOn = false;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
			isScreenOn = powerManager.isInteractive();
		} else {
			isScreenOn = powerManager.isScreenOn();

		}
		return isScreenOn;
	}
}