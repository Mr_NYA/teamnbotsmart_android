package com.orrobotics.orsmartrobot.modelImpl;

import android.app.Activity;
import android.content.Context;

import com.orrobotics.orsmartrobot.model.AuthentificationModel;

/**
 * 
 * @author Hajiba IFRAH
 * @version 1.3.2
 * @date 18/05/2015
 * 
 */
public class AuthentificationModelImpl implements AuthentificationModel {

	Context context;
	Activity activity;

	public AuthentificationModelImpl(Context con, Activity acti) {

		this.context = con;
		this.activity = acti;
	}

	/**
	 * 
	 * getter et setter
	 * 
	 */

	@Override
	public Activity getActivity() {
		// TODO Auto-generated method stub
		return activity;
	}

	@Override
	public Context getContext() {
		// TODO Auto-generated method stub
		return context;
	}

}
