package com.orrobotics.orsmartrobot.modelImpl;

import com.orrobotics.orsmartrobot.model.PassRecoverModel;

import android.app.Activity;
import android.content.Context;

/**
 * 
 * @author Hajiba IFRAH
 * @version 2.1.0
 * @date 18/05/2015
 * 
 */

public class PassRecoverModelImpl implements PassRecoverModel {

	Context context;
	Activity activity;

	public PassRecoverModelImpl(Context con, Activity acti) {

		this.context = con;
		this.activity = acti;
	}

	/**
	 * 
	 * Getter et les setter
	 * 
	 */
	@Override
	public Activity getActivity() {
		// TODO Auto-generated method stub
		return activity;
	}

	@Override
	public Context getContext() {
		// TODO Auto-generated method stub
		return context;
	}

}
