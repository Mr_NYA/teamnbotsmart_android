package com.orrobotics.orsmartrobot.modelImpl;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;

import com.orrobotics.orsmartrobot.model.BleScanModel;

/**
 * 
 * @author Hajiba IFRAH
 * @version 1.3.2
 * @date 18/05/2015
 * 
 */

public class BleScanModelImpl implements BleScanModel {

	Context context;
	ListActivity activity;
	Activity acti;

	boolean mScanning = false;

	public BleScanModelImpl(Context con, ListActivity acti) {

		this.context = con;
		this.activity = acti;
	}
	public BleScanModelImpl(Context con, Activity acti) {

		this.context = con;
		this.acti = acti;
	}
	/**
	 * 
	 * getter et setter
	 * 
	 */
	@Override
	public Context getContext() {
		// TODO Auto-generated method stub
		return context;
	}

	@Override
	public ListActivity getActivity() {
		// TODO Auto-generated method stub
		return activity;
	}

	@Override
	public void setScanning(boolean mScanning) {
		// TODO Auto-generated method stub
		this.mScanning = mScanning;
	}

	@Override
	public boolean getScanning() {
		// TODO Auto-generated method stub
		return mScanning;
	}
	@Override
	public Activity getActi() {
		// TODO Auto-generated method stub
		return acti;
	}

}
