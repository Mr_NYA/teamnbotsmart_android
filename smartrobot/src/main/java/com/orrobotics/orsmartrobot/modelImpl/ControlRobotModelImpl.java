package com.orrobotics.orsmartrobot.modelImpl;


import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.model.ControlRobotModel;
import com.orrobotics.orsmartrobot.view.BaseFragmentActivity;
import com.orrobotics.orsmartrobot.view.BaseFragmentActivityAlexa;

public class ControlRobotModelImpl implements ControlRobotModel

{
	Context context;
	BaseFragmentActivity acti;

	SmartRobotApplication application;
	String[] accelerationReadings = null;
	String[] magnetReadings = null;
	String[] gyroscopeReadings = null;
	String batterie = null;
	String temperature = null;
	String humidity = null;
	String pressure = null;
	String cardiaque = null;
	String step = null;
	String calories = null;
	String light = null;

	@Override
	public String[] getAccelerationReadings() {
		return accelerationReadings;
	}

	@Override
	public void setAccelerationReadings(String[] accelerationReadings) {
		this.accelerationReadings = accelerationReadings;
	}

	@Override
	public String[] getMagnetReadings() {
		return magnetReadings;
	}

	@Override
	public void setMagnetReadings(String[] magnetReadings) {
		this.magnetReadings = magnetReadings;
	}

	@Override
	public String[] getGyroscopeReadings() {
		return gyroscopeReadings;
	}

	@Override
	public void setGyroscopeReadings(String[] gyroscopeReadings) {
		this.gyroscopeReadings = gyroscopeReadings;
	}

	@Override
	public String getBatterie() {
		return batterie;
	}

	@Override
	public void setBatterie(String batterie) {
		this.batterie = batterie;
	}

	@Override
	public String getTemperature() {
		return temperature;
	}

	@Override
	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}

	@Override
	public String getHumidity() {
		return humidity;
	}

	@Override
	public void setHumidity(String humidity) {
		this.humidity = humidity;
	}

	@Override
	public String getPressure() {
		return pressure;
	}

	@Override
	public void setPressure(String pressure) {
		this.pressure = pressure;
	}

	@Override
	public String getCardiaque() {
		return cardiaque;
	}

	@Override
	public void setCardiaque(String cardiaque) {
		this.cardiaque = cardiaque;
	}

	@Override
	public String getStep() {
		return step;
	}

	@Override
	public void setStep(String step) {
		this.step = step;
	}

	@Override
	public String getCalories() {
		return calories;
	}

	@Override
	public void setCalories(String calories) {
		this.calories = calories;
	}

	@Override
	public String getLight() {
		return light;
	}

	@Override
	public void setLight(String light) {
		this.light = light;
	}

	@SuppressLint("NewApi")
	public ControlRobotModelImpl(Context applicationContext,
								 BaseFragmentActivity activity) {
		// TODO Auto-generated constructor stub

		context = applicationContext;
		acti = activity;
		application = (SmartRobotApplication) context;

		Log.v("", "------before tts init-----");

	}

	@Override
	public Context getContext() {
		// TODO Auto-generated method stub
		return context;
	}

	@Override
	public BaseFragmentActivity getActivity() {
		// TODO Auto-generated method stub
		return acti;
	}



}
