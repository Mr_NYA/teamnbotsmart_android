package com.orrobotics.orsmartrobot.modelImpl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.model.CreationCompteModel;

/**
 *
 * @author Hajiba IFRAH
 * @version 2.1.0
 * @date 18/05/2015
 *
 */

public class CreationCompteModelImpl implements CreationCompteModel,
		LocationListener {

	Context context;
	FragmentActivity activity;

	protected LocationManager locationManager;

	boolean isGPSEnabled = false;
	boolean isNetworkEnabled = false;
	boolean canGetLocation = false;

	private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;
	private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1;

	Location location;

	SmartRobotApplication model;

	String country;

	public CreationCompteModelImpl(Context con, FragmentActivity acti) {

		this.context = con;
		this.activity = acti;

		model = (SmartRobotApplication) context;

	}

	/**
	 * Getter et setter
	 *
	 */

	@Override
	public Context getContext() {
		// TODO Auto-generated method stub
		return context;
	}

	@Override
	public FragmentActivity getActivity() {
		// TODO Auto-generated method stub
		return activity;
	}

	/**
	 * Récupérer la location de l'utilisateur
	 *
	 */

	@SuppressLint("MissingPermission")
	@Override
	public Location getLocation() {
		// TODO Auto-generated method stub
		Log.d("get location", "------------------------------------");
		try {

			locationManager = (LocationManager) context
					.getSystemService(Context.LOCATION_SERVICE);

			isGPSEnabled = locationManager
					.isProviderEnabled(LocationManager.GPS_PROVIDER);

			isNetworkEnabled = locationManager
					.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

			if (!isGPSEnabled && !isNetworkEnabled) {

			} else {

				this.canGetLocation = true;
				if (isNetworkEnabled) {
					Log.d("isNetworkEnabled",
							"------------------------------------");

					locationManager.requestLocationUpdates(
							LocationManager.NETWORK_PROVIDER,
							MIN_TIME_BW_UPDATES,
							MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
					Log.d("Network", "Network");
					if (locationManager != null) {
						Log.d("location manager null",
								"------------------------------------");
						location = locationManager
								.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
						if (location != null) {
							Log.d("isNetworkEnabled",
									"------------------------------------");
							return location;

						}
					}
				}
				// if GPS Enabled get lat/long using GPS Services
				if (isGPSEnabled) {
					Log.d("isGPSEnabled",
							"------------------------------------");
					if (location == null) {
						locationManager.requestLocationUpdates(
								LocationManager.GPS_PROVIDER,
								MIN_TIME_BW_UPDATES,
								MIN_DISTANCE_CHANGE_FOR_UPDATES, this, null);
						Log.d("GPS Enabled", "GPS Enabled");
						if (locationManager != null) {
							location = locationManager
									.getLastKnownLocation(LocationManager.GPS_PROVIDER);
							if (location != null) {
								Log.d("isGPSEnabled",
										"------------------------------------");
								return location;
							}
						}
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * Stocker le pays et la ville de l'utilisateur dans une liste la france est
	 * le pays par défaut et Paris la ville par défaut
	 *
	 **/

	@Override
	public List<String> getList() {

		List<String> list = new ArrayList<String>();

		location = getLocation();
		if (location != null) {
			Geocoder gcd = new Geocoder(context, Locale.getDefault());
			List<Address> addresses;
			try {
				addresses = gcd.getFromLocation(location.getLatitude(),
						location.getLongitude(), 1);

				if (addresses.size() > 0) {
					list.add(addresses.get(0).getCountryName());
					list.add(addresses.get(0).getLocality());
				}
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				list.add("France");
				list.add("Paris");
			}
		} else {
			list.add("France");
			list.add("Paris");
		}
		return list;
	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	/**
	 * Récupérer la liste des pays de monde
	 *
	 */

	@Override
	public ArrayList<String> getContries() {
		// TODO Auto-generated method stub
		Locale[] locales = Locale.getAvailableLocales();
		final ArrayList<String> countries = new ArrayList<String>();
		for (Locale locale : locales) {

			country = locale.getDisplayCountry();

			if (country.trim().length() > 0 && !countries.contains(country)) {
				countries.add(country);
			}
		}

		Collections.sort(countries);
		return countries;
	}

}
