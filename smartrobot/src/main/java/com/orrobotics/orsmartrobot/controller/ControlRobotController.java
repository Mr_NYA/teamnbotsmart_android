package com.orrobotics.orsmartrobot.controller;

import android.speech.tts.TextToSpeech;

public interface ControlRobotController {

	public void switchCamera();

	public void terminateCall();

	void stopMotion();

	void move(String message);

	void destroy();

	void speak(String message);

	public double printDistance(String s1, String s2);

	public TextToSpeech getTTS();

	public void callSkype(String string);

	public String getPseudo(String msg);

	public void startService();

	public void stopService();

    boolean isEqualPhonetic(String result, String keyword);
}
