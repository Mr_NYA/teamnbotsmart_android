package com.orrobotics.orsmartrobot.controller;

public interface ListRobotController {

	public int getlength();

	public void setData(int lenght);

	public void removeObserver();

}
