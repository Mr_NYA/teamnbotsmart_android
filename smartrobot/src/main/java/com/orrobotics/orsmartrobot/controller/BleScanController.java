package com.orrobotics.orsmartrobot.controller;

public interface BleScanController {

	public void updateMacAdresse(String address, String name,
			boolean fromBleScan);

	public void updateMacInternFile(String address, String name);

}
