package com.orrobotics.orsmartrobot.controller;

public interface SettingListController {

    int setLanguage(String language);

    void SendTts();

    String getLanguage();

    String getLanguageCode(String langageName);
}
