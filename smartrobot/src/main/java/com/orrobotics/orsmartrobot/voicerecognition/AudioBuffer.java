/*
 * Copyright (c) 2017  STMicroelectronics – All rights reserved
 * The STMicroelectronics corporate logo is a trademark of STMicroelectronics
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions
 *   and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice, this list of
 *   conditions and the following disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 *
 * - Neither the name nor trademarks of STMicroelectronics International N.V. nor any other
 *   STMicroelectronics company nor the names of its contributors may be used to endorse or
 *   promote products derived from this software without specific prior written permission.
 *
 * - All of the icons, pictures, logos and other images that are provided with the source code
 *   in a directory whose title begins with st_images may only be used for internal purposes and
 *   shall not be redistributed to any third party or modified in any way.
 *
 * - Any redistributions in binary form shall not include the capability to display any of the
 *   icons, pictures, logos and other images that are provided with the source code in a directory
 *   whose title begins with st_images.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 */

package com.orrobotics.orsmartrobot.voicerecognition;

import android.media.AudioTrack;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Class containing an audio buffer
 */
public class AudioBuffer {

    private int mSamplingRate;
    private short mData[];
    private int mLastWriteData;
    private short mAudioData[];
    private byte bmAudioData[];
    public AudioBuffer(int samplingRate, int maxLengthSec){
        mSamplingRate=samplingRate;
        mData = new short[samplingRate*maxLengthSec];
        mAudioData = new short[]{};
        bmAudioData = new byte[]{};
        mLastWriteData=0;
    }

    public AudioBuffer(short[] sampleData){
        mData=sampleData;
        mLastWriteData = sampleData.length;
    }
    public void setData(short[] sampleData) {
        mAudioData = sampleData;
    }
    public short[] getData() {
        return mData;
    }
    public short[] getmAudioData() {
        return mAudioData;
    }

    public byte[] getbmAudioData() {
        return bmAudioData;
    }

    public int getSamplingRate() {
        return mSamplingRate;
    }

    public int append(short data[]){
        synchronized (this) {
            int size = Math.min(mData.length - mLastWriteData, data.length);
            System.arraycopy(data, 0, mData, mLastWriteData, size);
            mLastWriteData += size;
            return mLastWriteData;
        }
    }
    public int dynamicAppend(short toAppend[]){
        synchronized (this) {


            //long[] s = new long[] {0, 1, 2};
            //long[] toAppend = { 3, 4, 5 };

            short[] tmp = new short[mAudioData.length + toAppend.length];
            System.arraycopy(mAudioData, 0, tmp, 0, mAudioData.length);
            System.arraycopy(toAppend, 0, tmp, mAudioData.length, toAppend.length);

            mAudioData = tmp;
            return 1;
        }
    }


    public int dynamicAppend(byte toAppend[]){
        synchronized (this) {


            //long[] s = new long[] {0, 1, 2};
            //long[] toAppend = { 3, 4, 5 };

            byte[] tmp = new byte[bmAudioData.length + toAppend.length];
            System.arraycopy(bmAudioData, 0, tmp, 0, bmAudioData.length);
            System.arraycopy(toAppend, 0, tmp, bmAudioData.length, toAppend.length);

            bmAudioData = tmp;
            return 1;
        }
    }

    public boolean isFull() {
        return mData.length==mLastWriteData;
    }

    public int getBufferLength() {
        return mData.length;
    }

    public void writeLittleEndianTo(OutputStream out) throws IOException {
        synchronized (this) {
            for (int i = 0; i < mLastWriteData; i++) {
                short temp = mData[i];
                out.write(temp & 0x00FF);
                out.write(temp >> 8);
            }
        }
    }

    synchronized public void writeTo(AudioTrack out){
        out.write(mData,0,mLastWriteData);
    }
}