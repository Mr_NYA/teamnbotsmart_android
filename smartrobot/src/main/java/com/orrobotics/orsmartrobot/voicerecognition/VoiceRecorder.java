package com.orrobotics.orsmartrobot.voicerecognition;

/**
 * Created by KABLY AHMED on 13-04-2018.
 */

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.support.annotation.NonNull;
import android.util.Log;

import com.orrobotics.orsmartrobot.application.SmartRobotApplication;

import java.util.ArrayList;
import java.util.List;


public class VoiceRecorder {

    static {
        System.loadLibrary("native-lib");
    }

    private static final int[] SAMPLE_RATE_CANDIDATES = new int[]{16000, 11025, 22050, 44100};

    private static final int CHANNEL = AudioFormat.CHANNEL_IN_MONO;
    private static final int ENCODING = AudioFormat.ENCODING_PCM_16BIT;

    private static final int AMPLITUDE_THRESHOLD = 900; //1500;

    private static final int SPEECH_TIMEOUT_MILLIS = 2000;
    private static final int MAX_SPEECH_LENGTH_MILLIS = 60 * 1000;


    private static int aggressivity;//=3;
    private final double vad_seuil = 0.75;
    private static double intens_seuil;//=0.3; // (%)
    private static int silence;//=2;

    public static abstract class Callback {

        /**
         * Called when the recorder starts hearing voice.
         */
        public void onVoiceStart() {
        }

        /**
         * Called when the recorder is hearing voice.
         *
         * @param data The audio data in {@link AudioFormat#ENCODING_PCM_16BIT}.
         * @param size The size of the actual data in {@code data}.
         */
        public void onVoice(byte[] data, int size, boolean test) {
        }

        /**
         * Called when the recorder stops hearing voice.
         */
        public void onVoiceEnd() {
        }
    }

    private final Callback mCallback;

    private static AudioRecord mAudioRecord;

    private static Thread mThread;

    private byte[] mBuffer;

    //private static List<Integer> voiced_one_sec = new ArrayList<Integer>();

    public int indice=0;
    private final Object mLock = new Object();

    /** The timestamp of the last time that voice is heard. */
    private long mLastVoiceHeardMillis = Long.MAX_VALUE;

    /** The timestamp when the current voice is started. */
    private long mVoiceStartedMillis;

    public VoiceRecorder(@NonNull Callback callback) {
        mCallback = callback;
    }

    /**
     * Starts recording audio.
     *
     * <p>The caller is responsible for calling {@link #stop()} later.</p>
     */
    public void start() {


        silence = Integer.parseInt(SmartRobotApplication.getParamFromFile(
                "nbrSecSil", "Configuration.properties", null));

        aggressivity = Integer.parseInt(SmartRobotApplication.getParamFromFile(
                "agressivity", "Configuration.properties", null));
        intens_seuil = Double.parseDouble(SmartRobotApplication.getParamFromFile(
                "seuil_intensity", "Configuration.properties", null));



        // Stop recording if it is currently ongoing.
        stop();
        // Try to create a new recording session.
        mAudioRecord = createAudioRecord();
        if (mAudioRecord == null) {
            throw new RuntimeException("Cannot instantiate VoiceRecorder");
        }
        else {
            // Start recording.
            try {
                mAudioRecord.startRecording();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }
            // Start processing the captured audio.
            mThread = new Thread(new ProcessVoice());
            mThread.start();
        }
    }

    /**
     * Stops recording audio.
     */
    public void stop() {
        synchronized (mLock) {
            dismiss();
            if (mThread != null) {
                mThread.interrupt();
                mThread = null;
            }
            if (mAudioRecord != null) {
                try {
                    mAudioRecord.stop();
                    mAudioRecord.release();
                }
                catch (IllegalStateException e){
                    e.printStackTrace();
                }
                mAudioRecord = null;
            }
            mBuffer = null;
        }
    }

    /**
     * Dismisses the currently ongoing utterance.
     */
    public void dismiss() {
        if (mLastVoiceHeardMillis != Long.MAX_VALUE) {
            mLastVoiceHeardMillis = Long.MAX_VALUE;
            mCallback.onVoiceEnd();
        }
    }

    /**
     * Retrieves the sample rate currently used to record audio.
     *
     * @return The sample rate of recorded audio.
     */
    public int getSampleRate() {
        if (mAudioRecord != null) {
            return mAudioRecord.getSampleRate();
        }
        return 0;
    }

    /**
     * Creates a new {@link AudioRecord}.
     *
     * @return A newly created {@link AudioRecord}, or null if it cannot be created (missing
     * permissions?).
     */
    private AudioRecord createAudioRecord() {
        for (int sampleRate : SAMPLE_RATE_CANDIDATES) {
            final int sizeInBytes = AudioRecord.getMinBufferSize(sampleRate, CHANNEL, ENCODING);
            if (sizeInBytes == AudioRecord.ERROR_BAD_VALUE) {
                continue;
            }
            final AudioRecord audioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC,
                    sampleRate, CHANNEL, ENCODING, sizeInBytes);
            if (audioRecord.getState() == AudioRecord.STATE_INITIALIZED) {
                mBuffer = new byte[sizeInBytes];


                return audioRecord;
            } else {
                audioRecord.release();
            }
        }
        return null;
    }

    /**
     * Continuously processes the captured audio and notifies {@link #mCallback} of corresponding
     * events.
     */
    private class ProcessVoice implements Runnable {

        @Override
        public void run() {
            while (true && mAudioRecord != null && mBuffer !=null) {
                synchronized (mLock) {
                    if (Thread.currentThread().isInterrupted()) {
                        break;
                    }
                    final int size = mAudioRecord.read(mBuffer, 0, mBuffer.length);
                    final long now = System.currentTimeMillis();

                    /***
                     * ajoutée par YBE: onVoice sans arrét

                     if (mLastVoiceHeardMillis == Long.MAX_VALUE) {
                     mVoiceStartedMillis = now;
                     Log.i("mic","----ProcessVoice----");

                     mCallback.onVoiceStart();
                     }
                     mCallback.onVoice(mBuffer, size);
                     mLastVoiceHeardMillis = now;**/
                    if (isSpeechJNI(bytesToShort(mBuffer), 0, mBuffer.length, 16000, 160, aggressivity, vad_seuil)== 1 && isHearingVoice(mBuffer, mBuffer.length) ) {
                        Log.i("record","----Speech----");

                        if (mLastVoiceHeardMillis == Long.MAX_VALUE) {
                            mVoiceStartedMillis = now;
                            mCallback.onVoiceStart();
                        }
                        mCallback.onVoice(mBuffer, size,false);
                        indice ++;
                        if (indice >2){
                            mLastVoiceHeardMillis = now;
                        }
                        mLastVoiceHeardMillis = now;
                        if (now - mVoiceStartedMillis > MAX_SPEECH_LENGTH_MILLIS) {
                            Log.i("record","---->30 ----");
                            indice = 0;
                            end();
                        }
                    } else if (mLastVoiceHeardMillis != Long.MAX_VALUE) {
                        Log.i("record","---- silence ----");
                        mCallback.onVoice(mBuffer, size, false);

                        if ((now - mLastVoiceHeardMillis) >= silence*1000 ) {
                            Log.i("record","---->1 ----");
                            indice=0;
                            //mCallback.onVoice(mBuffer, size, false);
                            end();
                        }

                    }
                }
            }
        }

        private void end() {
            indice = 0;
            mLastVoiceHeardMillis = Long.MAX_VALUE;
            mCallback.onVoiceEnd();
        }
        private void end2() {
            // mLastVoiceHeardMillis = Long.MAX_VALUE;
            mCallback.onVoiceEnd();
        }

        private boolean isHearingVoice(byte[] buffer, int size) {
            for (int i = 0; i < size - 1; i += 2) {
                // The buffer has LINEAR16 in little endian.
                int s = buffer[i + 1];
                if (s < 0) s *= -1;
                s <<= 8;
                s += Math.abs(buffer[i]);
               // Log.i("record","---->isHearingVoice ----"+ s);
                if (s > AMPLITUDE_THRESHOLD) {
                    return true;
                }
            }
            return false;
        }

    }
    public short[] bytesToShort(byte[] bb){
        // Grab size of the byte array, create an array of shorts of the same size
        int size = bb.length;
        short[] shortArray = new short[size];

        for (int index = 0; index < size; index++)
            shortArray[index] = (short) bb[index];

        return shortArray;
    }

    public native int isSpeechJNI(short[] buffer,int offseetInshort,int readSize,int sampleRate,int vadFrame, int aggressivity, double vad_seuil);


}
