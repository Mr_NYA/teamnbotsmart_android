package com.orrobotics.orsmartrobot.voicerecognition;

import java.util.List;

import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.util.LanguageChecker;
import com.orrobotics.orsmartrobot.util.OnLanguageListener;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;

@SuppressLint("UseValueOf")
public class SpeechRecognition implements RecognitionListener {
	private static final String TAG = SpeechRecognition.class.getSimpleName();
	boolean restart = true;
	protected long mSpeechRecognizerStartListeningTime = 0;
	Intent intent;
	long duration = 0;
	private SpeechRecognizer mSpeechRecognizer;
	Context con;
	Bundle res = null;
	String reason = null;
	Handler mHandler = new Handler();
	Boolean flagStart = false;
	Boolean flagEnd = false;
	SmartRobotApplication model;
	boolean isLangSupported = false;

	public interface ResultCallback {
		void onResults(List<String> results);

		void onError(String reason);
	}

	private ResultCallback mResultCallback;

	public SpeechRecognition(Context context) {
		mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(context);

		mSpeechRecognizer.setRecognitionListener(this);
		this.con = context;
		model = (SmartRobotApplication) con.getApplicationContext();

	}

	public void start(ResultCallback callback, Boolean isEnabled) {
		mResultCallback = callback;

		final Intent intent = new Intent(
				RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

		OnLanguageListener andThen = new OnLanguageListener() {

			@Override
			public void onLanguageDetailsReceived(LanguageChecker data) {
				// TODO Auto-generated method stub
				Log.v("", "-----onLanguageDetailsReceived-----");

				List<String> langs = data.getSupportedLanguages();

				for (int i = 0; i < langs.size(); i++) {

					if (langs.get(i).equals(
							model.getLangageTTS().replace("_", "-"))) {
						isLangSupported = true;
						break;
					} else
						isLangSupported = false;
				}

				if (isLangSupported) {
					intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, model
							.getLangageTTS().replace("_", "-"));

				} else {
					intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "en-US");
				}

				intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1);
				// intent.putExtra(
				// RecognizerIntent.EXTRA_SPEECH_INPUT_COMPLETE_SILENCE_LENGTH_MILLIS,
				// frequence);
				// intent.putExtra(
				// RecognizerIntent.EXTRA_SPEECH_INPUT_MINIMUM_LENGTH_MILLIS,
				// frequence);
				intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,
						SpeechRecognition.class.getPackage().getName());
				flagStart = false;
				flagEnd = false;
				if (mSpeechRecognizer != null)
					mSpeechRecognizer.startListening(intent);
			}
		};

		Intent detailsIntent = new Intent(
				RecognizerIntent.ACTION_GET_LANGUAGE_DETAILS);
		con.sendOrderedBroadcast(detailsIntent, null, new LanguageChecker(
				andThen), null, Activity.RESULT_OK, null, null);

	}

	@Override
	public void onBeginningOfSpeech() {
		// TODO Auto-generated method stub
		Log.i(TAG, "onBeginningOfSpeech");
		if (flagStart == false) {
			flagStart = true;
			// mHandler.postDelayed(run, frequence);
		}
		// model.setVolume();
		// TODO Auto-generated method stub

		res = null;
		reason = null;
	}

	@Override
	public void onBufferReceived(byte[] arg0) {
		// TODO Auto-generated method stub
		Log.i(TAG, "onBufferReceived");
	}

	@Override
	public void onEndOfSpeech() {
		// TODO Auto-generated method stub
		Log.i(TAG, "onEndOfSpeech");
		flagEnd = true;
		model.notifyObservers("stopGif");
	}

	@Override
	public void onError(int error) {
		if (mResultCallback != null) {
			switch (error) {
				case SpeechRecognizer.ERROR_AUDIO:
					reason = "SpeechRecognizer.ERROR_AUDIO";
					break;

				case SpeechRecognizer.ERROR_CLIENT:
					reason = "SpeechRecognizer.ERROR_CLIENT";
					restart = false;
					break;
				case SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS:
					reason = "SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS";
					restart = false;
					break;
				case SpeechRecognizer.ERROR_NETWORK:
					reason = "SpeechRecognizer.ERROR_NETWORK";
					break;
				case SpeechRecognizer.ERROR_NETWORK_TIMEOUT:
					reason = "SpeechRecognizer.ERROR_NETWORK_TIMEOUT";
					break;
				case SpeechRecognizer.ERROR_NO_MATCH:
					reason = "SpeechRecognizer.ERROR_NO_MATCH";
					break;
				case SpeechRecognizer.ERROR_RECOGNIZER_BUSY:
					reason = "SpeechRecognizer.ERROR_RECOGNIZER_BUSY";
					break;
				case SpeechRecognizer.ERROR_SERVER:
					reason = "SpeechRecognizer.ERROR_SERVER";
					break;
				case SpeechRecognizer.ERROR_SPEECH_TIMEOUT:
					reason = "SpeechRecognizer.ERROR_SPEECH_TIMEOUT";
					break;
			}
			Log.i(TAG, "onError " + reason);
			// if (flagStart == true)
			mResultCallback.onError(reason);

			flagStart = false;
			//model.notifyObservers("stopGif");
		}

	}

	@Override
	public void onEvent(int eventType, Bundle params) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPartialResults(Bundle partialResults) {
		// If you feel like it, you can use the partial result.

		Log.i(TAG, "onPartialResults " + duration);

	}

	@Override
	public void onReadyForSpeech(Bundle params) {
		// TODO Auto-generated method stub
		Log.i(TAG, "onReadyForSpeech");
		flagStart = true;
		model.notifyObservers("startGif");

	}

	@Override
	public void onResults(Bundle results) {
		Log.i(TAG, "onResults");
		res = results;
		receiveResults(results);

	}

	@Override
	public void onRmsChanged(float rmsdB) {
		// TODO Auto-generated method stub
		// Log.i(TAG, "onRmsChanged");
		flagStart = true;

	}

	private void receiveResults(Bundle results) {
		if ((results != null)
				&& results.containsKey(SpeechRecognizer.RESULTS_RECOGNITION)) {
			List<String> res = results
					.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
			if (mResultCallback != null)
				mResultCallback.onResults(res);
			duration = 0;
		}
	}

	public void destroy() {
		// TODO Auto-generated method stub
		if (restart) {
			model.getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					if (mSpeechRecognizer != null)
						mSpeechRecognizer.cancel();
				}
			});
		}
		// mSpeechRecognizer.cancel();
		if (mSpeechRecognizer != null)
			mSpeechRecognizer.destroy();
		mSpeechRecognizer = null;
		//model.notifyObservers("stopGif");
	}

}
