package com.orrobotics.orsmartrobot.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

public class BatteryView extends View {

	/**
	 * @author Hajiba IFRAH
	 * @version 2.1.0
	 * @date 18/05/2015
	 * */

	/**
	 * Brush information
	 */
	private Paint mBatteryPaint;
	private Paint mPowerPaint;
	private float mBatteryStroke = 2f;
	/**
	 * Screen width
	 */
	private int measureWidth;
	private int measureHeigth;
	/**
	 * 
	 * Cell parameters
	 */
	private float mBatteryHeight = 30f; // Cell height
	private float mBatteryWidth = 70.0F; // The width of the battery
	private float mCapHeight = 15f;
	private float mCapWidth = 5f;
	/**
	 * 
	 * Battery power
	 */
	private float mPowerPadding = 1;
	private float mPowerHeight = mBatteryHeight - mBatteryStroke
			- mPowerPadding * 2; // The battery body height
	private float mPowerWidth = mBatteryWidth - mBatteryStroke - mPowerPadding
			* 2;// The total width of the battery body
	private float mPower = 0f;
	/**
	 * 
	 * Rectangular
	 */
	private RectF mBatteryRect;
	private RectF mCapRect;
	private RectF mPowerRect;

	public BatteryView(Context context) {
		super(context);
		initView();
	}

	public BatteryView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initView();
	}

	public BatteryView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		initView();
	}

	public void initView() {
		/**
		 * Set battery brush
		 */
		mBatteryPaint = new Paint();
		mBatteryPaint.setColor(Color.WHITE);
		mBatteryPaint.setAntiAlias(true);
		mBatteryPaint.setStyle(Paint.Style.STROKE);
		mBatteryPaint.setStrokeWidth(mBatteryStroke);
		/**
		 * Electric brush set
		 */
		mPowerPaint = new Paint();
		mPowerPaint.setColor(Color.WHITE);
		mPowerPaint.setAntiAlias(true);
		mPowerPaint.setStyle(Style.FILL_AND_STROKE);
		mPowerPaint.setStrokeWidth(mBatteryStroke);
		/**
		 * Set battery rectangular
		 */
		mBatteryRect = new RectF(mCapWidth, 0, mBatteryWidth, mBatteryHeight);
		/**
		 * Set battery cover rectangular
		 */
		mCapRect = new RectF(0, (mBatteryHeight - mCapHeight) / 2, mCapWidth,
				(mBatteryHeight - mCapHeight) / 2 + mCapHeight);
		/**
		 * Setting power rectangular
		 */
		mPowerRect = new RectF(mCapWidth + mBatteryStroke / 2 + mPowerPadding
				+ mPowerWidth * ((100f - mPower) / 100f), // The need to adjust
															// the left position
				mPowerPadding + mBatteryStroke / 2, // The need to take into
													// account the width of the
													// brush
				mBatteryWidth - mPowerPadding * 2, mBatteryStroke / 2
						+ mPowerPadding + mPowerHeight);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		canvas.save();
		// canvas.translate(measureWidth /20, measureHeigth / 20);
		canvas.drawRoundRect(mBatteryRect, 2f, 2f, mBatteryPaint); // Draw the
																	// cell
																	// contour
																	// needs to
																	// consider
																	// the width
																	// of the
																	// brush
		canvas.drawRoundRect(mCapRect, 2f, 2f, mBatteryPaint);// Painting the
																// battery cover
		canvas.drawRect(mPowerRect, mPowerPaint);// Drawing power
		canvas.restore();
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		measureWidth = MeasureSpec.getSize(widthMeasureSpec);
		measureHeigth = MeasureSpec.getSize(heightMeasureSpec);
		setMeasuredDimension(measureWidth, measureHeigth/5);
	}

	/**
	 * ]
	 * 
	 * @Category battery
	 * @param power
	 */
	public void setPower(float power) {
		mPower = power;
		if (mPower < 0) {
			mPower = 0;
		}
		mPowerRect = new RectF(mCapWidth + mBatteryStroke / 2 + mPowerPadding
				+ mPowerWidth * ((100f - mPower) / 100f), // The need to adjust
															// the left position
				mPowerPadding + mBatteryStroke / 2, // The need to take into
													// account the width of the
													// brush
				mBatteryWidth - mPowerPadding * 2, mBatteryStroke / 2
						+ mPowerPadding + mPowerHeight);
		invalidate();
	}

}
