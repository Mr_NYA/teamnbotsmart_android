package com.orrobotics.orsmartrobot.ui;

import java.util.ArrayList;

import android.widget.Spinner;

public class ListHeaderSetting {

	String title;
	int icon;
	ArrayList<Spinner> setting_list = new ArrayList<Spinner>();

	public ListHeaderSetting(String title, int icon) {
		this.title = title;
		this.icon = icon;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getIcon() {
		return icon;
	}

	public void setIcon(int icon) {
		this.icon = icon;
	}

}
