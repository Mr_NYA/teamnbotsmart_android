// Copyright (c) Philipp Wagner. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

package com.orrobotics.orsmartrobot.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.modelImpl.FaceResult;
import com.tzutalin.dlib.Constants;
import com.tzutalin.dlib.FaceDet;
import com.tzutalin.dlib.PedestrianDet;
import com.tzutalin.dlib.VisionDetRet;


/**
 * This class is a simple View to display the faces.
 */
public class FaceOverlayView extends View {
    int rec_view;
    private Paint mPaint;
    private Paint mTextPaint;
    private int mDisplayOrientation;
    private int mOrientation;
    private int previewWidth;
    private int previewHeight;
    private FaceResult[] mFaces;
    List<VisionDetRet> mPedist;
    private Paint mFaceLandmardkPaint;

    int silo_det = 0;

    private double fps;
    private boolean isFront = false;
    SmartRobotApplication application;

    int flagTopPre;
    int flagLeftPre;
    int flagHeightPre =0;
    int id=-1;
    Boolean flag=false;
    Float topPreview=0.0f;
    Float leftPreview=0.0f;
    Boolean flagStart=true;
    int width;
    int height;

    public FaceOverlayView(Context context) {
        super(context);
        application = (SmartRobotApplication) context;


        initialize();
    }

    private void initialize() {
        // We want a green box around the face:
        DisplayMetrics metrics = getResources().getDisplayMetrics();

        silo_det = Integer.parseInt(application.getParamFromFile("silhouette.isactive", "Configuration.properties", null));
        rec_view = Integer.parseInt(application.getParamFromFile("rectangle.isvisible", "Configuration.properties", null));
        int stroke = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2, metrics);
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setColor(Color.GREEN);
        mPaint.setStrokeWidth(stroke);
        mPaint.setStyle(Paint.Style.STROKE);

        mTextPaint = new Paint();
        mTextPaint.setAntiAlias(true);
        mTextPaint.setDither(true);
        int size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 15, metrics);
        mTextPaint.setTextSize(size);
        mTextPaint.setColor(Color.GREEN);
        mTextPaint.setStyle(Paint.Style.FILL);

        mFaceLandmardkPaint = new Paint();
        mFaceLandmardkPaint.setColor(Color.YELLOW);
        mFaceLandmardkPaint.setStrokeWidth(5);
        mFaceLandmardkPaint.setStyle(Paint.Style.STROKE);
    }

    public void setFPS(double fps) {
        this.fps = fps;
    }

    public void setFaces(FaceResult[] faces) {
        if(faces==null){
            flagStart=false;
        }else {
            mFaces = faces;
            flagStart=true;
        }
        invalidate();
    }
    public void setPedist(List<VisionDetRet> pedist, int w, int h) {
        //if(pedist==null){
            //flagStart=false;
        //}else {
            mPedist = pedist;
            flagStart=true;
            width= w;
            height = h;
        //}
        //invalidate();
    }


    public void setOrientation(int orientation) {
        mOrientation = orientation;
    }

    public void setDisplayOrientation(int displayOrientation) {
        mDisplayOrientation = displayOrientation;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        flag = false;
        if(flagStart==true) {
            application.set_myfaces(null);

            /***
             * draw pedistrian rectangle
             */

            if (mPedist != null && mPedist.size() > 0 && silo_det == 1 ) {
                //canvas.save();
                //canvas.rotate(-mOrientation);
                int lastHeight = 0;
                VisionDetRet large_ret = mPedist.get(0);
                for (final VisionDetRet ret : mPedist) {
                    application.setStartSuivi(true);
                    int newHeight = ret.getBottom() - ret.getTop();
                    if (newHeight > lastHeight){
                        lastHeight = newHeight;
                        large_ret = ret;
                    }
                    float resizeRatio = 1.0f;
                    Rect bounds = new Rect();
                    bounds.right = (int) (getWidth() - large_ret.getLeft()* (float) getWidth() / (float) width);
                    bounds.top = (int) (large_ret.getTop() * (float) getHeight() / (float) height);
                    bounds.left =  (int) (getWidth() - large_ret.getRight() * (float) getWidth() / (float) width);
                    bounds.bottom = (int) (large_ret.getBottom() * (float) getHeight() / (float) height);

                    application.settSiloTop(bounds.top);
                    application.setSiloLeft(bounds.left);

                    application.setCenterXsilo((int)(bounds.left + (bounds.right - bounds.left)/2));
                    application.setCenterYsilo((int)(bounds.top + (bounds.bottom - bounds.top)/2));

                    //Canvas canvas = new Canvas(mCroppedBitmap);
                    flag = true;
                    if (rec_view==1 && application.getBackgroundType().equals("camera")){
                        canvas.drawRect(bounds, mFaceLandmardkPaint);

                    }
                    //canvas.drawRect(bounds, mFaceLandmardkPaint);

                }
            }
            /**
             * end
             */
            if (mFaces != null && mFaces.length > 0) {
                application.set_myfaces("ok");

                float scaleX = (float) getWidth() / (float) previewWidth;
                float scaleY = (float) getHeight() / (float) previewHeight;

                switch (mDisplayOrientation) {
                    case 90:
                    case 270:
                        scaleX = (float) getWidth() / (float) previewHeight;
                        scaleY = (float) getHeight() / (float) previewWidth;
                        break;
                }

                canvas.save();
                canvas.rotate(-mOrientation);
                RectF rectF = new RectF();

                for (FaceResult face : mFaces) {

                }
                int max_face = 0;
                for (FaceResult face : mFaces) {
                    PointF mid = new PointF();
                    face.getMidPoint(mid);


                    if (mid.x != 0.0f && mid.y != 0.0f) {

                        float eyesDis = face.eyesDistance();
                        rectF.set(new RectF(
                                (mid.x - eyesDis * 1.2f) * scaleX,
                                (mid.y - eyesDis * 0.65f) * scaleY,
                                (mid.x + eyesDis * 1.2f) * scaleX,
                                (mid.y + eyesDis * 1.75f) * scaleY));
                        if (isFront) {
                            float left = rectF.left;
                            float right = rectF.right;
                            rectF.left = getWidth() - right;
                            rectF.right = getWidth() - left;
                        }
                        if (rec_view==1 && application.getBackgroundType().equals("camera")){
                            canvas.drawRect(rectF, mPaint);

                        }
                        //canvas.drawRect(rectF, mPaint);
                        //canvas.drawRect(rectF, mPaint);
                        if (application.isEnableSuivi()) {


                            if (application.isStartSuivi() == false) {


                                int canvasW = getWidth();
                                int canvasH = getHeight();
                                Point centerOfCanvas = new Point(canvasW / 2, canvasH / 2);
                                int rectW = application.getPx();
                                int rectH = application.getPx();
                                int left = centerOfCanvas.x - (rectW / 2);
                                int top = centerOfCanvas.y - (rectH / 2);
                                int right = centerOfCanvas.x + (rectW / 2);
                                int bottom = centerOfCanvas.y + (rectH / 2);
                                RectF rectCenter = new RectF(left, top, right, bottom);
                                //if (rectCenter.contains(rectF)) {
                                application.setStartSuivi(true);
                                flagTopPre = (int) rectF.top / 100;
                                flagLeftPre = (int) rectF.left / 100;
                                flagHeightPre = (int) rectF.height() / 100;
                                application.setTop(flagHeightPre);
                                application.setLeft(flagHeightPre);
                                application.setHeight(flagHeightPre);

                                if (flagHeightPre> max_face){
                                    max_face =flagHeightPre;
                                    id = face.getId();
                                    application.setIdSuivi(id);
                                }
                                // max_face = flagHeightPre;
                                //}
                            }
                            if (face.getId() == application.getId()) {
                                if (application.isStartSuivi() == true) {
                                    flag = true;
                                    application.setTop(rectF.top);
                                    application.setLeft( rectF.left);

                                    application.setBoutom(rectF.bottom);
                                    application.setRight( rectF.right);

                                    application.setHeight(rectF.height());
                                    application.setCenterX(rectF.centerX());
                                    application.setCenterY(rectF.centerY());

                                }
                            }

                        }
                        if (rec_view==1 && application.getBackgroundType().equals("camera")){
                            canvas.drawText("ID " + face.getId(), rectF.left, rectF.bottom + mTextPaint.getTextSize(), mTextPaint);

                        }
                        //canvas.drawText("ID " + face.getId(), rectF.left, rectF.bottom + mTextPaint.getTextSize(), mTextPaint);
                    }

                }
                if (flag == false) {
                    if (application.isStartSuivi() == true) {
                        application.setStartSuivi(false);
                    }
                }
            }


        }
        application.setFlagSuivi(flag);

    }

    private boolean contains(List<Integer> ids, int id) {
        for(int identifiant : ids){
            if(identifiant==id)
                return true;
        }
        return false;
    }

    public void setPreviewWidth(int previewWidth) {
        this.previewWidth = previewWidth;
    }

    public void setPreviewHeight(int previewHeight) {
        this.previewHeight = previewHeight;
    }

    public void setFront(boolean front) {
        isFront = front;
    }
}