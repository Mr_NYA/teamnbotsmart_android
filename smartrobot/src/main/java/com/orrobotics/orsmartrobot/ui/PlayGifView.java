package com.orrobotics.orsmartrobot.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Movie;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

public class PlayGifView extends View {

	private static final int DEFAULT_MOVIEW_DURATION = 1000;

	private int mMovieResourceId;
	private Movie mMovie;

	private long mMovieStart = 0;
	private int mCurrentAnimationTime = 0;

	private int width;
	private int height;

	@SuppressLint("NewApi")
	public PlayGifView(Context context, AttributeSet attrs) {
		super(context, attrs);

		/**
		 * Starting from HONEYCOMB have to turn off HardWare acceleration to
		 * draw Movie on Canvas.
		 */
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		}
	}

	public void setImageResourceFromFile(Movie movie, int deviceWidth,
			int deviceHeight) {

		mMovie = movie;
		width = deviceWidth;
		height = deviceHeight;
		requestLayout();
	}

	public void setImageResource(int mvId) {
		this.mMovieResourceId = mvId;
		mMovie = Movie.decodeStream(getResources().openRawResource(
				mMovieResourceId));
		width = mMovie.width();
		height = mMovie.height();
		requestLayout();
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		Log.i("", "---width=" + width);
		Log.i("", "---height=" + height);
		if (mMovie != null) {
			Log.v("", "---mMovie---");
			setMeasuredDimension(width, height);
		} else {
			setMeasuredDimension(getSuggestedMinimumWidth(),
					getSuggestedMinimumHeight());
		}
	}

	@Override
	protected void onDraw(Canvas canvas) {

		if (mMovie != null) {
			updateAnimtionTime();
			drawGif(canvas);
			invalidate();
		} else {
			drawGif(canvas);
		}
	}

	private void updateAnimtionTime() {
		long now = android.os.SystemClock.uptimeMillis();

		if (mMovieStart == 0) {
			mMovieStart = now;
		}
		int dur = mMovie.duration();
		if (dur == 0) {
			dur = DEFAULT_MOVIEW_DURATION;
		}
		mCurrentAnimationTime = (int) ((now - mMovieStart) % dur);
	}

	private void drawGif(Canvas canvas) {
		mMovie.setTime(mCurrentAnimationTime);
		mMovie.draw(canvas, 0, 0);
		canvas.restore();
	}

}