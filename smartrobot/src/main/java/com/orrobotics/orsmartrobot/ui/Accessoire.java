package com.orrobotics.orsmartrobot.ui;

public class Accessoire {

	String title;
	ToggleSwitch accessoir;

	public Accessoire(String title, ToggleSwitch accessoir) {
		this.title = title;
		this.accessoir = accessoir;
	}

	public Accessoire() {

	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public ToggleSwitch getAccessoir() {
		return accessoir;
	}

	public void setAccessoir(ToggleSwitch accessoir) {
		this.accessoir = accessoir;
	}
}
