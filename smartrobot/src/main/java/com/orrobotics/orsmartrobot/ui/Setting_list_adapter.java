package com.orrobotics.orsmartrobot.ui;

import com.orrobotics.orsmartrobot.R;
import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


public class Setting_list_adapter extends BaseAdapter {

	private Context cntxt;
	String[] setting_list;
	int[] setting_icons;
	SmartRobotApplication application;

	public Setting_list_adapter(Context cntxt, String[] setting_list,
			int[] setting_icons) {
		this.cntxt = cntxt;
		this.setting_icons = setting_icons;
		this.setting_list = setting_list;
		application= (SmartRobotApplication) cntxt;
		
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return setting_list.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return setting_list[position];
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View row = null;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) cntxt
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(R.layout.custom_row, parent, false);
		} else {
			row = convertView;
		}

		TextView titleRow = (TextView) row.findViewById(R.id.textView1);
		ImageView imgRow = (ImageView) row.findViewById(R.id.imageView1);
		titleRow.setText(setting_list[position]);
		imgRow.setImageResource(setting_icons[position]);
	
  
		return row;
	}

	


}