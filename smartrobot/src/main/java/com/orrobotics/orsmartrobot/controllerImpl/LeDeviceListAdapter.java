package com.orrobotics.orsmartrobot.controllerImpl;

import java.util.ArrayList;

import com.orrobotics.orsmartrobot.R;
import com.orrobotics.orsmartrobot.model.Device;

import android.annotation.SuppressLint;
import android.app.ListActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class LeDeviceListAdapter extends BaseAdapter {
	private ArrayList<Device> mLeDevices;
	private LayoutInflater mInflator;
	boolean flag = false;

	public LeDeviceListAdapter(ListActivity deviceScanActivity) {
		super();
		mLeDevices = new ArrayList<Device>();
		mInflator = deviceScanActivity.getLayoutInflater();
	}

	public void addDevice(Device device) {
		flag = false;
		for (Device device1 : mLeDevices) {
			if (device1.getAdresse().equals(device.getAdresse())) {
				flag = true;
			}
		}
		if (flag == false) {

			mLeDevices.add(device);
		}
	}

	public Device getDevice(int position) {
		return mLeDevices.get(position);
	}

	public void clear() {
		mLeDevices.clear();
	}

	@Override
	public int getCount() {
		return mLeDevices.size();
	}

	@Override
	public Object getItem(int i) {
		return mLeDevices.get(i);
	}

	@Override
	public long getItemId(int i) {
		return i;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int i, View view, ViewGroup viewGroup) {
		ViewHolder viewHolder;
		if (view == null) {
			view = mInflator.inflate(R.layout.listitem_device, null);
			viewHolder = new ViewHolder();
			viewHolder.deviceAddress = (TextView) view
					.findViewById(R.id.device_address);
			viewHolder.deviceName = (TextView) view
					.findViewById(R.id.device_name);
			view.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) view.getTag();
		}

		Device device = mLeDevices.get(i);

		final String deviceName = device.getNumeroSerie();

		if (deviceName != null && deviceName.length() > 0)
			viewHolder.deviceName.setText(deviceName);
		else
			viewHolder.deviceName.setText(R.string.unknown_device);

		viewHolder.deviceAddress.setText(device.getAdresse());

		return view;
	}

	static class ViewHolder {
		TextView deviceName;
		TextView deviceAddress;
	}
}
