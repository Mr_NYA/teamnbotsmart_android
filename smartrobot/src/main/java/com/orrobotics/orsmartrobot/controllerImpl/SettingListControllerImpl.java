package com.orrobotics.orsmartrobot.controllerImpl;

import android.util.Log;

import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.controller.SettingListController;
import com.orrobotics.orsmartrobot.model.SettingListModel;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class SettingListControllerImpl implements SettingListController {

	SettingListModel model;
	SmartRobotApplication application;

	String langue;
	String speed;
	Locale local;
	int lastSelection;

	public SettingListControllerImpl(final SettingListModel model) {

		this.model = model;
		application = (SmartRobotApplication) model.getContext();



	}

	@Override
	public void SendTts() {
		// TODO Auto-generated method stub
		int Speedpos = model.getLanguageSpeedSelection();
		if (Speedpos == 0) {

			speed = "0.1f";
		}

		else if (Speedpos == 1) {

			speed = "0.5f";

		} else if (Speedpos == 2) {

			speed = "1.5f";

		} else if (Speedpos == 3) {

			speed = "2.0f";

		} else {

			speed = "1.0f";

		}

		langue = model.getLanguageSelection();

	}

	@Override
	public String getLanguage() {
		// TODO Auto-generated method stub

		return model.getLanguageSelection();

	}

	@Override
	public String getLanguageCode(String langageName) {
		// TODO Auto-generated method stub
		HashMap<String, String> listLanguages = new HashMap<>();
		String ttsCode = "";
		listLanguages = model.getListLanguage();
		for (Map.Entry<String, String> entry : listLanguages.entrySet()) {
			if (entry.getValue().equals(langageName)) {
				ttsCode = entry.getKey();

			}
		}
		return ttsCode;

	}

	@Override
	public int setLanguage(String language) {
		// TODO Auto-generated method stub
		int selection = 0;
		int cmpt = 0;
		HashMap<String, String> mapLangues = new HashMap<>();

		mapLangues = model.getListLanguage();

		for (Map.Entry<String, String> entry : mapLangues.entrySet()) {
			cmpt++;

			if (entry.getValue().equals(language)) {
				selection = cmpt - 1;
			}
		}
        Log.i("hhh","--selection="+selection);
		lastSelection = selection;
		return selection;
	}

}

