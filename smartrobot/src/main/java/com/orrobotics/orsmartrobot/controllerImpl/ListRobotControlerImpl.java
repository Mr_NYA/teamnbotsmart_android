package com.orrobotics.orsmartrobot.controllerImpl;

import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import android.annotation.SuppressLint;

import com.orrobotics.webservice.json.JsonParser;
import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.controller.ListRobotController;
import com.orrobotics.orsmartrobot.model.ListRobotModel;
import com.orrobotics.orsmartrobot.observer.OrSmarRobotObserver;
import com.orrobotics.orsmartrobot.util.GestionDesFichiers;
import com.orrobotics.orsmartrobot.util.RemoteControlUtil;

/**
 *
 * @author Hajiba IFRAH
 * @version 2.1.0
 * @date 18/05/2015
 *
 */
@SuppressLint("NewApi")
public class ListRobotControlerImpl implements ListRobotController,
		OrSmarRobotObserver {

	ListRobotModel listRobot;
	SmartRobotApplication application;

	JSONArray jr = null;

	int length;
	int j = 0;
	Boolean flag = false;
	ArrayList<String> robotNames = new ArrayList<String>();
	ArrayList<String> robotAdresses = new ArrayList<String>();
	ArrayList<String> robotSerie = new ArrayList<String>();
	ArrayList<String> pseudoSkype = new ArrayList<String>();
	ArrayList<String> pseudoOpenFire = new ArrayList<String>();
	ArrayList<String> passwordOpenFire = new ArrayList<String>();
	ArrayList<String> pseudoSIP = new ArrayList<String>();
	ArrayList<String> passwordSIP = new ArrayList<String>();
	ArrayList<Integer> index_pseudo = new ArrayList<Integer>();
	ArrayList<Boolean> robotChecked = new ArrayList<Boolean>();
	ArrayList<Integer> IndexrobotChecked = new ArrayList<Integer>();
	ArrayList<Integer> idRobot = new ArrayList<Integer>();

	public ListRobotControlerImpl(ListRobotModel listRobot) {

		this.listRobot = listRobot;

		application = (SmartRobotApplication) this.listRobot.getContext();
		application.registerObserver(ListRobotControlerImpl.this);
	}

	/**
	 * Permet de calculer le nombre des robots récupérés via le site espace
	 * cleint
	 *
	 */
	@Override
	public int getlength() {
		// TODO Auto-generated method stub

		if (listRobot.getFlagRefresh() == true
				|| application.isSetting() == true) {
			try {
				jr = new JSONArray(application.getJsonList());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

			}
		} else {
			jr = JsonParser.getListRobot(application.getJsonList());
			listRobot.setFlagRefresh(false);

		}
		if (jr != null)
			length = jr.length();
		GestionDesFichiers.log("le nombre des robots dans l'espace client ", ""
				+ length, "Log_SmartRobot");

		return length;
	}

	/**
	 * Remplir les pseudo open fire, les noms et les pseudo skypes de chaque
	 * robot dans des tableaux
	 *
	 */

	@Override
	public void setData(int lenght) {
		// TODO Auto-generated method stub

		for (int i = 0; i < lenght; i++) {
			try {
				String ListeRobot = jr.getString(i);
				String typeRobot = JsonParser.getTypeRobot(ListeRobot);
				if (typeRobot.equals(Integer.toString(application
						.getRobotType()))) {
					String pseudoOpenfire = JsonParser
							.getPseuoOpenFire(ListeRobot);
					pseudoOpenFire.add(pseudoOpenfire);
					passwordOpenFire.add(JsonParser
							.getPasswordOpenFire(ListeRobot));
					pseudoSIP.add(JsonParser.getPseuoSIP(ListeRobot));
					passwordSIP.add(JsonParser.getPasswordSIP(ListeRobot));
					String pseuudoSky = JsonParser.getPseudoSkype(ListeRobot);
					pseudoSkype.add(pseuudoSky);
					String robotName = JsonParser.getNom(ListeRobot);
					robotNames.add(j, robotName);
					String macAdress = JsonParser.getMacAdresse(ListeRobot);
					robotAdresses.add(macAdress);
					robotSerie.add(JsonParser.getSerie(ListeRobot));
					index_pseudo.add(j);
					String id = JsonParser.getIdRobot(ListeRobot);
					idRobot.add(Integer.parseInt(id));
					robotChecked.add(false);
					application.setEditor(RemoteControlUtil.ID, id);
					j++;
				}

				GestionDesFichiers.log("INFO ", "pseudoOpenFire "
						+ pseudoOpenFire + " passwordOpenFire "
						+ passwordOpenFire + "pseudoSIP " + pseudoSIP
						+ " passwordSIP " + passwordSIP + " pseudoSkype "
						+ pseudoSkype + " robotNames " + robotNames
						+ " robotAdresses " + robotAdresses + " robotSerie "
						+ robotSerie + " robotChecked " + robotChecked
						+ " index_pseudo " + index_pseudo, "Log_SmartRobot");
				listRobot.setPseudoOpenFire(pseudoOpenFire);
				listRobot.setPasswordOpenFire(passwordOpenFire);
				listRobot.setPseudoSIP(pseudoSIP);
				listRobot.setPasswordSIP(passwordSIP);
				listRobot.setPseudoSkype(pseudoSkype);
				listRobot.setRobotNames(robotNames);
				listRobot.setRobotAdresses(robotAdresses);
				listRobot.setRobotSerie(robotSerie);
				listRobot.setRobotChecked(robotChecked);
				listRobot.setIndexPseudo(index_pseudo);
				listRobot.setIdRobot(idRobot);

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				GestionDesFichiers.log("INFO lister les robots", e.getMessage()
						.toString(), "Log_SmartRobot");
			}

		}
	}

	@Override
	public void removeObserver() {
		// TODO Auto-generated method stub
		application.removeObserver(ListRobotControlerImpl.this);
	}

	@Override
	public void update(String message) throws IOException {
		// TODO Auto-generated method stub
		if (message.contains("adresse")) {
			for (int j = 0; j < robotAdresses.size(); j++) {
				if (robotAdresses.get(j).equals(message.split(";")[1]))
					robotChecked.set(j, true);
				else
					IndexrobotChecked.add(j);
			}
			flag = true;
			listRobot.setRobotChecked(robotChecked);
			application.notifyObservers("updateAdapter");
		} else if (message.contains("endScan")) {
			if (IndexrobotChecked.size() == 0 && flag == false) {
				for (int j = 0; j < robotChecked.size(); j++)
					robotChecked.set(j, false);
			} else {
				for (int i = 0; i < IndexrobotChecked.size(); i++) {
					robotChecked.set(IndexrobotChecked.get(i), false);
				}
			}
			listRobot.setRobotChecked(robotChecked);
			application.notifyObservers("updateAdapter");
			IndexrobotChecked.clear();
			flag = false;

		}
	}

}
