package com.orrobotics.orsmartrobot.controllerImpl;

import com.orrobotics.orsmartrobot.controller.PassRecoverControler;
import com.orrobotics.orsmartrobot.model.PassRecoverModel;

/**
 * 
 * @author Hajiba IFRAH
 * @version 2.1.0
 * @date 18/05/2015
 * 
 */

public class PassRecoverControllerImpl implements PassRecoverControler {

	PassRecoverModel model;

	public PassRecoverControllerImpl(PassRecoverModel model) {

		this.model = model;
	}

}
