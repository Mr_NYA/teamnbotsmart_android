package com.orrobotics.orsmartrobot.controllerImpl;

import com.orrobotics.orsmartrobot.controller.AuthentificationController;
import com.orrobotics.orsmartrobot.model.AuthentificationModel;

/**
 * 
 * @author Hajiba IFRAH
 * @version 1.3.2
 * @date 18/05/2015
 * 
 */
public class AuthentificationControllerImpl implements
		AuthentificationController {

	AuthentificationModel modelAuth;

	public AuthentificationControllerImpl(AuthentificationModel modelAuth) {

		this.modelAuth = modelAuth;
	}

}
