package com.orrobotics.orsmartrobot.controllerImpl;


import org.apache.commons.codec.language.Soundex;
import org.linphone.core.LinphoneCall;
import org.linphone.core.LinphoneCore;
import org.linphone.mediastream.video.capture.hwconf.AndroidCameraConfiguration;

import android.accounts.Account;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.speech.tts.TextToSpeech;
import android.util.Log;

import com.orrobotics.linphone.CallManager;
import com.orrobotics.linphone.LinphoneRegistration;
import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.controller.ControlRobotController;
import com.orrobotics.orsmartrobot.model.ControlRobotModel;
import com.orrobotics.orsmartrobot.service.HexiwearBluetoothService;
import com.orrobotics.orsmartrobot.service.LinphoneService;
import com.orrobotics.orsmartrobot.service.ServiceRobot;
import com.orrobotics.orsmartrobot.service.TestDebitService;
import com.orrobotics.orsmartrobot.util.GestionDesFichiers;
import com.orrobotics.orsmartrobot.util.Phonetic;
import com.orrobotics.orsmartrobot.util.RemoteControlUtil;
import com.orrobotics.orsmartrobot.util.SkypeUtilities;
import com.orrobotics.orsmartrobot.hexiwear.Characteristic;

@SuppressLint({ "InlinedApi", "NewApi" })
public class ControlRobotControllerImpl implements ControlRobotController {

	ControlRobotModel model;
	SmartRobotApplication application;
	Intent serviceXmpp;
	Intent linphoneService;
	Boolean flagCon = true;
	HexiwearBluetoothService mBluetoothLe;
	Intent bluetoothLeIntent;
	Handler mHandler = new Handler();
	Boolean flagFound = false;
	int i = 0;
	Boolean flagIsbund = false;
	Intent serviceDebitTest;

	private final ServiceConnection mServiceConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName componentName,
									   IBinder service) {
			flagIsbund = true;
			mBluetoothLe = ((HexiwearBluetoothService.LocalBinder) service)
					.getService();
			mBluetoothLe.initialize();

		}

		@Override
		public void onServiceDisconnected(ComponentName componentName) {
			mBluetoothLe = null;
			flagIsbund = false;
		}
	};

	public ControlRobotControllerImpl(final ControlRobotModel model) {

		this.model = model;
		application = (SmartRobotApplication) model.getContext();

	}

	@Override
	public void move(String message) {
		// TODO Auto-generated method stub
		application.notifyObservers(message);

	}

	@Override
	public void stopMotion() {
		// TODO Auto-generated method stub
		application.notifyObservers("stop");
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

		flagCon = false;
		application.shutDownTTS();

		mHandler.removeCallbacks(run);
		serviceXmpp = new Intent(this.model.getActivity(), ServiceRobot.class);
		linphoneService = new Intent(this.model.getActivity(),
				LinphoneService.class);

		application.getActivity().stopService(linphoneService);

		application.getActivity().stopService(serviceXmpp);
		serviceDebitTest= new Intent(this.model.getActivity(),
				TestDebitService.class);
		application.getActivity().stopService(serviceDebitTest);


	}

	@Override
	public void switchCamera() {
		// TODO Auto-generated method stub

		try {
			if (LinphoneRegistration.lc != null) {
				int videoDeviceId = LinphoneRegistration.lc.getVideoDevice();
				videoDeviceId = (videoDeviceId + 1)
						% AndroidCameraConfiguration.retrieveCameras().length;

				LinphoneRegistration.lc.setVideoDevice(videoDeviceId);
				application.setRobotEditor(RemoteControlUtil.CAMERA_ID, ""
						+ videoDeviceId);
				CallManager.getInstance().updateCall();

			} else {
				GestionDesFichiers.log("INFO", "---LinphoneRegistration.lc =="
						+ LinphoneRegistration.lc, "Log_SmartRobot");
			}

		} catch (Exception e) {
			Log.v("linphone",
					"Impossible de changer la camera : pas de 2éme camera");
			GestionDesFichiers.log("INFO",
					"---LinphoneRegistration.lc Exception=="
							+ e.getMessage().toString(), "Log_SmartRobot");
		}
	}

	@Override
	public void terminateCall() {
		// TODO Auto-generated method stub
		LinphoneCore lc = LinphoneRegistration.lc;
		if (lc != null) {
			LinphoneCall currentCall = lc.getCurrentCall();

			if (currentCall != null) {
				lc.terminateCall(currentCall);
				ServiceRobot.sendMessage(application.getPseudoOpenFireSender(),
						"vocalCallEnd");

			} else {
				lc.terminateAllCalls();

			}
		}

	}

	@Override
	public void speak(final String message) {
		// TODO Auto-generated method stub

		application.getTts().speak(message, TextToSpeech.QUEUE_ADD, null);

	}

	@Override
	public double printDistance(String s1, String s2) {
		// TODO Auto-generated method stub
		double similarityOfStrings = 0.0;
		int editDistance = 0;
		if (s1.length() < s2.length()) { // s1 should always be bigger
			String swap = s1;
			s1 = s2;
			s2 = swap;
		}
		int bigLen = s1.length();
		editDistance = computeEditDistance(s1, s2);
		if (bigLen == 0) {
			similarityOfStrings = 1.0; /* both strings are zero length */
		} else {
			similarityOfStrings = (bigLen - editDistance) / (double) bigLen;
		}
		return similarityOfStrings;
	}

	@SuppressLint("DefaultLocale")
	public static int computeEditDistance(String s1, String s2) {
		s1 = s1.toLowerCase();
		s2 = s2.toLowerCase();

		int[] costs = new int[s2.length() + 1];
		for (int i = 0; i <= s1.length(); i++) {
			int lastValue = i;
			for (int j = 0; j <= s2.length(); j++) {
				if (i == 0) {
					costs[j] = j;
				} else {
					if (j > 0) {
						int newValue = costs[j - 1];
						if (s1.charAt(i - 1) != s2.charAt(j - 1)) {
							newValue = Math.min(Math.min(newValue, lastValue),
									costs[j]) + 1;
						}
						costs[j - 1] = lastValue;
						lastValue = newValue;
					}
				}
			}
			if (i > 0) {
				costs[s2.length()] = lastValue;
			}
		}
		return costs[s2.length()];
	}

	private final BroadcastReceiver receiver = new BroadcastReceiver() {

		@SuppressLint("NewApi")
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			if (intent.getAction().equals(HexiwearBluetoothService.CONNECTED)) {
				i = 0;
				mHandler.postDelayed(run, 30000);
			} else {
				final String uuid = intent
						.getStringExtra(HexiwearBluetoothService.READING_TYPE);
				final String data = intent
						.getStringExtra(HexiwearBluetoothService.STRING_DATA);
				if (data.isEmpty()) {
					return;
				}

				final Characteristic characteristic = Characteristic
						.byUuid(uuid);
				if (characteristic == null) {
					Log.w("INFO", "UUID " + uuid + " is unknown. Skipping.");
					return;
				}

				switch (characteristic) {
					case BATTERY:
						model.setBatterie(data);
						break;
					case TEMPERATURE:

						model.setTemperature(data);
						break;
					case HUMIDITY:

						model.setHumidity(data);
						break;
					case PRESSURE:

						model.setPressure(data);
						break;
					case HEARTRATE:

						model.setCardiaque(data);
						break;
					case LIGHT:

						model.setLight(data);
						break;
					case STEPS:

						model.setStep(data);
						break;
					case CALORIES:

						model.setCalories(data);
						break;
					case ACCELERATION:

						model.setAccelerationReadings(data.split(";"));
						break;
					case MAGNET:

						model.setMagnetReadings(data.split(";"));
						break;
					case GYRO:

						model.setGyroscopeReadings(data.split(";"));
						break;
					default:
						break;
				}

				String action = application.getHexiwearInfo().split(",")[1];
				if (((!model.getTemperature().contains("0,00"))
						&& model.getBatterie() != null
						&& model.getHumidity() != null
						&& model.getLight() != null
						&& model.getPressure() != null
						&& model.getAccelerationReadings() != null
						&& model.getActivity() != null
						&& model.getGyroscopeReadings() != null && model
						.getMagnetReadings() != null)
						|| (action.contains("heartbeat") && model
						.getCardiaque() != null)
						|| (action.contains("steps") && model.getStep() != null && i > 5)
						|| (action.contains("calories")
						&& model.getCalories() != null && i > 5)) {

					Log.i("TAG", "~ getTemperature");
					flagFound = true;
					mHandler.removeCallbacks(run);
					if (flagIsbund == true) {
						stopService();
					}
					flagIsbund = false;

					application.notifyObservers("treatement");

				}
				i = i + 1;

			}
		}
	};

	Runnable run = new Runnable() {
		@Override
		public void run() {
			Log.i("TAG", "~ Stopping Hexiwear (timeout)");
			if (flagFound == false) {
				i = 0;
				if (flagIsbund == true) {
					stopService();
				}
				flagIsbund = false;
				application.notifyObservers("treatement");
			} else {
				i = 0;
				application.notifyObservers("infodispo");
				flagFound = false;
				stopService();
			}
		}
	};

	@Override
	public TextToSpeech getTTS() {
		// TODO Auto-generated method stub
		return application.getTts();
	}

	@Override
	public void callSkype(String pseudo) {
		// TODO Auto-generated method stub
		if (!application.isAppInstalled(application.getActivity(),
				"com.skype.raider")) {
			SkypeUtilities.downloadSkype(application.getActivity());
		} else {
			Account[] pseudoSkypeUser = SkypeUtilities.PseudoSkype(application
					.getApplicationContext());
			if (pseudoSkypeUser.length != 1) {
				SkypeUtilities.login_to_Skype(model.getActivity(),
						model.getContext());
			} else {

				SkypeUtilities.initier_appel_Skype(model.getContext(), pseudo);

			}

		}
	}

	@Override
	public String getPseudo(String msg) {
		// TODO Auto-generated method stub
		String[] data = msg.split(" ");
		String word;
		int indexTypeCall;
		String pseudo = "";
		String pseudo_ = "";
		if (application.getLangageTTS().contains("en"))
			word = "call";
		else
			word = "appeler";
		int index = getIndex(data, word);
		if (msg.contains("skype")) {
			indexTypeCall = getIndex(data, "skype");
		} else if (msg.contains("linphone")) {
			indexTypeCall = getIndex(data, "linphone");
		} else {
			indexTypeCall = data.length + 1;
		}

		for (int i = 1; i < ((indexTypeCall - index) - 1); i++) {
			pseudo = pseudo + data[index + i];
			if (i == 1)
				pseudo_ = data[index + 1];
			else
				pseudo_ = pseudo_ + " " + data[index + i];
		}

		pseudo = pseudo.replace("de.", ":");
		pseudo = pseudo.replace("depain", ":");
		pseudo = pseudo.replace("2.", ":");
		pseudo = pseudo.replace("dupont", ":");
		pseudo = pseudo.replace("à", "a");
		pseudo = pseudo.replace("to.", ":");
		pseudo = pseudo.replace("à", "a");

		return pseudo + ";" + pseudo_;
	}

	private int getIndex(String[] data, String word) {
		// TODO Auto-generated method stub

		for (int i = 0; i < data.length; i++) {
			if (data[i].equals(word)) {
				return i;
			}
		}
		return -1;

	}

	@Override
	public void startService() {
		// TODO Auto-generated method stub
		bluetoothLeIntent = new Intent(model.getContext(),
				HexiwearBluetoothService.class);
		model.getActivity().bindService(bluetoothLeIntent, mServiceConnection,
				Context.BIND_AUTO_CREATE);

		IntentFilter filter = new IntentFilter(
				HexiwearBluetoothService.DATA_AVAILABLE);
		IntentFilter filter1 = new IntentFilter(
				HexiwearBluetoothService.CONNECTED);
		model.getActivity().registerReceiver(receiver, filter);
		model.getActivity().registerReceiver(receiver, filter1);

	}

	@Override
	public void stopService() {
		// TODO Auto-generated method stub
		model.getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				try {
					model.getActivity().unregisterReceiver(receiver);
					application.getActivity().unbindService(mServiceConnection);
				} catch (Exception e) {

				}

			}
		});

	}

	@Override
	public boolean isEqualPhonetic(String result, String keyword) {
		String resultPhonetic = "";
		String keywordPhonetic = "";

		if(application.getLangageTTS().contains("en")){
			Soundex sound = new Soundex();

			resultPhonetic = sound.soundex(result);
			keywordPhonetic = sound.soundex(keyword);

		}
		else {
			resultPhonetic = Phonetic.genererPhonetic(result);
			keywordPhonetic = Phonetic.genererPhonetic(keyword);
		}

		Log.i("root", "----resultRoot=" + resultPhonetic);
		Log.i("root", "----keywordRoot=" + keywordPhonetic);

		if (resultPhonetic.equals(keywordPhonetic))
			return true;
		else return false;

	}
}
