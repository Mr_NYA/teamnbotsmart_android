package com.orrobotics.orsmartrobot.controllerImpl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import com.orrobotics.orsmartrobot.controller.CreationCompteController;
import com.orrobotics.orsmartrobot.model.CreationCompteModel;

/**
 *
 * @author Hajiba IFRAH
 * @version 2.1.0
 * @date 18/05/2015
 *
 */

public class CreationCompteControllerImpl implements CreationCompteController {

	CreationCompteModel modelCrea;

	public CreationCompteControllerImpl(CreationCompteModel model) {

		modelCrea = model;

	}

	/**
	 * Calculer la différence entre une date sélectionnée par l'utilisateur et
	 * la date d'aujourd'hui
	 *
	 */

	@Override
	public float getDiffrentTime(String dateInString) {
		// TODO Auto-generated method stub
		SimpleDateFormat formatter;

		formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
		float different_time = 0;
		try {

			Date date = formatter.parse(dateInString);

			String currentDateandTime = formatter.format(new Date());
			Date date_current = formatter.parse(currentDateandTime);

			different_time = TimeUnit.MILLISECONDS.toSeconds(date_current
					.getTime() - date.getTime());

		} catch (java.text.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return different_time;
	}

}
