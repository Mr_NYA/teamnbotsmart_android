package com.orrobotics.orsmartrobot.controllerImpl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.orrobotics.orsmartrobot.R;
import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.controller.BleScanController;
import com.orrobotics.orsmartrobot.model.BleScanModel;
import com.orrobotics.orsmartrobot.util.GestionDesFichiers;
import com.orrobotics.orsmartrobot.util.RemoteControlUtil;
import com.orrobotics.webservice.WebService;
import com.orrobotics.webservice.json.JsonParser;
import com.orrobotics.webservice.url.urlParser;

/**
 *
 * @author Hajiba IFRAH
 * @version 2.1.0
 * @date 18/05/2015
 *
 */
public class BleScanControllerImpl implements BleScanController {

	BleScanModel model;
	SmartRobotApplication application;

	String Json;
	String code;

	public BleScanControllerImpl(BleScanModel model) {
		// TODO Auto-generated constructor stub

		this.model = model;
		application = (SmartRobotApplication) model.getContext();
	}

	/**
	 *
	 * Mise à jour de l'adresse MAC
	 *
	 */

	@Override
	public void updateMacAdresse(String address, String name,
								 boolean fromBleScan) {
		// TODO Auto-generated method stub
		final Activity currentActi = application.getActivity();
		String url = SmartRobotApplication.getParamFromFile("param",
				"configuration.properties", null);

		String serverURL;
		try {
			serverURL = urlParser.getURL_UpdaRobot(url, application.getEmail(),
					application.getRobotPseudoSkype(),
					application.getRobotName(), application.getRobotType(),
					address, URLEncoder.encode(name, "UTF-8"));
			Log.i("Serial", " serverURL " + serverURL);

			GestionDesFichiers
					.log("l'url du web servcie de mise à jour des informations d'un robot",
							serverURL, "Log_SmartRobot");
			//Json = WebService.connection(serverURL);
			WebService ws = new WebService();
			WebService.HttpRequest callbackservice = ws.new HttpRequest() {
				@Override
				public void receiveData(Object object) {
                 Json = (String)object;
					GestionDesFichiers
							.log("réponse du web servcie de mise à jour des informations d'un robot",
									Json, "Log_SmartRobot");

					if (Json != null) {
						Log.i("Serial", "------JSon Not NULL");

						if (Json.contains("</html>")) {
							currentActi.runOnUiThread(new Runnable() {

								@Override
								public void run() {
									// TODO Auto-generated method stub
									Toast.makeText(
											application.getApplicationContext(),
											application
													.getApplicationContext()
													.getResources()
													.getString(
															R.string.error_calling_ws_msg),
											Toast.LENGTH_SHORT).show();
								}
							});

						} else {
							Log.i("Serial", "------JSon-------");
							code = JsonParser.getCode_Crea(Json);
							if (code.equals(String.valueOf(1))) {

								if (fromBleScan)
									application.notifyObservers("startActi");

								else {
									application
											.setEditor(RemoteControlUtil.NEW_MAC, "");
									currentActi.runOnUiThread(new Runnable() {

										@Override
										public void run() {
											// TODO Auto-generated method stub
											currentActi.recreate();
										}
									});

								}
							} else if (code.equals(String.valueOf(-1))) {
								Log.i("Serial", "------JSon=" + Json);
								currentActi.runOnUiThread(new Runnable() {

									@Override
									public void run() {
										// TODO Auto-generated method stub
										Toast.makeText(
												application.getApplicationContext(),
												JsonParser.getMessage(Json),
												Toast.LENGTH_SHORT).show();
									}
								});

							} else {
								currentActi.runOnUiThread(new Runnable() {

									@Override
									public void run() {
										// TODO Auto-generated method stub
										Toast.makeText(
												application.getApplicationContext(),
												application
														.getApplicationContext()
														.getResources()
														.getString(
																R.string.error_calling_ws_msg),
												Toast.LENGTH_SHORT).show();
									}
								});

							}
						}
					} else {
						Log.i("Serial", " web service Json Null");
						currentActi.runOnUiThread(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub
								Toast.makeText(
										application.getApplicationContext(),
										application
												.getApplicationContext()
												.getResources()
												.getString(
														R.string.error_calling_ws_msg),
										Toast.LENGTH_SHORT).show();
							}
						});

					}

				}
			};
			callbackservice.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, serverURL);

		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void updateMacInternFile(String address, String name) {
		// TODO Auto-generated method stub
		String jsonList = application.getJsonList();
		String robotName = application.getRobotName();

		String updatedJson = null;
		try {
			JSONArray jr = new JSONArray(jsonList);
			for (int i = 0; i < jr.length(); i++) {
				JSONObject jsonobject = jr.getJSONObject(i);
				String nom = jsonobject.getString("nom");
				Log.v("", "------nom=" + nom);
				if (nom.equals(robotName)) {
					jsonobject.put("adresseMAC", address);
					jsonobject.put("numeroSerie", name);
				}
				Log.v("", "------newMac=" + jsonobject.getString("adresseMAC"));
			}
			updatedJson = jr.toString();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Log.v("", "------updatedJson=" + updatedJson);
		application.setEditor(RemoteControlUtil.JSON_LIST, updatedJson);
		application.notifyObservers("startActi");
	}

}
