package com.orrobotics.orsmartrobot.controllerImpl;

import java.util.ArrayList;

import com.orrobotics.orsmartrobot.R;
import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.model.ListRobotModel;
import com.orrobotics.orsmartrobot.util.RemoteControlUtil;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

@SuppressLint({ "ResourceAsColor", "ViewHolder" })
public class ListRobotArrayAdapter extends ArrayAdapter<String> {

	Activity acti;
	ArrayList<String> robotName;
	ArrayList<String> robotAdresse;
	ArrayList<String> robotSerie;
	Bitmap imageId;
	ArrayList<Boolean> robotChecked;
	ListRobotModel model;
	SmartRobotApplication application;

	public ListRobotArrayAdapter(Activity acti, ListRobotModel model,
								 ArrayList<String> robotName, ArrayList<String> robotAdresse,
								 ArrayList<String> robotSerie, Bitmap bitmap,
								 ArrayList<Boolean> robotChecked) {
		super(acti, R.layout.list_item, robotName);
		this.acti = acti;
		this.robotName = robotName;
		this.robotAdresse = robotAdresse;
		this.robotSerie = robotSerie;
		this.imageId = bitmap;
		this.robotChecked = robotChecked;
		this.model = model;

		application = (SmartRobotApplication) this.model.getContext();

	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(final int position, final View view, ViewGroup parent) {
		LayoutInflater inflater = acti.getLayoutInflater();
		View rowView = inflater.inflate(R.layout.list_item_listviews, null,
				true);
		rowView.setBackgroundResource(R.drawable.selected);

		ImageView imageView = (ImageView) rowView.findViewById(R.id.img);
		imageView.setImageBitmap(imageId);

		TextView nom = (TextView) rowView.findViewById(R.id.Nom);
		nom.setText(robotName.get(position));
		TextView adresse = (TextView) rowView.findViewById(R.id.Adresse);
		TextView serie = (TextView) rowView.findViewById(R.id.Serie);
		String adresseRobot = robotAdresse.get(position);
		String serierobot = robotSerie.get(position);


		adresse.setTextColor(R.color.color);
		serie.setTextColor(R.color.color);
		if (adresseRobot.equals("null"))
			adresse.setText("");
		else
			adresse.setText(adresseRobot);

		if (serierobot.equalsIgnoreCase("null"))
			serie.setText("");
		else
			serie.setText(serierobot);

		LinearLayout scan = (LinearLayout) rowView.findViewById(R.id.Scan);
		scan.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				int index = model.getIndexPseudo().get(position);
				application.setEditor(RemoteControlUtil.ROBOT_NAME, model
						.getRobotNames().get(index));
				application.setEditor(RemoteControlUtil.ROBOT_PSEUDO_SKYPE,
						model.getPseudoSkype().get(index));
				application.notifyObservers("demarreNouvelleActivity");

			}
		});

		return rowView;
	}

}
