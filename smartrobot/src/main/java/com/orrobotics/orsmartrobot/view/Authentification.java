package com.orrobotics.orsmartrobot.view;

import com.orrobotics.orsmartrobot.R;
import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.controller.AuthentificationController;
import com.orrobotics.orsmartrobot.controllerImpl.AuthentificationControllerImpl;
import com.orrobotics.orsmartrobot.model.AuthentificationModel;
import com.orrobotics.orsmartrobot.modelImpl.AuthentificationModelImpl;
import com.orrobotics.orsmartrobot.ui.CircularProgress;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 *
 * @author Hajiba IFRAH Cette activity sert à récupérer les éléments de layout
 *         authentification et de passer ces éléments à la classe view pour les
 *         traités
 * @version 2.1.0
 * @date 18/05/2015
 *
 */

public class Authentification extends BaseActivity {

	/**
	 * Pour saisir l'email
	 */
	AutoCompleteTextView email;
	/**
	 * Pour saisir le mot de passe
	 */
	AutoCompleteTextView password;
	/**
	 * Permet d'afficher les erreurs de l'autentification
	 */
	TextView error;
	/**
	 * Le clic sur cette textView permet d'afficher l'interface de création de
	 * compte
	 */
	TextView compte;
	/**
	 * Le clic sur cette textView permet d'afficher l'interface de mot de passe
	 * oublié
	 */
	TextView PasswordForgted;
	/**
	 * Ce string est différent de null si l'interface de l'authentification à
	 * été consulté après l'interface de céation de compte
	 */
	String createCompte;

	/**
	 * Le bouton qui permet vérifier les informations saisie par l'utilisateur
	 * et d'appeler le web service auth
	 */
	Button connection;

	SmartRobotApplication application;

	Activity acti;

	AuthentificationView mView;
	Bitmap open;
	ImageView app_name1;
	String emptyList;
	CircularProgress progress;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.authentification);
		overridePendingTransition(R.anim.fadein, R.anim.fadeout);

		acti = this;
		/**
		 * Construction du MVC
		 */
		AuthentificationModel model = new AuthentificationModelImpl(
				getApplicationContext(), acti);
		AuthentificationController controller = new AuthentificationControllerImpl(
				model);
		mView = new AuthentificationView(controller, model);
		/**
		 * Initialisation des paramètres
		 */
		email = (AutoCompleteTextView) findViewById(R.id.editText1);
		password = (AutoCompleteTextView) findViewById(R.id.editText2);
		connection = (Button) findViewById(R.id.connectebtn);
		compte = (TextView) findViewById(R.id.comptebtn);
		error = (TextView) findViewById(R.id.error);
		PasswordForgted = (TextView) findViewById(R.id.passwordforgtedbtn);
		app_name1 = (ImageView) findViewById(R.id.textsplash1);
		application = (SmartRobotApplication) this.getApplicationContext();
		application.stricMode();
		progress = (CircularProgress) findViewById(R.id.progress);

		/**
		 * Géstion du logo
		 */
		if(modelApp.isLogoOptionEnabled()){
			open = application.getLogoFromExternalStorage();
		}
		if(open != null)
			app_name1.setImageBitmap(Bitmap.createScaledBitmap(open, 128, 128, false));

		mView.setWidgets(email, password, connection, error, compte,
				PasswordForgted,progress);

		mView.setListeners();

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			createCompte = extras.getString("EXTRA_SESSION_ID");
			emptyList = extras.getString("emptyList");
			/**
			 * Vérification si l'interface de l'authentification est consulté
			 * après l'interface de création de compte
			 *
			 */
			if (createCompte != null) {

				mView.init(createCompte);

			}
              /**
               * Vérification si l'interface de l'authentification est consulté
               * après que la liste des robot est vide
                **/
			if (emptyList != null){
				mView.init(emptyList);
			}

		} else {
			mView.init(null);
		}
	}

	/**
	 * Le clic sur le bouton retoure du Smartphone
	 *
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			application.notifyObservers("fermer");
			Intent intent = new Intent(Intent.ACTION_MAIN);
			intent.addCategory(Intent.CATEGORY_HOME);
			startActivity(intent);
			finish();
			android.os.Process.killProcess(android.os.Process.myPid());
			System.exit(0);
			return true;
		}
		return false;
	}

	@Override
	protected void onPause() {

		super.onPause();

	}

	@Override
	protected void onResume() {

		super.onResume();
		compte.setBackgroundResource(R.drawable.transparent_whiteborder_btn);

	}
}
