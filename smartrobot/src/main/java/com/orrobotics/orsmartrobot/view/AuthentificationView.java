package com.orrobotics.orsmartrobot.view;

import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;

import com.orrobotics.orsmartrobot.R;
import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.controller.AuthentificationController;
import com.orrobotics.orsmartrobot.model.AuthentificationModel;
import com.orrobotics.orsmartrobot.ui.CircularProgress;
import com.orrobotics.orsmartrobot.util.GestionDesFichiers;
import com.orrobotics.orsmartrobot.util.RemoteControlUtil;
import com.orrobotics.webservice.WebService;
import com.orrobotics.webservice.json.JsonParser;
import com.orrobotics.webservice.url.urlParser;

import org.json.JSONArray;
import org.json.JSONException;

/**
 *
 * @author Hajiba IFRAH
 * @version 2.1.0
 * @date 18/05/2015
 *
 */

public class AuthentificationView {

	AuthentificationController mController;
	AuthentificationModel modelAuth;

	AutoCompleteTextView email;
	AutoCompleteTextView password;
	Button connection;
	TextView error;
	TextView compte;
	TextView passwordForgted;
	SmartRobotApplication application;

	String emailTxt;
	String passwordTxt;
	String Json = null;
	String auth_attribut = null;
	Boolean flag = true;
    CircularProgress progress;

	public AuthentificationView(AuthentificationController controller,
								AuthentificationModel modelAut) {

		modelAuth = modelAut;
		mController = controller;

		application = (SmartRobotApplication) modelAuth.getContext();

	}

	/**
	 * Récupérer et affecter les paramètres initialiser dans la classe
	 * authentification
	 *
	 */
	public void setWidgets(AutoCompleteTextView email,
						   AutoCompleteTextView password, Button connection, TextView error,
						   TextView compte, TextView passwordForgted,CircularProgress progress) {
		// TODO Auto-generated method stub
		this.email = email;
		this.password = password;
		this.connection = connection;
		this.compte = compte;
		this.error = error;
		this.passwordForgted = passwordForgted;
        this.progress=progress;
        progress.setColor(application.getResources().getColor(R.color.blue));
        progress.setVisibility(View.INVISIBLE);

	}

	/**
	 * Cette méthode rassemble les évènements de clic
	 *
	 */
	public void setListeners() {
		// TODO Auto-generated method stub
		connection.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				emailTxt = email.getText().toString();
				passwordTxt = password.getText().toString();
				/**
				 * Stockage de l'adresse Email
				 *
				 */

				application.setEditor(RemoteControlUtil.EMAIL1, emailTxt);
				/**
				 * Vérification si tous les champs sont non vide
				 *
				 */
				if (emailTxt.trim().length() == 0
						|| passwordTxt.trim().length() == 0) {
					error.setText(modelAuth.getContext().getResources()
							.getString(R.string.fill_all_fields_msg));
				} else {
					Boolean isValid = android.util.Patterns.EMAIL_ADDRESS
							.matcher(emailTxt).matches();
					/**
					 * Vérification si la structure de l'adresse email est
					 * correcte
					 *
					 */
					if (isValid == false) {
						error.setText(modelAuth.getContext().getResources()
								.getString(R.string.invalid_mail_msg));
					} else {
                        application.getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progress.setVisibility(View.VISIBLE);
                            }
                        });
						String url = SmartRobotApplication.getParamFromFile(
								"param", "configuration.properties", null);
						String pseudoSkype = null;

						pseudoSkype = application.getPseudoSkype();

						String serverURL = urlParser.getURL_authentification(
								url, emailTxt, passwordTxt, "ninebot",
								pseudoSkype, "null", "null",
								Build.VERSION.RELEASE, "null");
						GestionDesFichiers.log("URL de serveur", serverURL,
								"Log_SmartRobot");
						//Json = WebService.connection(serverURL);

						WebService ws = new WebService();
						WebService.HttpRequest callbackservice = ws.new HttpRequest() {
							@Override
							public void receiveData(Object object) {
								Json = (String)object;
								GestionDesFichiers.log("Retour de wb service", Json,
										"Log_SmartRobot");
								if (Json != null) {
									auth_attribut = JsonParser.getAuth(Json);
									if (auth_attribut.equals(String.valueOf(true))
											|| auth_attribut.equals(String
											.valueOf(false))) {

									}
									setResponce();
								}
							}
						};
						callbackservice.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, serverURL);

					}
				}
			}
		});

		passwordForgted.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {

				application.StartAct(null, null, PassRecover.class,
						modelAuth.getActivity());
			}
		});

		compte.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				compte.setBackgroundResource(R.drawable.transparent_greyborder_btn);

				application.StartAct(null, null, CreationCompte.class,
						modelAuth.getActivity());

			}
		});
	}

	/**
	 * Si l'interface de l'authentification a été consulté après l'interface de
	 * création de compte, les champas email et le mot de passe seront remplit
	 * par les données saisit par l'utilisateur, sinon le champs sera remplit
	 * par l'email sauvegardé dans l'application
	 * Si l'interface de l'authentification a été ouvert après que la liste des root est vide
	 * @param extra
	 *            : cette valeur est différent de null si l'interface de
	 *            l'authentification a été consulté après la création de compte
	 *
	 */
	public void init(String extra) {
		// TODO Auto-generated method stub

		if (extra != null) {
            if(extra.equals("empty")){
                GestionDesFichiers.log("Retour de wb service", "cas true : pas de robot sur ce compte",
                        "Log_SmartRobot");

                error.setText(modelAuth.getContext().getResources()
                        .getString(R.string.noRobot));
			}
			else {
				GestionDesFichiers.log("authentification après la création de compte",
                        extra, "Log_SmartRobot");
				String[] array = extra.split(";");
				email.setText(array[0]);
				password.setText(array[1]);
				error.setTextColor(modelAuth.getContext().getResources()
						.getColor(R.color.error));
				error.setText(modelAuth.getContext().getResources()
						.getString(R.string.create_succes));
			}
		} else {
			email.setText(application.getEmail1());
		}
	}

	/**
	 * Méthode pour gère la réponse du web service
	 *
	 */
	private void setResponce() {

		if (auth_attribut.equals(String.valueOf(true))) {

			JSONArray jr = JsonParser.getListRobot(Json);

			GestionDesFichiers.log("Retour de wb service", "cas true",
					"Log_SmartRobot");

			try {
				if(jr.length() < 1 || (jr.length() >= 1 && !application.hasRobot(jr))){
                    application.getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progress.setVisibility(View.INVISIBLE);
                        }
                    });
                    GestionDesFichiers.log("Retour de wb service", "cas true : pas de robot sur ce compte",
                            "Log_SmartRobot");

                    error.setText(modelAuth.getContext().getResources()
                            .getString(R.string.noRobot));
                }
                else {
                    application.setEditor(RemoteControlUtil.EMAIL, emailTxt);
                    application.setEditor(RemoteControlUtil.PASSWORD, passwordTxt);

                    String nom = JsonParser.getNom(Json);
                    application.setEditor(RemoteControlUtil.NOM, nom);
                    String prenom = JsonParser.getPrenom(Json);
                    application.setEditor(RemoteControlUtil.PRENOM, prenom);
                    String pseudoOpenFire = JsonParser.getPseuoOpenFire(Json);
                    application.setEditor(
                            RemoteControlUtil.EXTRAS_PSEUDO_OPENFIRE_CONTROL,
                            pseudoOpenFire);
                    String pseudoSIP = JsonParser.getPseuoSIP(Json);
                    application.setEditor(RemoteControlUtil.PSEUDO_SIP, pseudoSIP);
                    String passwordSIP = JsonParser.getPasswordSIP(Json);
                    application.setEditor(RemoteControlUtil.PASSWORD_SIP, passwordSIP);

                    application.setData(emailTxt, passwordTxt, nom, prenom,
                            pseudoOpenFire, "", pseudoSIP, passwordSIP);
                    application.setEditor(RemoteControlUtil.JSON_LIST, Json);

                    application.StartAct("authentification", "true", ListRobot.class,
                            modelAuth.getActivity());
                    modelAuth.getActivity().finish();
                }
			} catch (JSONException e) {
				e.printStackTrace();
			}
		} else if (auth_attribut.equals(String.valueOf(false))) {
            application.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progress.setVisibility(View.INVISIBLE);
                }
            });
			GestionDesFichiers.log("Retour de wb service", "cas false",
					"Log_SmartRobot");
			if(application.getWsUrl().contains("ontomantics")) {
				error.setText(modelAuth.getContext().getResources()
						.getString(R.string.errorAuth));
			}
			else
			error.setText(JsonParser.getMessage(Json));

		}
	}

}
