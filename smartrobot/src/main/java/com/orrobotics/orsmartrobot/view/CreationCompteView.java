package com.orrobotics.orsmartrobot.view;

import java.util.ArrayList;
import java.util.Calendar;

import android.app.DatePickerDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.orrobotics.orsmartrobot.R;
import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.controller.CreationCompteController;
import com.orrobotics.orsmartrobot.model.CreationCompteModel;
import com.orrobotics.orsmartrobot.ui.CircularProgress;
import com.orrobotics.orsmartrobot.util.DatePickerFragment;
import com.orrobotics.orsmartrobot.util.GestionDesFichiers;
import com.orrobotics.webservice.WebService;
import com.orrobotics.webservice.json.JsonParser;
import com.orrobotics.webservice.url.urlParser;

public class CreationCompteView {

	AutoCompleteTextView vnom;
	AutoCompleteTextView vprenom;
	AutoCompleteTextView vemail;
	AutoCompleteTextView vpassword;
	AutoCompleteTextView vconfirm;
	AutoCompleteTextView vcity;

	TextView verror;
	TextView verrorEmail;
	TextView verrorPass;
	TextView verrorPassConfirm;
	TextView verrorNom;
	TextView verrorPrenom;
	TextView verrorDate;
	TextView verrorCity;

	ListView vListCivility;
	ListView vListCountries;
	EditText vedit;
	EditText veditCountry;
	EditText vpDisplayDate;

	Button vsave;

	SmartRobotApplication model;

	CreationCompteController mController;

	private int pYear;
	private int pMonth;
	private int pDay;

	ArrayAdapter<String> adapter;
	ArrayAdapter<String> adapter_country;

	int i = 0;
	int j = 0;

	ArrayList<String> civilitys = new ArrayList<String>();

	String selected_civility = null;
	String civility;
	String serverURL;
	String Json_crea = null;
	String code_attribut = null;
	CreationCompteModel modelCrea;
    CircularProgress vprogress;

	public CreationCompteView(CreationCompteController controller,
							  CreationCompteModel modelCr) {

		modelCrea = modelCr;
		mController = controller;
		model = (SmartRobotApplication) modelCrea.getContext();

	}

	/**
	 * Récupérer et affecter les paramètres initialiser dans la classe
	 * CreationCompte
	 *
	 */
	public void setWidgets(ListView ListCivility, ListView ListCountries,
						   EditText edit, EditText editCountry, EditText pDisplayDate,
						   AutoCompleteTextView nom, AutoCompleteTextView prenom,
						   AutoCompleteTextView email, AutoCompleteTextView password,
						   AutoCompleteTextView confirm, AutoCompleteTextView city,
						   Button save, TextView error, TextView errorCity,
						   TextView errorDate, TextView errorEmail, TextView errorNom,
						   TextView errorPass, TextView errorPassConfirm, TextView errorPrenom,CircularProgress progress) {

		vnom = nom;
		vprenom = prenom;
		vemail = email;
		vpassword = password;
		vconfirm = confirm;
		vcity = city;
		verror = error;
		verrorCity = errorCity;
		verrorDate = errorDate;
		verrorEmail = errorEmail;
		verrorNom = errorNom;
		verrorPass = errorPass;
		verrorPassConfirm = errorPassConfirm;
		verrorPrenom = errorPrenom;
		vListCivility = ListCivility;
		vListCountries = ListCountries;
		vedit = edit;
		veditCountry = editCountry;
		vpDisplayDate = pDisplayDate;
		vsave = save;
		vprogress = progress;
        progress.setColor(model.getResources().getColor(R.color.white));
        progress.setVisibility(View.INVISIBLE);
		/**
		 * Remplir le contenu de la liste
		 */
		civilitys.add(modelCrea.getContext().getResources()
				.getString(R.string.madame));
		civilitys.add(modelCrea.getContext().getResources()
				.getString(R.string.monsieur));

		/**
		 * Remplir les champs pays et ville
		 */
		veditCountry.setText(modelCrea.getList().get(0));
		vcity.setText(modelCrea.getList().get(1));

		/**
		 * récupérer la date d'aujourd'hui
		 */
		final Calendar cal = Calendar.getInstance();
		pYear = cal.get(Calendar.YEAR);
		pMonth = cal.get(Calendar.MONTH);
		pDay = cal.get(Calendar.DAY_OF_MONTH);

		/**
		 * afficher la date d'aujourd'hui
		 */
		updateDisplay();

		adapter_country = new ArrayAdapter<String>(modelCrea.getContext(),
				R.layout.list_item, modelCrea.getContries());
		ListCountries.setAdapter(adapter_country);

		/**
		 * appeler la liste des civilités
		 */
		adapter = new ArrayAdapter<String>(modelCrea.getContext(),
				R.layout.list_item, civilitys);
		vListCivility.setAdapter(adapter);

	}

	/**
	 * Gestion des événements
	 */
	public void setListeners() {

		vpDisplayDate.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				showDatePicker();
			}
		});

		vListCountries.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
									int position, long id) {

				// ListView Clicked item value
				String itemValue = modelCrea.getContries().get(
						position);
				veditCountry.setText(itemValue);

				vListCountries.setVisibility(View.GONE);

			}
		});

		veditCountry.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {

				if (i == 0) {

					vListCountries.setVisibility(View.VISIBLE);
					vListCountries.setSelection(modelCrea.getContries()
							.indexOf(veditCountry.getText().toString()));

					i = 1;
				}

				else {
					vListCountries.setVisibility(View.GONE);
					i = 0;
				}

			}
		});

		/** list civilities click listener */
		vListCivility.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
									int position, long id) {

				// ListView Clicked item value
				String itemValue = (String) vListCivility
						.getItemAtPosition(position);
				vedit.setText(itemValue);

				vListCivility.setVisibility(View.GONE);

			}
		});

		vedit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {

				if (j == 0) {

					if (selected_civility != null) {
						civilitys.add(selected_civility);

					}

					selected_civility = vedit.getText().toString();
					civilitys.remove(vedit.getText().toString());

					modelCrea.getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {

							adapter.notifyDataSetChanged();
							vListCivility.setAdapter(adapter);

						}
					});

					adapter.notifyDataSetChanged();

					vListCivility.setVisibility(View.VISIBLE);
					j = 1;
				}

				else {
					vListCivility.setVisibility(View.GONE);
					j = 0;
				}

			}
		});

		vsave.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				// check if all fields are not empty
				if (vemail.getText().toString().trim().length() == 0
						|| vpassword.getText().toString().trim().length() == 0
						|| vedit.getText().toString().trim().length() == 0
						|| vprenom.getText().toString().trim().length() == 0
						|| vnom.getText().toString().trim().length() == 0
						|| vcity.getText().toString().trim().length() == 0) {

					if (vemail.getText().toString().trim().length() == 0) {
						verrorEmail.setText(modelCrea.getContext()
								.getResources()
								.getString(R.string.errorchampsEmail));
					} else
						verrorEmail.setText("");

					if (vpassword.getText().toString().trim().length() == 0) {
						verrorPass.setText(modelCrea.getContext()
								.getResources()
								.getString(R.string.errorchampsPass));
					} else
						verrorPass.setText("");

					if (vnom.getText().toString().trim().length() == 0) {
						verrorNom.setText(modelCrea.getContext().getResources()
								.getString(R.string.errorchampsNom));
					} else
						verrorNom.setText("");
					if (vprenom.getText().toString().trim().length() == 0) {
						verrorPrenom.setText(modelCrea.getContext()
								.getResources()
								.getString(R.string.errorchampsPrenom));
					} else
						verrorPrenom.setText("");
					if (vconfirm.getText().toString().trim().length() == 0) {
						verrorPassConfirm.setText(modelCrea.getContext()
								.getResources()
								.getString(R.string.errorchampsPassConfirm));
					} else
						verrorPassConfirm.setText("");

					if (vcity.getText().toString().trim().length() == 0) {
						verrorCity.setText(modelCrea.getContext()
								.getResources()
								.getString(R.string.errorchampsCity));
					} else
						verrorCity.setText("");
				} else {

					verrorEmail.setText("");
					verrorCity.setText("");
					verrorDate.setText("");
					verrorNom.setText("");
					verrorPass.setText("");
					verrorPassConfirm.setText("");
					verrorPrenom.setText("");

					// check if email adresse is valide
					Boolean isValid = android.util.Patterns.EMAIL_ADDRESS
							.matcher(vemail.getText().toString()).matches();

					if (isValid == false) {

						verror.setText(modelCrea.getContext().getResources()
								.getString(R.string.invalid_mail_msg));

					} else {

						String dateInString = vpDisplayDate.getText()
								.toString();

						float different_time = mController
								.getDiffrentTime(dateInString);

						if (different_time <= 0) {

							verror.setText(modelCrea.getContext()
									.getResources()
									.getString(R.string.invalid_birthday_msg));
						}

						else if (vpassword.getText().toString().trim().length() < 4) {

							verror.setText(modelCrea.getContext()
									.getResources()
									.getString(R.string.error_password1));
						}
						// check if password equals confirm password
						else if (!vpassword.getText().toString()
								.equals(vconfirm.getText().toString())) {

							verror.setText(modelCrea.getContext()
									.getResources()
									.getString(R.string.error_password));
						} else {

							String url = SmartRobotApplication
									.getParamFromFile("param",
											"Configuration.properties", null);

							String naissance = vpDisplayDate.getText()
									.toString().replace(" ", "");

							if (vedit.getText().toString().equals("Monsieur")) {

								civility = "M";
							} else {

								civility = "Mme";
							}
                            model.getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    vprogress.setVisibility(View.VISIBLE);
                                }
                            });
							serverURL = urlParser.getURL_Creation_Compte(url,
									vemail.getText().toString(), vpassword
											.getText().toString(), civility,
									vnom.getText().toString(), vprenom
											.getText().toString(), naissance);
							GestionDesFichiers
									.log("l'url du web servcie de création de compte",
											serverURL, "Log_SmartRobot");
							// get response
							//Json_crea = WebService.connection(serverURL);
							WebService ws = new WebService();
							WebService.HttpRequest callbackservice = ws.new HttpRequest() {
								@Override
								public void receiveData(Object object) {
									Json_crea = (String)object;
									GestionDesFichiers
											.log("la réponse du web servcie de création de compte",
													Json_crea, "Log_SmartRobot");
									if (Json_crea != null) {
										if (Json_crea.contains("</html>")) {
                                            model.getActivity().runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    vprogress.setVisibility(View.INVISIBLE);
                                                }
                                            });
											verror.setText(modelCrea
													.getContext()
													.getResources()
													.getString(
															R.string.error_calling_ws_msg));
										} else {

											// get code value from response
											code_attribut = JsonParser
													.getCode_Crea(Json_crea);
											new Task_crea()
													.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

										}
									} else {
                                        model.getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                vprogress.setVisibility(View.INVISIBLE);
                                            }
                                        });
										verror.setText(modelCrea
												.getContext()
												.getResources()
												.getString(
														R.string.error_calling_ws_msg));

									}
								}
							};
							callbackservice.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, serverURL);


						}
					}
				}

			}

		});

	}

	/**
	 * Ce task traite la réponse du web servcie
	 *
	 */
	class Task_crea extends AsyncTask<String, Integer, Boolean> {

		int progress = 0;

		@Override
		protected void onPreExecute() {

			super.onPreExecute();

		}

		@Override
		protected void onPostExecute(Boolean result) {

			if (result == true) {

				GestionDesFichiers.log("code_attribut ", code_attribut,
						"Log_SmartRobot");

				if (code_attribut.equals(Integer.toString(1))) {

					model.StartAct("EXTRA_SESSION_ID", vemail.getText()
									.toString() + ";" + vpassword.getText().toString(),
							Authentification.class, modelCrea.getActivity());

				}

				else if (code_attribut.equals(Integer.toString(-1))) {

					verror.setText(JsonParser.getMessageCrea(Json_crea));

				} else {

					verror.setText(Json_crea);
				}

			}

			else {

				verror.setText(modelCrea.getContext().getResources()
						.getString(R.string.error_calling_ws_msg));

			}

			super.onPostExecute(result);
		}

		@Override
		protected Boolean doInBackground(String... params) {

			return model.iSCodeAttribut(code_attribut, progress);

		}
	}

	/**
	 * Gestion du clic sur une date dans le calendrier
	 *
	 */

	public DatePickerDialog.OnDateSetListener pDateSetListener = new DatePickerDialog.OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
							  int dayOfMonth) {
			// TODO Auto-generated method stub

			pYear = year;
			pMonth = monthOfYear;
			pDay = dayOfMonth;
			updateDisplay();
		}
	};

	/**
	 * Affchage de la mise à jour de la date
	 *
	 */
	public void updateDisplay() {

		vpDisplayDate.setText(new StringBuilder()
				// Month is 0 based so add 1
				.append(pDay).append("/").append(pMonth + 1).append("/")
				.append(pYear).append(" "));
	}

	/**
	 * Gestion de l'affichage de la calendrier
	 *
	 */
	public void showDatePicker() {
		// TODO Auto-generated method stub

		DatePickerFragment date = new DatePickerFragment();
		Calendar calender = Calendar.getInstance();
		Bundle args = new Bundle();
		args.putInt("year", calender.get(Calendar.YEAR));
		args.putInt("month", calender.get(Calendar.MONTH));
		args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
		date.setArguments(args);
		date.setCallBack(pDateSetListener);
		date.show(modelCrea.getActivity().getSupportFragmentManager(),
				"Date Picker");

	}

}
