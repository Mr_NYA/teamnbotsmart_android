package com.orrobotics.orsmartrobot.view;

import com.orrobotics.orsmartrobot.R;
import com.orrobotics.orsmartrobot.controllerImpl.PassRecoverControllerImpl;
import com.orrobotics.orsmartrobot.model.PassRecoverModel;
import com.orrobotics.orsmartrobot.modelImpl.PassRecoverModelImpl;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 *
 * @author Hajiba IFRAH
 * @version 2.1.0
 * @date 18/05/2015
 *
 */

public class PassRecover extends BaseActivity {
	/**
	 * Le clic sur ce bouton permet d'appeler le web service pour récupérer le
	 * mot de passe
	 */

	Button send;
	/**
	 * Pour saisir l'email
	 */
	AutoCompleteTextView emailForPassword;
	/**
	 * Permet d'afficher les messages d'erreurs
	 */
	TextView error_forgetPassword;
	Activity acti;
	ImageView back;

	PassRecoverView mView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.password_recovery_activity);
		/**
		 * Initialisaion des paramètres
		 *
		 */

		acti = this;
		send = (Button) findViewById(R.id.sendbtn);
		emailForPassword = (AutoCompleteTextView) findViewById(R.id.emaill_);
		error_forgetPassword = (TextView) findViewById(R.id.error_forgetPassword);
		back = (ImageView) findViewById(R.id.fermerPopup);
		/**
		 * Construction du MVC
		 */

		PassRecoverModel model = new PassRecoverModelImpl(
				getApplicationContext(), acti);
		PassRecoverControllerImpl controller = new PassRecoverControllerImpl(
				model);
		mView = new PassRecoverView(controller, model);

		mView.setWidgets(send, emailForPassword, error_forgetPassword,back);

		mView.setListeners();

	}

	/**
	 * le clic sur le bouton retour de l'appariel
	 *
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intent = new Intent(PassRecover.this, Authentification.class);
			startActivity(intent);
			finish();

			return true;
		}
		return false;
	}

}
