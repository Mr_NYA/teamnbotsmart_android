package com.orrobotics.orsmartrobot.view;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.media.AudioManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.orrobotics.linphone.LinphoneRegistration;
import com.orrobotics.orsmartrobot.R;
import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.controller.SettingListController;
import com.orrobotics.orsmartrobot.model.SettingListModel;
import com.orrobotics.orsmartrobot.service.ServiceRobot;
import com.orrobotics.orsmartrobot.ui.ToggleSwitch;
import com.orrobotics.orsmartrobot.ui.BaseToggleSwitch.OnToggleSwitchChangeListener;
import com.orrobotics.orsmartrobot.util.RemoteControlUtil;
import android.text.TextWatcher;
import android.text.Editable;
/**
 *
 * @author Manar Boukhari
 *
 */
@SuppressLint({ "NewApi", "ClickableViewAccessibility", "RtlHardcoded" })
public class SettingListActivityView implements OnItemSelectedListener{

    SettingListController vController;
    SettingListModel vModel;
    SmartRobotApplication application;
    ImageButton back;
    ToggleSwitch enablevideo;
    ToggleSwitch enableMic;
    ToggleSwitch enableSpeaker;
    ToggleSwitch showBatterie;
    ToggleSwitch editTtsLang;
    ToggleSwitch editSpeechRate;
    ToggleSwitch showRobotApp;
    Spinner videoSizes;
    Spinner batterieValues;

    SeekBar soundMediaBar;
    SeekBar soundSystemBar;
    SeekBar soundNotifBar;
    ToggleSwitch echoConcelation;
    TextView speedLineaire;
    TextView speedLaterale;
    SeekBar speedbarLineaire;
    SeekBar speedbarLaterale;
    private AudioManager myAudioManager;
    int selection = 0;
    TextView robotId;
    ToggleSwitch editIsPermanance;

    EditText edit_delai_declench;



    TextView email;
    TextView nom;
    private ImageButton deconexion;
    private ImageButton robotSwitch;
    private Spinner appLangages;
    private Spinner ttsLangages;
    private Spinner ttsRates;
    TextView ttsLangagesText;
    TextView ttsRatesText;
    Boolean flagTts = false;
    int l = 0;
    int j = 0;
    int m = 0;
    String langTTS = "";
    TextView robotName;
    ToggleSwitch editIsCloudSpeech;

    public SettingListActivityView(SettingListController controller,
                                   SettingListModel model) {
        vController = controller;
        vModel = model;

    }
    /**
     * Récupèrer et affecter les paramétres initialiser dans la classe
     * SettingListActivity
     *
     * @param editIsPermanance
     */
    public void setWidget(ImageButton back, ToggleSwitch enableVideo,
                          ToggleSwitch enableMic, ToggleSwitch enableSpeaker,
                          Spinner videoSizes, Spinner batterieValues, SeekBar soundMediaBar,
                          SeekBar soundSystemBar, SeekBar soundNotifBar,
                          ToggleSwitch echoConcelation, TextView speedLineaire,
                          TextView speedLaterale, SeekBar speedbarLineaire,
                          SeekBar speedbarLaterale, ToggleSwitch showBatterie,
                          ToggleSwitch editTtsLang,
                          ToggleSwitch editSpeechRate, ToggleSwitch showRobotApp,
                          TextView robotId,TextView robotName,
                          EditText edit_delai_declench,
                          ToggleSwitch editIsPermanance, TextView email, TextView nom, ImageButton deconexion,
                          ImageButton robotSwitch, Spinner appLangages, Spinner ttsLangages,
                          Spinner ttsRates, TextView ttsLangagesText,
                          TextView ttsRatesText,ToggleSwitch editIsCloudSpeech) {
        // TODO Auto-generated method stub
        Log.v("", "----SetWidget------");
        this.back = back;
        this.enablevideo = enableVideo;
        this.videoSizes = videoSizes;
        this.batterieValues = batterieValues;
        this.enableMic = enableMic;
        this.enableSpeaker = enableSpeaker;
        this.showBatterie = showBatterie;
        this.showRobotApp = showRobotApp;
        this.editTtsLang = editTtsLang;
        this.editSpeechRate = editSpeechRate;
        this.soundMediaBar = soundMediaBar;
        this.soundSystemBar = soundSystemBar;
        this.soundNotifBar = soundNotifBar;
        this.echoConcelation = echoConcelation;
        this.speedLineaire = speedLineaire;
        this.speedLaterale = speedLaterale;
        this.speedbarLineaire = speedbarLineaire;
        this.speedbarLaterale = speedbarLaterale;
        this.robotId = robotId;
        this.robotName = robotName;
        this.editIsPermanance = editIsPermanance;
        this.edit_delai_declench = edit_delai_declench;

        this.email = email;
        this.nom = nom;
        this.deconexion = deconexion;
        this.robotSwitch = robotSwitch;
        this.appLangages = appLangages;
        this.ttsLangages = ttsLangages;
        this.ttsRates = ttsRates;
        this.ttsLangagesText = ttsLangagesText;
        this.ttsRatesText =ttsRatesText;
        this.editIsCloudSpeech = editIsCloudSpeech;

        application = (SmartRobotApplication) vModel.getContext();

        init();

    }

    private void init() {
        // TODO Auto-generated method stub


        /**
         * Remplir les champs email et nom
         *
         */

        email.setText(application.getParam(RemoteControlUtil.EMAIL));
        nom.setText(application.getParam(RemoteControlUtil.NOM) + " "
                + application.getParam(RemoteControlUtil.PRENOM));

        if (!application.getVocalFrqDeclench().equals("")) {
            edit_delai_declench.setText(application.getVocalFrqDeclench());

        }
        robotId.setText(application.getDeviceSerie());
        robotName.setText(application.getRobotName());
        if (application.isMicMute())
            enableMic.setCheckedTogglePosition(1);
        else
            enableMic.setCheckedTogglePosition(0);

        String speaker_enable = application.getSpeakerState();

        if (speaker_enable.equals("false") || speaker_enable.equals("")) {
            enableSpeaker.setCheckedTogglePosition(1);

        } else
            enableSpeaker.setCheckedTogglePosition(0);

        if(SmartRobotApplication.call.equals("linphone")) {

            if (application.getCamState().equals("off"))
                enablevideo.setCheckedTogglePosition(1);
            else
                enablevideo.setCheckedTogglePosition(0);


            ArrayAdapter<String> dataAdapter_videoSizes = new ArrayAdapter<String>(
                    vModel.getContext(), R.layout.list_item_setting_spinner,
                    vModel.getListVideoSize());

            dataAdapter_videoSizes
                    .setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
            videoSizes.setAdapter(dataAdapter_videoSizes);

            videoSizes
                    .setSelection(Integer.parseInt(application.getVideoQuality()));
            videoSizes.setOnItemSelectedListener(this);
        }

        List<String> list_batterie_delay = Arrays.asList(vModel.getContext()
                .getResources().getStringArray(R.array.battrie_delay));
        ArrayAdapter<String> dataAdapter_battrieValues = new ArrayAdapter<String>(
                vModel.getContext(), R.layout.list_item_setting_spinner,
                list_batterie_delay);
        dataAdapter_battrieValues
                .setDropDownViewResource(R.layout.simple_spinner_dropdown_item);

        batterieValues.setAdapter(dataAdapter_battrieValues);
        batterieValues.setSelection((application.getbatterieSelection(application.getBatteryDelay())));
        batterieValues.setOnItemSelectedListener(this);

        myAudioManager = (AudioManager) application
                .getSystemService(Context.AUDIO_SERVICE);

        soundMediaBar.setMax(myAudioManager
                .getStreamMaxVolume(AudioManager.STREAM_MUSIC));

        soundMediaBar.setProgress(myAudioManager
                .getStreamVolume(AudioManager.STREAM_MUSIC));

        soundSystemBar.setMax(myAudioManager
                .getStreamMaxVolume(AudioManager.STREAM_ALARM));
        soundSystemBar.setProgress(myAudioManager
                .getStreamVolume(AudioManager.STREAM_ALARM));

        soundNotifBar.setMax(myAudioManager
                .getStreamMaxVolume(AudioManager.STREAM_NOTIFICATION));
        soundNotifBar.setProgress(myAudioManager
                .getStreamVolume(AudioManager.STREAM_NOTIFICATION));

        String speed_lineaire = application.getSpeedLinear();
        if (speed_lineaire.equals("lineaire") || speed_lineaire.equals("")) {
            speedLineaire.setText(Integer.toString(1));
            speedbarLineaire.setProgress(0);
        } else {
            speedLineaire.setText(speed_lineaire);
            speedbarLineaire.setProgress(Integer.parseInt(speed_lineaire) - 1);
        }

        String speed_lateral = application.getSpeedLateral();
        if (speed_lateral.equals("laterale") || speed_lateral.equals("")) {
            speedLaterale.setText(Integer.toString(1));
            speedbarLaterale.setProgress(0);
        } else {
            speedLaterale.setText(speed_lateral);
            speedbarLaterale.setProgress(Integer.parseInt(speed_lateral) - 1);
        }


        if (application.getShowBatterie().equals("true"))
            showBatterie.setCheckedTogglePosition(0);

        else
            showBatterie.setCheckedTogglePosition(1);
        Log.i("mbu","---application.getEditTtsLang="+application.getEditTtsLang());
        if (application.getEditTtsLang().equals("true"))
            editTtsLang.setCheckedTogglePosition(0);
        else
            editTtsLang.setCheckedTogglePosition(1);

        if (application.getEditTtsSpeed().equals("true"))
            editSpeechRate.setCheckedTogglePosition(0);
        else
            editSpeechRate.setCheckedTogglePosition(1);

        if (application.getRobotType() == 1) {
            if (application.getShowRobotApp().equals("true"))
                showRobotApp.setCheckedTogglePosition(0);
            else
                showRobotApp.setCheckedTogglePosition(1);
        }

        String data1 = application.getIsPermanant();
        if (!data1.equals("")) {
            if (data1.equals("1"))
                editIsPermanance.setCheckedTogglePosition(0);
            else
                editIsPermanance.setCheckedTogglePosition(1);

        }

        String valSpeech = application.getIsCloudSpeech();
        if (!valSpeech.equals("")) {
            if (valSpeech.equals("1"))
                editIsCloudSpeech.setCheckedTogglePosition(0);
            else
                editIsCloudSpeech.setCheckedTogglePosition(1);

        }

        ArrayAdapter<String> dataAdapter_appLang = new ArrayAdapter<String>(
                vModel.getContext(), R.layout.list_item_setting_spinner,
                vModel.getListAppLangages());

        dataAdapter_appLang
                .setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        appLangages.setAdapter(dataAdapter_appLang);

        appLangages.setOnItemSelectedListener(this);

        if (application.getLangApp()
                .equals("en"))
            appLangages.setSelection(1);
        else
            appLangages.setSelection(0);

        ArrayAdapter<String> dataAdapter_speed = new ArrayAdapter<String>(
                vModel.getContext(), R.layout.list_item_setting_spinner,
                vModel.getListLanguageSpeed());

        dataAdapter_speed
                .setDropDownViewResource(R.layout.simple_spinner_dropdown_item);

        ttsRates.setAdapter(dataAdapter_speed);

        if (application.getEditTtsSpeed().equals("false")) {
            View parent = (View) ttsRates.getParent();
            parent.setVisibility(View.INVISIBLE);
            ttsRatesText.setVisibility(View.INVISIBLE);
        } else {
            View parent = (View) ttsRates.getParent();
            parent.setVisibility(View.VISIBLE);
            ttsRatesText.setVisibility(View.VISIBLE);
            String pos = application.getSpeedTTS();
            if (!pos.equals(""))
                ttsRates.setSelection(Integer.parseInt(application
                        .getSpeedTTS()));
            else
                ttsRates.setSelection(2);
        }

        ttsRates.setOnItemSelectedListener(this);

        List<String> listLang = new ArrayList<String>();
        HashMap<String, String> mapLangues = vModel.getListLanguage();
        Log.i("", "----size= " + vModel.getListLanguage().size());
        for (Map.Entry<String, String> entry : mapLangues.entrySet()) {
            listLang.add(entry.getValue());
            if (entry.getKey().equals(application.getLangageTTS()))
                vModel.setLanguageSelection(entry.getValue());

        }

        ArrayAdapter<String> dataAdapter_ttsLangues = new ArrayAdapter<String>(
                vModel.getContext(), R.layout.list_item_setting_spinner, listLang);

        dataAdapter_ttsLangues
                .setDropDownViewResource(R.layout.simple_spinner_dropdown_item);

        ttsLangages.setAdapter(dataAdapter_ttsLangues);

        if (application.getEditTtsLang().equals("false")) {
            View parent = (View) ttsLangages.getParent();
            parent.setVisibility(View.INVISIBLE);
            ttsLangagesText.setVisibility(View.INVISIBLE);
        } else {
            View parent = (View) ttsLangages.getParent();
            parent.setVisibility(View.VISIBLE);
            ttsLangagesText.setVisibility(View.VISIBLE);
            ttsLangages.setSelection(vController.setLanguage(vController
                    .getLanguage()));

        }

        ttsLangages.setOnItemSelectedListener(this);

        addListenerOnSpinnerItemSelection();





        if (application.getChatEnabled() == true) {
            ServiceRobot.sendMessage(application.getPseudoOpenFireSender(),
                    "tts" + vController.getLanguage());
        }
    }

    /**
     * Gestion des évenements de clic
     */
    @SuppressLint("RtlHardcoded")
    public void setListners() {
        // TODO Auto-generated method stub
        edit_delai_declench.addTextChangedListener(new TextWatcher() {


            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                if (edit_delai_declench.getText().toString().equals("")) {
                    application.getActivity().runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            Toast.makeText(application.getApplicationContext(),
                                    R.string.champsVide, Toast.LENGTH_SHORT)
                                    .show();

                        }
                    });
                } else {

                    if (!edit_delai_declench.getText().toString().equals("")) {

                        application.setRobotEditor(
                                RemoteControlUtil.VOCAL_FREQ_DECLENCH,
                                edit_delai_declench.getText().toString());

                    }

                }
            }
        });

        deconexion.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                application.getActivity().runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        AlertDialog.Builder builder = new AlertDialog.Builder(application.getActivity());
                        builder.setMessage(R.string.deconexRequest)
                                .setPositiveButton(R.string.yes,
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog,
                                                                int id) {

                                                application.clear();
                                                application.shutDownTTS();
                                                application.notifyObservers("deconnexion");
                                                application.getActivity().finish();
                                                application.StartAct(null, null, Authentification.class,
                                                        vModel.getActivity());

                                            }
                                        })
                                .setNegativeButton(R.string.no,
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog,
                                                                int id) {
                                            }
                                        });

                        builder.create();
                        builder.show();
                    }
                });
            }

        });

        robotSwitch.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                application.notifyObservers("deconnexion");
                application.StartAct(null, null, ListRobot.class,
                        vModel.getActivity());
                application.getActivity().finish();

            }

        });


        back.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                application.notifyObservers("restartVoice");
                application.getActivity().finish();

                application.StartAct(null, null, ControlRobot.class,
                        application.getApplicationContext());



            }
        });
        speedbarLineaire
                .setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

                    int speed;

                    @Override
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progress, boolean fromUser) {
                        if (fromUser) {
                            speed = progress + 1;
                            speedLineaire.setText("" + speed);

                        }
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        application.setRobotEditor(
                                RemoteControlUtil.SPEED_LINEAIRE,
                                Integer.toString(speed));
                    }

                });

        speedbarLaterale
                .setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    int speed;

                    @Override
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progress, boolean fromUser) {
                        // TODO Auto-generated method stub

                        if (fromUser) {
                            speed = progress + 1;
                            speedLaterale.setText("" + speed);
                        }
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        // TODO Auto-generated method stub
                        application.setRobotEditor(
                                RemoteControlUtil.SPEED_LATERALE,
                                Integer.toString(speed));

                    }

                });

        showBatterie
                .setOnToggleSwitchChangeListener(new OnToggleSwitchChangeListener() {

                    @Override
                    public void onToggleSwitchChangeListener(int position,
                                                             boolean isChecked) {
                        // TODO Auto-generated method stub
                        if (position == 0) {
                            application.setRobotEditor(RemoteControlUtil.SHOW_BATTERIE,"true");
                            application.notifyObservers("showBatt");
                        } else {
                            application.setRobotEditor(RemoteControlUtil.SHOW_BATTERIE,"false");
                            application.notifyObservers("hideBatt");
                        }
                    }
                });

        editTtsLang
                .setOnToggleSwitchChangeListener(new OnToggleSwitchChangeListener() {

                    @Override
                    public void onToggleSwitchChangeListener(int position,
                                                             boolean isChecked) {
                        // TODO Auto-generated method stub
                        View parent = (View) ttsLangages.getParent();
                        if (position == 0) {
                            application.setRobotEditor(RemoteControlUtil.EDIT_TTS_LANGUE,"true");
                            parent.setVisibility(View.VISIBLE);
                            ttsLangagesText.setVisibility(View.VISIBLE);
                        } else {
                            application.setRobotEditor(RemoteControlUtil.EDIT_TTS_LANGUE,"false");
                            parent.setVisibility(View.INVISIBLE);
                            ttsLangagesText.setVisibility(View.INVISIBLE);
                        }
                    }
                });
        editIsPermanance
                .setOnToggleSwitchChangeListener(new OnToggleSwitchChangeListener() {

                    @Override
                    public void onToggleSwitchChangeListener(int position,
                                                             boolean isChecked) {
                        // TODO Auto-generated method stub
                        if (position == 0) {
                            application.setRobotEditor(
                                    RemoteControlUtil.ISPERMANCE, "1");
                        } else {
                            application.setRobotEditor(
                                    RemoteControlUtil.ISPERMANCE, "0");
                            application.setVolume();
                        }
                    }
                });
        editIsCloudSpeech
                .setOnToggleSwitchChangeListener(new OnToggleSwitchChangeListener() {

                    @Override
                    public void onToggleSwitchChangeListener(int position,
                                                             boolean isChecked) {
                        // TODO Auto-generated method stub
                        if (position == 0) {
                            application.setRobotEditor(
                                    RemoteControlUtil.ISCLOUDSPEECH, "1");
                        } else {
                            application.setRobotEditor(
                                    RemoteControlUtil.ISCLOUDSPEECH, "0");
                        }
                    }
                });
        editSpeechRate
                .setOnToggleSwitchChangeListener(new OnToggleSwitchChangeListener() {

                    @Override
                    public void onToggleSwitchChangeListener(int position,
                                                             boolean isChecked) {
                        // TODO Auto-generated method stub
                        View parent = (View) ttsRates.getParent();
                        if (position == 0) {
                            application.setRobotEditor(RemoteControlUtil.EDIT_TTS_SPEED,"true");
                            parent.setVisibility(View.VISIBLE);
                            ttsRatesText.setVisibility(View.VISIBLE);
                        } else {
                            application.setRobotEditor(RemoteControlUtil.EDIT_TTS_SPEED,"false");
                            parent.setVisibility(View.INVISIBLE);
                            ttsRatesText.setVisibility(View.INVISIBLE);
                        }
                    }
                });
        showRobotApp
                .setOnToggleSwitchChangeListener(new OnToggleSwitchChangeListener() {

                    @Override
                    public void onToggleSwitchChangeListener(int position,
                                                             boolean isChecked) {
                        // TODO Auto-generated method stub
                        if (position == 0) {
                            application.setRobotEditor(RemoteControlUtil.SHOW_ROBOT_APP,"true");
                        } else {
                            application.setRobotEditor(RemoteControlUtil.SHOW_ROBOT_APP,"false");
                        }
                    }
                });

        enableSpeaker
                .setOnToggleSwitchChangeListener(new OnToggleSwitchChangeListener() {

                    @Override
                    public void onToggleSwitchChangeListener(int position,
                                                             boolean isChecked) {
                        // TODO Auto-generated method stub
                        if (position == 1) {
                            Log.v("", "isChecked = false");
                            application.enableSpeaker(false);
                            application.setRobotEditor(
                                    RemoteControlUtil.SPEAKER, "false");
                            if (application.getChatEnabled())
                                ServiceRobot.sendMessage(
                                        application.getPseudoOpenFireSender(),
                                        "speakerOff");
                        } else {
                            application.setRobotEditor(
                                    RemoteControlUtil.SPEAKER, "true");
                            application.enableSpeaker(true);
                            if (application.getChatEnabled())
                                ServiceRobot.sendMessage(
                                        application.getPseudoOpenFireSender(),
                                        "speakerOn");
                        }
                    }
                });
        enablevideo
                .setOnToggleSwitchChangeListener(new OnToggleSwitchChangeListener() {

                    @Override
                    public void onToggleSwitchChangeListener(int position,
                                                             boolean isChecked) {
                        // TODO Auto-generated method stub
                        if (LinphoneRegistration.lc != null)

                            if (position == 1) {

                                application.enableOrDisableCamera(false);
                                if (application.getChatEnabled())
                                    ServiceRobot.sendMessage(application
                                                    .getPseudoOpenFireSender(),
                                            "camBotOff");
                            } else if (position == 0) {

                                application.enableOrDisableCamera(true);
                                if (application.getChatEnabled())
                                    ServiceRobot.sendMessage(application
                                                    .getPseudoOpenFireSender(),
                                            "camBotOn");
                            }

                    }

                });
        echoConcelation
                .setOnToggleSwitchChangeListener(new OnToggleSwitchChangeListener() {

                    @Override
                    public void onToggleSwitchChangeListener(int position,
                                                             boolean isChecked) {
                        // TODO Auto-generated method stub
                        if (position == 1) {
                            application.echoCancellation("" + false);
                            if (application.getChatEnabled())
                                ServiceRobot.sendMessage(
                                        application.getPseudoOpenFireSender(),
                                        "botEcho;false");
                        } else {
                            if (application.getChatEnabled())
                                ServiceRobot.sendMessage(
                                        application.getPseudoOpenFireSender(),
                                        "botEcho;true");
                            application.echoCancellation("" + true);

                        }

                    }
                });

        enableMic
                .setOnToggleSwitchChangeListener(new OnToggleSwitchChangeListener() {

                    @Override
                    public void onToggleSwitchChangeListener(int position,
                                                             boolean isChecked) {
                        // TODO Auto-generated method stub
                        if (position == 1) {
                            application.enableOrDisableMic(true);
                            application.setRobotEditor(
                                    RemoteControlUtil.MIC, "false");
                            if (application.getChatEnabled()) {
                                if(SmartRobotApplication.call.equals("webrtc")){
                                    application.notifyObservers("botMicOff");

                                }else {
                                    ServiceRobot.sendMessage(application
                                                    .getPseudoOpenFireSender(),
                                            "botMicOff");
                                }
                            }
                        } else {
                            application.setRobotEditor(
                                    RemoteControlUtil.MIC, "true");
                            application.enableOrDisableMic(false);
                            if (application.getChatEnabled())
                                if(SmartRobotApplication.call.equals("webrtc")){
                                    application.notifyObservers("botMicOn");

                                }else {
                                    ServiceRobot.sendMessage(application
                                                    .getPseudoOpenFireSender(),
                                            "botMicOn");
                                }

                        }


                    }
                });

        soundMediaBar
                .setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

                    @Override
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progress, boolean fromUser) {
                        // TODO Auto-generated method stub
                        myAudioManager.setStreamVolume(
                                AudioManager.STREAM_MUSIC, progress,
                                AudioManager.STREAM_MUSIC);
                        application.setEditor(RemoteControlUtil.VOLUME,
                                Integer.toString(progress));
                        soundMediaBar.setProgress(progress);
                        if (application.getChatEnabled())
                            ServiceRobot.sendMessage(
                                    application.getPseudoOpenFireSender(),
                                    "botMedia;" + progress + ";"
                                            + soundMediaBar.getMax());
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        // TODO Auto-generated method stub

                    }

                });

        soundSystemBar
                .setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

                    @Override
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progress, boolean fromUser) {
                        // TODO Auto-generated method stub
                        myAudioManager.setStreamVolume(
                                AudioManager.STREAM_ALARM, progress,
                                AudioManager.STREAM_ALARM);

                        soundSystemBar.setProgress(progress);
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        // TODO Auto-generated method stub

                    }

                });

        soundNotifBar
                .setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

                    @Override
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progress, boolean fromUser) {
                        // TODO Auto-generated method stub
                        myAudioManager.setStreamVolume(
                                AudioManager.STREAM_NOTIFICATION, progress,
                                AudioManager.STREAM_NOTIFICATION);
                        soundNotifBar.setProgress(progress);
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        // TODO Auto-generated method stub

                    }

                });

    }

    @SuppressLint("DefaultLocale")
    public void addListenerOnSpinnerItemSelection() {

        Log.i("hhh", "----addListenerOnSpinnerItemSelection();---");
        appLangages
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent,
                                               View view, int pos, long id) {
                        selection = pos;

                        if (m != 0 ) {
                            if (pos == 0) {

                                application
                                        .setRobotEditor(
                                                RemoteControlUtil.APPLICATION_LANGAGE,
                                                "fr");
                            } else {
                                application
                                        .setRobotEditor(
                                                RemoteControlUtil.APPLICATION_LANGAGE,
                                                "en");
                            }
                            application.setLocal();
                            refresh();
                        }
                        m = m + 1;
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        // TODO Auto-generated method stub

                    }

                });

        ttsLangages
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent,
                                               View view, int pos, long id) {
                        Log.i("", "-----spinner lang selected----");

                        langTTS = parent.getItemAtPosition(pos).toString();
                        Log.i("", "-----spinner lang=" + langTTS);
                        vModel.setLanguageSelection(langTTS);

                        if (l != 0 && application.getEditTtsLang().equals("true")) {

                            application.setRobotEditor(
                                    RemoteControlUtil.TTS_LANGUAGE,
                                    vController.getLanguageCode(langTTS));
                            application.setRobotEditor(
                                    RemoteControlUtil.TTS_LANG_SELECTION,
                                    langTTS);
                            ttsLangages.setSelection(pos);
                            vController.setLanguage(langTTS);
                            vController.SendTts();
                        }

                        l = l + 1;

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        // TODO Auto-generated method stub

                    }

                });

        ttsRates.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int pos, long id) {
                Log.i("", "-----spinner speed selected----");
                vModel.setLanguageSpeedSelection(pos);

                if (flagTts == false) {
                    if (j != 0 && application.getEditTtsSpeed().equals("true")) {
                        application.setRobotEditor(RemoteControlUtil.TTS_SPEED_POS,
                                Integer.toString(pos));
                        vController.SendTts();
                    }
                }
                flagTts = false;
                j = j + 1;

            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        videoSizes
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent,
                                               View view, int pos, long id) {
                        selection = pos;

                        if (pos != 0) {
                            selection = pos;
                            application.changeVideoSize(selection);
                            videoSizes.setSelection(selection);
                            application.setRobotEditor(
                                    RemoteControlUtil.VIDEO_QUALITY, ""
                                            + selection);
                            if (application.getChatEnabled())
                                ServiceRobot.sendMessage(
                                        application.getPseudoOpenFireSender(),
                                        "botVideoSize;" + selection);
                        }

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        // TODO Auto-generated method stub

                    }

                });
        batterieValues
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView,
                                               View view, int i, long l) {
                        String batterieDelay = null;

                        switch (i) {
                            case 0:
                                batterieDelay = "10 s";
                                break;
                            case 1:
                                batterieDelay = "10 s";
                                break;
                            case 2:
                                batterieDelay = "50 s";
                                break;
                            case 3:
                                batterieDelay = "1 min";
                                break;
                            case 4:
                                batterieDelay = "2 min";
                                break;

                            default:
                                break;
                        }
                        batterieValues.setSelection(i);
                        application.setEditor(RemoteControlUtil.BATTERIE_DELAY,batterieDelay);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {
                        return;
                    }
                });

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position,
                               long id) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        // TODO Auto-generated method stub

    }


    public void refresh() {
        // TODO Auto-generated method stub
        Log.i("hhh","----refresh-----");
        application.getActivity().finish();
        application.getActivity().getIntent().putExtra("rechargement", "true");
        application.getActivity().startActivity(
                application.getActivity().getIntent());
    }




}
