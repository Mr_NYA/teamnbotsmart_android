package com.orrobotics.orsmartrobot.view;

import android.content.Intent;
import android.os.AsyncTask;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.orrobotics.orsmartrobot.R;
import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.controller.PassRecoverControler;
import com.orrobotics.orsmartrobot.model.PassRecoverModel;
import com.orrobotics.orsmartrobot.util.GestionDesFichiers;
import com.orrobotics.webservice.WebService;
import com.orrobotics.webservice.json.JsonParser;
import com.orrobotics.webservice.url.urlParser;

public class PassRecoverView {

	PassRecoverControler mController;
	PassRecoverModel vModel;
	SmartRobotApplication application;

	Button send;
	AutoCompleteTextView emailForPassword;
	TextView error_forgetPassword;

	String emailTxt;
	String Json_password = null;
	String code_attribut_password = null;
	ImageView back;

	public PassRecoverView(PassRecoverControler controller,
						   PassRecoverModel model) {

		vModel = model;
		mController = controller;
		application = (SmartRobotApplication) vModel.getContext();

	}

	/**
	 * Récupérer et affecter les paramètres initialiser dans la classe
	 * PassRecover
	 *
	 */
	public void setWidgets(Button send, AutoCompleteTextView emailForPassword,
						   TextView error_forgetPassword, ImageView back) {
		// TODO Auto-generated method stub

		this.send = send;
		this.emailForPassword = emailForPassword;
		this.error_forgetPassword = error_forgetPassword;
		this.back = back;
	}

	/**
	 * Gestion des événements
	 */
	public void setListeners() {
		// TODO Auto-generated method stub
		back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(vModel.getContext(),
						Authentification.class);
				vModel.getActivity().startActivity(intent);
				vModel.getActivity().finish();

			}
		});
		send.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				emailTxt = emailForPassword.getText().toString();
				if (emailTxt.trim().length() == 0) {
					error_forgetPassword
							.setText(vModel.getContext().getResources()
									.getString(R.string.insert_mail_msg));
				} else {
					Boolean isValid = android.util.Patterns.EMAIL_ADDRESS
							.matcher(emailForPassword.getText().toString())
							.matches();
					if (isValid == false) {
						error_forgetPassword.setText(vModel.getContext()
								.getResources()
								.getString(R.string.invalid_mail_msg));
					} else {

						String url = SmartRobotApplication.getParamFromFile(
								"param", "configuration.properties",null);
						String serverURL = urlParser.getURL_Password(url,
								emailTxt);
						GestionDesFichiers.log(
								"url pour l'appel du web service passforg", ""
										+ serverURL, "Log_SmartRobot");
						//Json_password = WebService.connection(serverURL);
						WebService ws = new WebService();
						WebService.HttpRequest callbackservice = ws.new HttpRequest() {
							@Override
							public void receiveData(Object object) {
								Json_password = (String)object;
								GestionDesFichiers.log(
										"retourl du web service passforg", ""
												+ Json_password, "Log_SmartRobot");
								if (Json_password != null) {
									if (Json_password.contains("</html>")) {
										error_forgetPassword.setText(vModel
												.getContext()
												.getResources()
												.getString(
														R.string.error_calling_ws_msg));
									} else {
										new TaskPassword()
												.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
										code_attribut_password = JsonParser
												.getCode_Crea(Json_password);
									}
								} else {
									error_forgetPassword.setText(vModel.getContext()
											.getResources()
											.getString(R.string.error_calling_ws_msg));
								}
							}
						};
						callbackservice.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, serverURL);

					}
				}

			}
		});
	}

	/**
	 * Ce task traite la réponse du web servcie
	 *
	 */
	class TaskPassword extends AsyncTask<String, Integer, Boolean> {
		int progress = 0;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected void onPostExecute(Boolean result) {
			if (result == true) {
				GestionDesFichiers.log("code_attribut_password ",
						code_attribut_password, "Log_SmartRobot");

				if (code_attribut_password.equals(Integer.toString(1))) {
					error_forgetPassword.setText(vModel.getContext()
							.getResources()
							.getString(R.string.pass_send_tomail_msg));
				} else if (code_attribut_password.equals(Integer.toString(-1))) {
					error_forgetPassword.setText(JsonParser
							.getMessageForPassword(Json_password));
				} else {
					error_forgetPassword.setText(Json_password);
				}
			} else {
				error_forgetPassword.setText(vModel.getContext().getResources()
						.getString(R.string.error_calling_ws_msg));
			}

			super.onPostExecute(result);
		}

		@Override
		protected Boolean doInBackground(String... params) {
			return application.iSCodeAttribut(code_attribut_password, progress);
		}
	}

}
