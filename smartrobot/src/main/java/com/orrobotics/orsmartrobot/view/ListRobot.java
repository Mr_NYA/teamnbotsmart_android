package com.orrobotics.orsmartrobot.view;

import com.orrobotics.orsmartrobot.R;
import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.controller.ListRobotController;
import com.orrobotics.orsmartrobot.controllerImpl.ListRobotControlerImpl;
import com.orrobotics.orsmartrobot.model.ListRobotModel;
import com.orrobotics.orsmartrobot.modelImpl.ListRobotModelImpl;
import com.orrobotics.orsmartrobot.ui.CircularProgress;
import com.orrobotics.orsmartrobot.ui.DrawerArrowDrawable;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

/**
 *
 * @author Hajiba IFRAH
 * @version 2.1.0
 * @date 18/05/2015
 *
 */
@SuppressLint("NewApi")
public class ListRobot extends BaseActivity {

	SmartRobotApplication application;

	Activity acti;
	/**
	 * email de l'utilisateur
	 */
	TextView email;
	/**
	 * nom de l'utilisateur
	 */
	TextView nom;

	/**
	 * La liste des robots
	 */
	ListView listView;
	/**
	 * des paramètres utiliser l'affichage de menu
	 */
	DrawerArrowDrawable drawerArrowDrawable;
	float offset;
	boolean flipped;

	CircularProgress progress;
	ListRobotView mView;
	Bitmap open;
	ImageView app_name1;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.listrobot);
		overridePendingTransition(R.anim.slide_in_from_bottom,
				R.anim.slide_out_to_bottom);

		acti = this;

		/**
		 * la construction du MVC
		 */
		ListRobotModel model = new ListRobotModelImpl(getApplicationContext(),
				acti);
		ListRobotController controller = new ListRobotControlerImpl(model);
		mView = new ListRobotView(controller, model);

		application = (SmartRobotApplication) this.getApplicationContext();
		application.stricMode();
		application.setActivity(acti);

		/**
		 * Initialisation des paramètres
		 */
		email = (TextView) findViewById(R.id.email);
		nom = (TextView) findViewById(R.id.nom);

		listView = (ListView) findViewById(R.id.list_view);
		app_name1 = (ImageView) findViewById(R.id.imageView2);

		/**
		 * Géstion du logo
		 */
		if (modelApp.isLogoOptionEnabled()) {
			open = application.getLogoFromExternalStorage();
		}
		if (open != null)
			app_name1.setImageBitmap(Bitmap.createScaledBitmap(open, 128, 128,
					false));

		/**
		 * Gérer le menu
		 */
		final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		final ImageView imageView = (ImageView) findViewById(R.id.drawer_indicator);
		final Resources resources = this.getResources();
		drawer.setScrimColor(getResources().getColor(R.color.back));

		drawerArrowDrawable = new DrawerArrowDrawable(resources);
		drawerArrowDrawable.setStrokeColor(resources.getColor(R.color.white));
		imageView.setImageDrawable(drawerArrowDrawable);

		drawer.setDrawerListener(new DrawerLayout.SimpleDrawerListener() {
			@Override
			public void onDrawerSlide(View drawerView, float slideOffset) {
				offset = slideOffset;

				// Sometimes slideOffset ends up so close to but not quite 1 or
				// 0.
				if (slideOffset >= .995) {
					flipped = true;
					drawerArrowDrawable.setFlip(flipped);
				} else if (slideOffset <= .005) {
					flipped = false;
					drawerArrowDrawable.setFlip(flipped);
				}

				drawerArrowDrawable.setParameter(offset);
			}
		});

		imageView.setOnClickListener(new View.OnClickListener() {
			@SuppressLint("RtlHardcoded")
			@Override
			public void onClick(View v) {
				if (drawer.isDrawerVisible(Gravity.LEFT)) {
					drawer.closeDrawer(Gravity.LEFT);
				} else {
					drawer.openDrawer(Gravity.LEFT);
				}
			}
		});

		progress = (CircularProgress) findViewById(R.id.progress);

		/**
		 * Vérification si l'interface de la liste des robots est consulté après
		 * directement l'interface de l'authentification Si l'interface de liste
		 * des robots n'a pas été consulté après l'interface de
		 * l'authentification, on appel le web servie pour actualisé la liste
		 * des robots
		 *
		 */
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			Boolean autoStart = extras.getBoolean("autostart");
			String auth = extras.getString("authentification");
			String refreshxml = extras.getString("refreshxml");
			if (autoStart != null) {
				application.setData(application.getEmail(),
						application.getPasswod(), application.getNom(),
						application.getPrenom(),
						application.getPseudoOpenFire(),
						application.getPasswordOpenFire(),
						application.getPseudoSIP(),
						application.getPasswordSIP());
			}
			if (auth != null) {

				mView.setFlagRefresh(false);
			} else {
				mView.setFlagRefresh(true);
			}
			if (refreshxml != null) {
				application.setSetting(true);
				mView.setFlagRefresh(false);
			} else {
				application.setSetting(false);

			}
		}

		mView.setWidgets(email, nom, listView, progress);
		mView.setListeners();

	}

	/**
	 * Le clic sur le bouton retoure du Smartphone
	 *
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (!SmartRobotApplication.isKindleFire()) {
				Intent intent = new Intent(Intent.ACTION_MAIN);
				intent.addCategory(Intent.CATEGORY_HOME);
				startActivity(intent);
			}
			application.notifyObservers("fermer");

			finish();
			moveTaskToBack(true);
			System.exit(0);
			android.os.Process.killProcess(android.os.Process.myPid());
			return true;
		}

		return false;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mView.destroy();

	}
}
