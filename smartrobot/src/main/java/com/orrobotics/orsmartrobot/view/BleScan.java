package com.orrobotics.orsmartrobot.view;

import com.orrobotics.orsmartrobot.R;
import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.controller.BleScanController;
import com.orrobotics.orsmartrobot.controllerImpl.BleScanControllerImpl;
import com.orrobotics.orsmartrobot.controllerImpl.LeDeviceListAdapter;
import com.orrobotics.orsmartrobot.model.BleScanModel;
import com.orrobotics.orsmartrobot.model.Device;
import com.orrobotics.orsmartrobot.modelImpl.BleScanModelImpl;
import com.orrobotics.orsmartrobot.observer.OrSmarRobotObserver;
import com.orrobotics.orsmartrobot.ui.PlayGifView;
import com.orrobotics.orsmartrobot.util.AdvertizingDataParser;
import com.orrobotics.orsmartrobot.util.GestionDesFichiers;
import com.orrobotics.orsmartrobot.util.RemoteControlUtil;

import android.annotation.SuppressLint;
import android.app.ListActivity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;

import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.view.View.OnClickListener;
import android.widget.ListView;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Set;

/**
 *
 * @author Hajiba IFRAH
 * @version 2.1.0
 * @date 18/05/2015
 *
 */

@SuppressLint("NewApi")
public class BleScan extends ListActivity implements OrSmarRobotObserver {

	SmartRobotApplication application;
	ListActivity acti;
	LeDeviceListAdapter mLeDeviceListAdapter;
	BleScanController controller;
	BleScanModel model;
	Handler mHandler;
	Handler tps;
	ImageButton bluetooth;
	PlayGifView circle;

	TextView text;
	ImageView back;
	private BluetoothAdapter mBTAdapter;
	private Set<BluetoothDevice> mPairedDevices;
	String communicationType;
	String robotBlId;
	String [] robotsBlId;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		application = (SmartRobotApplication) this.getApplicationContext();
		application.stricMode();
		application.onActivityCreateSetTheme(this);
		communicationType = SmartRobotApplication.getParamFromFile(
				"robot.connexion.type", "Configuration.properties", null);
		if(communicationType == null)
			communicationType = "bluetooth";

		robotBlId = SmartRobotApplication.getParamFromFile(
				"robot.bluetooth.id", "Configuration.properties", null);

		if(robotBlId == null)
			robotBlId = "89,32,32,86,97,108,101,116,116,45,100,101,118;76,0,2,21,226,197,109,181,223,251,72,210,176,96,208,245,167,16,150,224,0,0,0,0,197";

		if (robotBlId.contains(";"))
			robotsBlId = robotBlId.split(";");



		// Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.bluetooth);

		acti = this;

		/**
		 * Construction du MVC
		 */
		model = new BleScanModelImpl(getApplicationContext(), acti);
		controller = new BleScanControllerImpl(model);

		mHandler = new Handler();
		mBTAdapter = BluetoothAdapter.getDefaultAdapter();
		if(communicationType.equals("bluetooth") && !mBTAdapter.isEnabled())
			mBTAdapter.enable();
		mLeDeviceListAdapter = new LeDeviceListAdapter(this);
		setListAdapter(mLeDeviceListAdapter);
		application.setActivity(acti);
		application.registerObserver(BleScan.this);
		mHandler = new Handler();
		tps = new Handler();

		bluetooth = (ImageButton) findViewById(R.id.bluetooth_icon);
		// circle = (CircularProgressBlack)
		// findViewById(R.id.circularProgressBlack1);

		circle = (PlayGifView) findViewById(R.id.circularProgressBlack1);

		circle.setImageResource(R.drawable.load);

		text = (TextView) findViewById(R.id.text);
		back = (ImageView) findViewById(R.id.back);
		setListeners();
	}

	public void setListeners() {

		bluetooth.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (!model.getScanning()) {
					mLeDeviceListAdapter.clear();
					application.notifyObservers("robotId");
					scanLeDevice(true);
					circle.setVisibility(View.VISIBLE);
					text.setText(R.string.bleDiscovery);
					tps.postDelayed(run1, 10000);

				} else {
					scanLeDevice(false);
					circle.setVisibility(View.INVISIBLE);
					text.setText(R.string.scan);

				}

			}
		});

		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				scanLeDevice(false);

				mLeDeviceListAdapter.clear();
				application.removeObserver(BleScan.this);
				application.notifyObservers("stopdiscoveringDevcie");
				application.StartAct(null, null, ListRobot.class, acti);
				finish();
			}
		});

	}

	Runnable run1 = new Runnable() {
		@Override
		public void run() {
			circle.setVisibility(View.INVISIBLE);
			text.setText(R.string.scan);

		}
	};

	/**
	 * Gérer le clic sur une adresse MAC
	 *
	 */
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		scanLeDevice(false);

		final Device device = mLeDeviceListAdapter.getDevice(position);
		if (device == null)
			return;
		GestionDesFichiers.log("l'adresse mac séléctionné page de scan",
				device.getNumeroSerie(), "Log_SmartRobot");
		application.setEditor(RemoteControlUtil.EXTRAS_DEVICE_ADDRESS,
				device.getAdresse());

		if (application.isConnectedToInternet())
			controller.updateMacAdresse(device.getAdresse(),
					device.getNumeroSerie(), true);
		else {
			application.setEditor(RemoteControlUtil.NEW_MAC,
					device.getAdresse());
			application.setEditor(RemoteControlUtil.NEW_NUM_SERIE,
					device.getNumeroSerie());
			controller.updateMacInternFile(device.getAdresse(),
					device.getNumeroSerie());
		}
	}

	/**
	 * GérerThread pour arrêter le scan
	 *
	 */
	Runnable run = new Runnable() {
		@Override
		public void run() {
			model.setScanning(false);
			if(communicationType.equals("usb"))
				application.notifyObservers("stopdiscoveringDevcie");
			else
				discover(false);
			invalidateOptionsMenu();
		}
	};

	/**
	 * Permet soit de démarré soit d'arrêter l'opération de scan
	 *
	 */
	public void scanLeDevice(boolean enable) {
		// TODO Auto-generated method stub
		if (enable) {
			/**
			 * Démarrer l'opération de scan pendant 10s
			 *
			 */
			mHandler.postDelayed(run, 10000);
			if(communicationType.equals("usb"))
				application.notifyObservers("discoveringDevice");
			else
				discover(true);

			model.setScanning(true);

		} else {
			model.setScanning(false);
			application.notifyObservers("stopdiscoveringDevcie");
		}
		invalidateOptionsMenu();

	}

	/**
	 * Le clic sur le bouton retour du Smartphone
	 *
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {

			scanLeDevice(false);
			mLeDeviceListAdapter.clear();
			application.removeObserver(BleScan.this);
			application.notifyObservers("stopdiscoveringDevcie");
			application.StartAct(null, null, ListRobot.class, acti);
			finish();

			return true;
		}
		return false;
	}

	/**
	 * Cette méthode est déclenché après la notification de l'observateur
	 *
	 */
	@Override
	public void update(final String message) {
		// TODO Auto-generated method stub
		GestionDesFichiers.log("message reçu par l'observateur ", message,
				"Log_SmartRobot");

		if (message.contains("startActi")) {
			application.StartAct(null, null, ListRobot.class, acti);
			finish();
		} else if (message.contains("adresse")) {

			model.getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					String[] data = message.split(";");
					if(data.length > 1) {
						Device device;
						if(data.length > 2)
							device = new Device(data[1], data[2]);
						else
							device = new Device(data[1], "");
						GestionDesFichiers.log(
								" une adresse mac est détecté, page de scan",
								data[1], "Log_SmartRobot");
						mLeDeviceListAdapter.addDevice(device);
						mLeDeviceListAdapter.notifyDataSetChanged();
					}
				}
			});
		}

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		/**
		 * Arrêter le scan périodique
		 *
		 */
		mHandler.removeCallbacks(run);
		/**
		 * Suppression de l'observateur
		 *
		 */
		application.removeObserver(BleScan.this);
		if (!communicationType.equals("usb"))
			mBTAdapter.stopLeScan(mLeScanCallback);
	}

	private void discover(boolean enableDiscovery){
		if(!mBTAdapter.isEnabled())
			mBTAdapter.enable();
		Log.v("mbu", "--mBTAdapter.isDiscovering() = " + mBTAdapter.isDiscovering());
		if(!enableDiscovery){
			mBTAdapter.stopLeScan(mLeScanCallback);
		}
		else{
			mLeDeviceListAdapter.clear(); // clear items
			listPairedDevices();
			mBTAdapter.startLeScan(mLeScanCallback);

		}
	}

	// Device scan callback.
	private BluetoothAdapter.LeScanCallback mLeScanCallback =
			new BluetoothAdapter.LeScanCallback() {

				@Override
				public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {

					Device dev = new Device(device.getAddress(),device.getName());
					byte manufacturerData[] = SmartRobotApplication.getBleManufacturerData(scanRecord);

					Log.i("mbu", "--device = "+device.getName());
					if(manufacturerData != null) {
						int unsignedManufacturerData[] = new int[manufacturerData.length] ;
						for (int i = 0; i < manufacturerData.length; i++) {
							//Log.i("mbu", "--manufacturerData[] = "+manufacturerData[i]);
							unsignedManufacturerData[i] = manufacturerData[i] & 0xFF;
							Log.i("mbu", "--UnsignedManufacturerData[] = "+unsignedManufacturerData[i]);
						}
						if (robotBlId.contains(";")) {
							for (int i = 0; i < robotsBlId.length; i++) {
								for (int j = 0; j < stringToIntArray(robotsBlId[i]).length; j++) {
									Log.i("mbu", "--String[] = "+stringToIntArray(robotsBlId[i])[j]);
								}
								Log.i("mbu", "--equals 11 = "+Arrays.equals(unsignedManufacturerData,stringToIntArray(robotsBlId[i])));
								if (Arrays.equals(unsignedManufacturerData,stringToIntArray(robotsBlId[i]))) {
									Log.i("mbu", "--equals----");
									mLeDeviceListAdapter.addDevice(dev);
									model.getActivity().runOnUiThread(new Runnable() {
										@Override
										public void run() {
											mLeDeviceListAdapter.notifyDataSetChanged();
										}
									});
								}
							}
						}
						else{
							if (Arrays.equals(unsignedManufacturerData,stringToIntArray(robotBlId))) {
								mLeDeviceListAdapter.addDevice(dev);
								model.getActivity().runOnUiThread(new Runnable() {
									@Override
									public void run() {
										mLeDeviceListAdapter.notifyDataSetChanged();
									}
								});
							}
						}}




					}

			};


	private void listPairedDevices(){
		mPairedDevices = mBTAdapter.getBondedDevices();
		if(mBTAdapter.isEnabled()) {

			for (BluetoothDevice device : mPairedDevices) {
				if (device.getName() != null) {
					if (device.getName().equals(robotBlId)) {
						Device dev = new Device(device.getAddress(), device.getName());
						mLeDeviceListAdapter.addDevice(dev);
					}
				}
			}

		}
	}
	private int[] stringToIntArray(String text){

		String []tab = text.split(",");
		int []tabInt = new int[tab.length];
		for (int i=0;i<tab.length;i++){
			tabInt[i] = Integer.parseInt(tab[i]);
	}

    return tabInt;
}
}
