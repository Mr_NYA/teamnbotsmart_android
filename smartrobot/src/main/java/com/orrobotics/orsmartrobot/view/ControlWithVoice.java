

package com.orrobotics.orsmartrobot.view;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.orrobotics.linphone.LinphoneRegistration;
import com.orrobotics.linphone.VideoCallFragment;
import com.orrobotics.orsmartrobot.R;
import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.controller.ControlRobotController;
import com.orrobotics.orsmartrobot.model.ControlRobotModel;
import com.orrobotics.orsmartrobot.observer.OrSmarRobotObserver;
import com.orrobotics.orsmartrobot.service.ServiceRobot;
import com.orrobotics.orsmartrobot.service.SpeechRecognitionService;
import com.orrobotics.orsmartrobot.util.AvsListenerInterface;
import com.orrobotics.orsmartrobot.util.GNewsObject;
import com.orrobotics.orsmartrobot.util.Message;
import com.orrobotics.orsmartrobot.util.MessageAdapter;
import com.orrobotics.orsmartrobot.util.MessageVocalObject;
import com.orrobotics.orsmartrobot.util.ParamVocalObject;
import com.orrobotics.orsmartrobot.util.ResultVocalObject;
import com.orrobotics.orsmartrobot.util.SmartDeviceObject;
import com.orrobotics.orsmartrobot.util.GestionDesFichiers;
import com.orrobotics.orsmartrobot.util.RemoteControlUtil;
import com.orrobotics.orsmartrobot.voicerecognition.AudioBuffer;
import com.orrobotics.orsmartrobot.voicerecognition.SpeechAPI;
import com.orrobotics.orsmartrobot.voicerecognition.SpeechRecognition;
import com.orrobotics.orsmartrobot.voicerecognition.VoiceRecorder;
import com.st.BlueSTSDK.Feature;
import com.st.BlueSTSDK.Features.FeatureAudioADPCM;
import com.st.BlueSTSDK.Features.FeatureAudioADPCMSync;
import com.st.BlueSTSDK.Manager;
import com.st.BlueSTSDK.Utils.BVAudioSyncManager;
import com.willblaschko.android.alexa.AlexaManager;
import com.willblaschko.android.alexa.callbacks.AsyncCallback;
import com.willblaschko.android.alexa.interfaces.AvsResponse;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.res.Resources.NotFoundException;
import android.database.DataSetObserver;
import android.graphics.drawable.AnimationDrawable;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.speech.tts.TextToSpeech;
import android.app.FragmentTransaction;
import android.support.annotation.WorkerThread;
import android.support.v4.app.ActivityCompat;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import edu.cmu.pocketsphinx.Assets;
import edu.cmu.pocketsphinx.Hypothesis;
import edu.cmu.pocketsphinx.RecognitionListener;
import edu.cmu.pocketsphinx.SpeechRecognizer;

import static com.orrobotics.orsmartrobot.alexa.Constants.PRODUCT_ID;
import static edu.cmu.pocketsphinx.SpeechRecognizerSetup.defaultSetup;

@SuppressLint({"SdCardPath", "NewApi", "DefaultLocale", "Recycle"})
public class ControlWithVoice implements OrSmarRobotObserver, RecognitionListener {

    public static SmartRobotApplication model;
    String tmp = "";
    Handler Handler = new Handler();
    Runnable mHandlerTask = null;
    static String msg = " ";
    static File pathOfMyFile = Environment
            .getExternalStoragePublicDirectory("/SmartRobot/CommandVocal");
    ControlRobotController vControl;
    ControlRobotModel vModel;
    static ImageButton Cmdvocale;
    static TextView Cmdtext;
    Handler mHandler = new Handler();
    File f;
    private SpeechRecognitionService mSpeechRecognitionService;
    long frequance;
    Intent intent;
    private String mResults;
    Boolean flagAutoDecl = false;
    String data;
    ResultVocalObject result = null;
    Boolean destroy = false;
    Timer remoteTimer;
    HashMap<String, String> params = new HashMap<String, String>();
    int delaiAffaichage;
    Handler hand = new Handler();
    Handler handler = new Handler();
    int headValueUP;
    int headValueDOWN;
    String optionCmdVocal;
    TextView commandeResponce;

    static MessageAdapter messageAdapter;
    private boolean side = false;
    static ListView messagesView;

    int flagMode3enabled = 1;
    Boolean flagNotPermanance = false;
    AudioManager audioManager;
    Boolean flagIsPermance = true;
    int Speedpos = 2;
    float speed = 1.0f;
    int i = 0;
    int progress;
    boolean isDeviceActif = false;
    String deviceType = null;
    String deviceOwner = "";
    String deviceName = "";
    String deviceMac = "";
    VideoCallFragment videoCallFragment;
    public static FragmentTransaction transaction;
    private Handler mControlsHandler = new Handler();
    private Runnable mControls;

    Boolean flagTts = false;
    AudioManager mAudioManager;
    int duration = 0;
    String errorNoRadioFound = "";

    String chanelName = "";
    String chanelurl = "";
    String errorNoMusicFound = "";
    String fileName = "";
    int nbrMorceau = 1;
    ArrayList<String> filePath;
    List<SmartDeviceObject> deviceListObject = new ArrayList<SmartDeviceObject>();
    ArrayList<MessageVocalObject> vocalMsgs;
    String errorNoResult = "";
    String errorNoInternet = "";
    String callError = "";
    String message_invitation = "";
    String rienEntendue = "";
    String alexanoResponce = "";
    Boolean flagHexiwear = false;
    String commandeInconue = "";
    String errorNoDeviceActif = "";
    String errorNoInfo = "";
    String failedConnexion = "";
    String errorNoRobot = "";
    ImageButton stop_music;
    ImageView microGif;
    AnimationDrawable animationKeyWord;
    AnimationDrawable animationCmd;
    AnimationDrawable animationResponse;
    private static final String KWS_SEARCH = "wakeup";
    String keyWordType;
    private static SpeechRecognizer recognizer;
    protected Manager mManager;

    private FeatureAudioADPCM mAudio;
    private FeatureAudioADPCMSync mAudioSync;
    private static AudioBuffer mRecordedAudio;
    private static AudioBuffer bmRecordedAudio;
    private static final int AUDIO_SAMPLING_FREQ = 8000;
    private static final int MAX_RECORDING_TIME_S = 5;
    private boolean mIsRecording;
    boolean flagBlueMicOn = false;
    private BVAudioSyncManager mBVAudioSyncManager = new BVAudioSyncManager();
    com.st.BlueSTSDK.Node mConnectedNode = null;
    boolean resultGot = false;
    boolean sphinxTaskIsRunning = false;

    MediaPlayer firstBip;
    MediaPlayer secondeBip;
    InitSphinxTask initSphinxTask = null;

    private static TimerTask permanatRecognitionTask;
    boolean test = true;
    final Handler handle_ap = new Handler();

    String timeSpeech;
    int TimeSpeech;
    private List<String> stringList;
    private SpeechAPI speechAPI;
    private static VoiceRecorder mVoiceRecorder;
    String call = "webrtc";
    int responseTimout;
    String textDissmisDefil;
    int processusSequence;
    int gifWidth;
    int gifHeight;
    String blueMicScan;
    int scan_period;
    AlexaManager alexaManager;

    /***
     * declaration des donn�es globales li�es � la d�tection de silence
     * ajout� par Yassine Berradi
     */
    static {
        System.loadLibrary("native-lib");
    }

    private static short sData[] = new short[80];
    private static List<Short> voiced_data = new ArrayList<Short>();
    //private static short voiced_data[][] = new short[80][];

    private static int sampleRate = 8000;
    private final int vadFrame = 80;
    private static int aggressivity;//=3;
    private final double vad_seuil = 0.6;
    private static double intens_seuil;//=0.3; // (%)
    private static int silence;//=2;
    private static int len_for_one_sec = 50;
    private static int sec_steps = ((sampleRate / 40) / len_for_one_sec);
    private static int nbr_sec_sil;//=8;
    private int vadtest = 0;
    private static int sec_counter = 0;
    private final int bufferSize = 80;
    private boolean verbose_record = false;
    //private int scroll_counter = 0;
    long startTime;
    private static int indice = 0;
    private static List<Integer> voiced_one_sec = new ArrayList<Integer>();

    private static List<Byte[]> validata = new ArrayList<Byte[]>();


    private static List<Double> intens_List = new ArrayList<Double>();
    private static Double[] sorted_intens = new Double[(int) len_for_one_sec];
    private static short lastmSohrtAudio[] = new short[(int) len_for_one_sec * 40];


    private static int timeNoSpeech;
    public boolean found = false;

    /***
     * d�finition de la fonction rootMeanSquare qui permit de calculer l'intensit� du signal audio
     * ajout� par Yassine Berradi
     */
    @WorkerThread
    public static double rootMeanSquare(double... nums) {
        double sum = 0.0;
        for (double num : nums)
            sum += num * num;
        return Math.sqrt(sum / nums.length);
    }

    /***
     * d�finition de la fonction calculateSum qui permit de calculer la sum des valeurs dans une liste
     * ajout� par Yassine Berradi
     */
    @WorkerThread
    public double calculateSum(List<Integer> marks) {
        Integer sum = 0;
        if (!marks.isEmpty()) {
            for (Integer mark : marks) {
                sum += mark;
            }
            return sum.doubleValue();// / marks.size();
        }
        return sum;
    }

    /***
     * d�finition de la fonction calculateAvg qui permit de calculer la moyenne des valeurs dans une liste
     * ajout� par Yassine Berradi
     */
    @WorkerThread
    public double calculateAvg(List<Double> marks) {
        Double sum = 0.0;
        if (!marks.isEmpty()) {
            for (Double mark : marks) {
                sum += mark;
            }
            return sum / marks.size();
        }
        return sum;
    }

    /***
     * declaration de la fonction native qui permet la d�tection de l'activit� vocale (VAD)
     * ajout� par Yassine Berradi
     */
    public native int isSpeechJNI(short[] buffer, int offseetInshort, int readSize, int sampleRate, int vadFrame, int aggressivity, double vad_seuil);

    private final VoiceRecorder.Callback mVoiceCallback = new VoiceRecorder.Callback() {

        @Override
        public void onVoiceStart() {

            Log.i("mic", "----onVoiceStart----");
            if (speechAPI == null)
                initSpeechCloudApi();
            if (mVoiceRecorder != null) {
                test = true;
                Log.i("mic", "--onVoiceStart-------test----" + test);

                speechAPI.startRecognizing(mVoiceRecorder.getSampleRate(), model.getLangageTTS());
            }
            //bmRecordedAudio=new AudioBuffer(AUDIO_SAMPLING_FREQ, MAX_RECORDING_TIME_S);
        }

        @Override
        public void onVoice(byte[] data, int size, boolean vt) {

            if (speechAPI == null)
                initSpeechCloudApi();

            if (!flagTts)
                if (size > 0) {
                    test = false;
                    Log.i("mic", "--onVoice-------test----" + test);
                    //bmRecordedAudio.dynamicAppend(data);
                    speechAPI.recognize(data, size);
                }


        }

        @Override
        public void onVoiceEnd() {

            Log.i("mic", "----onVoiceEnd----");

            /**if (speechAPI == null)
             initSpeechCloudApi();
             if (bmRecordedAudio != null){
             speechAPI.recognize(bmRecordedAudio.getbmAudioData(), bmRecordedAudio.getbmAudioData().length);
             }**/

            if (speechAPI == null)
                initSpeechCloudApi();
            if (!flagTts) {
                test = true;
                Log.i("mic", "--onVoiceEnd------test----" + test);
                speechAPI.finishRecognizing();
            }

            handle_ap.postDelayed(new Runnable() {
                @Override
                public void run() {
                    // TODO Auto-generated method stub


                    if (!flagBlueMicOn && flagNotPermanance) {
                        Log.i("mic", "----test----" + test + "; flagTts: " + flagTts + "; resultGot: " + resultGot);
                        if (!flagTts && !resultGot && test) {
                            stopSpeechAnimationCmd();
                            secondeBip.start();
                            TonError();
                        }

                    }

                    //stopSpeechAnimationCmd();
                    //Log.i("mic","----test----"+test+"; flagTts: "+flagTts+"; resultGot: "+resultGot);

                    /**if(!flagBlueMicOn) {
                     Log.i("mic","----test----"+test+"; flagTts: "+flagTts+"; resultGot: "+resultGot);
                     if(!flagTts && !resultGot && test) {
                     stopSpeechAnimationCmd();
                     secondeBip.start();
                     TonError();
                     }

                     }**/
								/*else{
									stopBlueMicRecognition();
								}*/


                }

            }, TimeSpeech * 1000);


            //bmRecordedAudio=new AudioBuffer(AUDIO_SAMPLING_FREQ, MAX_RECORDING_TIME_S);

        }

    };

    /**
     * Listener pour les r�sultats de l'api cloud
     */
    private final SpeechAPI.Listener mSpeechServiceListener =
            new SpeechAPI.Listener() {
                @Override
                public void onSpeechRecognized(final String text, final boolean isFinal) {
                    if (isFinal) {
                        if (mVoiceRecorder != null)
                            mVoiceRecorder.dismiss();

                    }
                    model.getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (isFinal) {

                                stringList.add(0, text);

                                //resultGot = true;


                            } else {
                                if (stringList.size() != 0) {
                                }
                            }

                            if (stringList.size() != 0) {
                            }
                        }


                    });

                    if (stringList.size() != 0) {
                    }

                }

                @Override
                public void onSpeechApiCompleted() {

                }
            };

    /**
     * D�marrer l'enregitrement audio avec micro int�gr�
     */
    private void startVoiceRecorder() {
        if (mVoiceRecorder == null) {
            mVoiceRecorder = new VoiceRecorder(mVoiceCallback);
            mVoiceRecorder.start();
        }
    }

    /**
     * Arreter l'enregitrement audio avec micro int�gr�
     */
    private void stopVoiceRecorder() {

        if (mVoiceRecorder != null) {
            mVoiceRecorder.stop();
            mVoiceRecorder = null;
        }
    }

    /**
     * D�truire l'api cloud
     */
    protected void onStop() {
        stopVoiceRecorder();

        // Stop Cloud Speech API
        if (speechAPI != null) {
            speechAPI.removeListener(mSpeechServiceListener);
            speechAPI.destroy();
            speechAPI = null;
        }

    }


    private ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {


            mSpeechRecognitionService = ((SpeechRecognitionService.LocalBinder) service)
                    .getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mSpeechRecognitionService = null;

        }

    };

    private SpeechRecognition.ResultCallback mResultCallback = new SpeechRecognition.ResultCallback() {

        @Override
        public void onResults(List<String> results) {
            synchronized (vModel.getContext()) {
                mResults = results.get(0);
                GestionDesFichiers.log("onSucces mResults TonSucces()",
                        mResults, "Log_SmartRobot");
                TonSucces();
            }

        }

        @Override
        public void onError(String reason) {
            synchronized (vModel.getContext()) {
                if (destroy == false) {

                    mResults = new String(reason);
                    Intent servcice = new Intent(model.getActivity(),
                            SpeechRecognitionService.class);
                    model.getActivity().stopService(servcice);
                    vModel.getContext().startService(servcice);
                    vModel.getContext().bindService(servcice,
                            mServiceConnection, Context.BIND_AUTO_CREATE);

                    GestionDesFichiers.log("onError mResults TonError()",
                            mResults, "Log_SmartRobot");
                    TonError();
                }

            }

        }

    };

    protected AsyncCallback<AvsResponse, Exception> getRequestCallback() {
        if (model.getActivity() != null && model.getActivity() instanceof AvsListenerInterface) {
            return ((AvsListenerInterface) model.getActivity()).getRequestCallback();
        }
        return null;
    }


    public ControlWithVoice(ControlRobotController control,
                            ControlRobotModel modelControl) {


        if (remoteTimer != null)
            remoteTimer.cancel();
        vControl = control;
        vModel = modelControl;
        model = (SmartRobotApplication) vModel.getContext();
        model.removeObserver(this);
        model.registerObserver(ControlWithVoice.this);
        headValueUP = Integer.parseInt(SmartRobotApplication.getParamFromFile(
                "head.up", "Configuration.properties", null));
        headValueDOWN = Integer
                .parseInt(SmartRobotApplication.getParamFromFile("head.down",
                        "Configuration.properties", null));
        mAudioManager = (AudioManager) vModel.getContext().getSystemService(
                Context.AUDIO_SERVICE);

        alexaManager = AlexaManager.getInstance(model.getActivity(), PRODUCT_ID);


        timeSpeech = SmartRobotApplication.getParamFromFile("time.speech",
                "Configuration.properties", null);

        if (timeSpeech == null)
            timeSpeech = "5 s";
        TimeSpeech = Integer.parseInt(timeSpeech.split(" ")[0]);

        String responseTimoutS = SmartRobotApplication.getParamFromFile("response.timout",
                "Configuration.properties", null);

        if (responseTimoutS == null)
            responseTimoutS = "1 s";
        responseTimout = Integer.parseInt(responseTimoutS.split(" ")[0]);

        String processusSequenceS = SmartRobotApplication.getParamFromFile("processus.sequence",
                "Configuration.properties", null);
        if (processusSequenceS != null)
            processusSequence = Integer.parseInt(processusSequenceS);
        else
            processusSequence = 3;


        String gifWidthS = SmartRobotApplication.getParamFromFile("gif.size.width",
                "Configuration.properties", null);
        if (gifWidthS != null)
            gifWidth = Integer.parseInt(gifWidthS);
        else
            gifWidth = 250;

        String gifHeightS = SmartRobotApplication.getParamFromFile("gif.size.height",
                "Configuration.properties", null);
        if (gifHeightS != null)
            gifHeight = Integer.parseInt(gifHeightS);
        else
            gifHeight = 250;

        if (model.isConnectedToInternet())
            initSpeechCloudApi();
        firstBip = MediaPlayer.create(model.getApplicationContext(), R.raw.bip_first);
        secondeBip = MediaPlayer.create(model.getApplicationContext(), R.raw.bip_last);

        stringList = new ArrayList<>();
        call = model.getParamFromFile("call", "Configuration.properties", null);

        if (call == null) {
            call = "webrtc";

        }
        init();
        optionCmdVocal = model.getOptCmdVocal();
        audioManager = (AudioManager) vModel.getContext().getSystemService(
                Context.AUDIO_SERVICE);
        progress = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        model.setEditor(RemoteControlUtil.VOLUME, Integer.toString(progress));
        if (!getData().equals("0")) {
            if (remoteTimer == null) {
                remoteTimer = new Timer();

            } else {
                remoteTimer.cancel();
                remoteTimer = null;
                remoteTimer = new Timer();
            }
            remoteTimer.schedule(new MyRemoteTaskSimple(), getfrequence());
        }
        GestionDesFichiers.log("frequancefrequance",
                frequance + " " + data.length(), "");
        delaiAffaichage = Integer.parseInt(SmartRobotApplication
                .getParamFromFile("delai.affichage",
                        "Configuration.properties", null).split(" ")[0]) * 1000;

    }

    /**
     * Initialiser l'api cloud
     */
    private void initSpeechCloudApi() {
        speechAPI = new SpeechAPI(model.getApplicationContext(), model.getLangageTTS());
        speechAPI.addListener(mSpeechServiceListener);
    }

    private long getfrequence() {
        // TODO Auto-generated method stub
        String[] date = getData().split("/");
        if (date.length == 3) {
            frequance = (Integer.parseInt(date[0]) * 24 * 3600
                    + Integer.parseInt(date[1]) * 3600 + Integer
                    .parseInt(date[2]) * 60) * 1000;
        } else if (date.length == 2) {
            frequance = (Integer.parseInt(date[0]) * 3600 + Integer
                    .parseInt(date[1]) * 60) * 1000;
        } else {
            frequance = Integer.parseInt(date[0]) * 60 * 1000;
        }
        return frequance;
    }

    private String getData() {
        // TODO Auto-generated method stub
        if (model.getVocalFrqDeclench().equals(""))

            data = SmartRobotApplication
                    .getParamFromFile("Frequence.declenchement",
                            "Configuration.properties", null);
        else
            data = model.getVocalFrqDeclench();
        return data;
    }

    private int geIsPermanance() {
        // TODO Auto-generated method stub

        if (model.getIsPermanant().equals(""))
            flagMode3enabled = Integer.parseInt(SmartRobotApplication
                    .getParamFromFile("vocale.mode.3.isenable",
                            "Configuration.properties", null));

        else
            flagMode3enabled = Integer.parseInt(model.getIsPermanant());
        if (flagMode3enabled == 0)
            model.setVolume();
        return flagMode3enabled;
    }

    @SuppressWarnings("static-access")
    public void setWidget(ImageButton Cmdvocale, final TextView Cmdtext,
                          TextView commandeResponce, ListView messagesView, MessageAdapter messageAdapter, ImageButton stop_music, ImageView microGif,
                          AnimationDrawable animationKeyWord, AnimationDrawable animationCmd, AnimationDrawable animationResponse) {


        tmp = SmartRobotApplication
                .getParamFromFile("Defilement",
                        "Configuration.properties", null);
        if (tmp == null) {
            tmp = "1";
        }
        textDissmisDefil = SmartRobotApplication
                .getParamFromFile("delaiPourDisparaitre",
                        "Configuration.properties", null);
        if (textDissmisDefil == null) {
            textDissmisDefil = "10";
        }
        ControlWithVoice.Cmdvocale = Cmdvocale;
        ControlWithVoice.Cmdtext = Cmdtext;
        this.stop_music = stop_music;
        this.commandeResponce = commandeResponce;

        ControlWithVoice.messagesView = messagesView;
        ControlWithVoice.messageAdapter = messageAdapter;
        ControlWithVoice.messagesView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        ControlWithVoice.messagesView.setAdapter(ControlWithVoice.messageAdapter);

        this.microGif = microGif;
        this.animationKeyWord = animationKeyWord;
        this.animationCmd = animationCmd;
        this.animationResponse = animationResponse;
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) microGif.getLayoutParams();
        layoutParams.height = gifHeight;
        layoutParams.width = gifWidth;
        microGif.setLayoutParams(layoutParams);

        if (optionCmdVocal.equals("true")) {
            Cmdvocale.setVisibility(View.VISIBLE);
        } else if (optionCmdVocal.equals("false")) {
            Cmdvocale.setVisibility(View.INVISIBLE);
        }
        Cmdtext.setTextColor(model.changeTextColor("question"));
        Cmdtext.setTypeface(model.changeTextFont());
        Cmdtext.setTextSize(model.changeTextSize());
        Cmdtext.setMovementMethod(new ScrollingMovementMethod());
        if (Integer.parseInt(tmp) == 0) {
            Cmdtext.setText("");
        }

        mHandlerTask = new Runnable() {
            @Override
            public void run() {
                if (Integer.parseInt(tmp) == 1) {
                    messageAdapter.clear();
                    messagesView.setAdapter(null);
                }
            }
        };

        commandeResponce.setVisibility(View.VISIBLE);
        commandeResponce.setTextColor(model.changeTextColor("response"));
        commandeResponce.setTypeface(model.changeTextFont());
        commandeResponce.setTextSize(model.changeTextSize());
        commandeResponce.setText("");

        ControlWithVoice.messagesView.setVisibility(View.VISIBLE);

        int baseSampleRate = 44100;
        int channel = AudioFormat.CHANNEL_IN_MONO;
        int format = AudioFormat.ENCODING_PCM_16BIT;
        int buffSize = AudioRecord.getMinBufferSize(baseSampleRate, channel,
                format);
        android.media.AudioRecord audio = new android.media.AudioRecord(
                MediaRecorder.AudioSource.MIC, baseSampleRate, channel, format,
                buffSize);
        audio.release();
        setListeners();
    }

    @SuppressLint("NewApi")
    private void init() {

        /**
         * Cr�ation du fichier CmdVocale.xml Fichier de param�trage des
         * commandes vocale
         */
        GestionDesFichiers.WriteCmdVocaleFile();
        initVocalMessages();

        // initialisation screenReceiver
        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        BroadcastReceiver mReceiver = new ScreenReceiver();
        vModel.getContext().registerReceiver(mReceiver, filter);

        initBlueMicRecognition();

        Intent intent = new Intent(vModel.getContext(),
                SpeechRecognitionService.class);
        vModel.getContext().startService(intent);
        vModel.getContext().bindService(intent, mServiceConnection,
                Context.BIND_AUTO_CREATE);
        mResults = new String();

        geIsPermanance();

        keyWordType = SmartRobotApplication.getParamFromFile(
                "type.declenchement", "configuration.properties", null);
        if (keyWordType == null)
            keyWordType = "pocketsphinx";

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                if (flagMode3enabled == 1 && model.isTelepresence() == false
                        && flagNotPermanance == false) {

                    startpermanentRecognition();
                }
            }
        }, 5000);

    }

    /**
     * Initialiser la reconnaissance par BlueMic
     */
    private void initBlueMicRecognition() {
        mManager = Manager.getSharedInstance();
        mManager.addListener(BlueMicDiscovery);
        startBlueMicDiscovery();

    }

    /**
     * D�marrer le prcessus de Scan p�riodique du BlueMic
     */
    private void startBlueMicDiscovery() {
        cancelBlueMicDiscovery();
        blueMicScan = SmartRobotApplication
                .getParamFromFile("scanBlueST.period",
                        "Configuration.properties", null);
        silence = Integer.parseInt(SmartRobotApplication.getParamFromFile(
                "nbrSecSil", "Configuration.properties", null));

        aggressivity = Integer.parseInt(SmartRobotApplication.getParamFromFile(
                "agressivity", "Configuration.properties", null));
        intens_seuil = Double.parseDouble(SmartRobotApplication.getParamFromFile(
                "seuil_intensity", "Configuration.properties", null));
        nbr_sec_sil = sec_steps * silence - 1;

        timeNoSpeech = Integer.parseInt(SmartRobotApplication.getParamFromFile(
                "timeNoSpeech", "Configuration.properties", null));

        if (blueMicScan == null)
            blueMicScan = "5 s";
        scan_period = Integer.parseInt(blueMicScan.split(" ")[0]);

        if (scan_period != 0) {
            handler.postDelayed(mBlueMicDiscover, scan_period * 1000);
        }

    }

    Runnable mBlueMicDiscover = new Runnable() {
        @Override
        public void run() {
            startDiscovery(scan_period * 1000);
            handler.postDelayed(mBlueMicDiscover, scan_period * 1000);
        }
    };

    /**
     * D�marrer le Scan du BlueMic
     */
    private void startDiscovery(int periode) {

        if (ActivityCompat.checkSelfPermission(model.getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mManager.resetDiscovery();
        mManager.startDiscovery(periode);
    }

    /**
     * D�marrer la reconnaissance par BlueMic
     */
    private void startBlueMicRecognition() {
        mRecordedAudio = new AudioBuffer(AUDIO_SAMPLING_FREQ, MAX_RECORDING_TIME_S);
        mIsRecording = true;
        voiced_one_sec.clear();
        if (voiced_one_sec.size() < 1) {
            for (int h = 0; h < nbr_sec_sil; h++) {
                voiced_one_sec.add(0);
            }
        }
        intens_List.clear();
        vadtest = 0;
        sec_counter = 0;
        verbose_record = false;
        voiced_data.clear();
        intens_List.clear();
        indice = 0;
        Arrays.fill(sorted_intens, null);
    }

    /**
     * Arreter la reconnaissance par BlueMic
     */
    private void stopBlueMicRecognition() {
        /**if(mRecordedAudio != null) {
         mIsRecording = false;

         if (speechAPI == null)
         initSpeechCloudApi();
         speechAPI.recognizeAudioInput(ShortToByte_Method(mRecordedAudio.getData()), mRecordedAudio.getData().length, model.getLangageTTS());

         mRecordedAudio = mIsRecording ? new AudioBuffer(AUDIO_SAMPLING_FREQ, MAX_RECORDING_TIME_S) : null;
         }**/
        mRecordedAudio = null;
        mIsRecording = false;
        voiced_one_sec.clear();
        intens_List.clear();
        vadtest = 0;
        sec_counter = 0;
        verbose_record = false;
        voiced_data.clear();
        intens_List.clear();
        indice = 0;
        Arrays.fill(sorted_intens, null);
    }

    /**
     * listener pour l'audio ansynch
     */

    private final Feature.FeatureListener mAudioSyncListener = new Feature.FeatureListener() {
        @Override
        public void onUpdate(final Feature f, final Feature.Sample sample) {
            if (mBVAudioSyncManager != null) {
                mBVAudioSyncManager.setSyncParams(sample);
            }
        }


    };

    /**
     * listener pour r�cup�rer les donn�es audio depuis le blueMic
     */
    private final Feature.FeatureListener mAudioListener = new Feature.FeatureListener() {
        @Override
        public void onUpdate(final Feature f, final Feature.Sample sample) {
            //Log.i("updatevad", " flagTts: "+flagTts+ "firstBip.isPlaying(): "+firstBip.isPlaying()+"; mIsRecording:"+mIsRecording);

            if (mIsRecording && !flagTts && !firstBip.isPlaying()) {
                short[] audioSample = FeatureAudioADPCM.getAudio(sample);
                synchronized (mRecordedAudio) {
                    /***
                     * d�tecter le silence en temps r�el pour chaque mise � jour des donn�es audio
                     * ajout� par Yassine Berradi
                     */
                    if (!verbose_record) {
                        startTime = System.nanoTime();
                    }
                    if (verbose_record) {
                        int nRecordedSample = mRecordedAudio.dynamicAppend(audioSample);
                    }
                    if (sec_counter != len_for_one_sec) {
                        int l = 0;
                        while (l < sData.length) {
                            if (l < audioSample.length) {
                                sData[l] = audioSample[l];
                                voiced_data.add(audioSample[l]);
                                l++;
                            } else {
                                sData[l] = 0;
                                l++;
                            }

                        }
                        double[] transformed = new double[bufferSize];

                        for (int j = 0; j < bufferSize; j++) {
                            transformed[j] = (double) sData[j];
                        }

                        double intensity = rootMeanSquare(transformed);
                        sorted_intens[sec_counter] = intensity;
                        sec_counter++;
                        vadtest += isSpeechJNI(sData, 0, sData.length, sampleRate, vadFrame, aggressivity, vad_seuil);
                        //Log.i("updatevad", " flagTts: "+flagTts+ "sec_counter: "+sec_counter+"; mIsRecording:"+mIsRecording+"vadtest: "+vadtest+"audioSample.size"+audioSample.length+"; voiced_one_sec.size(): "+voiced_one_sec.size()+"indice: "+indice);

                    } else {


                        if (!verbose_record) {

                            if (lastmSohrtAudio == null) {
                                lastmSohrtAudio = new short[len_for_one_sec * 40];
                            }

                            for (int i = 0; i < voiced_data.size(); i++) {
                                lastmSohrtAudio[i] = voiced_data.get(i);
                            }
                        }

                        voiced_data.clear();
                        sec_counter = 0;
                        float vad_rat = (float) vadtest / len_for_one_sec;
                        Arrays.sort(sorted_intens, Collections.reverseOrder());
                        double sum = 0;
                        int j = 0;
                        for (j = 0; j < (int) sorted_intens.length * 0.2; j++) {
                            sum += sorted_intens[j];
                        }
                        double moy_intens = 0.0;
                        moy_intens = sum / j;
                        Log.e("updatevad", "voiced_one_sec.size" + voiced_one_sec.size() + "moy_intens: " + moy_intens);
                        if (vad_rat > vad_seuil && (moy_intens / 32767) * 100 > intens_seuil) {
                            voiced_one_sec.add(1);
                            indice = 0;
                            i = 0;
                            verbose_record = true;
                            if (verbose_record) {
                                if (lastmSohrtAudio != null) {
                                    int nRecordedSample = mRecordedAudio.dynamicAppend(lastmSohrtAudio);
                                    int nRcordedSample = mRecordedAudio.dynamicAppend(audioSample);

                                    lastmSohrtAudio = null;
                                }
                            }
                            intens_List.add(moy_intens);
                            vadtest = 0;
                            Arrays.fill(sorted_intens, null);
                            Log.e("updatevad", "voiced_one_sec.size()" + voiced_one_sec.get(voiced_one_sec.size() - 1) + "sorted_intens.size" + sorted_intens.length + ";voiced_data.size: " + voiced_data.size());

                        } else {
                            voiced_one_sec.add(0);
                            int v = 0;
                            for (int i = voiced_one_sec.size() - nbr_sec_sil; i < voiced_one_sec.size(); i++) {
                                v += voiced_one_sec.get(i);
                            }
                            double sum_vad = calculateSum(voiced_one_sec);
                            Log.e("updatevad", "voiced_one_sec.size()" + voiced_one_sec.size() + "sum_vad: " + sum_vad);

                            if (v == 0 && sum_vad != 0) {
                                long endTime = System.nanoTime();
                                long duration = (endTime - startTime);  //divide by 1000000 to get milliseconds.
                                double avg_vad = sum_vad / (voiced_one_sec.size() - 2 * nbr_sec_sil);
                                double mean_ints = calculateAvg(intens_List);
                                double min_intes = (Collections.min(intens_List) / 32767) * 100;
                                double ints_mean = (mean_ints / 32767) * 100;
                                double max_ints = (Collections.max(intens_List) / 32767) * 100;
                                double timePeriod = (double) (voiced_one_sec.size() - 2 * nbr_sec_sil) / sec_steps;
                                Log.e(" updatevad", " avg_vad: " + avg_vad + " mRecordedAudio.length: " + mRecordedAudio.getmAudioData().length + "; duration (ms)" + duration / 1000000);

                                if (mRecordedAudio != null) {

                                    //if (speechAPI == null)(mRecordedAudio.getmAudioData()), mRecordedAudio.getData().length, model.getLangageTTS());
                                    //Log.i("mic","----onVoiceStart----");
                                    if (speechAPI == null)
                                        initSpeechCloudApi();
                                    if (verbose_record) {
                                        if (!flagTts | !firstBip.isPlaying()) {
                                            speechAPI.startRecognizing(sampleRate, model.getLangageTTS());
                                        }
                                    }

                                    if (speechAPI == null)
                                        initSpeechCloudApi();
                                    if (!flagTts | !firstBip.isPlaying()) {
                                        speechAPI.recognize(ShortToByte_Method(mRecordedAudio.getmAudioData()), mRecordedAudio.getmAudioData().length);
                                    }
									/*handler.postDelayed(new Runnable() {
										@Override
										public void run() {

										}
									}, 1000);*/
                                    speechAPI.finishRecognizing();
                                    mRecordedAudio = mIsRecording ? new AudioBuffer(AUDIO_SAMPLING_FREQ, MAX_RECORDING_TIME_S) : null;

                                }

                                verbose_record = false;
                                voiced_data.clear();
                                indice = 0;
                                i = 0;

                                voiced_one_sec.clear();
                                intens_List.clear();
                                vadtest = 0;
                                Arrays.fill(sorted_intens, null);
                                if (voiced_one_sec.size() < 1) {
                                    for (int h = 0; h < nbr_sec_sil; h++) {
                                        voiced_one_sec.add(0);
                                    }
                                }


                            } else if (v == 0 && sum_vad == 0) {
                                indice++;
                                if (indice == 4 * timeNoSpeech) {
                                    //stopBlueMicRecognition();
                                    TonError();
									/*handler.postDelayed(new Runnable() {
										@Override
										public void run() {
											startBlueMicRecognition();
										}
									}, 5000);*/
                                    //startBlueMicRecognition();
                                    indice = 0;
                                }


                                voiced_one_sec.clear();
                                intens_List.clear();
                                vadtest = 0;
                                Arrays.fill(sorted_intens, null);
                                if (voiced_one_sec.size() < 1) {
                                    for (int h = 0; h < nbr_sec_sil; h++) {
                                        voiced_one_sec.add(0);
                                    }
                                }

                            } else {

                                indice = 0;
                                i = 0;
                            }


                        }


                    }

                }

            }
        }

    };


    /**
     * Convertir les donn�es audio du short vers byte
     */
    byte[] ShortToByte_Method(short[] input) {
        int short_index, byte_index;
        int iterations = input.length;

        byte[] buffer = new byte[input.length * 2];

        short_index = byte_index = 0;

        for (/*NOP*/; short_index != iterations; /*NOP*/) {
            buffer[byte_index] = (byte) (input[short_index] & 0x00FF);
            buffer[byte_index + 1] = (byte) ((input[short_index] & 0xFF00) >> 8);

            ++short_index;
            byte_index += 2;
        }

        return buffer;
    }

    /**
     * listener pour l'etat de connexion au BlueMic
     */
    private com.st.BlueSTSDK.Node.NodeStateListener mNodeStatusListener = new com.st.BlueSTSDK.Node.NodeStateListener() {
        @Override
        public void onStateChange(final com.st.BlueSTSDK.Node node, final com.st.BlueSTSDK.Node.State newState, com.st.BlueSTSDK.Node.State prevState) {
            if (newState == com.st.BlueSTSDK.Node.State.Connected) {

                model.getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(model.getApplicationContext(), model.getResources().getString(R.string.blueMicConnected), Toast.LENGTH_SHORT).show();
                    }
                });
                cancelBlueMicDiscovery();
                flagBlueMicOn = true;
                mConnectedNode = node;
                handle_ap.removeCallbacksAndMessages(null);
                stopVoiceRecorder();

                mAudio = node.getFeature(FeatureAudioADPCM.class);
                mAudioSync = node.getFeature(FeatureAudioADPCMSync.class);
                mAudio.addFeatureListener(mAudioListener);
                mBVAudioSyncManager.reinitResetFlag();
                mAudio.setAudioSyncManager(mBVAudioSyncManager);

                node.enableNotification(mAudio);

                mAudioSync.addFeatureListener(mAudioSyncListener);
                node.enableNotification(mAudioSync);


                startpermanentRecognition();


            } else if (newState == com.st.BlueSTSDK.Node.State.Lost || newState == com.st.BlueSTSDK.Node.State.Dead || newState == com.st.BlueSTSDK.Node.State.Unreachable) {
                handle_ap.removeCallbacksAndMessages(null);
                flagBlueMicOn = false;
                mConnectedNode = null;
                startpermanentRecognition();
                startBlueMicDiscovery();
                model.getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(model.getApplicationContext(), model.getResources().getString(R.string.blueMicDisconnected), Toast.LENGTH_SHORT).show();
                    }
                });


            } else if (newState == com.st.BlueSTSDK.Node.State.Connecting) {
                model.getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(model.getApplicationContext(), model.getResources().getString(R.string.blueMicConnecting), Toast.LENGTH_LONG).show();
                    }
                });
            }

        }
    };

    /**
     * Listener pour le Scan du BlueMic
     */
    private Manager.ManagerListener BlueMicDiscovery = new Manager.ManagerListener() {

        /**
         * call the stopNodeDiscovery for update the gui state
         * @param m manager that start/stop the process
         * @param enabled true if a new discovery start, false otherwise
         */
        @Override
        public void onDiscoveryChange(Manager m, boolean enabled) {
        }

        @Override
        public void onNodeDiscovered(Manager m, com.st.BlueSTSDK.Node node) {
            cancelBlueMicDiscovery();
            mManager.addNode(node);
            node.addNodeStateListener(mNodeStatusListener);
            node.connect(model.getActivity());


        }
    };

    /**
     * Arreter le task de reconnaissance permanante
     */
    private void stopPermanentRecognition() {

        if (permanatRecognitionTask != null) {
            permanatRecognitionTask.cancel();
            permanatRecognitionTask = null;
        }
    }

    /**
     * D�marrer le task de reconnaissance permanante
     */
    private void startpermanentRecognition() {
        stopPermanentRecognition();
        permanatRecognitionTask = new TimerTask() {
            public void run() {

                if (model.isTelepresence() == false) {
                    startCmdVocal(true);
                    flagNotPermanance = false;

                }
            }
        };
        permanatRecognitionTask.run();
    }

    /**
     * le task d'initialisation de l'api pocketSphinx
     */
    private class InitSphinxTask extends AsyncTask<Void, Void, Exception> {
        @Override
        protected Exception doInBackground(Void... params) {
            try {
                Assets assets = new Assets(model.getApplicationContext());
                File assetDir = assets.syncAssets();
                setupRecognizer(assetDir);
            } catch (IOException e) {
                return e;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Exception result) {
            if (result != null) {
            } else {
                sphinxTaskIsRunning = false;
                startPocketSphinxRecognition();

            }
        }
    }

    ;

    /**
     * Initialisation l'api pocketSphinx
     */
    private void initPocketSphinxAsynTask() {
        // TODO Auto-generated method stub
        stopVoiceRecorder();
        if (initSphinxTask == null)
            initSphinxTask = new InitSphinxTask();
        if (!initSphinxTask.getStatus().equals(AsyncTask.Status.RUNNING)) {
            sphinxTaskIsRunning = true;
            if (initSphinxTask.getStatus().equals(AsyncTask.Status.FINISHED))
                stopPocketSphinxRecognition();
            initSphinxTask.execute();
        }
    }


    private void setupRecognizer(File assetsDir) {
        File keys = null;
        File modelsDir = new File(assetsDir, "models");

        String mDirectoryPath = Environment.getExternalStorageDirectory() + "/SmartRobot/";
        File sphinxDir = new File(mDirectoryPath, "sphinx");
        if (!sphinxDir.exists())
            sphinxDir.mkdir();

        if (model.getLangageTTS().contains("fr")) {
            try {
                recognizer = defaultSetup()
                        .setAcousticModel(
                                new File(modelsDir, "hmm/fr-fr-adapt"))
                        .setDictionary(new File(modelsDir, "dict/fr.dict"))
                        //.setRawLogDir(assetsDir)
                        .getRecognizer();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            File frKeys = new File(mDirectoryPath
                    + "sphinx/", "keysFr.gram");
            if (!frKeys.exists()) {
                try {
                    frKeys.createNewFile();
                    model.copyFile(new File(modelsDir, "grammar/keysFr.gram"), frKeys);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            keys = frKeys;
        } else {
            try {
                recognizer = defaultSetup()
                        .setAcousticModel(new File(modelsDir, "hmm/en-us-semi"))
                        .setDictionary(new File(modelsDir, "dict/en.dict"))
                        .setRawLogDir(assetsDir).getRecognizer();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            File enKeys = new File(mDirectoryPath
                    + "sphinx/", "keysEn.gram");
            if (!enKeys.exists()) {
                try {
                    enKeys.createNewFile();
                    model.copyFile(new File(modelsDir, "grammar/keysEn.gram"), enKeys);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            keys = enKeys;
        }
        recognizer.addListener(this);

        recognizer.addKeywordSearch(KWS_SEARCH, keys);

    }

    /**
     * D�marrer la reconnaissance par l'api pocketSphinx
     */
    private void startPocketSphinxRecognition() {
        // TODO Auto-generated method stub
        if (mSpeechRecognitionService != null)
            mSpeechRecognitionService.destroyVoiceRecognition();
        handle_ap.removeCallbacksAndMessages(null);
        Boolean result = false;
        if (recognizer != null)
            result = recognizer.startListening(KWS_SEARCH);
        if (result)
            startSpeechAnimationKeyWord();

    }

    /**
     * Arreter la reconnaissance par l'api pocketSphinx
     */
    private void stopPocketSphinxRecognition() {
        // TODO Auto-generated method stub
        if (recognizer != null) {
            recognizer.stop();
            recognizer.removeListener(this);
            recognizer.shutdown();
            recognizer = null;
            initSphinxTask = null;
        }
    }

    private void rienEntendu() {
        // TODO Auto-generated method stub
        if (destroy == false) {
            if (flagAutoDecl)
                model.notifyObservers("screenOff");
            flagAutoDecl = false;
            if (!getData().equals("0")) {
                model.getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        commandeResponce.setText("");
                    }
                });

                if (remoteTimer == null) {
                    remoteTimer = new Timer();

                } else {
                    remoteTimer.cancel();
                    remoteTimer = null;
                    remoteTimer = new Timer();
                }
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    // TODO
                    // Auto-generated
                    // catch
                    // block
                    e.printStackTrace();
                }
                remoteTimer.schedule(new MyRemoteTaskSimple(),

                        getfrequence());
            }

            if (flagMode3enabled == 1) {
                startpermanentRecognition();
            }

            if (model.isTelepresence() == true) {
                ServiceRobot.sendMessage(model.getPseudoOpenFireSender(),
                        "call");
            }

        }
    }

    public void setListeners() {

        Cmdvocale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initVocalMessages();
                vModel.getActivity().runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        if (vControl.getTTS().isSpeaking()) {
                            vControl.getTTS().stop();
                        }
                        handle_ap.removeCallbacksAndMessages(null);
                        found = false;
                        i = 0;
                        data = model.getVocalFrqDeclench();
                        flagNotPermanance = true;
                        flagAutoDecl = false;
                        model.logMe("D�clenchement avec bouton", null, "/storage/emulated/0/" +
                                        "SmartRobot/CommandVocal/logCommandVocalSmart",
                                "logCommandVocalSmart", pathOfMyFile);
                        startCmdVocal(false);

                    }
                });

            }
        });

        stop_music.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopRadioOrMusic();
            }
        });
    }

    @Override
    public void update(final String message) {
        if (message != null) {

            if (message.equals("settingStarted")) {
                stopPermanentRecognition();
                stopSpeechAnimationKey();
            }
            if (message.equals("internetChanged")) {
                stopPermanentRecognition();
                if (!model.isConnectedToInternet()) {
                    cancelBlueMicDiscovery();
                    onStop();
                } else {
                    if (!flagBlueMicOn)
                        startBlueMicDiscovery();
                    initSpeechCloudApi();
                }
                if (!sphinxTaskIsRunning) {
                    geIsPermanance();
                    mSpeechRecognitionService.destroyVoiceRecognition();
                    stopSpeechAnimationKey();
                    stopVoiceRecorder();
                    stopBlueMicRecognition();
                    stopPocketSphinxRecognition();
                    startpermanentRecognition();
                }
            }
            if (message.equals("SpeechAPIError")) {
                resultGot = false;
                TonError();
            }
            if (message.contains("speaking")) {
                startSpeechAnimation();
            }
            if (message.contains("SpeechAPIComplete")) {
                String result = message.split(";")[1];
                if (!result.equals("null") && !resultGot) {
                    if (result.equals("Thibault"))
                        result = "ubo";
                    mResults = result;
                    resultGot = true;
                    TonSucces();

                }
            }

            if (message.contains("ttsDone") || message.contains("alexafinished")) {
                if (message.split(";")[0].equals("alexafinished") && message.split(";")[1].equals("false")) {
                    speak(alexanoResponce,
                            "alexaResponse");
                } else {

                    flagTts = false;
                    stopSpeechAnimation();

                    if (message.split(";")[1].equals("startcmd")) {
                        flagAutoDecl = true;


                        vModel.getActivity().runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                startCmdVocal(false);
                            }
                        });

                    } else if (message.split(";")[1].equals("inconnue")
                            || message.split(";")[1].equals("batterie")
                            || message.split(";")[1].equals("withingsNoConnexion")
                            || message.split(";")[1].equals("chatBotNoConnexion")
                            || message.split(";")[1].equals("chatBotNoResponce")
                            || message.split(";")[1].equals("chatBotServerNoResponce")
                            || message.split(";")[1].equals("chatBotNotFound")
                            || message.split(";")[1].equals("googleNoNews")
                            || message.split(";")[1].equals("errorWiki")
                            || message.split(";")[1].equals("radioNotFound")
                            || message.split(";")[1].equals("musicNoFile")
                            || message.split(";")[1].equals("googleNoNews")
                    ) {
                        vModel.getActivity().runOnUiThread(new Runnable() {

                            @Override
                            public void run() {

                                if (flagAutoDecl)
                                    model.notifyObservers("screenOff");
                                flagNotPermanance = false;
                                if (flagMode3enabled == 1 && model.isTelepresence() == false) {
                                    vModel.getActivity().runOnUiThread(
                                            new Runnable() {

                                                @Override
                                                public void run() {

                                                    startCmdVocal(false);

                                                }
                                            });

                                }


                                if (!getData().equals("0")) {

                                    if (message.split(";")[1].equals("inconnue")
                                            || message.split(";")[1]
                                            .equals("withingsNoConnexion")
                                            || message.split(";")[1]
                                            .equals("chatBotNoConnexion")
                                            || message.split(";")[1]
                                            .equals("chatBotNoResponce")
                                            || message.split(";")[1]
                                            .equals("chatBotNotFound")
                                            || message.split(";")[1]
                                            .equals("googleNoNews")

                                            || message.split(";")[1]
                                            .equals("errorWiki")) {
                                        commandeResponce.setText("");
                                        if (remoteTimer == null) {
                                            remoteTimer = new Timer();

                                        } else {
                                            remoteTimer.cancel();
                                            remoteTimer = null;
                                            remoteTimer = new Timer();
                                        }
                                        try {
                                            Thread.sleep(200);
                                        } catch (InterruptedException e) {
                                            // TODO
                                            // Auto-generated
                                            // catch
                                            // block
                                            e.printStackTrace();
                                        }
                                        remoteTimer.schedule(
                                                new MyRemoteTaskSimple(),

                                                getfrequence());
                                    }
                                    flagAutoDecl = false;
                                }
                                if (model.isTelepresence() == true) {
                                    ServiceRobot.sendMessage(
                                            model.getPseudoOpenFireSender(), "call");
                                }
                            }
                        });

                    } else if (message.split(";")[1].equals("robotoff")
                            || message.split(";")[1].equals("withings")
                            || message.split(";")[1].equals("chatBot")
                            || (message.split(";")[0].equals("alexafinished") && message.split(";")[1].equals("true"))
                            || message.split(";")[1].equals("hexiwear")
                            || message.split(";")[1].equals("callnotavailable")
                            || message.split(";")[1].equals("googleNews")
                            || message.split(";")[1].equals("wikiResult")
                            || message.split(";")[1].equals("alexaResponse")) {
                        vModel.getActivity().runOnUiThread(new Runnable() {

                            @Override
                            public void run() {

                                geIsPermanance();
                                handle_ap.removeCallbacksAndMessages(null);
                                reStartComm();


                            }
                        });
                    } else if (message.split(";")[1].equals("rienEntendue")) {
                        vModel.getActivity().runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                rienEntendu();
                            }

                        });

                    } else if (message.split(";")[1].equals("rienEntendueSimple")) {

                        vModel.getActivity().runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                startCmdVocal(false);
                            }
                        });


                    }

                }
            }
            if (message.equals("treatement")) {
                treatment(result, true);
            }
            if (message.contains("message") && !message.contains("SpeechAPIComplete")) {

                speak(message.replace("message", ""), "");

            }
            if (message.contains("ttsconnexion") && !message.contains("SpeechAPIComplete")) {
                speak(message.replace("ttsconnexion", ""), "");
            }
            if (message.contains("hexiwearError")) {
                vControl.stopService();
                String msgError = deviceOwner + " " + failedConnexion;

                speak(msgError, "hexiwear");

            }
            if (message.contains("infodispo")) {
                // vControl.stopService();
                if (errorNoResult.equals(""))
                    for (int i = 0; i < vocalMsgs.size(); i++) {

                        if (vocalMsgs.get(i).getType()
                                .equals("errorDevice.noResult")) {
                            errorNoResult = vocalMsgs.get(i).getValue();
                        }

                    }

                speak(errorNoResult, "hexiwear");

            }
            if (message.contains("pseudonotfound")) {
                vModel.getActivity().runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        textDismiss();
                        speak(callError, "callnotavailable");
                    }
                });

            }
            if (message.contains("speek")) {

                String msgBatterie = model.getResources().getString(
                        R.string.BatterieResponse)
                        + " " + message.split(";")[1];
                // Cmdtext.setText(msgBatterie);
                // textDismiss();
                speak(msgBatterie, "batterie");

            }
            if (message.contains("acceuil") && !message.contains("SpeechAPIComplete")) {

                speak(message.replace("acceuil", ""), "bienvenue");

            }
            if (message.equals("hideTexts")) {
                mHandler.post(new Runnable() {

                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        if (Integer.parseInt(tmp) == 0) {
                            Cmdtext.clearComposingText();
                            Cmdtext.setText("");
                            commandeResponce.setText("");
                        } else {
                            messageAdapter.clear();
                            messagesView.setAdapter(null);
                        }

                    }
                });
                new Handler(vModel.getContext().getMainLooper())
                        .post(new Runnable() {
                            @Override
                            public void run() {
                                if (Integer.parseInt(tmp) == 0) {
                                    Cmdtext.setText("  ");
                                    commandeResponce.setText("       ");
                                } else {
                                    messageAdapter.clear();
                                    messagesView.setAdapter(null);
                                }
                            }
                        });
            }
            if (message.equals("showMic")) {
                if (model.getOptCmdVocal().equals("true"))
                    Cmdvocale.setVisibility(View.VISIBLE);
                microGif.setVisibility(View.VISIBLE);

            }
            if (message.equals("orientationChanged")) {
                if (LinphoneRegistration.lc != null) {
                    if (LinphoneRegistration.lc.getCalls().length >= 1)
                        replaceFragmentAudioByVideo();
                }
            }
            if (message.equals("endtelepresence")) {
                if (model.isMicMute())
                    model.enableOrDisableMic(false);

                vModel.getActivity().runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        if (!getData().equals("0")) {
                            if (remoteTimer == null) {
                                remoteTimer = new Timer();

                            } else {
                                remoteTimer.cancel();
                                remoteTimer = null;
                                remoteTimer = new Timer();
                            }
                            remoteTimer.schedule(new MyRemoteTaskSimple(),
                                    getfrequence());
                        }
                        geIsPermanance();
                        if (flagMode3enabled == 1) {
                            startpermanentRecognition();
                        }

                    }
                });
            }
            if (message.equals("istelepresence")) {
                // setVolume();

                model.getActivity().runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        mSpeechRecognitionService.destroyVoiceRecognition();
                        stopVoiceRecorder();
                        stopSpeechAnimationKey();
                        flagMode3enabled = 0;
                        if (call.equals("linphone"))
                            showVideoView();
                    }
                });

            }
            if (message.equals("restartVoice") || message.equals("callEnded")) {
                geIsPermanance();
                mSpeechRecognitionService.destroyVoiceRecognition();
                if (flagMode3enabled == 1 && model.isTelepresence() == false) {
                    if (message.equals("restartVoice")) {
                        initVocalMessages();
                        stopVoiceRecorder();
                        stopBlueMicRecognition();
                        stopPocketSphinxRecognition();

                    }


                    startpermanentRecognition();
                }
                if (!getData().equals("0")) {
                    if (remoteTimer == null) {
                        remoteTimer = new Timer();

                    } else {
                        remoteTimer.cancel();
                        remoteTimer = null;
                        remoteTimer = new Timer();
                    }
                    remoteTimer.schedule(new MyRemoteTaskSimple(),
                            getfrequence());
                }
            }
            if (message.equals("hideMic")) {
                Cmdvocale.setVisibility(View.INVISIBLE);
                microGif.setVisibility(View.INVISIBLE);
            }

            if (message.contains("ecrire") && !message.contains("SpeechAPIComplete")) {
                mHandler.post(new Runnable() {

                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        tmp = SmartRobotApplication
                                .getParamFromFile("Defilement",
                                        "Configuration.properties", null);
                        if (tmp == null) {
                            tmp = "1";
                        }
                        if (Integer.parseInt(tmp) == 0) {
                            Cmdtext.setVisibility(View.VISIBLE);
                            commandeResponce.setVisibility(View.VISIBLE);
                            messagesView.setVisibility(View.GONE);
                            Cmdtext.setText(message.split(";")[1]);
                        } else {
                            Cmdtext.setVisibility(View.GONE);
                            commandeResponce.setVisibility(View.GONE);
                            messagesView.setVisibility(View.VISIBLE);
                            chatList(message.split(";")[1], true);
                            //Cmdtext.setText(Cmdtext.getText() + " \n" + message.split(";")[1]);
                        }
                        model.logMe(message.split(";")[1], false, "/storage/emulated/0/" +
                                        "SmartRobot/CommandVocal/logCommandVocalSmart",
                                "logCommandVocalSmart", pathOfMyFile);

                    }
                });

            }
            if (message.contains("dire") && !message.contains("SpeechAPIComplete")) {
                vControl.getTTS().setLanguage(Locale.FRENCH);
                params.clear();
                params.put(TextToSpeech.Engine.KEY_PARAM_STREAM,
                        String.valueOf(AudioManager.STREAM_NOTIFICATION));
                String data = model.getSpeedTTS();
                if (data.equals(""))
                    Speedpos = 2;
                else
                    Speedpos = Integer.parseInt(data);

                if (Speedpos == 0) {

                    speed = 0.1f;
                } else if (Speedpos == 1) {

                    speed = 0.5f;

                } else if (Speedpos == 3) {

                    speed = 1.5f;

                } else if (Speedpos == 4) {

                    speed = 2.0f;

                } else {

                    speed = 1.0f;

                }
                vControl.getTTS().setSpeechRate(speed);
                vControl.getTTS().speak(message.split(";")[1],
                        TextToSpeech.QUEUE_FLUSH, params);
            }

            if (message.contains("speak;") && !message.contains("SpeechAPIComplete")) {
                speak(message.split(";")[1], "");
            }

        }
    }

    public void TonSucces() {
        found = true;
        if (flagNotPermanance == false) {

            String keyWord = SmartRobotApplication.getParamFromFile(
                    "vocale.mode.3.keyword", "configuration.properties", null);
            if (keyWord == null)
                keyWord = "ubo,alice,bonjour alice,bonjour alexa,alexa,ok google";

            if (keyWordInResult(keyWord, mResults)) {
                model.logMe("D�clenchement avec mot cl�", null, "/storage/emulated/0/" +
                                "SmartRobot/CommandVocal/logCommandVocalSmart",
                        "logCommandVocalSmart", pathOfMyFile);
                found = false;
                flagNotPermanance = true;
                vModel.getActivity().runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        startCmdVocal(false);

                    }
                });
                model.unlockScreen();

            } else {
                if (model.isTelepresence() == false) {
                    startpermanentRecognition();
                }
            }

        } else {
            model.unlockScreen();
            mResults = mResults.toLowerCase();
            if (mResults.equals("top"))
                mResults = mResults.replace("top", "stop");
            if (mResults.equals("sto"))
                mResults = mResults.replace("sto", "stop");
            if (mResults.equals("sauce"))
                mResults = mResults.replace("sauce", "stop");
            if (mResults.contains("golf"))
                mResults = mResults.replace("golf", "gauche");
            if (mResults.contains("bosse"))
                mResults = mResults.replace("bosse", "gauche");
            if (mResults.contains("grosse"))
                mResults = mResults.replace("grosse", "gauche");
            if (mResults.contains("avanc"))
                mResults = mResults.replace("mResults", "avancer");
            if (mResults.contains("recul"))
                mResults = mResults.replace("mResults", "reculer");
            if (mResults.contains("bc"))
                mResults = mResults.replace("bc", "baisser");
            if (mResults.contains("basket"))
                mResults = mResults.replace("basket", "baisser");
            if (mResults.contains("d�c�s"))
                mResults = mResults.replace("d�c�s", "baisser");
            if (mResults.equals("mon text"))
                mResults = mResults.replace("mon text", "monter");
            if (mResults.contains("mod�le"))
                mResults = mResults.replace("mod�le", "monter");
            if (mResults.contains("inventer"))
                mResults = mResults.replace("inventer", "avancer");
            if (mResults.contains("patri"))
                mResults = mResults.replace("patri", "batterie");
            if (mResults.contains("de ce mot"))
                mResults = mResults.replace("de ce mot", "doucement");
            if (mResults.contains("auteur"))
                mResults = mResults.replace("auteur", "hauteur");
            if (mResults.contains("calories"))
                mResults = mResults.replace("calories", "calorie");
            if (mResults.contains("calories"))
                mResults = mResults.replace("calories", "calorie");
            if (mResults.contains("iphone"))
                mResults = mResults.replace("iphone", "linphone");
            if (mResults.contains("l'iphone"))
                mResults = mResults.replace("l'iphone", "linphone");
            if (mResults.contains("l'linphone"))
                mResults = mResults.replace("l'linphone", "linphone");
            if (mResults.contains("lille"))
                mResults = mResults.replace("lille", "linphone");
            if (mResults.contains("lignephone"))
                mResults = mResults.replace("lignephone", "linphone");
            if (mResults.contains("ninphone"))
                mResults = mResults.replace("ninphone", "linphone");
            if (mResults.contains("mille dix"))
                mResults = mResults.replace("mille dix", "1010");

            String[] dataSeperation = mResults.split(" ");
            String lastString = dataSeperation[dataSeperation.length - 1];
            if (lastString.contains(".")) {
                String updatedString = lastString.replace(".", "");
                mResults = mResults.replace(lastString, updatedString);
            }

            msg = mResults;

            result = new ResultVocalObject();
            String isVocalControl = SmartRobotApplication.getParamFromFile(
                    "vocal.control", "Configuration.properties", null);
            if (isVocalControl.equals("0")) {
                result.setAction(msg);
                treatment(result, false);
            } else {
                try {
                    DocumentBuilderFactory factory = DocumentBuilderFactory
                            .newInstance();
                    DocumentBuilder builder = factory.newDocumentBuilder();
                    if (model.getLangageTTS().split("_")[0].equals("fr"))
                        f = new File("/sdcard/SmartRobot/CmdVocale_fr.xml");
                    else
                        f = new File("/sdcard/SmartRobot/CmdVocale_en.xml");

                    Document document = builder.parse(f.toURI().toString());

                    NodeList cmdVocalList = document.getDocumentElement()
                            .getChildNodes();

                    NodeList listDates;
                    String date = "";

                    Node device = cmdVocalList.item(1);

                    deviceOwner = GestionDesFichiers.getDevicesOwner(device);

                    NodeList listMsg = ((Element) cmdVocalList.item(0))
                            .getElementsByTagName("Message");
                    if (msg.contains("appeler") || msg.contains("call")) {

                        String[] data = vControl.getPseudo(msg).split(";");
                        if (data.length > 0) {
                            String pseudo = data[0];

                            String commande = msg.replace(data[1], "%");
                            makeVocalResult(cmdVocalList, 5, "call", commande);
                            ParamVocalObject pseudoParam = new ParamVocalObject(
                                    "pseudo", pseudo);
                            if (result.getParamsList() == null)
                                result.setParamsList(new ArrayList<ParamVocalObject>());
                            result.getParamsList().add(pseudoParam);

                        } else
                            result.setAction("Commande.inconnue");
                    } else if (msg.contains(deviceOwner.toLowerCase())) {

                        setSmartDevicesInfo(((Element) cmdVocalList.item(1))
                                .getElementsByTagName("Device"));

                        if (isDeviceActif) {
                            if (deviceType.equals("withings")) {

                                listDates = ((Element) cmdVocalList.item(3))
                                        .getElementsByTagName("Date");

                                if (listDates.getLength() != 0)
                                    date = GestionDesFichiers.datesParser(
                                            listDates, msg);

                                makeVocalResult(cmdVocalList, 3, "withings",
                                        msg);
                                result.setDate(date);
                            } else if (deviceType.equals("hexiwear")) {
                                flagHexiwear = true;
                                makeVocalResult(cmdVocalList, 4, "hexiwear",
                                        msg);

                            }

                        } else {
                            result.setAction("noDeviceActif");

                        }

                    } else if (msg.contains("radio")) {

                        NodeList listChannels = ((Element) cmdVocalList.item(6))
                                .getElementsByTagName("Channel");

                        makeVocalResult(cmdVocalList, 6, "radio", msg);

                        String name = "";
                        if (msg.indexOf("radio") + 6 < msg.length()) {
                            name = msg.substring(msg.indexOf("radio") + 6);
                            result.getParamsList().add(
                                    new ParamVocalObject("channelName", name));

                            ParamVocalObject objChannel = GestionDesFichiers
                                    .channelParser(listChannels, name);
                            if (objChannel != null)
                                result.getParamsList().add(objChannel);

                        }

                    } else if (msg.contains("jouer") || msg.contains("play")) {

                        makeVocalResult(cmdVocalList, 7, "music", msg);

                        result.getParamsList().add(
                                new ParamVocalObject("musicFile", msg
                                        .split(" ")[1]));
                    } else if (msg.contains("news") || msg.contains("nouvelles")) {

                        makeVocalResult(cmdVocalList, 8, "news", msg);
                    } else if (msg.contains("wiki")) {

                        makeVocalResult(cmdVocalList, 9, "wiki", msg);
                        String search = "";
                        if (msg.indexOf("wiki") + 5 < msg.length()) {
                            search = msg.substring(msg.indexOf("wiki") + 5);
                            ParamVocalObject objSearch = new ParamVocalObject(
                                    "search", search);
                            if (objSearch != null)
                                result.getParamsList().add(objSearch);
                        }
                    } else {
                        makeVocalResult(cmdVocalList, 2, "robot", msg);

                    }

                    treatment(result, false);
                } catch (IOException e) {
                    Log.v("voice", "IOException e");
                    e.printStackTrace();
                } catch (ParserConfigurationException e) {
                    Log.v("voice", "ParserConfigurationException e");
                    e.printStackTrace();
                } catch (SAXException e) {
                    Log.v("voice", "SAXException e");
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                    Log.v("voice", "NullPointerException e");
                    result.setAction(msg);
                }
            }
        }

    }

    private void initVocalMessages() {
        // TODO Auto-generated method stub

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            if (model.getLangageTTS().contains("fr"))
                f = new File("/sdcard/SmartRobot/CmdVocale_fr.xml");
            else
                f = new File("/sdcard/SmartRobot/CmdVocale_en.xml");

            Document document = builder.parse(f.toURI().toString());

            NodeList cmdVocalList = document.getDocumentElement()
                    .getChildNodes();

            NodeList listMsg = ((Element) cmdVocalList.item(0))
                    .getElementsByTagName("Message");

            vocalMsgs = GestionDesFichiers.msgParser(listMsg, model);

            for (int i = 0; i < vocalMsgs.size(); i++) {
                if (vocalMsgs.get(i).getType().equals("Message.invitation")) {
                    message_invitation = vocalMsgs.get(i).getValue();
                }
                if (vocalMsgs.get(i).getType().equals("Commande.rienEntendue")) {
                    rienEntendue = vocalMsgs.get(i).getValue();
                }
                if (vocalMsgs.get(i).getType().equals("alexa.noResponce")) {
                    alexanoResponce = vocalMsgs.get(i).getValue();
                }
                if (vocalMsgs.get(i).getType().equals("Commande.inconnue")) {
                    commandeInconue = vocalMsgs.get(i).getValue();
                }
                if (vocalMsgs.get(i).getType().equals("errorDevice.noDeviceActif")) {
                    errorNoDeviceActif = vocalMsgs.get(i).getValue();
                }
                if (vocalMsgs.get(i).getType().equals("errorDevice.noInfo")) {
                    errorNoInfo = vocalMsgs.get(i).getValue();
                }
                if (vocalMsgs.get(i).getType().equals("errorDevice.noResult")) {
                    errorNoResult = vocalMsgs.get(i).getValue();
                }
                if (vocalMsgs.get(i).getType().equals("errorDevice.noInternet")) {
                    errorNoInternet = vocalMsgs.get(i).getValue();
                }
                if (vocalMsgs.get(i).getType().equals("Hexiwear.failedConnexion")) {
                    failedConnexion = vocalMsgs.get(i).getValue();
                }
                if (vocalMsgs.get(i).getType().equals("error.noRobot")) {
                    errorNoRobot = vocalMsgs.get(i).getValue();
                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void makeVocalResult(NodeList cmdVocalList, int j, String type,
                                 String msg) {
        // TODO Auto-generated method stub

        NodeList listWithingCmd = ((Element) cmdVocalList.item(j))
                .getElementsByTagName("Cmd");
        NodeList listParams = ((Element) cmdVocalList.item(j))
                .getElementsByTagName("Param");
        NodeList listMessages = ((Element) cmdVocalList.item(j))
                .getElementsByTagName("Message");
        result = GestionDesFichiers.cmdParser(listWithingCmd, type, msg);
        if (listMessages.getLength() != 0)
            result.setMessagesList(GestionDesFichiers.msgParser(listMessages,
                    model));
        if (listParams.getLength() != 0)
            result.setParamsList(GestionDesFichiers.paramParser(listParams));
    }

    private boolean keyWordInResult(String keyword, String result) {
        // TODO Auto-generated method stub
        boolean isIn = false;
        String keywordList[] = keyword.split(",");

        for (int i = 0; i < keywordList.length; i++) {
            Log.i("mbu", "---result=" + result + " --keyWord=" + keywordList[i]);
            if (vControl.isEqualPhonetic(result, keywordList[i])) {
                isIn = true;
                break;
            }
        }

        return isIn;
    }

    private int resultWordsCount(String result) {
        // TODO Auto-generated method stub
        int wordsNbr = 1;
        if (result.split(" ").length != 0) {
            wordsNbr = result.split(" ").length;
        }
        return wordsNbr;
    }

    private void setSmartDevicesInfo(NodeList listDevice) {
        // TODO Auto-generated method stub

        isDeviceActif = false;

        deviceListObject = GestionDesFichiers.deviceParser(listDevice);

        for (int l = 0; l < deviceListObject.size(); l++) {

            if (deviceListObject.get(l).getDeviceType().equals("hexiwear")) {
                deviceName = deviceListObject.get(l).getDeviceName();
            }

            if (msg.contains(deviceListObject.get(l).getDeviceName())
                    && deviceListObject.get(l).getDeviceState().equals("1")) {
                deviceName = deviceListObject.get(l).getDeviceName();
                isDeviceActif = true;
                deviceMac = deviceListObject.get(l).getDeviceMac();
                deviceType = deviceListObject.get(l).getDeviceType();

            }
        }

    }


    public void TonError() {
        if (flagMode3enabled == 1 && flagNotPermanance == false) {
            if (model.isTelepresence() == false) {
                startpermanentRecognition();
            }
        } else if (flagIsPermance == false) {

            GestionDesFichiers.log("Commande.rienEntendue ", rienEntendue,
                    "Log_SmartRobot");

            if (i == processusSequence - 1) {
                //speak(rienEntendue, "rienEntendue");
                vModel.getActivity().runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        rienEntendu();
                    }

                });
                i = 0;
            } else {
                if (flagMode3enabled == 1) {

                    if (i == 0) {
                        if (found == false) {
                            speak(rienEntendue, "rienEntendueSimple");
                        } else {
                            vModel.getActivity().runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    startCmdVocal(false);
                                }
                            });
                        }
                    } else {

                        vModel.getActivity().runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                startCmdVocal(false);
                            }
                        });
                    }
                }
                i = i + 1;
            }
        }

    }

    private class MyRemoteTaskSimple extends TimerTask {

        public MyRemoteTaskSimple() {
            // TODO Auto-generated constructor stub
        }

        @Override
        public void run() {

            GestionDesFichiers.log("AutoDeclenchement", "AutoDeclenchement",
                    "Log_SmartRobot");
            found = false;
            startAutoDeclen();

        }

    }

    private void startAutoDeclen() {
        // TODO Auto-generated method stub
        model.logMe("D�clenchement p�riodique", null, "/storage/emulated/0/" +
                        "SmartRobot/CommandVocal/logCommandVocalSmart",
                "logCommandVocalSmart", pathOfMyFile);
        model.disableKeyGuard();
        flagNotPermanance = true;
        initVocalMessages();
        vModel.getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {

                speak(message_invitation, "startcmd");

            }
        });

    }

    public void stopRadioOrMusic() {
        if (model.isMusicPlaying()) {
            model.stopMusic();
        }
        if (model.isRadioPlaying()) {
            model.stopRadio();
        }
        stop_music.setVisibility(View.GONE);
    }

    public void destroy() {
        model.removeObserver(this);
        stopRadioOrMusic();
        if (model.getVolumeMedia().equals(""))
            audioManager
                    .setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0);
        else
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                    Integer.parseInt(model.getVolumeMedia()), 0);
        destroy = true;
        msg = "";
        if (remoteTimer != null) {
            remoteTimer.cancel();
            remoteTimer = null;

        }
        i = 0;
        if (Integer.parseInt(tmp) == 0) {
            Cmdtext.setText("");
            commandeResponce.setText("");
        } else {
            messageAdapter.clear();
            messagesView.setAdapter(null);
        }

        mSpeechRecognitionService.stopSelf();
        Intent servcice = new Intent(model.getActivity(),
                SpeechRecognitionService.class);
        model.getActivity().stopService(servcice);
        model.setVolume();

        stopPocketSphinxRecognition();
        if (mConnectedNode != null) {
            mConnectedNode.disableNotification(mAudio);
            mConnectedNode.disableNotification(mAudioSync);
            mConnectedNode.removeNodeStateListener(mNodeStatusListener);
            mAudio.removeFeatureListener(mAudioListener);
            mAudio.removeFeatureListener(mAudioSyncListener);
            mManager.removeListener(BlueMicDiscovery);
            cancelBlueMicDiscovery();
            mConnectedNode.disconnect();
        }
        onStop();
        handle_ap.removeCallbacksAndMessages(null);

    }

    /**
     * Arreter le task du scan periodique pour le BlueMic
     */
    public void cancelBlueMicDiscovery() {
        // TODO Auto-generated method stub
        if (mManager.isDiscovering())
            mManager.stopDiscovery();
        handler.removeCallbacks(mBlueMicDiscover);
    }

    public static void Messg() {
        //Cmdtext.setText("");
    }

    public void textDismiss() {

        hand.postDelayed(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                if (Integer.parseInt(tmp) == 0) {
                    Cmdtext.setText("");
                    commandeResponce.setText("");
                }


            }
        }, Integer
                .parseInt(SmartRobotApplication.getParamFromFile(
                        "delai.affichage", "Configuration.properties", null)
                        .split(" ")[0]) * 1000);
    }

    public void startCmdVocal(final Boolean isEnabled) {
        resultGot = false;
        model.cancelPandorabotTask();
        stopPermanentRecognition();
        if (model.isTelepresence() == true) {
            vControl.terminateCall();
            hand.postDelayed(new Runnable() {

                @Override
                public void run() {
                    // TODO Auto-generated method stub

                    CommVocal(isEnabled);
                }

            }, 5000);
        } else
            CommVocal(isEnabled);

    }

    private void CommVocal(final Boolean isEnabled) {
        // TODO Auto-generated method stub
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub

                if (model.isConnectedToInternet() && model.getIsCloudSpeech().equals("1")) {
                    if (keyWordType.equals("pocketsphinx")) {
                        if (recognizer != null)
                            recognizer.stop();
                    }
                    if (mSpeechRecognitionService != null)
                        mSpeechRecognitionService.destroyVoiceRecognition();
                    if (isEnabled) {
                        if (flagTts == false) {
                            flagIsPermance = isEnabled;
                            model.setVolume();
                            if (flagBlueMicOn) {
                                startBlueMicRecognition();
                            } else
                                startVoiceRecorder();

                            startSpeechAnimationKeyWord();

                            Log.i("mic", "-----isEnabled-----test----" + test + "; flagTts: " + flagTts + "; resultGot: " + resultGot);


                            /**handle_ap.postDelayed(new Runnable() {
                            @Override public void run() {
                            // TODO Auto-generated method stub

                            if(flagBlueMicOn) {
                            stopSpeechAnimationKey();
                            stopBlueMicRecognition();
                            }
                            }

                            }, TimeSpeech*1000);**/

                        }
                    } else {
                        flagNotPermanance = true;
                        flagIsPermance = isEnabled;
                        firstBip.start();
                        if (flagBlueMicOn) {
                            startBlueMicRecognition();
                        } else
                            startVoiceRecorder();

                        startSpeechAnimationCmd();
                        //stopSpeechAnimationCmd();


                        handle_ap.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                // TODO Auto-generated method stub

                                //stopSpeechAnimationCmd();
                                //Log.i("mic","----test----"+test+"; flagTts: "+flagTts+"; resultGot: "+resultGot);

                                /**if(!flagBlueMicOn) {
                                 Log.i("mic","----test----"+test+"; flagTts: "+flagTts+"; resultGot: "+resultGot);
                                 if(!flagTts && !resultGot && test) {
                                 stopSpeechAnimationCmd();
                                 secondeBip.start();
                                 TonError();
                                 }

                                 }**/
								/*else{
									stopBlueMicRecognition();
								}*/


                            }

                        }, TimeSpeech * 1000);

                    }

                } else {
                    if (isEnabled) {
                        if (keyWordType.equals("pocketsphinx")) {
                            if (recognizer == null)
                                initPocketSphinxAsynTask();
                            else
                                startPocketSphinxRecognition();
                        } else {
                            if (flagTts == false) {
                                mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0);

                                model.getActivity().runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        flagIsPermance = isEnabled;
                                        if (mSpeechRecognitionService != null) {
                                            mSpeechRecognitionService.startRecognition(
                                                    mResultCallback, isEnabled);
                                            startSpeechAnimationKeyWord();
                                        }
                                    }
                                });

                            }
                        }
                    } else {
                        if (keyWordType.equals("pocketsphinx")) {
                            if (recognizer != null)
                                recognizer.stop();
                        }
                        model.setVolume();

                        model.getActivity().runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                flagIsPermance = isEnabled;
                                if (mSpeechRecognitionService != null) {
                                    mSpeechRecognitionService.startRecognition(
                                            mResultCallback, isEnabled);
                                    startSpeechAnimationCmd();
                                }
                            }
                        });

                    }
                }
                GestionDesFichiers
                        .log("onDone",
                                "mSpeechRecognitionService.startRecognition(mResultCallback) ==> bouton click",
                                "Log_SmartRobot");
            }

        }, responseTimout * 1000);
    }

    private void setCommdText(final TextView text, final String value) {

        model.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                text.setText(value);
            }
        });
    }

    public void treatment(ResultVocalObject resultObj, boolean hexResult) {
        String action = resultObj.getAction();
        ArrayList<MessageVocalObject> msgList = resultObj.getMessagesList();
        ArrayList<ParamVocalObject> paramList = resultObj.getParamsList();
        String date = resultObj.getDate();
        String accueilMsg = "";
        if (msgList != null)
            for (int i = 0; i < msgList.size(); i++) {
                if (msgList.get(i).getType().equals("accueil")) {
                    accueilMsg = msgList.get(i).getValue();
                    break;
                }
            }
        tmp = SmartRobotApplication
                .getParamFromFile("Defilement",
                        "Configuration.properties", null);
        if (tmp == null) {
            tmp = "1";
        }
        if (Integer.parseInt(tmp) == 0) {
            model.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Cmdtext.setVisibility(View.VISIBLE);
                    commandeResponce.setVisibility(View.VISIBLE);
                    messagesView.setVisibility(View.GONE);
                }
            });

            setCommdText(Cmdtext, msg);

        } else {
            model.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Cmdtext.setVisibility(View.GONE);
                    commandeResponce.setVisibility(View.GONE);
                    messagesView.setVisibility(View.VISIBLE);
                }
            });

            chatList(msg, true);
        }
        model.logMe(msg, false, "/storage/emulated/0/" +
                        "SmartRobot/CommandVocal/logCommandVocalSmart",
                "logCommandVocalSmart", pathOfMyFile);
        msg = "";
        i = 0;

        if (action.equals("noDeviceActif")) {

            speak(errorNoDeviceActif, "withings");
        } else if (action.equals("noInfo")) {
            if (Integer.parseInt(tmp) == 0) {
                Cmdtext.setVisibility(View.VISIBLE);
                commandeResponce.setVisibility(View.VISIBLE);
                messagesView.setVisibility(View.GONE);
                setCommdText(Cmdtext, msg);

            } else {
                Cmdtext.setVisibility(View.GONE);
                commandeResponce.setVisibility(View.GONE);
                messagesView.setVisibility(View.VISIBLE);
                chatList(msg, true);
            }
            model.logMe(msg, false, "/storage/emulated/0/" +
                            "SmartRobot/CommandVocal/logCommandVocalSmart",
                    "logCommandVocalSmart", pathOfMyFile);
            textDismiss();
            speak(errorNoInfo, "withings");

        } else if (action.equals("playMusic")) {

            for (int i = 0; i < msgList.size(); i++) {
                if (msgList.get(i).getType().equals("error")) {
                    errorNoMusicFound = msgList.get(i).getValue();
                    break;
                }
            }

            for (int i = 0; i < paramList.size(); i++) {
                if (paramList.get(i).getName().equals("musicFile")) {
                    fileName = paramList.get(i).getValue();
                }
                if (paramList.get(i).getName().equals("nbrMorceau")) {
                    nbrMorceau = Integer.parseInt(paramList.get(i).getValue());
                }
            }
            filePath = model.fileMusicExist(fileName);
            model.setNbrMorceau(nbrMorceau);
            model.setFilePath(filePath);
            if (filePath.size() != 0) {
                if (accueilMsg.contains("*"))
                    accueilMsg = accueilMsg.replace("*", fileName);
                speak(accueilMsg, "musicPlay");
                mHandler.postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        model.playMusic(filePath.get(0));
                        stop_music.setVisibility(View.VISIBLE);
                    }
                }, 3000);

            } else

                speak(errorNoMusicFound, "musicNoFile");

        } else if (action.equals("playRadio")) {

            for (int i = 0; i < paramList.size(); i++) {
                if (paramList.get(i).getName().equals("channelUrl")) {
                    chanelurl = paramList.get(i).getValue();
                }
                if (paramList.get(i).getName().equals("channelName")) {
                    chanelName = paramList.get(i).getValue();
                }
                if (paramList.get(i).getName().equals("duration")) {
                    duration = Integer.parseInt(paramList.get(i).getValue());
                }
            }

            for (int i = 0; i < msgList.size(); i++) {
                if (msgList.get(i).getType().equals("error")) {
                    errorNoRadioFound = msgList.get(i).getValue();
                    break;
                }
            }
            if (!chanelurl.equals("")) {
                if (accueilMsg.contains("*"))
                    accueilMsg = accueilMsg.replace("*", chanelName);
                speak(accueilMsg, "radioPlay");
                mHandler.postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        model.playRadioStation(chanelurl, duration);
                        stop_music.setVisibility(View.VISIBLE);
                        chanelurl = "";
                        chanelName = "";
                    }
                }, 3000);
                mSpeechRecognitionService.destroyVoiceRecognition();
                stopVoiceRecorder();

            } else

                speak(errorNoRadioFound.replace("*", chanelName),
                        "radioNotFound");

        } else if (action.equals("googleNews")) {
            String errorNoNews = "";
            int titreNbr = 2;
            String gNewsUrl = "";
            String apiKey = "";
            String chaineResult = "";

            for (int i = 0; i < msgList.size(); i++) {
                if (msgList.get(i).getType().equals("error")) {
                    errorNoNews = msgList.get(i).getValue();
                    break;
                }
            }
            for (int i = 0; i < paramList.size(); i++) {
                if (paramList.get(i).getName().equals("titleNbr")) {
                    titreNbr = Integer.parseInt(paramList.get(i).getValue());
                }
                if (paramList.get(i).getName().equals("GNewsUrl")) {
                    gNewsUrl = paramList.get(i).getValue();
                }
                if (paramList.get(i).getName().equals("GNewsApiKey")) {
                    apiKey = paramList.get(i).getValue();
                }

            }

            GNewsObject result = model.getGoogleNews(gNewsUrl, apiKey);
            if (result.getMessage().equals("Error GNews")) {

                speak(errorNoNews, "googleNoNews");

            } else {

                chaineResult += accueilMsg;
                for (int i = 0; i < titreNbr; i++) {

                    chaineResult += "   " + result.getReponse().get(i) + ";";

                }
                speak(chaineResult, "googleNews");

            }
        } else if (action.equals("wikiSearch")) {

            String errorWiki = "";
            String errorNoResult = "";
            String search = "";
            for (int i = 0; i < msgList.size(); i++) {
                if (msgList.get(i).getType().equals("error")) {
                    errorWiki = msgList.get(i).getValue();
                }
                if (msgList.get(i).getType().equals("errorNoResult")) {
                    errorNoResult = msgList.get(i).getValue();
                }
            }
            for (int i = 0; i < paramList.size(); i++) {
                if (paramList.get(i).getName().equals("search")) {
                    search = paramList.get(i).getValue();
                    break;
                }
            }

            final String result = model.wikiSearch(search);
            if (result.equals("Error wiki")) {

                speak(errorWiki, "errorWiki");

            } else if (result.equals("Error noResult")) {
                if (errorNoResult.contains("*"))
                    errorNoResult.replace("*", search);
                speak(errorNoResult, "errorWiki");
            } else {
                if (accueilMsg.contains("*"))
                    accueilMsg = accueilMsg.replace("*", search);
                speak(accueilMsg + "  " + result, "wikiResult");

            }

        } else if (action.contains("withings,")) {

            String response = "";
            String withingsUrl = "";
            String withingsConsumerKey = "";
            String withingsTokenKey = "";

            response = result.getResponse();

            for (int i = 0; i < paramList.size(); i++) {
                if (paramList.get(i).getName().equals("withings.url")) {
                    withingsUrl = paramList.get(i).getValue();
                }
                if (paramList.get(i).getName().equals("withings.ConsumerKey")) {
                    withingsConsumerKey = paramList.get(i).getValue();
                }
                if (paramList.get(i).getName().equals("withings.TokenKey")) {
                    withingsTokenKey = paramList.get(i).getValue();
                }
            }
            try {
                if (model.isConnectedToInternet()) {
                    String withingsResponse = "";

                    withingsResponse = SmartRobotApplication.getWithingsValues(
                            deviceOwner, withingsUrl, withingsConsumerKey,
                            withingsTokenKey, action.split(",")[1], response,
                            date.split(",")[0], date.split(",")[1]);

                    if (withingsResponse.equals("")) {
                        speak(errorNoResult, "withings");

                    } else {
                        speak(withingsResponse, "withings");

                    }
                } else {

                    speak(errorNoInternet, "withingsNoConnexion");

                }
            } catch (NotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        } else if (action.contains("hexiwear,")) {

            String response = "";

            String errorNoResult = "";
            for (int i = 0; i < vocalMsgs.size(); i++) {

                if (vocalMsgs.get(i).getType().equals("errorDevice.noResult")) {
                    errorNoResult = vocalMsgs.get(i).getValue();
                }

            }
            response = result.getResponse();
            if (!hexResult) {
                model.setMacAdressHexiwear(deviceMac);
                model.setHexiwearInfo(action);
                vControl.startService();
            } else {

                if (action.split(",")[1].contains("temperature")) {
                    String temp = vModel.getTemperature();
                    if (temp == null) {
                        speak(errorNoResult, "hexiwear");
                    } else {
                        String data = response.replace("%", temp);
                        if (model.getLangageTTS().contains("en"))
                            data = data + " Degree celsius";
                        else
                            data = data + " degr� celsius";

                        speak(data, "hexiwear");
                    }
                } else if (action.split(",")[1].contains("humidity")) {

                    String hum = vModel.getHumidity();
                    if (hum == null) {
                        speak(errorNoResult, "hexiwear");
                    } else {
                        String data = response.replace("%", hum);

                        speak(data, "hexiwear");
                    }
                } else if (action.split(",")[1].contains("heartbeat")) {
                    String cardiaque = vModel.getCardiaque();
                    if (cardiaque == null) {
                        speak(errorNoResult, "hexiwear");
                    } else {
                        String data = response.replace("%", cardiaque);

                        speak(data, "hexiwear");
                    }
                } else if (action.split(",")[1].contains("pression")) {

                    String pression = vModel.getPressure();
                    if (pression == null) {
                        speak(errorNoResult, "hexiwear");
                    } else {
                        String data = response.replace("%", pression);
                        speak(data, "hexiwear");

                    }

                } else if (action.split(",")[1].contains("steps")) {
                    String step = vModel.getStep();

                    if (step == null) {
                        speak(errorNoResult, "hexiwear");
                    } else {

                        String data = deviceOwner + " "
                                + response.replace("%", step);

                        speak(data, "hexiwear");
                    }
                } else if (action.split(",")[1].contains("light")) {
                    String light = vModel.getLight();
                    if (light == null) {
                        speak(errorNoResult, "hexiwear");
                    } else {
                        String data = response.replace("%", light);

                        speak(data, "hexiwear");
                    }
                } else if (action.split(",")[1].contains("calories")) {
                    String caloroies = vModel.getCalories();
                    if (caloroies == null) {
                        speak(errorNoResult, "hexiwear");
                    } else {
                        String data = deviceOwner + " "
                                + response.replace("%", caloroies);

                        speak(data, "hexiwear");
                    }
                } else if (action.split(",")[1].contains("battery")) {
                    String batterie = vModel.getBatterie();
                    if (batterie == null) {
                        speak(errorNoResult, "hexiwear");
                    } else {
                        String data = response.replace("%", batterie);

                        speak(data, "hexiwear");
                    }
                } else if (action.split(",")[1].contains("position")) {
                    String responce = response;
                    String[] data = responce.split("%");
                    String[] magnetReadings = vModel.getMagnetReadings();
                    String[] gyroscopeReadings = vModel.getGyroscopeReadings();
                    if (magnetReadings == null || gyroscopeReadings == null) {
                        speak(errorNoResult, "hexiwear");
                    } else {
                        String dataResponse = data[0] + " " + magnetReadings[0]
                                + " " + magnetReadings[1] + " " + data[2] + " "
                                + magnetReadings[2] + data[3] + " "
                                + gyroscopeReadings[0] + " "
                                + gyroscopeReadings[1] + " " + data[5] + " "
                                + gyroscopeReadings[2];

                        speak(dataResponse, "hexiwear");
                    }
                } else if (action.split(",")[1].contains("activity")) {
                    String[] data = response.split("%");
                    String[] accelerationReadings = vModel
                            .getAccelerationReadings();
                    if (accelerationReadings == null) {
                        speak(errorNoResult, "hexiwear");
                    } else {

                        String dataResponse = data[0] + " "
                                + accelerationReadings[0] + " "
                                + accelerationReadings[1] + " " + data[3] + " "
                                + accelerationReadings[2];

                        if (model.getLangageTTS().contains("en"))
                            dataResponse = dataResponse
                                    .replace("g", "severity");
                        else
                            dataResponse = dataResponse.replace("g", "gravit�");

                        speak(dataResponse, "hexiwear");
                    }
                } else {
                    commandeInconnue(action);
                }
            }
        } else if (action.contains("CallSkype")) {
            String pseudo = "";
            for (int i = 0; i < paramList.size(); i++) {
                if (paramList.get(i).getName().equals("pseudo")) {
                    pseudo = paramList.get(i).getValue();
                    break;

                }
            }

            for (int i = 0; i < msgList.size(); i++) {

                if (msgList.get(i).getType().equals("call.error")) {
                    callError = msgList.get(i).getValue();
                }

            }
            vControl.callSkype(pseudo);
            textDismiss();
            mSpeechRecognitionService.startHandler();
        } else if (action.contains("CallLinphone")) {
            String pseudo = "";
            for (int i = 0; i < paramList.size(); i++) {
                if (paramList.get(i).getName().equals("pseudo")) {
                    pseudo = paramList.get(i).getValue();
                    break;
                }
            }
            for (int i = 0; i < msgList.size(); i++) {

                if (msgList.get(i).getType().equals("call.error")) {
                    callError = msgList.get(i).getValue();
                }

            }
            mSpeechRecognitionService.destroyVoiceRecognition();
            stopVoiceRecorder();
            model.call(pseudo);
            textDismiss();

        } else {

            if (action.equals("batterie")
                    && (model.getRobotType() == 1 || model.getRobotType() == 6)) {
                model.notifyObservers("voiceRegonisation");
            } else if (action.equals("HEAD U")) {
                if (model.getRobotConnexion().equals("true")) {
                    model.notifyObservers("HEAD;U");
                    textDismiss();
                } else {
                    speakRobotOff();
                }
            } else if (action.equals("HEAD D")) {
                if (model.getRobotConnexion().equals("true")) {
                    model.notifyObservers("HEAD;D");
                    textDismiss();
                } else {
                    speakRobotOff();
                }

            } else if (action.equals("HEAD R")) {
                if (model.getRobotConnexion().equals("true")) {
                    model.notifyObservers("HEAD;R");
                    textDismiss();
                } else {
                    speakRobotOff();
                }
            } else if (action.equals("HEAD L")) {
                if (model.getRobotConnexion().equals("true")) {
                    model.notifyObservers("HEAD;L");
                    textDismiss();
                } else {
                    speakRobotOff();
                }
            } else if (action.equals("MOVE;0;0")) {
                if (model.isMusicPlaying()) {
                    stopRadioOrMusic();
                    speak("", "musicPlay");
                } else if (model.isRadioPlaying()) {
                    stopRadioOrMusic();
                    speak("", "radioPlay");
                } else {
                    if (model.getRobotConnexion().equals("true")) {
                        model.notifyObservers("arreter;avancer");
                        textDismiss();
                    } else {
                        speakRobotOff();
                    }
                }
            } else {
                String[] data = action.split(";");
                if (data.length > 2) {
                    if (data[2].equals("0")) {
                        if (model.getRobotConnexion().equals("true")) {
                            if (data[1].contains("-VL")) {
                                msg = action.replace("MOVE", "arriereB");
                                model.notifyObservers(msg);

                            } else {
                                msg = action.replace("MOVE", "devantB");
                                model.notifyObservers(msg);

                            }
                            textDismiss();
                        } else {
                            speakRobotOff();
                        }
                    } else if (data[1].equals("0")) {
                        if (model.getRobotConnexion().equals("true")) {
                            if (data[2].contains("-VA")) {
                                if (model.getOptFaceToFace().equals("false"))
                                    msg = action.replace("MOVE", "droiteB");
                                else
                                    msg = action.replace("MOVE", "gaucheB");
                                model.notifyObservers(msg);

                            } else {
                                if (model.getOptFaceToFace().equals("false"))
                                    msg = action.replace("MOVE", "gaucheB");
                                else
                                    msg = action.replace("MOVE", "droiteB");
                                model.notifyObservers(msg);
                            }
                            textDismiss();
                        } else {
                            speakRobotOff();
                        }
                    } else {
                        commandeInconnue(action);
                    }
                } else {
                    commandeInconnue(action);

                }
            }

            if (!msg.equals(model.getMessage("Commande.inconnue"))
                    && !action.contains("hexiwear,")
                    && !action.contains("Call") && !action.contains("batterie")
                    && model.getRobotConnexion().equals("true")
                    && !msg.equals("chatBot") && !msg.equals("wikiResult")
                    && !msg.equals("googleNews") && !msg.equals("musicPlay")
                    && !msg.equals("radioPlay")) {

                handle_ap.removeCallbacksAndMessages(null);
                reStartComm();


            }
        }

    }

    public void treat() {

    }

    private void reStartComm() {
        // TODO Auto-generated method stub
        if (flagAutoDecl == true) {
            startAutoDeclen();
        } else {
            if (!getData().equals("0")) {
                if (remoteTimer == null) {
                    remoteTimer = new Timer();

                } else {
                    remoteTimer.cancel();
                    remoteTimer = null;
                    remoteTimer = new Timer();
                }
                remoteTimer.schedule(new MyRemoteTaskSimple(), getfrequence());
            }
            if (flagMode3enabled == 1) {
                vModel.getActivity().runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                        startCmdVocal(false);
                    }
                });
            }
        }
    }

    private void commandeInconnue(String message) {
        // TODO Auto-generated method stub
        if (SmartRobotApplication.getParamFromFile("chatbot.actif",
                "Configuration.properties", null).equals("0")) {

            speak(commandeInconue, "inconnue");
        } else {
            msg = "chatBot";
            String chatBotResponse = "";
            String botId = "";
            if (model.getchatBotType().equals("botlibre")) {
                Boolean connect = model.connectToBotLibre();
                if (connect) {
                    botId = model.selectBotLibre();
                    if (botId.equals("")) {
                        speak(model.getResources().getString(
                                R.string.chatBotNoFound), "chatBotNotFound");

                    } else {
                        chatBotResponse = model
                                .botLibreSendChat(botId, message);
                        speak(chatBotResponse, "chatBot");

                    }
                } else {

                    speak(model.getResources().getString(
                            R.string.chatBotNoConnexion), "chatBotNoConnexion");

                }

            } else if (model.getchatBotType().equals("pandorabot")) {
                chatBotResponse = model.pandorabotSendChat(message);
                if (chatBotResponse.equals("Network unreachable")) {

                    speak(model.getResources().getString(
                            R.string.chatBotNoConnexion), "chatBotNoConnexion");

                } else if (chatBotResponse.equals("Failed to find bot")) {
                    speak(model.getResources().getString(
                            R.string.chatBotNoFound), "chatBotNotFound");
                } else if (chatBotResponse.equals("NoResponseServer")) {
                    speak(model.getResources().getString(
                            R.string.chatBotServerNoResponce),
                            "chatBotNoResponce");
                } else {
                    speak(chatBotResponse, "chatBot");

                }

            } else if (model.getchatBotType().equals("alexa") && model.getLangageTTS().contains("en")) {
                flagTts = true;
                alexaManager.sendTextRequest(message, getRequestCallback());


            } else {
                chatBotResponse = model.chatbotSendChat(message);
                if (chatBotResponse.equals("Network unreachable")) {

                    speak(model.getResources().getString(
                            R.string.chatBotNoConnexion), "chatBotNoConnexion");

                } else if (chatBotResponse.equals("Failed to find bot")) {
                    speak(model.getResources().getString(
                            R.string.chatBotNoFound), "chatBotNotFound");
                } else if (chatBotResponse.equals("NoResponseServer")) {
                    speak(model.getResources().getString(
                            R.string.chatBotServerNoResponce),
                            "chatBotNoResponce");
                } else {
                    speak(chatBotResponse, "chatBot");

                }
            }

        }
    }

    private void speakRobotOff() {
        // TODO Auto-generated method stub
        speak(errorNoRobot, "robotoff");

    }

    public void speak(final String message, final String indicateur) {
        stopSpeechAnimationCmd();
        if (!indicateur.equals("") && !indicateur.equals("ttsconnexion") && !indicateur.equals("bienvenue"))
            startSpeechAnimation();
        params.clear();
        mHandler.post(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                initVocalMessages();
                model.setVolume();
                vControl.getTTS()
                        .setLanguage(new Locale(model.getLangageTTS()));
                vControl.getTTS().setSpeechRate(1);
                params.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID,
                        indicateur);

                flagTts = true;
                vControl.getTTS().speak(message, TextToSpeech.QUEUE_FLUSH,
                        params);
                String extraData;
                String message1 = null;
                if (model.getLangageTTS().contains("en"))
                    extraData = "Degree celsius";
                else
                    extraData = "degr� celsius";
                if (message.contains(extraData)) {
                    message1 = message.replace(extraData, "");
                } else {

                    if (model.getLangageTTS().contains("en"))
                        extraData = "severity";
                    else
                        extraData = "gravit�";
                    if (message.contains(extraData))
                        message1 = message.replace(extraData, "g");
                    else
                        message1 = message;
                }

                if (Integer.parseInt(tmp) == 1) {

                    messagesView.setVisibility(View.VISIBLE);
                    Cmdtext.setVisibility(View.GONE);
                    commandeResponce.setVisibility(View.GONE);
                    chatList(message1, false);
                } else {
                    commandeResponce.setVisibility(View.VISIBLE);
                    Cmdtext.setVisibility(View.VISIBLE);
                    messagesView.setVisibility(View.GONE);
                    commandeResponce.setText(message1);
                    textDismiss();

                }

                if (message.contains(message_invitation)) {
                    model.logMe("Message d'invitation:" + message, null, "/storage/emulated/0/" +
                                    "SmartRobot/CommandVocal/logCommandVocalSmart",
                            "logCommandVocalSmart", pathOfMyFile);
                } else {
                    model.logMe(message1, true, "/storage/emulated/0/" +
                                    "SmartRobot/CommandVocal/logCommandVocalSmart",
                            "logCommandVocalSmart", pathOfMyFile);
                }


            }
        });

    }

    //Aficher le defilement du texte sur une liste
    public void chatList(String text, final boolean side) {
        if (text == null) {
            text = "...";
        }
        final String text1 = text;
        mHandler.post(new Runnable() {

            @Override
            public void run() {
                messagesView.setVisibility(View.VISIBLE);
                messageAdapter.add(new Message(side, text1.toString()));
                messageAdapter.notifyDataSetChanged();
                messageAdapter.registerDataSetObserver(new DataSetObserver() {
                    @Override
                    public void onChanged() {
                        super.onChanged();
                        if (messagesView.getAdapter() == null) {
                            messagesView.setAdapter(messageAdapter);
                        }
                        messagesView.setSelection(messagesView.getAdapter().getCount() - 1);
                    }
                });

            }
        });

        Handler.removeCallbacks(mHandlerTask);
        Handler.postDelayed(mHandlerTask, Integer.parseInt(textDissmisDefil) * 1000);

    }

    public void removeObserver() {
        // TODO Auto-generated method stub
        model.removeObserver(this);
    }

    private void showVideoView() {
        replaceFragmentAudioByVideo();
        setCallControlsVisibleAndRemoveCallbacks();
        model.notifyObservers("captureView");

    }

    private void replaceFragmentAudioByVideo() {
        videoCallFragment = new VideoCallFragment(vModel.getContext());

        FragmentTransaction transaction = model.getActivity().getFragmentManager()
                .beginTransaction();

        transaction.replace(R.id.fragmentContainer1, videoCallFragment);

        try {
            transaction.commitAllowingStateLoss();
        } catch (Exception e) {
        }

    }

    public void setCallControlsVisibleAndRemoveCallbacks() {
        if (mControlsHandler != null && mControls != null) {
            mControlsHandler.removeCallbacks(mControls);
        }
        mControls = null;

    }

    public class ScreenReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Intent intentService = new Intent(vModel.getContext(),
                    SpeechRecognitionService.class);

            if (!model
                    .isServiceRunning("com.orrobotics.orsmartrobot.service.SpeechRecognitionService")) {
                vModel.getContext().startService(intentService);
                vModel.getContext().bindService(intentService,
                        mServiceConnection, Context.BIND_AUTO_CREATE);
            }

            if (flagMode3enabled == 1 && model.isTelepresence() == false
                    && flagNotPermanance == false) {
                startpermanentRecognition();
            }

        }

    }


    @Override
    public void onBeginningOfSpeech() {
    }

    @Override
    public void onEndOfSpeech() {
    }

    @Override
    public void onPartialResult(Hypothesis hypothesis) {
        if (hypothesis == null)
            return;
        if (recognizer != null) {
            String text = hypothesis.getHypstr();
            stopSpeechAnimationKey();
            if (recognizer != null) {
                recognizer.stop();
            }
            flagNotPermanance = true;
            vModel.getActivity().runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    startCmdVocal(false);

                }
            });
        }
    }

    @Override
    public void onResult(Hypothesis hypothesis) {
        if (hypothesis != null) {
        }
    }

    @Override
    public void onError(Exception e) {
    }

    @Override
    public void onTimeout() {

    }

    /**
     * D�marrer l'animation de l'�coute du mot cl�
     */
    private void startSpeechAnimationKeyWord() {
        vModel.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (animationCmd.isRunning())
                    animationCmd.stop();
                Cmdvocale.setImageDrawable(model.getResources().getDrawable(R.drawable.micro_vocal_icone));
                microGif.setImageDrawable(animationKeyWord);
                animationKeyWord.start();
            }
        });
    }

    /**
     * D�marrer l'animation de l'�coute de la commande
     */
    private void startSpeechAnimationCmd() {
        vModel.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (animationKeyWord.isRunning())
                    animationKeyWord.stop();
                Cmdvocale.setImageDrawable(model.getResources().getDrawable(R.drawable.micro_red));
                microGif.setImageDrawable(animationCmd);
                animationCmd.start();
            }
        });
    }

    /**
     * D�marrer l'animation de la r�ponse de la commande
     */
    private void startSpeechAnimation() {
        vModel.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (animationCmd.isRunning())
                    animationCmd.stop();
                Cmdvocale.setImageDrawable(model.getResources().getDrawable(R.drawable.micro_blue));
                microGif.setImageDrawable(animationResponse);
                animationResponse.start();
            }
        });
    }

    /**
     * Arreter l'animation de l'�coute du mot cl�
     */
    private void stopSpeechAnimationKey() {
        vModel.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                animationKeyWord.stop();
            }
        });
    }

    /**
     * Arreter l'animation de l'�coute de la commande
     */
    private void stopSpeechAnimationCmd() {
        vModel.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                animationCmd.stop();
            }
        });
    }

    /**
     * Arreter l'animation de la r�ponse
     */
    private void stopSpeechAnimation() {
        vModel.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                animationResponse.stop();
            }
        });
    }
}