package com.orrobotics.orsmartrobot.view;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.orrobotics.orsmartrobot.R;
import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.controller.ListRobotController;
import com.orrobotics.orsmartrobot.controllerImpl.ListRobotArrayAdapter;
import com.orrobotics.orsmartrobot.model.ListRobotModel;
import com.orrobotics.orsmartrobot.observer.OrSmarRobotObserver;
import com.orrobotics.orsmartrobot.ui.CircularProgress;
import com.orrobotics.orsmartrobot.util.GestionDesFichiers;
import com.orrobotics.orsmartrobot.util.RemoteControlUtil;
import com.orrobotics.popup.SweetAlertDialog;
import com.orrobotics.webservice.WebService;
import com.orrobotics.webservice.url.urlParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Hajiba IFRAH
 * @version 2.1.0
 * @date 18/05/2015
 *
 */

public class ListRobotView implements OrSmarRobotObserver {

	ListRobotController vController;
	ListRobotModel vModel;
	ListRobotArrayAdapter adapter;

	SmartRobotApplication application;

	TextView email;
	TextView nom;
	ListView listView;

	String JsonList = null;

	Boolean flagJson = false;
	Boolean flag = true;

	Intent intentControle;
	int lenght = 0;

	CircularProgress progress;

	/**
	 * Récupérer et affecter les paramètres initialiser dans la classe ListRobot
	 *
	 */

	public ListRobotView(ListRobotController controller, ListRobotModel model) {

		vController = controller;
		vModel = model;
		application = (SmartRobotApplication) vModel.getContext();
		application.registerObserver(ListRobotView.this);

	}

	public void setFlagRefresh(boolean refresh) {
		// TODO Auto-generated method stub
		vModel.setFlagRefresh(refresh);
	}

	/**
	 * Gestion des événements
	 *
	 */
	public void setListeners() {
		// TODO Auto-generated method stub
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, final View v,
									final int position, final long id) {
				int index = vModel.getIndexPseudo().get(position);

				String robotadresse = vModel.getRobotAdresses().get(index);
				GestionDesFichiers.log("l'adresse Mac du robot sélectionné ",
						robotadresse, "Log_SmartRobot");

				if (robotadresse.equalsIgnoreCase("null")) {

					new SweetAlertDialog(vModel.getActivity(),
							SweetAlertDialog.ERROR_TYPE)
							.setTitleText("Erreur...")
							.setContentText("Scanner tout d'abord votre robot")
							.show();

				}

				else {
					// hand.removeCallbacks(run);
					application.setEditor(
							RemoteControlUtil.EXTRAS_DEVICE_ADDRESS,
							robotadresse);
					String pseudoOpenFire = vModel.getPseudoOpenFire().get(
							index);
					int identifiant = vModel.getIdRobot().get(index);
					application.setEditor(RemoteControlUtil.PSEUDO_OPEN_FIRE,
							pseudoOpenFire);
					application.setEditor(RemoteControlUtil.ID,
							Integer.toString(identifiant));
					application.initRobotInternParams(Integer
							.toString(identifiant));
					application.setPseudoOpenFire(pseudoOpenFire);

					String passwordOpenFire = vModel.getPasswordOpenFire().get(
							index);
					application.setEditor(RemoteControlUtil.PASSWORD_OPEN_FIRE,
							passwordOpenFire);
					application.setPasswordOpenFire(passwordOpenFire);
					String pseudoSIP = vModel.getPseudoSIP().get(index);
					application.setEditor(RemoteControlUtil.PSEUDO_SIP,
							pseudoSIP);
					application.setPseudoSIP(pseudoSIP);
					String passwordSIP = vModel.getPasswordSIP().get(index);
					application.setEditor(RemoteControlUtil.PASSWORD_SIP,
							passwordSIP);
					application.setPasswordSIP(passwordSIP);
					String mac = vModel.getRobotNames().get(index);
					application.setEditor(RemoteControlUtil.EXTRAS_DEVICE_NAME,
							mac);
					application.setAdresseMAC(mac);
					String numeroSerie = vModel.getRobotSerie().get(index);
					application.setNumeroSerie(numeroSerie);
					application.setEditor(
							RemoteControlUtil.EXTRAS_DEVICE_SERIE, numeroSerie);
					application.setEditor(RemoteControlUtil.ROBOT_NAME, vModel
							.getRobotNames().get(index));
					application.initInternParams();

					application.StartAct(null, null, ControlRobot.class,
							application.getActivity());

					vModel.getActivity().finish();

				}

			}
		});

	}

	/**
	 * Récupérer et affecter les paramètres initialiser dans la classe ListRobot
	 *
	 *
	 *
	 */
	public void setWidgets(TextView email, TextView nom, ListView listView,
						   CircularProgress progress) {
		// TODO Auto-generated method stub

		this.email = email;
		this.nom = nom;
		this.listView = listView;

		this.progress = progress;
		/**
		 * Affichage d'Email et le nom
		 *
		 */

		this.email.setText(application.getEmail());
		this.nom.setText(application.getNom());

		init();

	}

	/**
	 * Les traitements réalisées dès le démarrage de l'interface
	 *
	 */
	private void init() {
		// TODO Auto-generated method stub
		/**
		 * Récupérer la liste des robots via le site espace client dans le cas
		 * ou l'interface de la listes des robots n'est pas consulté directement
		 * après l'interface de l'authentification
		 */
		GestionDesFichiers.log("refresh flag", "" + vModel.getFlagRefresh(),
				"Log_SmartRobot");

		if (vModel.getFlagRefresh() == true) {
			refresh();
		}

		else {
			vModel.clearData();
			if (application.getJsonList() != null) {
				lenght = vController.getlength();
			} else {

				vModel.getActivity().finish();
			}

			if (lenght < 1) {
				vModel.getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						Toast.makeText(
								vModel.getActivity(),
								vModel.getContext().getResources()
										.getString(R.string.addRobot),
								Toast.LENGTH_SHORT).show();
					}
				});

			}

			else

				vController.setData(lenght);
			afficherListrobot();
		}

	}

	private void afficherListrobot() {
		// TODO Auto-generated method stub
		/**
		 * Affichage de la liste des robots
		 */
		String icon = application.getRobotIcone();
		byte[] bm = icon.getBytes();
		byte[] b = Base64.decode(bm, Base64.DEFAULT);
		Bitmap bmp = BitmapFactory.decodeByteArray(b, 0, b.length);
		adapter = new ListRobotArrayAdapter(vModel.getActivity(), vModel,
				vModel.getRobotNames(), vModel.getRobotAdresses(),
				vModel.getRobotSerie(), bmp, vModel.getRobotChecked());

		vModel.getActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				listView.setAdapter(adapter);

			}
		});
		application.notifyObservers("listRobotStart");
	}

	/**
	 * Récupérer la liste des robots via le site espace client
	 *
	 */

	private void refresh() {
		// TODO Auto-generated method stub*

		if (application.isConnectedToInternet()) {
			new Thread() {
				@Override
				public void run() {
					doListen();
				};
			}.start();
		} else {
			JsonList = application.getJsonList();
			setResponce();
		}

	}

	private void doListen() {
		vModel.getActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				progress.setVisibility(View.VISIBLE);
			}
		});
		String url = SmartRobotApplication.getParamFromFile("param",
				"configuration.properties", null);

		String serverURL = urlParser.getListRobot(url, application.getEmail());
		GestionDesFichiers.log("url pour la liste des robots", "" + serverURL,
				"Log_SmartRobot");
		//JsonList = WebService.connection(serverURL);
		WebService ws = new WebService();
		WebService.HttpRequest callbackservice = ws.new HttpRequest() {
			@Override
			public void receiveData(Object object) {
				JsonList = (String)object;
				if (JsonList != null) {
					if (!JsonList.contains("<html>") && !JsonList.contains("error")
							&& !JsonList.contains("Error")) {
						flag = false;
					}
					setResponce();
				}
			}
		};
		callbackservice.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, serverURL);





	}

	/**
	 * Cette méthode est déclenché après la notification de l'observateur
	 *
	 */
	@Override
	public void update(String message) {
		// TODO Auto-generated method stub
		GestionDesFichiers.log("message reçu par l'observateur ", message,
				"Log_SmartRobot");
		if (vModel != null) {
			if (message.contains("demarreNouvelleActivity")) {
				vController.removeObserver();
				application.StartAct(null, null, BleScan.class,
						vModel.getActivity());
				vModel.getActivity().finish();
			} else if (message.contains("updateAdapter")) {
				vModel.getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						if (adapter != null)
							adapter.notifyDataSetChanged();

					}
				});
			} else if (message.contains("finishActivity")) {
				vModel.getActivity().finish();
			} else if (message.contains("StartSetting")) {
				vController.removeObserver();
				application.StartAct(null, null, SettingListActivity.class,
						vModel.getActivity());
				vModel.getActivity().finish();
			}
		}
	}

	/**
	 * Cette méthode se déclenche à la fin de cycle de vie de l'activité
	 *
	 */
	public void destroy() {
		// TODO Auto-generated method stub
		vController.removeObserver();
		/**
		 * Notifier pour arrêter le scan périodique
		 *
		 */

		application.removeObserver(ListRobotView.this);

	}

	private void setResponce() {


		if(application.getWsUrl().contains("ontomantics")){
			try {
				JSONObject jsonObj = new JSONObject(JsonList);
              JSONArray array = jsonObj.getJSONArray("listRobot");

				JsonList = array.toString();
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}
		Log.i("mbu","----application.getWsUrl()="+application.getWsUrl());
		Log.i("mbu","----JsonList="+JsonList);
		Log.i("mbu","----JsonInApp="+application.getJsonList());
		if (!isJsonListEqual(JsonList))
			application.setEditor(RemoteControlUtil.JSON_LIST, JsonList);
		lenght = vController.getlength();
		if (lenght < 1) {

			vModel.getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					Toast.makeText(
							vModel.getActivity(),
							vModel.getContext().getResources()
									.getString(R.string.addRobot),
							Toast.LENGTH_SHORT).show();
				}
			});

			// vModel.getActivity().finish();
		}

		else {
			vController.setData(lenght);
			afficherListrobot();
		}
		vModel.getActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (progress.getVisibility() == View.VISIBLE) {
					progress.setVisibility(View.GONE);
				}

			}
		});

	}

	private boolean isJsonListEqual(String json) {
		boolean isEqual = true;
		if (!application.getJsonList().equals(json))
			isEqual = false;
		return isEqual;
	}
}
