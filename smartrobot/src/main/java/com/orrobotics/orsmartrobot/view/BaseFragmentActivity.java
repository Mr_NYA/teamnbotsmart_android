package com.orrobotics.orsmartrobot.view;

import com.orrobotics.orsmartrobot.application.SmartRobotApplication;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public class BaseFragmentActivity extends FragmentActivity {
	SmartRobotApplication modelApp;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		modelApp = (SmartRobotApplication) this.getApplicationContext();
		modelApp.onActivityCreateSetTheme(this);
		super.onCreate(savedInstanceState);
	}

	@Override
	protected void onStart() {
		super.onStart();
		modelApp.setLocal();

	}

	@Override
	protected void onRestart() {
		super.onRestart();

	}
}