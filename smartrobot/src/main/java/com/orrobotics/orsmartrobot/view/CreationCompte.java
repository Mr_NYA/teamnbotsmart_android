package com.orrobotics.orsmartrobot.view;

import com.orrobotics.orsmartrobot.R;
import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.controller.CreationCompteController;
import com.orrobotics.orsmartrobot.controllerImpl.CreationCompteControllerImpl;
import com.orrobotics.orsmartrobot.model.CreationCompteModel;
import com.orrobotics.orsmartrobot.modelImpl.CreationCompteModelImpl;
import com.orrobotics.orsmartrobot.ui.CircularProgress;
import com.orrobotics.orsmartrobot.view.CreationCompteView;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

/**
 *
 * @author Hajiba IFRAH
 * @version 2.1.0
 * @date 18/05/2015
 *
 */

public class CreationCompte extends BaseFragmentActivity {
	/**
	 * Pour saisir le nom
	 */
	AutoCompleteTextView nom;
	/**
	 * Pour saisir le prenom
	 */
	AutoCompleteTextView prenom;
	/**
	 * Pour saisir l'email
	 */
	AutoCompleteTextView email;
	/**
	 * Pour saisir le mot de passe
	 */
	AutoCompleteTextView password;
	/**
	 * Pour saisir le mot de passe confirmé
	 */
	AutoCompleteTextView confirm;
	/**
	 * Pour saisir la ville
	 */
	AutoCompleteTextView city;
	/**
	 * Pour afficher les messages d'erreur
	 */
	TextView error;
	TextView errorEmail;
	TextView errorPass;
	TextView errorPassConfirm;
	TextView errorNom;
	TextView errorPrenom;
	TextView errorDate;
	TextView errorCity;
	/**
	 * affiche madame/monsieur
	 */
	ListView ListCivility;
	/**
	 * affiche la liste des pays
	 */
	ListView ListCountries;
	/**
	 * le champ qui contient la civilité choisie par l'utilisateur
	 */
	EditText edit;
	/**
	 * le champ qui contient le pays choisie par l'utilisateur
	 */
	EditText editCountry;
	/**
	 * le champ qui contient la date de naissance sélectionné par l'utilisateur
	 */
	EditText pDisplayDate;
	/**
	 * le clic sur cette bouton permet de vérifier les informations saisie par
	 * l'utilisateur et d'appeler le web service
	 *
	 */
	Button save;

	CreationCompteView mView;

	FragmentActivity acti;

	String value = null;

	SmartRobotApplication modelApp;

	Bitmap open;
	ImageView app_name1;
	CircularProgress progress;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.compte);

		acti = this;
		/**
		 * Construction du MVC
		 */

		CreationCompteModel model = new CreationCompteModelImpl(
				getApplicationContext(), acti);
		CreationCompteController controller = new CreationCompteControllerImpl(
				model);
		mView = new CreationCompteView(controller, model);

		modelApp = (SmartRobotApplication) this.getApplicationContext();
		modelApp.stricMode();

		/**
		 * Initialisaion des paramètres
		 *
		 */

		ListCivility = (ListView) findViewById(R.id.listView);
		ListCountries = (ListView) findViewById(R.id.listView1);

		edit = (EditText) findViewById(R.id.civilite_);
		editCountry = (EditText) findViewById(R.id.pays_);

		pDisplayDate = (EditText) findViewById(R.id.naissance_);
		nom = (AutoCompleteTextView) findViewById(R.id.nom_);
		prenom = (AutoCompleteTextView) findViewById(R.id.prenom_);
		email = (AutoCompleteTextView) findViewById(R.id.email_);
		password = (AutoCompleteTextView) findViewById(R.id.password_);
		confirm = (AutoCompleteTextView) findViewById(R.id.comfirmer_);
		city = (AutoCompleteTextView) findViewById(R.id.city_);
		save = (Button) findViewById(R.id.comptebtn);
		error = (TextView) findViewById(R.id.error);
		errorDate = (TextView) findViewById(R.id.errorDate);
		errorEmail = (TextView) findViewById(R.id.errorEmail);
		errorNom = (TextView) findViewById(R.id.errorNom);
		errorPass = (TextView) findViewById(R.id.errorPass);
		errorPassConfirm = (TextView) findViewById(R.id.errorPassConfirm);
		errorPrenom = (TextView) findViewById(R.id.errorPrenom);
		errorCity = (TextView) findViewById(R.id.errorCity);
		app_name1 = (ImageView) findViewById(R.id.titre);
		progress = (CircularProgress) findViewById(R.id.progress);

		/**
		 * Géstion du logo
		 */
		if (modelApp.isLogoOptionEnabled()) {
			open = modelApp.getLogoFromExternalStorage();
		}
		if (open != null)
			app_name1.setImageBitmap(Bitmap.createScaledBitmap(open, 128, 128,
					false));

		mView.setWidgets(ListCivility, ListCountries, edit, editCountry,
				pDisplayDate, nom, prenom, email, password, confirm, city,
				save, error, errorCity, errorDate, errorEmail, errorNom,
				errorPass, errorPassConfirm, errorPrenom,progress);

		mView.setListeners();

	}

	/**
	 * le clic sur le bouton retour de l'appariel
	 *
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (value != null)

				modelApp.StartAct(null, null, Authentification.class, acti);

			finish();

			return true;
		}

		return false;
	}

}
