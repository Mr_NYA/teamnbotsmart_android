package com.orrobotics.orsmartrobot.view;

import com.orrobotics.orsmartrobot.R;
import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.controller.SettingListController;
import com.orrobotics.orsmartrobot.controllerImpl.SettingListControllerImpl;
import com.orrobotics.orsmartrobot.model.SettingListModel;
import com.orrobotics.orsmartrobot.modelImpl.SettingListModelImpl;
import com.orrobotics.orsmartrobot.ui.ToggleSwitch;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

/**
 *
 * @author Hajiba IFRAH
 * @version 1.3.2
 * @date 02/07/2015 Cette activité présente une liste des paramètres. Cette
 *
 */
public class SettingListActivity extends BaseActivity {

	SmartRobotApplication application;
	Activity acti;
	ToggleSwitch enableVideo;
	ToggleSwitch echoConcelation;
	ToggleSwitch enableMic;
	ToggleSwitch enableSpeaker;
	ToggleSwitch showBatterie;
	ToggleSwitch editTtsLang;
	ToggleSwitch editSpeechRate;
	ToggleSwitch showRobotApp;
	Spinner videoSizes;
	Spinner batterieValues;
	SeekBar soundMediaBar;
	SeekBar soundSystemBar;
	SeekBar soundNotifBar;
	View layoutShowRobotApp;
	TextView speedLineaire;
	TextView speedLaterale;
	SeekBar speedbarLineaire;
	SeekBar speedbarLaterale;
	ImageButton back;
	ToggleSwitch editIsPermanance;
	TextView robotId;
	TextView showBatterieText;
	RelativeLayout battrieDelais;

	SettingListActivityView mVieu;

	EditText edit_delai_declench;


	RelativeLayout layout_videoQuality;
	RelativeLayout layout_enableCam;
	RelativeLayout layout_enableEchoAnnulation;
	RelativeLayout layout_enableSpeaker;
	RelativeLayout layout_enableMic;
	TextView robotName;

	/**
	 * Pour afficher l'email de l'utilisateur
	 **/
	TextView email;
	/**
	 * Pour afficher le nom de l'utilisateur
	 **/
	TextView nom;

	ImageButton deconexion;

	ImageButton robotSwitch;
	Spinner appLangages;
	Spinner ttsLangages;
	Spinner ttsRates;
	TextView ttsLangagesText;
	TextView ttsRatesText;
	ToggleSwitch editIsCloudSpeech;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setting);
		acti = this;
		application = (SmartRobotApplication) this.getApplicationContext();
		application.setActivity(acti);

		/**
		 * Construction du MVC
		 **/
		SettingListModel model = new SettingListModelImpl(
				getApplicationContext(), acti);
		SettingListController control = new SettingListControllerImpl(model);

		mVieu = new SettingListActivityView(control, model);

		back = (ImageButton) findViewById(R.id.back);
		robotId = (TextView) findViewById(R.id.robotId);

		enableVideo = (ToggleSwitch) findViewById(R.id.switch_enableCam);
		enableMic = (ToggleSwitch) findViewById(R.id.switch_enableMic);
		enableSpeaker = (ToggleSwitch) findViewById(R.id.switch_enableSpeaker);
		showBatterie = (ToggleSwitch) findViewById(R.id.switch_showBatterie);
		showRobotApp = (ToggleSwitch) findViewById(R.id.switch_showRobotApp);
		editTtsLang = (ToggleSwitch) findViewById(R.id.switch_editTtsLang);
		editSpeechRate = (ToggleSwitch) findViewById(R.id.switch_editSpeechRate);
		editIsPermanance = (ToggleSwitch) findViewById(R.id.switch_editIsPermanance);
		editIsCloudSpeech = (ToggleSwitch) findViewById(R.id.switch_editIsCloudSpeech);

		echoConcelation = (ToggleSwitch) findViewById(R.id.switch_enableEchoAnnulation);
		if (application.getEchoConcellation().equals("false"))
			echoConcelation.setCheckedTogglePosition(1);
		else
			echoConcelation.setCheckedTogglePosition(0);

		videoSizes = (Spinner) findViewById(R.id.videoQuality);
		batterieValues = (Spinner) findViewById(R.id.delaiBatterie);
		showBatterieText = (TextView) findViewById(R.id.showBatterie);
		battrieDelais = (RelativeLayout) findViewById(R.id.layout_delaiBatterie);
		soundMediaBar = (SeekBar) findViewById(R.id.seekBarSoundMedia);
		soundSystemBar = (SeekBar) findViewById(R.id.seekBarSoundSystem);
		soundNotifBar = (SeekBar) findViewById(R.id.seekBarSoundNotif);

		speedLineaire = (TextView) findViewById(R.id.speed1);
		speedLaterale = (TextView) findViewById(R.id.speed2);

		speedbarLineaire = (SeekBar) findViewById(R.id.seekBar1);
		speedbarLaterale = (SeekBar) findViewById(R.id.seekBar2);

		edit_delai_declench = (EditText) findViewById(R.id.edit_delai_declench);


		layoutShowRobotApp = findViewById(R.id.layout_showRobotApp);
		layout_enableCam = (RelativeLayout) findViewById(R.id.layout_enableCam);
		layout_enableEchoAnnulation = (RelativeLayout) findViewById(R.id.layout_enableEchoAnnulation);
		layout_enableMic = (RelativeLayout) findViewById(R.id.layout_enableMic);
		layout_enableSpeaker = (RelativeLayout) findViewById(R.id.layout_enableSpeaker);
		layout_videoQuality = (RelativeLayout) findViewById(R.id.layout_videoQuality);



		email = (TextView) findViewById(R.id.email);
		nom = (TextView) findViewById(R.id.nom);
		robotName = (TextView) findViewById(R.id.robotName);
		deconexion = (ImageButton) findViewById(R.id.deconnexion);
		robotSwitch = (ImageButton) findViewById(R.id.robot_switch);
		appLangages = (Spinner) findViewById(R.id.spinner_appLang);
		ttsLangages = (Spinner) findViewById(R.id.spinner_ttsLang);
		ttsRates = (Spinner) findViewById(R.id.spinner_ttsSpeechRate);

		ttsLangagesText = (TextView) findViewById(R.id.ttsLang);
		ttsRatesText = (TextView) findViewById(R.id.ttsSpeechRate);

		if (application.getRobotType() != 1)
			layoutShowRobotApp.setVisibility(View.GONE);

		if (modelApp.getCallType().equals("linphone")) {
			layout_enableCam.setVisibility(View.VISIBLE);
			layout_enableEchoAnnulation.setVisibility(View.VISIBLE);
			layout_enableMic.setVisibility(View.VISIBLE);
			layout_enableSpeaker.setVisibility(View.VISIBLE);
			layout_videoQuality.setVisibility(View.VISIBLE);
		} else {
			layout_enableCam.setVisibility(View.GONE);
			layout_enableEchoAnnulation.setVisibility(View.GONE);
			layout_enableMic.setVisibility(View.GONE);
			layout_enableSpeaker.setVisibility(View.GONE);
			layout_videoQuality.setVisibility(View.GONE);
		}
		if(SmartRobotApplication.call.equals("webrtc")){
			layout_enableMic.setVisibility(View.VISIBLE);
			layout_enableSpeaker.setVisibility(View.VISIBLE);
			layout_enableCam.setVisibility(View.GONE);
			layout_enableEchoAnnulation.setVisibility(View.GONE);
			layout_videoQuality.setVisibility(View.GONE);
		}

		mVieu.setWidget(back, enableVideo, enableMic, enableSpeaker,
				videoSizes, batterieValues, soundMediaBar, soundSystemBar,
				soundNotifBar, echoConcelation, speedLineaire, speedLaterale,
				speedbarLineaire, speedbarLaterale, showBatterie,
				editTtsLang, editSpeechRate, showRobotApp, robotId,robotName,
				edit_delai_declench,
				editIsPermanance, email, nom, deconexion,
				robotSwitch,appLangages,ttsLangages,ttsRates, ttsLangagesText,ttsRatesText,editIsCloudSpeech);

		mVieu.setListners();

	}

	/**
	 * le clic sur cette bouton retour de l'appariel
	 *
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			application.notifyObservers("initSetting");

			finish();
			application.StartAct(null, null, ControlRobot.class,
					application.getApplicationContext());
			application.notifyObservers("restartVoice");


			return true;
		}

		return false;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		application.setSettingControl(true);

	}

}
