package com.orrobotics.orsmartrobot.view;

import java.util.Timer;
import java.util.TimerTask;

import android.os.Handler;
import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.controller.ControlRobotController;
import com.orrobotics.orsmartrobot.model.ControlRobotModel;
import com.zerokol.views.JoystickView;

public class ControlWithJoystique {
	ControlRobotController control;
	ControlRobotModel model;
	JoystickView joystick;
	Timer remoteTimer;
	SmartRobotApplication application;
	double speedMax;
	int speedMax_arriere;
	int speedMax_rotation;
	Handler mHandler = new Handler();
	Boolean joyMoved = false;

	public ControlWithJoystique(ControlRobotController control,
			ControlRobotModel model) {
		// TODO Auto-generated constructor stub
		this.control = control;
		this.model = model;
		application = (SmartRobotApplication) model.getContext();
	}

	public void setWidget(JoystickView joystick) {
		// TODO Auto-generated method stub
		this.joystick = joystick;

		joystick.initJoystickView(model.getContext().getResources()
				.getDisplayMetrics().density);

		if (remoteTimer != null) {
			remoteTimer.cancel();
			remoteTimer = null;
		}

		remoteTimer = new Timer();

		remoteTimer.scheduleAtFixedRate(new MyRemoteTask(), 1, 100);
	}

	private class MyRemoteTask extends TimerTask {

		@Override
		public void run() {

			double x = joystick.pX;
			double y = joystick.pY;

			// double z = x * x + y * y;

			if (x == 0 && y == 0) {
				if (joyMoved) {
					control.stopMotion();
					application.notifyObservers("arreter;all");
					joyMoved = false;
				}
			} else {
				joyMoved = true;
				control.move("joy;" + x + ";" + y + ";" + joystick.pXmax + ";"
						+ joystick.pYmax);

			}

		}
	}

	public void destroy() {
		// TODO Auto-generated method stub

		if (remoteTimer != null) {
			remoteTimer.cancel();
			remoteTimer = null;
		}
		if (control != null)
			control.destroy();
	}

}
