package com.orrobotics.orsmartrobot.view;

import java.util.Locale;

import com.orrobotics.orsmartrobot.application.SmartRobotApplication;

import android.app.Activity;
import android.os.Bundle;

public class BaseActivity extends Activity {

    SmartRobotApplication modelApp;
    private Locale mCurrentLocale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        modelApp = (SmartRobotApplication) this.getApplicationContext();
        modelApp.onActivityCreateSetTheme(this);

        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mCurrentLocale = getResources().getConfiguration().locale;
        modelApp.setLocal();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Locale locale = modelApp.getLocale();

        if (!locale.equals(mCurrentLocale)) {

            mCurrentLocale = locale;
            recreate();
        }
    }
}