package com.orrobotics.orsmartrobot.view;

import java.util.Timer;
import java.util.TimerTask;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.Surface;
import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.controller.ControlRobotController;
import com.orrobotics.orsmartrobot.model.ControlRobotModel;
import com.orrobotics.orsmartrobot.util.GestionDesFichiers;
import com.zerokol.views.JoystickViewModif;

public class ControlWithGyroscope implements SensorEventListener {
	ControlRobotController control;
	ControlRobotModel model;
	JoystickViewModif joyGyro;
	Display mDisplay;
	SensorManager sensorManager;
	Float x=0f;
	Float y=0f;
	Float z=0f;
	int k=0;
	int speed = 3;
	SmartRobotApplication application;
	int speedMax_rotation;
	float speedMax;
	int speedArriere;
	Boolean flagGyro;
	Handler mHandler = new Handler();
	float[] mGravity;
	float[] mGeomagnetic;
	Float azimut;
	double angle;
	int inclination;
	Timer remoteTimer;
	Boolean flagGyroMoved = false;

	public ControlWithGyroscope(ControlRobotController control,
			ControlRobotModel model) {
		// TODO Auto-generated constructor stub
		this.control = control;
		this.model = model;
		application = (SmartRobotApplication) model.getContext();

	}

	public void setWidget(JoystickViewModif joyGyro, Display mDisplay,
			SensorManager sensorManager, Boolean flagGyro) {
		// TODO Auto-generated method stub
		this.joyGyro = joyGyro;
		this.mDisplay = mDisplay;
		this.sensorManager = sensorManager;
		this.flagGyro = flagGyro;
		sensorManager.registerListener(this,
				sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
				SensorManager.SENSOR_DELAY_FASTEST);

		sensorManager.registerListener(this,
				sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE),
				SensorManager.SENSOR_DELAY_FASTEST);

		sensorManager.registerListener(this,
				sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),
				SensorManager.SENSOR_DELAY_FASTEST);
		application.startGyro();
		if (remoteTimer != null) {
			remoteTimer.cancel();
			remoteTimer = null;
		}

		remoteTimer = new Timer();
		remoteTimer.scheduleAtFixedRate(new MyRemoteTaskGyro(), 1, 100);
	}

	private class MyRemoteTaskGyro extends TimerTask {

		@Override
		public void run() {
			int power = joyGyro.getPower();
			GestionDesFichiers.log("INFO ", "gyroscope " + flagGyro + "valeur "
					+ x, "log_SmartRobot.txt");
			k = power;
			if (joyGyro.getAngle() == 180) {
				k = (-1) * k;
			}
//			Log.v("", "-----inclination =" + inclination);
//			Log.v("", "-----flagGyro =" + flagGyro);
//			Log.v("", "-----x =" + x);
			if (k == 0) {
				if (flagGyroMoved) {
					control.stopMotion();
					application.notifyObservers("arreter;all");
					flagGyroMoved = false;
				}
			}

			else if (flagGyro == false && (x >= 1 || x <= -1)
					&& inclination > 28) {
				flagGyroMoved = true;
				moveGyro(x, k, false);
			} else if (flagGyro == true && (x >= 4 || x <= -4)
					&& inclination > 28) {
				moveGyro(x, k, false);
				flagGyroMoved = true;
			} else {
				flagGyroMoved = true;
				if (k > 0) {
					Log.e("joyGyro", "Avancer");

					control.move("gyro;devantB;" + k);

				} else {
					Log.e("joyGyro", "en arriere");

					control.move("gyro;arriereB;" + k);

				}
			}

		}
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		// TODO Auto-generated method stub
		switch (event.sensor.getType()) {
		case Sensor.TYPE_ACCELEROMETER:
			switch (mDisplay.getRotation()) {
			case Surface.ROTATION_0:
				x = event.values[0];
				y = event.values[1];
				z = event.values[2];
				break;
			case Surface.ROTATION_90:
				x = -event.values[1];
				y = event.values[0];
				z = event.values[2];
				break;
			case Surface.ROTATION_180:
				x = -event.values[0];
				y = -event.values[1];
				z = event.values[2];
				break;
			case Surface.ROTATION_270:
				x = event.values[1];
				y = -event.values[0];
				z = event.values[2];
				break;

			}
			float[] g = new float[3];
			g = event.values.clone();

			double norm_Of_g = Math.sqrt(g[0] * g[0] + g[1] * g[1] + g[2]
					* g[2]);

			// Normalize the accelerometer vector
			g[0] = (float) (g[0] / norm_Of_g);
			g[1] = (float) (g[1] / norm_Of_g);
			g[2] = (float) (g[2] / norm_Of_g);

			inclination = (int) Math.round(Math.toDegrees(Math.acos(g[2])));

			angle = Math.atan2(x, y) / (Math.PI / 180);
			application.calculateAccMagOrientation(event.values);

			break;

		case Sensor.TYPE_GYROSCOPE:

			application.gyroFunction(event);
			break;

		case Sensor.TYPE_MAGNETIC_FIELD:
			mGeomagnetic = event.values;
			if (mGravity != null && mGeomagnetic != null) {
				float R[] = new float[9];
				float I[] = new float[9];
				boolean success = SensorManager.getRotationMatrix(R, I,
						mGravity, mGeomagnetic);
				if (success) {
					float orientation[] = new float[3];
					SensorManager.getOrientation(R, orientation);
					azimut = orientation[0]; // orientation contains: azimut,
												// pitch and roll
				}
			}
			application.magnFunction(event.values);
			break;
		}
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub

	}

	public void moveGyro(Float x2, int k, boolean flag) {

		application.notifyObservers("gyrox;" + x2 + ";" + flag + ";" + k + ";"
				+ angle);

	}

	public void destroy() {
		// TODO Auto-generated method stub

		sensorManager.unregisterListener(this);
		if (remoteTimer != null) {
			remoteTimer.cancel();
			remoteTimer = null;
		}
		if (control != null)
			control.destroy();
	}
}
