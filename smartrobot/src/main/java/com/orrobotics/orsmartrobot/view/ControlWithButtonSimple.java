package com.orrobotics.orsmartrobot.view;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageButton;
import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.controller.ControlRobotController;
import com.orrobotics.orsmartrobot.model.ControlRobotModel;

@SuppressLint("ClickableViewAccessibility")
public class ControlWithButtonSimple {

	ControlRobotController control;
	ControlRobotModel model;
	ImageButton forwardButton;
	ImageButton backwardButton;
	ImageButton rightButton;
	ImageButton leftButton;
	SmartRobotApplication application;
	Handler mHandler = new Handler();

	public ControlWithButtonSimple(ControlRobotController control,
			ControlRobotModel model) {
		// TODO Auto-generated constructor stub
		this.control = control;
		this.model = model;
		application = (SmartRobotApplication) model.getContext();
	}

	public void setWidget(ImageButton forwardButton,
			ImageButton backwardButton, ImageButton rightButton,
			ImageButton leftButton) {
		// TODO Auto-generated method stub

		this.forwardButton = forwardButton;
		this.backwardButton = backwardButton;
		this.rightButton = rightButton;
		this.leftButton = leftButton;

	}

	public void setListners() {
		// TODO Auto-generated method stub

		forwardButton.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent motion) {
				switch (motion.getAction()) {
				case MotionEvent.ACTION_DOWN:
					v.setPressed(true);
					control.move("devantB");

					break;
				case MotionEvent.ACTION_UP:
					v.setPressed(false);
					application.notifyObservers("arreter;avancer");
					break;
				}
				return true;
			}
		});

		backwardButton.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent motion) {
				switch (motion.getAction()) {
				case MotionEvent.ACTION_DOWN:
					v.setPressed(true);
					control.move("arriereB");

					break;
				case MotionEvent.ACTION_UP:
					v.setPressed(false);
					application.notifyObservers("arreter;reculer");
					break;
				}
				return true;
			}
		});

		rightButton.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent motion) {
				switch (motion.getAction()) {
				case MotionEvent.ACTION_DOWN:
					v.setPressed(true);
					if (application.getOptFaceToFace().equals("false")) {

						control.move("droiteB");
					} else {

						control.move("gaucheB");
					}

					break;
				case MotionEvent.ACTION_UP:
					v.setPressed(false);
					if (application.getOptFaceToFace().equals("false"))
						application.notifyObservers("arreter;droite");
					else
						application.notifyObservers("arreter;gauche");
					break;
				}
				return true;
			}
		});

		leftButton.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent motion) {
				switch (motion.getAction()) {
				case MotionEvent.ACTION_DOWN:
					v.setPressed(true);
					if (application.getOptFaceToFace().equals("false")) {

						control.move("gaucheB");
					} else {

						control.move("droiteB");
					}
					break;
				case MotionEvent.ACTION_UP:
					v.setPressed(false);
					if (application.getOptFaceToFace().equals("false"))
						application.notifyObservers("arreter;gauche");
					else
						application.notifyObservers("arreter;droite");
					break;
				}
				return true;
			}
		});

	}

}
