package com.orrobotics.orsmartrobot.view;

import java.io.IOException;

import android.accounts.Account;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.DrawerLayout.DrawerListener;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Toast;

import com.hannesdorfmann.mosby.mvp.MvpFragment;
import com.orrobotics.linphone.BackgroundFragment;
import com.orrobotics.linphone.VideoSnapFragment;
import com.orrobotics.orsmartrobot.R;
import com.orrobotics.orsmartrobot.service.TestDebitService;
import com.orrobotics.orsmartrobot.util.RemoteControlUtil;
import com.orrobotics.orsmartrobot.util.SettingsContentObserver;
import com.orrobotics.orsmartrobot.util.SkypeUtilities;
import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.controller.ControlRobotController;
import com.orrobotics.orsmartrobot.model.ControlRobotModel;
import com.orrobotics.orsmartrobot.observer.OrSmarRobotObserver;
import com.orrobotics.orsmartrobot.service.ServiceRobot;
import com.orrobotics.orsmartrobot.util.GestionDesFichiers;
import com.orrobotics.orsmartrobot.util.VerticalSeekBar;
import com.orrobotics.orsmartrobot.util.VerticalSeekBar_Reverse;
import com.orrobotics.webrtc.WebrtcFragment;
import com.zerokol.views.JoystickView;
import com.zerokol.views.JoystickViewModif;

/**
 * @author Manar Boukhari
 */
@SuppressLint({"NewApi", "ClickableViewAccessibility", "RtlHardcoded"})
public class ControlRobotView implements OrSmarRobotObserver {

    public static ControlRobotController vController;
    public static ControlRobotModel vModel;
    SmartRobotApplication application;

    ImageButton connect;

    Boolean flagServer = false;
    Boolean flagTest = false;
    Boolean flagRobotOn = false;
    Boolean flagDrawerClosed = true;
    Window win;

    Intent serviceXmpp;

    ImageButton camera;
    ImageButton skype;
    ImageView etat_robot;
    ImageView etat_sip;
    ImageView etat_xmpp;
    ImageButton setting;

    JoystickView joystick;
    JoystickViewModif joyGyro;

    DrawerLayout drawer;
    RelativeLayout control_panel;
    RelativeLayout control_panel_joystick;
    RelativeLayout control_panel_gyro;
    VideoSnapFragment vvideoSnapFragment;
    ImageView logo;
    String mode;
    String optionFaceToFace;
    String optionCmdVocal;
    Boolean flagPopupOpend = false;

    Handler hand = new Handler();

    RadioGroup radio;
    RadioButton radio01;
    RadioButton radio02;
    RadioButton radio03;
    RadioButton radio04;
    CheckBox radio00;
    CheckBox radio001;
    RelativeLayout switch_cam;

    Boolean flagStarted = true;

    BackgroundFragment backgroundFragment;
    Fragment backFragment;

    WebrtcFragment webrtcCallFragment;
    MvpFragment webrtcfragment;


    /**
     * le clic sur cette bouton permet d'avancer le robot
     */
    ImageButton forwardButton;
    /**
     * le clic sur cette bouton permet de reculer le robot
     */
    ImageButton backwardButton;
    /**
     * le clic sur cette bouton permet de faire tourner le robot à  droite
     */
    ImageButton rightButton;
    /**
     * le clic sur cette bouton permet de faire tourner le robot à  gauche
     */
    /**
     * le clic sur cette bouton permet de faire tourner le robot à  gauche
     */
    ImageButton leftButton;

    ControlWithButtonSimple controlSimple;
    ControlWithGyroscope controlGyroscope;
    ControlWithJoystique controlJoystique;

    SensorManager sensorManager;
    Display mDisplay;
    Boolean flagGyro = false;
    Account[] pseudoSkypeUser;
    SettingsContentObserver mSettingsContentObserver;

    VerticalSeekBar upload_speed_bar;
    VerticalSeekBar_Reverse download_speed_bar;
    TextView download_speed;
    TextView upload_speed;
    RelativeLayout control_panel_debit;
    TextView download_speed_min;
    TextView upload_speed_min;
    Intent serviceDebitTest;
    Handler handler = new Handler();
    int testDebitDuration = 5000;
    ImageButton stop_call;
    String call = "webrtc";

    RelativeLayout control_panel_debit_icons;
    ImageView upload_speed_icon;
    ImageView download_speed_icon;
    String debit_indicator;
    int bars_down_prev = 0;
    int bars_up_prev = 0;

    /**
     * ajoutée par Berradi YASSINE
     */

    String position = SmartRobotApplication.getParamFromFile("stop_call.position",
            "Configuration.properties", null);

    /**
     * end
     **/

    public ControlRobotView(ControlRobotController controller,
                            ControlRobotModel model) {
        vController = controller;
        vModel = model;
        if (position == null)
            position = "bas";

        application = (SmartRobotApplication) vModel.getContext();


        call = application.getParamFromFile("call", "Configuration.properties", null);


        if (call == null) {
            call = "webrtc";

        }
        if (call.equals("webrtc") || call.equals("")) {
            if ((application.isOrientation() == false && application.isTelepresence() == false)) {
                webrtcfragment = new WebrtcFragment(vModel.getActivity());
                webrtcCallFragment = (WebrtcFragment) webrtcfragment;
                vModel.getActivity().getSupportFragmentManager().beginTransaction()
                        .add(R.id.fragmentContainer1, webrtcfragment)
                        .commitAllowingStateLoss();
            }


        }


        if (application.isTelepresence() == false) {
            backFragment = new BackgroundFragment(vModel.getContext());
            backgroundFragment = (BackgroundFragment) backFragment;
            vModel.getActivity().getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragmentContainerBackground, backFragment)
                    .commitAllowingStateLoss();
        }

    }

    /**
     * Récupèrer et affecter les paramétres initialiser dans la classe
     * ControlRobot
     */
    public void setWidget(

            ImageView etat_robot, ImageView etat_sip,
            ImageView etat_xmpp, ImageButton setting,
            VideoSnapFragment videoSnapFragment, JoystickView joystick,
            JoystickViewModif joyGyro,
            DrawerLayout drawer, ImageView logo, RelativeLayout control_panel,
            RelativeLayout control_panel_joystick,
            RelativeLayout control_panel_gyro,
            ImageButton skype, ImageButton camera, ImageButton connect,
            RadioGroup radio, RadioButton radio01, RadioButton radio02,
            RadioButton radio03, RadioButton radio04, CheckBox radio00,
            CheckBox radio001, RelativeLayout switch_cam,
            VerticalSeekBar upload_speed_bar,
            VerticalSeekBar_Reverse download_speed_bar, TextView download_speed,
            TextView upload_speed, RelativeLayout control_panel_debit, TextView download_speed_min, TextView upload_speed_min, ImageButton stop_call,
            RelativeLayout control_panel_debit_icons, ImageView upload_speed_icon,
            ImageView download_speed_icon) {
        // TODO Auto-generated method stub
        this.etat_robot = etat_robot;
        this.etat_sip = etat_sip;
        this.etat_xmpp = etat_xmpp;
        this.setting = setting;
        this.joystick = joystick;
        this.joyGyro = joyGyro;
        this.drawer = drawer;

        this.logo = logo;
        this.control_panel = control_panel;
        this.control_panel_joystick = control_panel_joystick;
        this.control_panel_gyro = control_panel_gyro;
        this.connect = connect;
        this.skype = skype;
        this.radio = radio;
        this.radio01 = radio01;
        this.radio02 = radio02;
        this.radio03 = radio03;
        this.radio04 = radio04;
        this.radio00 = radio00;
        this.radio001 = radio001;

        this.camera = camera;
        this.switch_cam = switch_cam;
        vvideoSnapFragment = videoSnapFragment;
        this.stop_call = stop_call;

        application.setFlagCloseControl(false);
        application.registerObserver(ControlRobotView.this);

        this.upload_speed_bar = upload_speed_bar;
        this.download_speed_bar = download_speed_bar;
        this.download_speed = download_speed;
        this.upload_speed = upload_speed;
        this.control_panel_debit = control_panel_debit;
        this.download_speed_min = download_speed_min;
        this.upload_speed_min = upload_speed_min;

        this.control_panel_debit_icons = control_panel_debit_icons;
        this.upload_speed_icon = upload_speed_icon;
        this.download_speed_icon = download_speed_icon;

        if (call.equals("webrtc")) {
            stop_call.setVisibility(View.GONE);
        }
        //ImageView s = (ImageView) findViewById(R.id.stop_call);
        /**
         * ajoutée par Berradi YASSINE
         */
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT); // or wrap_content
        if (position.equals("bas")) {

            layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            layoutParams.bottomMargin = 30;


        } else {
            layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        }
        stop_call.setLayoutParams(layoutParams);
        /** end **/
        serviceXmpp = new Intent(vModel.getActivity(), ServiceRobot.class);

        init();

        mSettingsContentObserver = new SettingsContentObserver(
                vModel.getContext(), new Handler());
        vModel.getContext()
                .getContentResolver()
                .registerContentObserver(
                        android.provider.Settings.System.CONTENT_URI, true,
                        mSettingsContentObserver);
        drawer.closeDrawers();


    }

    private void setDebitMin() {
        int downloadSpeedMin = Integer.parseInt(application.getParamFromFile("debit.download.min", "Configuration.properties", null).split(" ")[0]);
        int uploadSpeedMin = Integer.parseInt(application.getParamFromFile("debit.upload.min", "Configuration.properties", null).split(" ")[0]);
        download_speed_min.setText("" + downloadSpeedMin + " Kbps");
        upload_speed_min.setText("" + uploadSpeedMin + " Kbps");
    }

    @SuppressWarnings("deprecation")
    private void init() {
        // TODO Auto-generated method stub

        debit_indicator = application.getParamFromFile("debit.indicator", "Configuration.properties", null);
        if (debit_indicator == null)
            debit_indicator = "icone";

        if (debit_indicator.equals("none")) {
            control_panel_debit.setVisibility(View.GONE);
            control_panel_debit_icons.setVisibility(View.GONE);
        } else if (debit_indicator.equals("values")) {
            control_panel_debit.setVisibility(View.VISIBLE);
            control_panel_debit_icons.setVisibility(View.GONE);
            if (application.isTelepresence() == false) {
                setDebitMin();
                upload_speed_bar.setMax(application.getMaxWifiDebit("down"));
                download_speed_bar.setMax(application.getMaxWifiDebit("up"));
            } else {
                application.notifyObservers("istelepresence");

            }


        } else {
            control_panel_debit.setVisibility(View.GONE);
            control_panel_debit_icons.setVisibility(View.VISIBLE);

        }

        if (application.isTelepresence() == false)
            GestionDesFichiers.collectLogs(vModel.getContext());
        application.notifyObservers("controlRobot");
        win = vModel.getActivity().getWindow();
        mode = application.getModeControl();


        if (mode.equals("gyroscope"))
            application.getActivity().setRequestedOrientation(
                    ActivityInfo.SCREEN_ORIENTATION_LOCKED);

        application.setEditor(RemoteControlUtil.AUTOSTART, "");

        optionFaceToFace = application.getOptFaceToFace();
        optionCmdVocal = application.getOptCmdVocal();
        /**
         * Activer le dernier mode de controle configuré par l'utilisateur par
         * défaut est le mode simple
         *
         */

        if (mode.equals("simple")) {
            radio01.setChecked(true);
        } else if (mode.equals("joystick")) {
            radio02.setChecked(true);
        } else if (mode.equals("gyroscope")) {
            radio03.setChecked(true);
        } else if (mode.equals("nocontrol")) {
            radio04.setChecked(true);
        } else {
            radio01.setChecked(true);
        }

        if (optionFaceToFace.equals("true")) {
            radio00.setChecked(true);
        } else if (optionFaceToFace.equals("false")) {
            radio00.setChecked(false);
        }

        if (optionCmdVocal.equals("true")) {
            radio001.setChecked(true);
        } else if (optionCmdVocal.equals("false")) {
            radio001.setChecked(false);
        }

        if (application.getMacConnectedDevice().equals(
                application.getDeviceAddress())
                || application.getRobotType() == 6) {

            if (application.getRobotConnexion().equals("true")) {
                etat_robot.setImageResource(R.drawable.pastill_yellow);
                showControlViews();
            }

        } else {

            etat_robot.setImageResource(R.drawable.offline_icon);
            hideControlViews();
        }
        application.setConnectedToOpenFire();
        application.setConnectedToSip();
        if (application.getConnectedToOpenFire().equals("true")) {
            etat_xmpp.setImageResource(R.drawable.pastill_blue);
        } else {
            etat_xmpp.setImageResource(R.drawable.offline_icon);
        }

        if (application.getConnectedToSip().equals("true")) {

            etat_sip.setImageResource(R.drawable.pastill_green);
        } else {
            etat_sip.setImageResource(R.drawable.offline_icon);
        }

        /**
         * Initialiser les paramètres
         *
         */


        if (application.isOrientation() == false && application.isTelepresence() == false) {

            serviceDebitTest = new Intent(application.getApplicationContext(), TestDebitService.class);
            if (application.isConnectedToInternet()) {
                if (application
                        .isServiceRunning("com.orrobotics.orsmartrobot.service.TestDebitService")) {
                    vModel.getActivity().stopService(serviceDebitTest);

                }
                vModel.getActivity().startService(serviceDebitTest);
            }

            if (call.equals("linphone")) {

                if (application
                        .isServiceRunning("com.orrobotics.orsmartrobot.service.ServiceRobot")) {
                    vModel.getActivity().stopService(serviceXmpp);

                }
                vModel.getActivity().startService(serviceXmpp);
            }
        }

        /**
         * Vérifications si Skype est déjà installé dans l'appareil
         *
         *
         */

        if (application.isOrientation() == false
                && application.isSettingControl() == false && application.isTelepresence() == false && call.equals("skype")) {


            if (!application.isAppInstalled(application.getActivity(),
                    "com.skype.raider")) {
                SkypeUtilities.downloadSkype(application.getActivity());
            } else {
                pseudoSkypeUser = SkypeUtilities.PseudoSkype(application
                        .getApplicationContext());
                if (pseudoSkypeUser.length != 1) {
                    SkypeUtilities.login_to_Skype(application.getActivity(),
                            application.getApplicationContext());
                } else {
                    application.setEditor(RemoteControlUtil.PSEUDOSKYPE,
                            pseudoSkypeUser[0].name);

                }
            }
        }

        if (application.isOrientation() == false) {
            String data = application.getParamFromFile("debit.test.duration", "Configuration.properties", null).split(" ")[0];
            if (data != null)
                testDebitDuration = Integer.parseInt(data);
            if (debit_indicator.equals("values")) {
                handler.postDelayed(runTestDebit, testDebitDuration);
            } else if (debit_indicator.equals("icone")) {
                handler.postDelayed(runDebitIconesTest, testDebitDuration);
            }
        }
    }


    Runnable runTestDebit = new Runnable() {
        @Override
        public void run() {
            final int progress = (int) TestDebitService.valueDown;
            download_speed_bar.setProgress(progress);
            download_speed.setText("" + progress + " Kbps");


            final int progressUp = (int) TestDebitService.valueUp;
            upload_speed_bar.setProgress(progressUp);
            upload_speed.setText("" + progressUp + " Kbps");

            handler.postDelayed(runTestDebit, testDebitDuration);
        }
    };

    Runnable runDebitIconesTest = new Runnable() {
        @Override
        public void run() {
//			handler.post(new Runnable() {
//				public void run() {
            double progressDown = TestDebitService.valueDownComplet;
            double progressUp = TestDebitService.valueUpComplet;

            double bars_down = 5 * (progressDown / application.getMaxWifiDebit("down"));
            if (bars_down > 0 && bars_down <= 1)
                bars_down = 1;
            else if (bars_down > 1 && bars_down <= 2)
                bars_down = 2;
            else if (bars_down > 2 && bars_down <= 3)
                bars_down = 3;
            else if (bars_down > 3 && bars_down <= 4)
                bars_down = 4;
            if (bars_down_prev != bars_down) {
                bars_down_prev = (int) bars_down;

                switch ((int) bars_down) {
                    case 0:
                        download_speed_icon.setBackground(application.getDrawable(R.drawable.signal_bars_off));
                        break;
                    case 1:
                        download_speed_icon.setBackground(application.getDrawable(R.drawable.signal_bars_down1));
                        break;
                    case 2:
                        download_speed_icon.setBackground(application.getDrawable(R.drawable.signal_bars_down2));
                        break;
                    case 3:
                        download_speed_icon.setBackground(application.getDrawable(R.drawable.signal_bars_down3));
                        break;
                    case 4:
                        download_speed_icon.setBackground(application.getDrawable(R.drawable.signal_bars_down4));
                        break;
                    default:
                        download_speed_icon.setBackground(application.getDrawable(R.drawable.signal_bars_down5));

                }
            }
            double bars_up = 5 * (progressUp / application.getMaxWifiDebit("up"));
            if (bars_up > 0 && bars_up <= 1)
                bars_up = 1;
            else if (bars_up > 1 && bars_up <= 2)
                bars_up = 2;
            else if (bars_up > 2 && bars_up <= 3)
                bars_up = 3;
            else if (bars_up > 3 && bars_up <= 4)
                bars_up = 4;
            if (bars_up_prev != bars_up) {
                bars_up_prev = (int) bars_up;
                switch ((int) bars_up) {
                    case 0:
                        upload_speed_icon.setBackground(application.getDrawable(R.drawable.signal_bars_off));
                        break;
                    case 1:
                        upload_speed_icon.setBackground(application.getDrawable(R.drawable.signal_bars_up1));
                        break;
                    case 2:
                        upload_speed_icon.setBackground(application.getDrawable(R.drawable.signal_bars_up2));
                        break;
                    case 3:
                        upload_speed_icon.setBackground(application.getDrawable(R.drawable.signal_bars_up3));
                        break;
                    case 4:
                        upload_speed_icon.setBackground(application.getDrawable(R.drawable.signal_bars_up4));
                        break;
                    default:
                        upload_speed_icon.setBackground(application.getDrawable(R.drawable.signal_bars_up5));

                }
            }
            handler.postDelayed(runDebitIconesTest, testDebitDuration);
        }

        //});
        //}
    };

    /**
     * Gestion des évenements de clic
     */
    @SuppressLint("RtlHardcoded")
    public void setListners() {
        // TODO Auto-generated method stub

        stop_call.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (call.equals("linphone")) {
                    vController.terminateCall();
                    ServiceRobot.sendMessage(application.getPseudoOpenFireSender(), "robotEndCall");
                } else {
                    application.notifyObservers("stopcalling");
                    stop_call.setVisibility(View.GONE);

                }
            }
        });
        radio00.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // is chkIos checked?
                if (((CheckBox) v).isChecked()) {
                    application.setRobotEditor(
                            RemoteControlUtil.OPT_FACE_TO_FACE, "true");

                } else {
                    application.setRobotEditor(
                            RemoteControlUtil.OPT_FACE_TO_FACE, "false");

                }

            }
        });

        radio00.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // is chkIos checked?
                if (((CheckBox) v).isChecked()) {
                    application.setRobotEditor(
                            RemoteControlUtil.OPT_FACE_TO_FACE, "true");

                } else {
                    application.setRobotEditor(
                            RemoteControlUtil.OPT_FACE_TO_FACE, "false");

                }

            }
        });
        radio001.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // is chkIos checked?
                if (((CheckBox) v).isChecked()) {
                    application.setRobotEditor(RemoteControlUtil.OPT_CMD_VOCAL,
                            "true");

                } else {
                    application.setRobotEditor(RemoteControlUtil.OPT_CMD_VOCAL,
                            "false");

                }

            }
        });
        radio.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected

                if (checkedId == R.id.radio0) {

                    application.setRobotEditor(RemoteControlUtil.MODE_CONTROLE,
                            "simple");
                    application.setEditor(RemoteControlUtil.MODE_CONTROLE,
                            "simple");

                } else if (checkedId == R.id.radio1) {

                    application.setRobotEditor(RemoteControlUtil.MODE_CONTROLE,
                            "joystick");
                    application.setEditor(RemoteControlUtil.MODE_CONTROLE,
                            "joystick");

                } else if (checkedId == R.id.radio3) {

                    application.setRobotEditor(RemoteControlUtil.MODE_CONTROLE,
                            "nocontrol");
                    application.setEditor(RemoteControlUtil.MODE_CONTROLE,
                            "nocontrol");

                } else {

                    application.setRobotEditor(RemoteControlUtil.MODE_CONTROLE,
                            "gyroscope");
                    application.setEditor(RemoteControlUtil.MODE_CONTROLE,
                            "gyroscope");

                }
            }
        });


        drawer.setDrawerListener(new DrawerListener() {
            @Override
            public void onDrawerSlide(View view, float v) {
                application.notifyObservers("drawerOuvert");
                setControlVisible(false);
                flagDrawerClosed = false;

            }

            @Override
            public void onDrawerOpened(View view) {
                setControlVisible(false);
                application.notifyObservers("drawerOuvert");
                flagDrawerClosed = false;
            }

            @Override
            public void onDrawerClosed(View view) {
                // your refresh code can be called from here
                flagDrawerClosed = true;
                application.notifyObservers("drawerFermer");
                if (!flagPopupOpend) {
                    setControlVisible(true);
                }

            }

            @Override
            public void onDrawerStateChanged(int i) {

            }
        });
        skype.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                application.lancer_app("com.skype.raider");

            }
        });

        camera.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (vController != null)
                    vController.switchCamera();

            }
        });

        connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                application.notifyObservers("connectOrDisconnect");

            }
        });
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                drawer.closeDrawers();
                Intent intent = new Intent(vModel.getActivity(),
                        SettingListActivity.class);
                // vModel.getActivity().finish();
                vModel.getActivity().startActivity(intent);
                application.notifyObservers("settingStarted");

            }
        });

    }

    public void close() {
        // TODO Auto-generated method stub
        if (application.isTelepresence() == true && vController != null)
            vController.terminateCall();
        application.notifyObservers("destroyControlRobotView");
        application.setFlagCloseControl(true);
        application.setOrientation(false);
        application.setFlagServer(false);
        application.setFlagTest(false);
        application.setChatEnabled(false);

        destroy();
        application.setStop(false);
        vModel.getActivity().finish();
        if (debit_indicator.equals("icone")) {
            handler.removeCallbacks(runDebitIconesTest);
        } else if (debit_indicator.equals("values")) {
            handler.removeCallbacks(runTestDebit);
        }
        application.setEditor(
                RemoteControlUtil.EXTRAS_DEVICE_ADDRESS,
                "");
    }

    public void startFragment() {
        if (flagStarted == false) {
            flagStarted = true;
            backFragment = new BackgroundFragment(vModel
                    .getContext());
            backgroundFragment = (BackgroundFragment) backFragment;
            vModel.getActivity()
                    .getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragmentContainerBackground,
                            backFragment).commitAllowingStateLoss();
        }
    }

    /**
     * Cette méthode est déclenché après la notification de l'observateur
     */
    @Override
    public void update(final String message) throws IOException {
        // TODO Auto-generated method stub

        GestionDesFichiers.log(
                "message reçu par l'observateur, page control robot ", message,
                "Log_SmartRobot");
        vModel.getActivity().runOnUiThread(new Runnable() {
            @SuppressWarnings("deprecation")
            @SuppressLint({"ShowToast", "Wakelock"})
            @Override
            public void run() {

                if (message.equals("screenOff")) {
                    if (application
                            .isServiceRunning("com.orrobotics.orsmartrobot.service.TestDebitService")) {
                        vModel.getActivity().stopService(serviceDebitTest);

                    }

                    vModel.getActivity().getSupportFragmentManager()
                            .beginTransaction().remove(backgroundFragment)
                            .commitAllowingStateLoss();


                }
                if (message.equals("screenOn")) {
                    if (!application
                            .isServiceRunning("com.orrobotics.orsmartrobot.service.TestDebitService")) {
                        vModel.getActivity().startService(serviceDebitTest);

                    }

                    backFragment = new BackgroundFragment(vModel.getContext());
                    backgroundFragment = (BackgroundFragment) backFragment;
                    vModel.getActivity()
                            .getSupportFragmentManager()
                            .beginTransaction()
                            .add(R.id.fragmentContainerBackground, backFragment)
                            .commitAllowingStateLoss();
                }
                if (message.contains("stopSuivi")) {
                    application.setRobotEditor(RemoteControlUtil.ICON_SUIVI, "false");
                    application.setFaceSuivi();
                } else if (message.contains("startSuivi")) {

                    application.setRobotEditor(RemoteControlUtil.ICON_SUIVI, "true");
                    application.setFaceSuivi();

                }
                Log.i("verbose:", " mmmmmmmmmmmmmmmmodel.getFaceSuivi()" + application.getFaceSuivi());
                if (message.contains("terminateCall")
                        || message.contains("resume")) {
                    if (vController != null)
                        vController.terminateCall();
                    startFragment();
                }
                if (message.equals("displayLinphone")) {
                    if (!call.equals("webrtc")) {


                        stop_call.setVisibility(View.VISIBLE);
                    } else {
                        skype.setVisibility(View.GONE);
                        switch_cam.setVisibility(View.VISIBLE);
                    }
                    flagStarted = false;
                }
                if (message.equals("endtelepresence")) {
                    startFragment();
                    stop_call.setVisibility(View.GONE);
                }
                if (message.equals("back-end")) {
                    if (backFragment != null) {
                        vModel.getActivity().getSupportFragmentManager()
                                .beginTransaction().remove(backgroundFragment)
                                .commitAllowingStateLoss();
                    }
                }

                if (message.contains("deconnexion")) {
                    close();
                }


                if (message.contains("orientationChanged")) {

                    if (application.isTelepresence() == false) {
                        vModel.getActivity().getSupportFragmentManager()
                                .beginTransaction().remove(backgroundFragment)
                                .commitAllowingStateLoss();
                        backFragment = new BackgroundFragment(vModel.getContext());
                        backgroundFragment = (BackgroundFragment) backFragment;
                        vModel.getActivity()
                                .getSupportFragmentManager()
                                .beginTransaction()
                                .add(R.id.fragmentContainerBackground, backFragment)
                                .commitAllowingStateLoss();
                    }
                }
                if (message.contains("test;")) {


                    if (message.contains("true")) {

                        if (SmartRobotApplication
                                .isControlVisible())


                            if (backFragment != null) {
                                vModel.getActivity().getSupportFragmentManager()
                                        .beginTransaction().remove(backgroundFragment)
                                        .commitAllowingStateLoss();
                            }
                        application.setTelepresenceControl(true);

                    } else
                        application
                                .setTelepresenceControl(false);


                    application.setEditor(
                            RemoteControlUtil.REMOTE_CONTROL,
                            "ON");
                    application.unlockScreen();
                    application.setChatEnabled(true);

                    String messagetoSend = "";

                    if (!application.getRobotConnexion().equals(
                            "true")) {

                        if (application.isEnoughBandWith())
                            messagetoSend = "test norobot goodBandWith";
                        else
                            messagetoSend = "test norobot badBandWith";
                    } else {
                        if (application.isEnoughBandWith())
                            messagetoSend = "test OKCONTROL goodBandWith";
                        else
                            messagetoSend = "test OKCONTROL badBandWith";
                    }
                    application
                            .notifyObservers(messagetoSend);

                    if (application.isControlVisible() == false)
                        application.StartAct(null, null, ControlRobot.class,
                                vModel.getActivity());


                } else if (message.contains("test")
                        && !message.contains("openSnapFile") && !message.contains("SpeechAPI") && !message.contains(";")) {
                    if (call.equals("linphone"))
                        sendLinphoneInitSetting();
                    if (message.contains("LognIn")) {
                        if (!application.getRobotConnexion().equals("true"))

                            application.notifyObservers("connect");
                    } else
                        flagTest = true;
                    application.setChatEnabled(true);

                }
                if (message.contains("noncontrol")
                        || message.equals("controlViewStarted")) {
                    sendLinphoneInitSetting();
                }
                if (message.equals("startCalling")) {
                    sendLinphoneInitSetting();
                }
                if (message.contains("speakerOff")) {
                    application.setRobotEditor(RemoteControlUtil.SPEAKER,
                            "false");
                    application.enableSpeaker(false);
                }
                if (message.contains("speakerOn")) {
                    application.setRobotEditor(RemoteControlUtil.SPEAKER,
                            "true");
                    application.enableSpeaker(true);
                }
                if (message.contains("closedrawer")) {
                    drawer.closeDrawers();
                }
                if (message.equals("sacanStarted")) {
                    final Toast toast = Toast.makeText(
                            application.getApplicationContext(),
                            R.string.bleDiscovery, Toast.LENGTH_SHORT);
                    application.getActivity().runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            toast.show();

                        }
                    });

                    hand.postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            toast.cancel();
                        }
                    }, 1000);
                }
                if (message.equals("launchControl")
                        || message.equals("pause")) {
                    if (application.isMusicPlaying())
                        application.stopMusic();

                    if (application.isRadioPlaying())
                        application.stopRadio();
                    flagStarted = false;
                    if (message.equals("launchControl")) {
                        if (application.isControlVisible() == false)
                            application.StartAct(null, null, ControlRobot.class,
                                    vModel.getActivity());
                    }


                }
                if (message.contains("RobotOn")) {

                    flagRobotOn = true;

                    connect.setImageResource(R.drawable.disconnect_btn);

                    if (flagDrawerClosed) {
                        if (application.getMacConnectedDevice().equals(
                                application.getDeviceAddress())) {

                            showControlViews();
                        }
                    }
                    if (application.getChatEnabled() == true) {
                        if (call.equals("linphone")) {
                            ServiceRobot.sendMessage(
                                    application.getPseudoOpenFireSender(),
                                    "robot;true;"
                                            + application.getInstalledPseudoSkype());
                        } else if (call.equals("webrtc")) {
                            application.notifyObservers("webrtcsend:robot;true");
                        }
                    }

                    if (application.isFlagTest() == true) {
                        if (application.getFlagLock() == true) {
                            if (call.equals("linphone")) {
                                ServiceRobot.sendMessage(
                                        application.getPseudoOpenFireSender(),
                                        "test " + RemoteControlUtil.XMPP_OK
                                                + ";true");
                            }
                        } else {
                            if (call.equals("linphone")) {
                                ServiceRobot.sendMessage(
                                        application.getPseudoOpenFireSender(),
                                        "test " + RemoteControlUtil.XMPP_OK
                                                + ";false");
                            }

                        }
                    }

                }
                if (message.equals("RobotOff")) {

                    flagRobotOn = false;
                    // application.setIsServiceOn(false);

                    hideControlViews();
                    etat_robot.setImageResource(R.drawable.offline_icon);

                    connect.setImageResource(R.drawable.connect_btn);

                    if (application.getChatEnabled() == true) {
                        if (call.equals("linphone")) {
                            ServiceRobot.sendMessage(
                                    application.getPseudoOpenFireSender(),
                                    "robot;false;"
                                            + application.getInstalledPseudoSkype());
                        } else if (call.equals("webrtc")) {
                            application.notifyObservers("webrtcsend:robot;false");
                        }

                    }
                }
                if (message.contains("noncontrol")) {
                    disconnect(false);
                }
                if (message.contains("serviceConnected")) {

                    if (call.equals("linphone")) {
                        flagServer = true;
                        etat_xmpp.setImageResource(R.drawable.pastill_blue);
                        String presence = ServiceRobot.roster.getPresence(
                                application.getEmail().replace("@", "_") + "@"
                                        + application.getAdresseOpenFire())
                                .toString();

                        if (presence.equals("available")) {
                            ServiceRobot.sendMessage(
                                    application.getPseudoOpenFireSender(),
                                    "isControl");
                        }
                    } else {
                        etat_xmpp.setImageResource(R.drawable.pastill_blue);
                    }
                    application.setFlagServer(true);

                }
                if (message.contains("ControlYes")) {

                    application.setChatEnabled(true);
                    if (application.getRobotConnexion().equals("true")) {
                        if (call.equals("linphone")) {
                            ServiceRobot.sendMessage(
                                    application.getPseudoOpenFireSender(),
                                    "robot;true;"
                                            + application.getInstalledPseudoSkype());
                        }

                    } else {
                        if (call.equals("webrtc")) {
                            application.notifyObservers("webrtcsend:robot;true");

                        }

                    }
                }
                if (message.equals("displaySkype")) {
                    skype.setVisibility(View.VISIBLE);
                    switch_cam.setVisibility(View.GONE);
                }

                if (message.equals("enableCamera")) {
                    application.enableOrDisableCamera(true);

                }
                if (message.equals("disableCamera")) {

                    application.enableOrDisableCamera(false);
                }
                if (message.equals("switchCamera")) {
                    vController.switchCamera();
                }
                if (message.contains("robotVideoSize")) {
                    application.setVideoSize(message);
                }
                if (message.equals("terminateCall")) {
                    if (vController != null)
                        vController.terminateCall();

                }
                if (message.equals("disableMicro")) {
                    application.setRobotEditor(RemoteControlUtil.MIC, "false");
                    application.enableOrDisableMic(true);
                }
                if (message.equals("enableMicro")) {
                    application.setRobotEditor(RemoteControlUtil.MIC, "true");
                    application.enableOrDisableMic(false);

                }
                if (message.contains("echoCancelation")) {
                    application.echoCancellation(message);
                }
                if (message.contains("volume")) {
                    application.adjustVolume(
                            Integer.parseInt(message.split(";")[1]),
                            Integer.parseInt(message.split(";")[2]));

                }
                if (message.contains("serverlogout")) {
                    if (call.equals("linphone")) {
                        flagServer = false;
                    }
                    etat_xmpp.setImageResource(R.drawable.offline_icon);
                    application.setFlagServer(false);


                }
                if (message.contains("siplogout")) {
                    etat_sip.setImageResource(R.drawable.offline_icon);

                }
                if (message.contains("siplogin")) {

                    etat_sip.setImageResource(R.drawable.pastill_green);

                }
                if (message.equals("setControlInvisible")) {
                    flagPopupOpend = true;
                    setControlVisible(false);
                    drawer.closeDrawers();
                }
                if (message.equals("setControlVisible")) {
                    setControlVisible(true);
                    drawer.closeDrawers();

                }

            }

        });

    }


    private void hideControlViews() {
        // TODO Auto-generated method stub
        control_panel.setVisibility(View.GONE);
        control_panel_joystick.setVisibility(View.GONE);
        control_panel_gyro.setVisibility(View.GONE);
    }

    private void showControlViews() {
        // TODO Auto-generated method stub

        if (application.getModeControl().equals("joystick")) {
            controlJoystique = new ControlWithJoystique(vController, vModel);
            controlJoystique.setWidget(joystick);
            control_panel_joystick.setVisibility(View.VISIBLE);
            application.getActivity().setRequestedOrientation(
                    ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        } else if (application.getModeControl().equals("gyroscope")) {
            controlGyroscope = new ControlWithGyroscope(vController, vModel);
            PackageManager packageManager = vModel.getActivity()
                    .getPackageManager();
            boolean gyroExists = packageManager
                    .hasSystemFeature(PackageManager.FEATURE_SENSOR_GYROSCOPE);
            mDisplay = ((WindowManager) vModel.getActivity().getSystemService(
                    Context.WINDOW_SERVICE)).getDefaultDisplay();
            if (gyroExists == true) {
                sensorManager = (SensorManager) vModel.getActivity()
                        .getSystemService(Context.SENSOR_SERVICE);
                flagGyro = true;

            } else {
                sensorManager = (SensorManager) vModel.getActivity()
                        .getSystemService(Context.SENSOR_SERVICE);

            }
            controlGyroscope.setWidget(joyGyro, mDisplay, sensorManager,
                    flagGyro);
            control_panel_gyro.setVisibility(View.VISIBLE);
            application.getActivity().setRequestedOrientation(
                    ActivityInfo.SCREEN_ORIENTATION_LOCKED);
        } else if (application.getModeControl().equals("nocontrol")) {
            control_panel.setVisibility(View.GONE);
            control_panel_joystick.setVisibility(View.GONE);
            control_panel_gyro.setVisibility(View.GONE);
        } else {
            controlSimple = new ControlWithButtonSimple(vController, vModel);
            forwardButton = (ImageButton) control_panel.findViewById(R.id.haut);
            backwardButton = (ImageButton) control_panel.findViewById(R.id.bas);
            rightButton = (ImageButton) control_panel
                    .findViewById(R.id.turnright);
            leftButton = (ImageButton) control_panel
                    .findViewById(R.id.turnleft);
            controlSimple.setWidget(forwardButton, backwardButton, rightButton,
                    leftButton);
            controlSimple.setListners();
            control_panel.setVisibility(View.VISIBLE);
            application.getActivity().setRequestedOrientation(
                    ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        }

        etat_robot
                .setImageResource(R.drawable.pastill_yellow);
    }

    @SuppressWarnings("deprecation")
    public void destroy() {
        // TODO Auto-generated method stub
        application.removeObserver(ControlRobotView.this);
        try {
            if (flagStarted == true) {
                vModel.getActivity().getSupportFragmentManager()
                        .beginTransaction().remove(backgroundFragment)
                        .commitAllowingStateLoss();
            }
        } catch (Exception e) {

        }

        if (vController != null) {
            vController.destroy();
            vController = null;

        }
        handler.removeCallbacks(runTestDebit);


    }

    private void disconnect(Boolean local) {
        // TODO Auto-generated method stub
        // application.setIsServiceOn(false);

        hideControlViews();
        etat_robot.setImageResource(R.drawable.offline_icon);

        connect.setImageResource(R.drawable.connect_btn);

    }

    public PopupWindow initialpoup_setting(View layoutSetteing, View button,
                                           PopupWindow popup) {

        popup = new PopupWindow(layoutSetteing,
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT, true);

        popup.showAtLocation(button, 1, 1, 1);

        final PopupWindow pwindo_setting = popup;

        flagPopupOpend = true;
        setControlVisible(false);
        drawer.closeDrawers();

        return pwindo_setting;
    }

    public void setControlVisible(Boolean flag) {
        if (flag) {
            application.notifyObservers("showMic");
            flagPopupOpend = false;
            if (debit_indicator.equals("values"))
                control_panel_debit.setVisibility(View.VISIBLE);
            else if (debit_indicator.equals("icone"))
                control_panel_debit_icons.setVisibility(View.VISIBLE);
            logo.setVisibility(View.VISIBLE);
            etat_robot.setVisibility(View.VISIBLE);
            etat_xmpp.setVisibility(View.VISIBLE);
            etat_sip.setVisibility(View.VISIBLE);

            if (application.getMacConnectedDevice().equals(
                    application.getDeviceAddress())
                    || application.getRobotType() == 6) {
                if (application.getRobotConnexion().equals("true")
                        && !application.getModeControl().equals("nocontrol")) {
                    showControlViews();
                    application.notifyObservers("visible;oui");
                }
            }
        } else {
            application.notifyObservers("hideMic");
            if (debit_indicator.equals("values"))
                control_panel_debit.setVisibility(View.GONE);
            else if (debit_indicator.equals("icone"))
                control_panel_debit_icons.setVisibility(View.GONE);


            logo.setVisibility(View.GONE);
            etat_robot.setVisibility(View.GONE);
            etat_xmpp.setVisibility(View.GONE);
            etat_sip.setVisibility(View.GONE);
            application.notifyObservers("hideTexts");
            application.notifyObservers("visible;non");
            hideControlViews();
        }

    }

    public void sendLinphoneInitSetting() {
        // TODO Auto-generated method stub
        if (call.equals("linphone")) {
            String MIC_enable = application.getMicState();
            if (MIC_enable.equals("false"))
                ServiceRobot.sendMessage(application.getPseudoOpenFireSender(),
                        "botMicOff");
            else
                ServiceRobot.sendMessage(application.getPseudoOpenFireSender(),
                        "botMicOn");

            if (application.getCamState().equals("off")) {
                ServiceRobot.sendMessage(application.getPseudoOpenFireSender(),
                        "camBotOff");
            } else {
                ServiceRobot.sendMessage(application.getPseudoOpenFireSender(),
                        "camBotOn");
            }
            String speaker_enable = application.getSpeakerState();

            if (speaker_enable.equals("false")) {
                ServiceRobot.sendMessage(application.getPseudoOpenFireSender(),
                        "speakerOff");

            } else
                ServiceRobot.sendMessage(application.getPseudoOpenFireSender(),
                        "speakerOn");

            ServiceRobot.sendMessage(application.getPseudoOpenFireSender(),
                    "botVideoSize;" + application.getVideoQuality());

            if (application.getEchoConcellation().equals("false")) {
                ServiceRobot.sendMessage(application.getPseudoOpenFireSender(),
                        "botEcho;false");
            } else {
                ServiceRobot.sendMessage(application.getPseudoOpenFireSender(),
                        "botEcho;true");
            }
            AudioManager au = (AudioManager) application
                    .getSystemService(Context.AUDIO_SERVICE);
            int maxMedia = au.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
            ServiceRobot.sendMessage(application.getPseudoOpenFireSender(),
                    "botMedia;" + application.getVolumeMedia() + ";" + maxMedia);
        } else if (call.equals("webrtc")) {
            String MIC_enable = application.getMicState();
            if (MIC_enable.equals("false")) {
                application.notifyObservers("botMicOff");
            } else {
                application.notifyObservers("botMicOn");
            }
        }

    }
}