package com.orrobotics.orsmartrobot.view;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import org.linphone.core.LinphoneAddress;
import org.linphone.core.LinphoneAuthInfo;
import org.linphone.core.LinphoneCall;
import org.linphone.core.LinphoneCall.State;
import org.linphone.core.LinphoneCallStats;
import org.linphone.core.LinphoneChatMessage;
import org.linphone.core.LinphoneChatRoom;
import org.linphone.core.LinphoneContent;
import org.linphone.core.LinphoneCore;
import org.linphone.core.LinphoneCore.AuthMethod;
import org.linphone.core.LinphoneCore.EcCalibratorStatus;
import org.linphone.core.LinphoneCore.GlobalState;
import org.linphone.core.LinphoneCore.LogCollectionUploadState;
import org.linphone.core.LinphoneCore.RegistrationState;
import org.linphone.core.LinphoneCore.RemoteProvisioningState;
import org.linphone.core.LinphoneCoreListener;
import org.linphone.core.LinphoneEvent;
import org.linphone.core.LinphoneFriend;
import org.linphone.core.LinphoneFriendList;
import org.linphone.core.LinphoneInfoMessage;
import org.linphone.core.LinphoneProxyConfig;
import org.linphone.core.PublishState;
import org.linphone.core.SubscriptionState;

import com.orrobotics.linphone.FinishListener;
import com.orrobotics.linphone.VideoSnapFragment;
import com.orrobotics.orsmartrobot.R;
import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.controller.ControlRobotController;
import com.orrobotics.orsmartrobot.controllerImpl.ControlRobotControllerImpl;
import com.orrobotics.orsmartrobot.model.ControlRobotModel;
import com.orrobotics.orsmartrobot.modelImpl.ControlRobotModelImpl;
import com.orrobotics.orsmartrobot.service.TestDebitService;
import com.orrobotics.orsmartrobot.ui.BatteryView;
import com.orrobotics.orsmartrobot.util.MessageAdapter;
import com.orrobotics.orsmartrobot.util.VerticalSeekBar;
import com.orrobotics.orsmartrobot.util.VerticalSeekBar_Reverse;
import com.zerokol.views.JoystickView;
import com.zerokol.views.JoystickViewModif;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * @author Manar Boukhari
 *
 */
@SuppressLint("InlinedApi")
public class ControlRobot extends BaseFragmentActivity implements
		LinphoneCoreListener, FinishListener {


	/**
	 * le numero de serie du robot
	 *
	 */
	TextView nineId;

	/**
	 * le clic sur cette bouton permet de connecter/déconnecter le robot
	 **/
	ImageButton connect;


	/**
	 * Afficher la valeur de batterie
	 **/
	BatteryView battrie;

	Activity acti;

	ControlRobotView mVieu;

	SmartRobotApplication application;
	/**
	 * mode de control
	 **/
	String mode;
	/**
	 * Intanciation de la joystique
	 *
	 */
	JoystickView joystick;
	/**
	 * Intanciation de la gyroscope
	 *
	 */
	JoystickViewModif joyGyro;
	/**
	 * Paramétre pour la gyroscope
	 *
	 */
	ImageButton setting;

	/**
	 * list defilement des message
	 */
	ListView messagesView;
	MessageAdapter messageAdapter;

	ImageView etat_robot;
	ImageView etat_sip;
	ImageView etat_xmpp;


	DrawerLayout drawer;
	ImageView logo;
	RelativeLayout control_panel;
	RelativeLayout control_panel_joystick;
	RelativeLayout control_panel_gyro;
	RelativeLayout control_panel_vocale;
	Fragment snapFragment;
	VideoSnapFragment videoSnapFragment;
	TextView commandeResponce;

	ControlWithVoice controlVoice;
	ImageButton skype;
	ImageView robot_icon;
	TextView robot_name;
	View drawerView;
	/**
	 * le clic sur ce bouton pour lancer une commande vocale
	 */
	ImageButton microButton;
	/**
	 * champ pour afficher commande vocale
	 */
	TextView Cmdtext;

	Bitmap open;
	String btnPosition = "haut/bas(vertical,droite);gauche/droite(vertical,gauche)";


	/**
	 * Les radios boutons pour changer le mode de contrôle
	 */

	RadioGroup radio;
	RadioButton radio01;
	RadioButton radio02;
	RadioButton radio03;
	RadioButton radio04;

	CheckBox radio00;
	CheckBox radio001;
	ImageButton camera;
	RelativeLayout switch_cam;
	ControlRobotModel model;
	ControlRobotController control;

	public static FragmentTransaction transaction;
	private Handler mControlsHandler = new Handler();
	private static List<FinishListener> finishListeners = new ArrayList<FinishListener>();

	private Runnable mControls;

	VerticalSeekBar_Reverse download_speed_bar ;
	VerticalSeekBar upload_speed_bar;
	TextView download_speed;
	TextView upload_speed;
	RelativeLayout control_panel_debit;
	TextView download_speed_min;
	TextView upload_speed_min;
	ImageButton stop_music;
	String screenLock="off";
	ImageView microGif;
	AnimationDrawable animationKeyWord = new AnimationDrawable();
	AnimationDrawable animationCmd = new AnimationDrawable();
	AnimationDrawable animationResponse = new AnimationDrawable();
	ImageButton stop_call;

	RelativeLayout control_panel_debit_icons;
	ImageView upload_speed_icon;
	ImageView download_speed_icon;

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		/**
		 * Initialisation des paramètres
		 **/
		View v = getLayoutInflater().inflate(R.layout.control_layout, null);
		screenLock = SmartRobotApplication.getParamFromFile("screen.lock",
				"Configuration.properties", null);
		if(screenLock == null)
			screenLock="off";

		if(screenLock.equals("off"))
			v.setKeepScreenOn(true);
		else
			v.setKeepScreenOn(false);

		setContentView(v);

		application = (SmartRobotApplication) this.getApplicationContext();

		acti = this;

		application.stricMode();
		application.setIsControl(true);
		application.setActivity(acti);

		initLayouts();

	}

	private void initLayouts() {

		// TODO Auto-generated method stub

		mode = application.getModeControl();
		if (SmartRobotApplication.getParamFromFile("boutons.position",
				"Configuration.properties", null) != null)
			btnPosition = SmartRobotApplication.getParamFromFile(
					"boutons.position", "Configuration.properties", null);

		logo = (ImageView) findViewById(R.id.logo);
		/**
		 * Géstion du logo
		 */
		if (modelApp.isLogoOptionEnabled()) {
			open = application.getLogoFromExternalStorage();
		}
		if (open != null)
			logo.setImageBitmap(Bitmap
					.createScaledBitmap(open, 128, 128, false));

		control_panel_joystick = (RelativeLayout) findViewById(R.id.control_panel_joystick);
		control_panel_gyro = (RelativeLayout) findViewById(R.id.control_panel_gyro);
		control_panel_vocale = (RelativeLayout) findViewById(R.id.control_panel_vocale);
		control_panel_debit = (RelativeLayout) findViewById(R.id.control_panel_debit);
		switch_cam = (RelativeLayout) findViewById(R.id.relative2);

		control_panel_debit_icons = (RelativeLayout) findViewById(R.id.control_panel_debit_icons);
		upload_speed_icon = (ImageView) findViewById(R.id.upload_speed_icon_off);
		download_speed_icon = (ImageView) findViewById(R.id.download_speed_icon_off);

		if (btnPosition
				.equals("haut/bas(vertical,droite);gauche/droite(horizontal,gauche)")) {
			control_panel = (RelativeLayout) findViewById(R.id.control_panel_simple_buttons_2);
		} else if (btnPosition
				.equals("haut/bas(vertical,gauche);gauche/droite(horizontal,droite)")) {
			control_panel = (RelativeLayout) findViewById(R.id.control_panel_simple_buttons_2_);
		} else if (btnPosition
				.equals("haut/bas(horizontal,droite);gauche/droite(vertical,gauche)")) {
			control_panel = (RelativeLayout) findViewById(R.id.control_panel_simple_buttons_3);
		} else if (btnPosition
				.equals("haut/bas(horizontal,gauche);gauche/droite(vertical,droite)")) {
			control_panel = (RelativeLayout) findViewById(R.id.control_panel_simple_buttons_3_);
		} else if (btnPosition
				.equals("haut/bas(horizontal,droite);gauche/droite(horizontal,gauche)")) {
			control_panel = (RelativeLayout) findViewById(R.id.control_panel_simple_buttons_1_);
		} else if (btnPosition
				.equals("haut/bas(horizontal,gauche);gauche/droite(horizontal,droite)")) {
			control_panel = (RelativeLayout) findViewById(R.id.control_panel_simple_buttons_1);
		} else if (btnPosition
				.equals("haut/bas(vertical,gauche);gauche/droite(vertical,gauche)")) {
			control_panel = (RelativeLayout) findViewById(R.id.control_panel_simple_buttons_4);
		} else if (btnPosition
				.equals("haut/bas(vertical,droite);gauche/droite(vertical,droite)")) {
			control_panel = (RelativeLayout) findViewById(R.id.control_panel_simple_buttons_5);
		} else if (btnPosition
				.equals("haut/bas(vertical,gauche);gauche/droite(vertical,droite)")) {
			control_panel = (RelativeLayout) findViewById(R.id.control_panel_simple_buttons_0_);
		} else {
			control_panel = (RelativeLayout) findViewById(R.id.control_panel_simple_buttons_0);
		}

		joystick = (JoystickView) control_panel_joystick
				.findViewById(R.id.joystickView1);
		joyGyro = (JoystickViewModif) control_panel_gyro
				.findViewById(R.id.joystickView);

		if (mode.equals("nocontrol")) {
			control_panel.setVisibility(View.GONE);
			control_panel_joystick.setVisibility(View.GONE);
			control_panel_gyro.setVisibility(View.GONE);
			control_panel_vocale.setVisibility(View.GONE);
		}

		if (application.getRobotType() != 3) {
			control_panel_vocale.setVisibility(View.VISIBLE);
			microButton = (ImageButton) control_panel_vocale
					.findViewById(R.id.micro);
			Cmdtext = (TextView) findViewById(R.id.TextView1);
			commandeResponce = (TextView) findViewById(R.id.TextView2);

			messagesView = (ListView) findViewById(R.id.msgslist);
			messageAdapter = new MessageAdapter(getApplicationContext(), R.layout.right);
			messagesView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
			messagesView.setAdapter(messageAdapter);
			messageAdapter.registerDataSetObserver(new DataSetObserver() {
				@Override
				public void onChanged() {
					super.onChanged();
					messagesView.setSelection(messageAdapter.getCount() - 1);
				}
			});

			stop_music = (ImageButton) findViewById(R.id.stop_music);
			stop_call = (ImageButton) findViewById(R.id.stop_call);

			animationKeyWord.addFrame(getResources().getDrawable(R.drawable.f0), 50);
			animationKeyWord.addFrame(getResources().getDrawable(R.drawable.f1), 50);
			animationKeyWord.addFrame(getResources().getDrawable(R.drawable.f2), 50);
			animationKeyWord.addFrame(getResources().getDrawable(R.drawable.f3), 50);
			animationKeyWord.addFrame(getResources().getDrawable(R.drawable.f4), 50);
			animationKeyWord.addFrame(getResources().getDrawable(R.drawable.f5), 50);
			animationKeyWord.addFrame(getResources().getDrawable(R.drawable.f6), 50);
			animationKeyWord.addFrame(getResources().getDrawable(R.drawable.f7), 50);
			animationKeyWord.addFrame(getResources().getDrawable(R.drawable.f8), 50);
			animationKeyWord.addFrame(getResources().getDrawable(R.drawable.f9), 50);
			animationKeyWord.addFrame(getResources().getDrawable(R.drawable.f11), 50);
			animationKeyWord.addFrame(getResources().getDrawable(R.drawable.f12), 50);
			animationKeyWord.addFrame(getResources().getDrawable(R.drawable.f13), 50);
			animationKeyWord.setOneShot(false);

			microGif = (ImageView) findViewById(R.id.microGif);

			animationCmd.addFrame(getResources().getDrawable(R.drawable.fram0), 50);
			animationCmd.addFrame(getResources().getDrawable(R.drawable.fram1), 50);
			animationCmd.addFrame(getResources().getDrawable(R.drawable.fram2), 50);
			animationCmd.addFrame(getResources().getDrawable(R.drawable.fram3), 50);
			animationCmd.addFrame(getResources().getDrawable(R.drawable.fram4), 50);
			animationCmd.addFrame(getResources().getDrawable(R.drawable.fram5), 50);
			animationCmd.addFrame(getResources().getDrawable(R.drawable.fram6), 50);
			animationCmd.addFrame(getResources().getDrawable(R.drawable.fram7), 50);
			animationCmd.addFrame(getResources().getDrawable(R.drawable.fram8), 50);
			animationCmd.addFrame(getResources().getDrawable(R.drawable.fram9), 50);
			animationCmd.addFrame(getResources().getDrawable(R.drawable.fram11), 50);
			animationCmd.addFrame(getResources().getDrawable(R.drawable.fram12), 50);
			animationCmd.addFrame(getResources().getDrawable(R.drawable.fram13), 50);
			animationCmd.addFrame(getResources().getDrawable(R.drawable.fram14), 50);
			animationCmd.addFrame(getResources().getDrawable(R.drawable.fram15), 50);
			animationCmd.addFrame(getResources().getDrawable(R.drawable.fram16), 50);
			animationCmd.addFrame(getResources().getDrawable(R.drawable.fram17), 50);
			animationCmd.setOneShot(false);

			animationResponse.addFrame(getResources().getDrawable(R.drawable.frame_0), 50);
			animationResponse.addFrame(getResources().getDrawable(R.drawable.frame_1), 50);
			animationResponse.addFrame(getResources().getDrawable(R.drawable.frame_2), 50);
			animationResponse.addFrame(getResources().getDrawable(R.drawable.frame_3), 50);
			animationResponse.addFrame(getResources().getDrawable(R.drawable.frame_4), 50);
			animationResponse.addFrame(getResources().getDrawable(R.drawable.frame_5), 50);
			animationResponse.addFrame(getResources().getDrawable(R.drawable.frame_6), 50);
			animationResponse.addFrame(getResources().getDrawable(R.drawable.frame_7), 50);
			animationResponse.addFrame(getResources().getDrawable(R.drawable.frame_8), 50);
			animationResponse.addFrame(getResources().getDrawable(R.drawable.frame_9), 50);
			animationResponse.addFrame(getResources().getDrawable(R.drawable.frame_10), 50);
			animationResponse.addFrame(getResources().getDrawable(R.drawable.frame_11), 50);
			animationResponse.addFrame(getResources().getDrawable(R.drawable.frame_12), 50);
			animationResponse.addFrame(getResources().getDrawable(R.drawable.frame_13), 50);
			animationResponse.addFrame(getResources().getDrawable(R.drawable.frame_14), 50);
			animationResponse.addFrame(getResources().getDrawable(R.drawable.frame_15), 50);
			animationResponse.addFrame(getResources().getDrawable(R.drawable.frame_16), 50);
			animationResponse.setOneShot(false);
		}

		// recuperation de l'icone du robot
		String icon = application.getRobotIcone();
		byte[] bm = icon.getBytes();
		byte[] b = Base64.decode(bm, Base64.DEFAULT);
		Bitmap bmp = BitmapFactory.decodeByteArray(b, 0, b.length);

		// linphone video fragment

		// snap files fragment
		snapFragment = new VideoSnapFragment(getApplicationContext());
		videoSnapFragment = (VideoSnapFragment) snapFragment;
		snapFragment.setArguments(getIntent().getExtras());
		getSupportFragmentManager().beginTransaction()
				.add(R.id.fragmentContainerSnap, snapFragment)
				.commitAllowingStateLoss();

		skype = (ImageButton) findViewById(R.id.skype);
		robot_icon = (ImageView) findViewById(R.id.robot);
		robot_icon.setImageBitmap(bmp);

		robot_name = (TextView) findViewById(R.id.robot_name);
		setting = (ImageButton) findViewById(R.id.setting_robot);

		radio = (RadioGroup) findViewById(R.id.radioGroup1);

		radio01 = (RadioButton) findViewById(R.id.radio0);
		radio02 = (RadioButton) findViewById(R.id.radio1);
		radio03 = (RadioButton) findViewById(R.id.radio2);
		radio04 = (RadioButton) findViewById(R.id.radio3);

		radio00 = (CheckBox) findViewById(R.id.radio);
		radio001 = (CheckBox) findViewById(R.id.radio01);
		robot_name.setText(application.getRobotName());

		if (application.getRobotType() == 1 || application.getRobotType() == 5
				|| application.getRobotType() == 6) {

			setting.setVisibility(View.VISIBLE);
		}

		etat_robot = (ImageView) findViewById(R.id.robotConnexion);
		etat_sip = (ImageView) findViewById(R.id.sipConnexion);
		etat_xmpp = (ImageView) findViewById(R.id.xmppConnexion);
		connect = (ImageButton) findViewById(R.id.conn);

		upload_speed_bar= (VerticalSeekBar) findViewById(R.id.upload_speed_bar);
		upload_speed_bar.setProgressDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.debit_bar));
		download_speed_bar =(VerticalSeekBar_Reverse)findViewById(R.id.download_speed_bar);
		download_speed_bar.setProgressDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.debit_bar));
		download_speed = (TextView) findViewById(R.id.download_speed);
		upload_speed  = (TextView) findViewById(R.id.upload_speed);
		download_speed_min = (TextView) findViewById(R.id.download_speed_min);
		upload_speed_min = (TextView) findViewById(R.id.upload_speed_min);

		if (application.getRobotType() == 5 || application.getRobotType() == 6)
			connect.setVisibility(View.GONE);
		camera = (ImageButton) findViewById(R.id.camera);

		if (modelApp.getCallType().equals("linphone")) {
			switch_cam.setVisibility(View.VISIBLE);
			skype.setVisibility(View.GONE);
		} else {
			switch_cam.setVisibility(View.GONE);
			skype.setVisibility(View.VISIBLE);
		}


		drawer = (DrawerLayout) findViewById(R.id.drawer_navigation_layout);
		drawer.setScrimColor(Color.TRANSPARENT);


		drawerView = findViewById(R.id.drawer_navigation);

		drawerView.setOnTouchListener(new OnTouchListener() {

			@SuppressLint("ClickableViewAccessibility")
			@Override
			public boolean onTouch(View v, android.view.MotionEvent event) {
				// TODO Auto-generated method stub
				return true;
			}
		});
		drawer.setScrimColor(Color.TRANSPARENT);


		/**
		 * Construction du MVC
		 **/
		if (application.isOrientation() == false) {
			model = new ControlRobotModelImpl(getApplicationContext(), this);
			control = new ControlRobotControllerImpl(model);

			mVieu = new ControlRobotView(control, model);

		}
		Bundle extras = getIntent().getExtras();
		if (extras != null) {

			Boolean autoStart = extras.getBoolean("autostart");
			if (autoStart != null) {
				application.setData(application.getEmail(),
						application.getPasswod(), application.getNom(),
						application.getPrenom(),
						application.getPseudoOpenFire(),
						application.getPasswordOpenFire(),
						application.getPseudoSIP(),
						application.getPasswordSIP());
			}

		}
		mVieu.setWidget(etat_robot, etat_sip, etat_xmpp, setting,
				videoSnapFragment, joystick, joyGyro, drawer,
				logo, control_panel, control_panel_joystick,
				control_panel_gyro,skype, camera, connect, radio,
				radio01, radio02, radio03, radio04, radio00, radio001,
				switch_cam,upload_speed_bar,download_speed_bar,download_speed,upload_speed,
				control_panel_debit,download_speed_min,upload_speed_min,stop_call,control_panel_debit_icons,upload_speed_icon,download_speed_icon);

		mVieu.setListners();
		//mVieu.setZOrderOnTop(true);

		if (application.getRobotType() != 3) {
			controlVoice = new ControlWithVoice(control, model);
			controlVoice.setWidget(microButton, Cmdtext, commandeResponce, messagesView, messageAdapter, stop_music,microGif,animationKeyWord,animationCmd,animationResponse);
		}

	}

	/**
	 * Le clic sur le bouton retoure du Smartphone
	 *
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_HOME) {
			application.enableKeyGuard();
			Intent intent = new Intent(Intent.ACTION_MAIN);
			intent.addCategory(Intent.CATEGORY_HOME);
			startActivity(intent);
			return true;
		}
		return false;
	}

	@SuppressWarnings("deprecation")
	@Override
	protected void onPause() {

		super.onPause();
		SmartRobotApplication.ControlPaused();


		if (!application.isScreenOn()) {

			application.notifyObservers("screenOff");

		}
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	@Override
	protected void onResume() {

		super.onResume();
		SmartRobotApplication.ControlResumed();
		application.disableKeyGuard();

		if (application.isScreenOn()) {
			application.notifyObservers("screenOn");

		}

	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		if(application!=null)
			application.setActivity(this);
		// Checks the orientation of the screen
		if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE
				|| newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
			if(application!=null) {
				application.setOrientation(true);
				application.notifyObservers("unregisterObserver");
				application.notifyObservers("orientationChanged");
			}

			setContentView(R.layout.control_layout);

			if(videoSnapFragment!=null) {
				videoSnapFragment.removeObserver();

				getSupportFragmentManager().beginTransaction()
						.remove(videoSnapFragment).commitAllowingStateLoss();
			}
			if(controlVoice!=null)
				controlVoice.removeObserver();
			initLayouts();

			if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {

			} else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
			}

		}

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		videoSnapFragment.removeObserver();
		if (mControlsHandler != null && mControls != null) {
			mControlsHandler.removeCallbacks(mControls);
		}
		mControls = null;
		mControlsHandler = null;

		application.notifyObservers("unregisterObserver");
		application.setIsControl(false);
		application.setSettingControl(false);
		mVieu.destroy();

		application.notifyObservers("Noplaying");
		if (application.getRobotType() != 3)
			controlVoice.destroy();

	}

//	@Override
//	protected void startListening() {
//
//	}
//
//	@Override
//	protected void stateListening() {
//
//	}
//
//	@Override
//	protected void stateProcessing() {
//
//	}
//
//	@Override
//	protected void stateSpeaking() {
//
//	}
//
//	@Override
//	protected void stateFinished() {
//
//	}
//
//	@Override
//	protected void statePrompting() {
//
//	}
//
//	@Override
//	protected void stateNone() {
//
//	}

	@Override
	public void authInfoRequested(LinphoneCore lc, String realm,
								  String username, String domain) {
		// TODO Auto-generated method stub

	}

	@Override
	public void authenticationRequested(LinphoneCore lc,
										LinphoneAuthInfo authInfo, AuthMethod method) {
		// TODO Auto-generated method stub

	}

	@Override
	public void callStatsUpdated(LinphoneCore lc, LinphoneCall call,
								 LinphoneCallStats stats) {
		// TODO Auto-generated method stub

	}

	@Override
	public void newSubscriptionRequest(LinphoneCore lc, LinphoneFriend lf,
									   String url) {
		// TODO Auto-generated method stub

	}

	@Override
	public void notifyPresenceReceived(LinphoneCore lc, LinphoneFriend lf) {
		// TODO Auto-generated method stub

	}

	@Override
	public void dtmfReceived(LinphoneCore lc, LinphoneCall call, int dtmf) {
		// TODO Auto-generated method stub

	}

	@Override
	public void notifyReceived(LinphoneCore lc, LinphoneCall call,
							   LinphoneAddress from, byte[] event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void transferState(LinphoneCore lc, LinphoneCall call,
							  State new_call_state) {
		// TODO Auto-generated method stub

	}

	@Override
	public void infoReceived(LinphoneCore lc, LinphoneCall call,
							 LinphoneInfoMessage info) {
		// TODO Auto-generated method stub

	}

	@Override
	public void subscriptionStateChanged(LinphoneCore lc, LinphoneEvent ev,
										 SubscriptionState state) {
		// TODO Auto-generated method stub

	}

	@Override
	public void publishStateChanged(LinphoneCore lc, LinphoneEvent ev,
									PublishState state) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show(LinphoneCore lc) {
		// TODO Auto-generated method stub

	}

	@Override
	public void displayStatus(LinphoneCore lc, String message) {
		// TODO Auto-generated method stub

	}

	@Override
	public void displayMessage(LinphoneCore lc, String message) {
		// TODO Auto-generated method stub

	}

	@Override
	public void displayWarning(LinphoneCore lc, String message) {
		// TODO Auto-generated method stub

	}

	@Override
	public void fileTransferProgressIndication(LinphoneCore lc,
											   LinphoneChatMessage message, LinphoneContent content, int progress) {
		// TODO Auto-generated method stub

	}

	@Override
	public void fileTransferRecv(LinphoneCore lc, LinphoneChatMessage message,
								 LinphoneContent content, byte[] buffer, int size) {
		// TODO Auto-generated method stub

	}

	@Override
	public int fileTransferSend(LinphoneCore lc, LinphoneChatMessage message,
								LinphoneContent content, ByteBuffer buffer, int size) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void globalState(LinphoneCore lc, GlobalState state, String message) {
		// TODO Auto-generated method stub

	}

	@Override
	public void registrationState(LinphoneCore lc, LinphoneProxyConfig cfg,
								  RegistrationState state, String smessage) {
		// TODO Auto-generated method stub

	}

	@Override
	public void configuringStatus(LinphoneCore lc,
								  RemoteProvisioningState state, String message) {
		// TODO Auto-generated method stub

	}

	@Override
	public void messageReceived(LinphoneCore lc, LinphoneChatRoom cr,
								LinphoneChatMessage message) {
		// TODO Auto-generated method stub

	}


	@Override
	public void callState(LinphoneCore lc, LinphoneCall call, State state,
						  String message) {
		// TODO Auto-generated method stub

	}

	@Override
	public void callEncryptionChanged(LinphoneCore lc, LinphoneCall call,
									  boolean encrypted, String authenticationToken) {
		// TODO Auto-generated method stub

	}

	@Override
	public void notifyReceived(LinphoneCore lc, LinphoneEvent ev,
							   String eventName, LinphoneContent content) {
		// TODO Auto-generated method stub

	}

	@Override
	public void isComposingReceived(LinphoneCore lc, LinphoneChatRoom cr) {
		// TODO Auto-generated method stub

	}

	@Override
	public void ecCalibrationStatus(LinphoneCore lc, EcCalibratorStatus status,
									int delay_ms, Object data) {
		// TODO Auto-generated method stub

	}

	@Override
	public void uploadProgressIndication(LinphoneCore lc, int offset, int total) {
		// TODO Auto-generated method stub

	}

	@Override
	public void uploadStateChanged(LinphoneCore lc,
								   LogCollectionUploadState state, String info) {
		// TODO Auto-generated method stub

	}

	@Override
	public void friendListCreated(LinphoneCore lc, LinphoneFriendList list) {
		// TODO Auto-generated method stub

	}

	@Override
	public void friendListRemoved(LinphoneCore lc, LinphoneFriendList list) {
		// TODO Auto-generated method stub

	}


	@Override
	public void onFinishCallback() {
		// TODO Auto-generated method stub
		this.finish();
	}

	public static void addFinishListener(FinishListener fl) {
		finishListeners.add(fl);
	}

	public void closeActivity(Context context) {

		for (FinishListener fl : finishListeners) {
			fl.onFinishCallback();

		}
	}

}