package com.orrobotics.orsmartrobot.model;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;

public interface ListRobotModel {
	
	public Context getContext();
	
	public Activity getActivity();
	
	public void setRobotNames(ArrayList<String>  robotNames);
	
	public ArrayList<String> getRobotNames();
	
	public void setRobotAdresses(ArrayList<String>  robotAdresses);
	
	public ArrayList<String> getRobotAdresses();
	
    public void setRobotSerie(ArrayList<String>  robotSerie);
	
	public ArrayList<String> getRobotSerie();
	
	public void setPseudoOpenFire(ArrayList<String>  pseudoOpenFire);
	
	public ArrayList<String> getPseudoOpenFire();
	
	public void setPasswordOpenFire(ArrayList<String>  passwordOpenFire);
	
	public ArrayList<String> getPasswordOpenFire();
	
    public void setPseudoSIP(ArrayList<String>  pseudoSIP);
	
	public ArrayList<String> getPseudoSIP();
	
    public void setPasswordSIP(ArrayList<String>  passwordSIP);
	
	public ArrayList<String> getPasswordSIP();
	
	
	public void setPseudoSkype(ArrayList<String>  pseudoSkype);
	
	public ArrayList<String> getPseudoSkype();
	
	public void setRobotChecked(ArrayList<Boolean>  robotChecked);
	
	public ArrayList<Boolean> getRobotChecked();

	public void clearData();

	public boolean getFlagRefresh();
	
	public void setFlagRefresh(Boolean flagRefresh);
	
	public void setIndexPseudo(ArrayList<Integer> index_pseudo);
	
	public ArrayList<Integer> getIndexPseudo();
	
	public ArrayList<Integer> getIdRobot();

	public void setIdRobot(ArrayList<Integer> idRobot);
	

	

}
