package com.orrobotics.orsmartrobot.model;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;

public interface BleScanModel {

	public Context getContext();

	public ListActivity getActivity();
	
	public Activity getActi();

	public void setScanning(boolean mScanning);

	public boolean getScanning();

}
