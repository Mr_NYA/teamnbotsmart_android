package com.orrobotics.orsmartrobot.model;

import android.app.Activity;
import android.content.Context;

public interface PassRecoverModel {
	
public Context getContext();
	
	public Activity getActivity();

}
