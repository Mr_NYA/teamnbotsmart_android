package com.orrobotics.orsmartrobot.model;


import android.content.Context;

import com.orrobotics.orsmartrobot.view.BaseFragmentActivity;
import com.orrobotics.orsmartrobot.view.BaseFragmentActivityAlexa;

public interface ControlRobotModel {
	
	public Context getContext();

	public BaseFragmentActivity getActivity();

	public String[] getAccelerationReadings();
	
	public void setAccelerationReadings(String[] accelerationReadings);
	
	public String[] getMagnetReadings();
	
	public void setMagnetReadings(String[] magnetReadings);
	
	public String[] getGyroscopeReadings();
	
	public void setGyroscopeReadings(String[] gyroscopeReadings);
	
	public String getBatterie();
	
	public void setBatterie(String batterie);
	
	public String getTemperature();
	
	public void setTemperature(String temperature);
	
	public String getHumidity();
	
	public void setHumidity(String humidity);
	
	public String getPressure();
	
	public void setPressure(String pressure);
	
	public String getCardiaque();
	
	public void setCardiaque(String cardiaque);
	
	public String getStep();
	
	public void setStep(String step);
	
	public String getCalories();
	
	public void setCalories(String calories);
	
	
	public String getLight();
	
	public void setLight(String light);
	

}
