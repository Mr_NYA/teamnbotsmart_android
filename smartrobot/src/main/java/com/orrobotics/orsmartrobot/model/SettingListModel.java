package com.orrobotics.orsmartrobot.model;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;

public interface SettingListModel {
	
	public ArrayList<String> getListVideoSize();

	public Context getContext();

	public Activity getActivity();

	public ArrayList<String> getListAppLangages();
	public ArrayList<String> getListLanguageSpeed();

	public void setLanguageSelection(String langCode);

	public void setLanguageSpeedSelection(int pos);

	public int getLanguageSpeedSelection();

	public String getLanguageSelection();

	public HashMap<String, String> getListLanguage();


}
