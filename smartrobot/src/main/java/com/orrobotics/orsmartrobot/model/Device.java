package com.orrobotics.orsmartrobot.model;

public class Device {

	public String adresse;
	public String numeroSerie;

	public Device(String adresse, String numeroSerie) {
		this.adresse = adresse;
		this.numeroSerie = numeroSerie;
	}

	public void setAdresse(String adresse) {

		this.adresse = adresse;
	}

	public void setNumeroSerie(String numeroSerie) {

		this.numeroSerie = numeroSerie;
	}

	public String getAdresse() {

		return adresse;
	}

	public String getNumeroSerie() {
		return numeroSerie;
	}
}
