/*
 * Copyright (c) 2011, Chad Rockey
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Android Sensors Driver nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.orrobotics.orsmartubo.sensor;

import java.util.List;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Looper;
/**
 * @author Hajiba IFRAH
 */
public class TemperaturePublisher {

	private TemperatureThread tmpThread;
	private SensorListener sensorListener;
	private SensorManager sensorManager;
	private int sensorType;
	private int sensorDelay;
	public static String frameId;
	public static double temperature;
	public static double variance;

	private class TemperatureThread extends Thread {
		private final SensorManager sensorManager;
		private SensorListener sensorListener;
		private final Sensor tmpSensor;

		private TemperatureThread(SensorManager sensorManager,
				SensorListener sensorListener) {
			this.sensorManager = sensorManager;
			this.sensorListener = sensorListener;
			this.tmpSensor = this.sensorManager.getDefaultSensor(sensorType);
		}

		public void run() {
			Looper.prepare();
			Looper.myLooper();
			this.sensorManager.registerListener(this.sensorListener,
					this.tmpSensor, sensorDelay);
			Looper.loop();
		}

		
	}

	private class SensorListener implements SensorEventListener {

		private SensorListener() {
		}

		@Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) {
		}

		@Override
		public void onSensorChanged(SensorEvent event) {
			if (event.sensor.getType() == sensorType) {
				frameId = ("/android_temperature");// TODO Make parameter

				temperature = event.values[0];
				variance = 0.0;

			}
		}
	}

	public TemperaturePublisher(SensorManager manager, int sensorDelay,
			int sensorType) {
		this.sensorManager = manager;
		this.sensorDelay = sensorDelay;
		this.sensorType = sensorType;
		onStart();
	}

	public void onStart() {
		List<Sensor> mfList = this.sensorManager.getSensorList(sensorType);

		if (mfList.size() > 0) {
			this.sensorListener = new SensorListener();
			this.tmpThread = new TemperatureThread(this.sensorManager,
					this.sensorListener);
			this.tmpThread.start();
		}

	}


}
