/*
 * Copyright (c) 2011, Chad Rockey
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Android Sensors Driver nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.orrobotics.orsmartubo.sensor;

import java.util.List;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Looper;
/**
 * @author Hajiba IFRAH
 */
public class MagneticFieldPublisher {

	private MagneticFieldThread mfThread;
	private SensorListener sensorListener;
	private SensorManager sensorManager;
	private int sensorDelay;
	public static String frameId;
	public static double[] magneticFieldCovariance;
	public static double x;
	public static double y;
	public static double z;

	private class MagneticFieldThread extends Thread {
		private final SensorManager sensorManager;
		private SensorListener sensorListener;
		private Looper threadLooper;

		private final Sensor mfSensor;

		private MagneticFieldThread(SensorManager sensorManager,
				SensorListener sensorListener) {
			this.sensorManager = sensorManager;
			this.sensorListener = sensorListener;
			this.mfSensor = this.sensorManager
					.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
		}

		public void run() {
			Looper.prepare();
			this.threadLooper = Looper.myLooper();
			this.sensorManager.registerListener(this.sensorListener,
					this.mfSensor, sensorDelay);
			Looper.loop();
		}

		public void shutdown() {
			this.sensorManager.unregisterListener(this.sensorListener);
			if (this.threadLooper != null) {
				this.threadLooper.quit();
			}
		}
	}

	private class SensorListener implements SensorEventListener {

		@Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) {
		}

		@Override
		public void onSensorChanged(SensorEvent event) {
			if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
		
				frameId="/android_magnetique";// TODO Make parameter

				x=(event.values[0] / 1e6);
				y=(event.values[1] / 1e6);
				z=(event.values[2] / 1e6);

				double[] tmpCov = { 0, 0, 0, 0, 0, 0, 0, 0, 0 }; // TODO Make
																	// Parameter
				magneticFieldCovariance=tmpCov;

			}
		}
	}

	public MagneticFieldPublisher(SensorManager manager, int sensorDelay) {
		this.sensorManager = manager;
		this.sensorDelay = sensorDelay;
		onStart();
	}

	public void onStart() {
		List<Sensor> mfList = this.sensorManager
				.getSensorList(Sensor.TYPE_MAGNETIC_FIELD);

		if (mfList.size() > 0) {
			this.sensorListener = new SensorListener();
			this.mfThread = new MagneticFieldThread(this.sensorManager,
					this.sensorListener);
			this.mfThread.start();
		}

	}

	public void onShutdown() {
		if (this.mfThread == null) {
			return;
		}

		this.mfThread.shutdown();
		try {
			this.mfThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
