package com.orrobotics.orsmartubo.bluetooth;

import android.Manifest;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Binder;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.util.RemoteControlUtil;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.util.UUID;

public class BluetoothService extends Service {


    public static boolean SERVICE_CONNECTED = false;
    static File pathOfMyFile = Environment
            .getExternalStoragePublicDirectory("/SmartRobot/CommandeRobot");
    static SmartRobotApplication model;

    private Context context;

    private Handler mHandler; // Our main handler that will receive callback notifications
    private static ConnectedThread mConnectedThread; // bluetooth background worker thread to send and receive data
    private BluetoothSocketWrapper  mBTSocket = null; // bi-directional client-to-client data path
    private final String TAG = "Arduino_ble";

    private static final UUID BTMODULEUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"); // "random" unique identifier


    // #defines for identifying shared types between calling functions
    private final static int MESSAGE_READ = 2; // used in bluetooth handler to identify message update
    private final static int CONNECTING_STATUS = 3; // used in bluetooth handler to identify message status
    private BluetoothAdapter mBTAdapter;
    private IBinder binder = new BluetoothBinder();
    private boolean socketClosed= false;
    BluetoothConnect mBluetoothConnection;

    @Override
    public void onCreate() {
        this.context = this;
        model = (SmartRobotApplication) getApplicationContext();
        BluetoothService.SERVICE_CONNECTED = true;
        // Ask for location permission if not already allowed
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(model.getActivity(), new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);

        mBTAdapter = BluetoothAdapter.getDefaultAdapter();
        if (!mBTAdapter.isEnabled())
            mBTAdapter.enable();

        IntentFilter blIntent = new IntentFilter();
        blIntent.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        blIntent.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        blIntent.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
        blIntent.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);

        registerReceiver(blReceiver,blIntent);

        String telecommande = model.getParamFromFile("telecomande.mode","Configuration.properties",null);
        if (telecommande == null)
            telecommande ="off";

        if (telecommande.equals("on"))
        mBluetoothConnection = new BluetoothConnect(getApplicationContext());

        connectToDevice(model.getDeviceAddress());

        mHandler = new Handler(){
            public void handleMessage(android.os.Message msg){
                if(msg.what == MESSAGE_READ) {
                    String readMessage = null;
                    try {
                        readMessage = new String((byte[]) msg.obj, "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    if (!readMessage.equals("")) {
                        model.logMe("Commande reçue : " + readMessage, null, "/storage/emulated/0/" +
                                        "SmartRobot/CommandeRobot/logCommandeRobot",
                                "logCommandeRobot", pathOfMyFile);
                        model.notifyObservers("msg" + readMessage);

                    }
                }
                if(msg.what == CONNECTING_STATUS){
                    if(msg.arg1 == 1) {
                        model.setEditor(RemoteControlUtil.CONNECTED_TO_APP, "true");
                        model.setRobotConnexion();
                        model.setEditor(RemoteControlUtil.MAC_CONNECTED_DEVICE,
                                model.getDeviceAddress());
                        model.setMacConnectedDevice();
                        model.notifyObservers("RobotOn");
                    }
                    else {
                         connectToDevice(model.getDeviceAddress());
                        model.notifyObservers("RobotOff");
                        model.setEditor(RemoteControlUtil.CONNECTED_TO_APP, "false");
                        model.setRobotConnexion();
                    }
                }
            }
        };

    }


    public static void write(String data) {
        if(mConnectedThread != null) { //First check to make sure thread created
            Log.i("mbu","---write="+data);
            model.logMe( "Commande envoyée : "+data,null,"/storage/emulated/0/" +
                            "SmartRobot/CommandeRobot/logCommandeRobot",
                    "logCommandeRobot",pathOfMyFile);
            mConnectedThread.write(data);
        }

    }
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return Service.START_NOT_STICKY;
    }
    @Override
    public boolean onUnbind(Intent intent) {
        Log.i("mbu","---onUnbind----");
        closeConnexion();
        unregisterReceiver(blReceiver);
        mBluetoothConnection.destroy();
        stopSelf();
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

     private void closeConnexion(){
        if(SERVICE_CONNECTED) {
            BluetoothService.SERVICE_CONNECTED = false;
            mHandler.removeCallbacksAndMessages(null);
            if (mConnectedThread != null)
                mConnectedThread.cancel();

            if (mBTSocket!= null)
            try {
                mBTSocket.close();
                socketClosed = true;
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

 }
    private BluetoothSocketWrapper  createBluetoothSocket(BluetoothDevice device) throws IOException {
        try {
            final Method m = device.getClass().getMethod("createInsecureRfcommSocketToServiceRecord", new Class[] { UUID.class } );
            return  new NativeBluetoothSocket((BluetoothSocket) m.invoke(device,BTMODULEUUID));
        } catch (Exception e) {
            Log.e(TAG, "Could not create Insecure RFComm Connection",e);
            return  new NativeBluetoothSocket(device.createRfcommSocketToServiceRecord(BTMODULEUUID));
        }

    }

    private void connectToDevice(String address){

        new Thread()
        {
            public void run() {
                if (mBTAdapter.isEnabled()) {
                    BluetoothDevice device = null;
                    boolean fail = false;
                    try {
                        device = mBTAdapter.getRemoteDevice(address);
                    }catch (IllegalArgumentException e){
                     e.printStackTrace();
                    }
                    if(device != null)
                    if (device.getBondState() == BluetoothDevice.BOND_BONDED) {
                        try {
                            //mBTSocket = null;
                            mBTSocket = createBluetoothSocket(device);
                        } catch (IOException e) {
                            e.printStackTrace();
                            fail = true;
                        }
                        // Establish the Bluetooth socket connection.
                        try {
                            mBTSocket.connect();
                        } catch (IOException e1) {
                            e1.printStackTrace();
                            try {
                                try {
                                    mBTSocket.close();
                                } catch (IOException ec) {
                                    ec.printStackTrace();
                                }
                                mBTSocket = new FallbackBluetoothSocket(mBTSocket.getUnderlyingSocket());
                                try {
                                    Thread.sleep(500);
                                } catch (InterruptedException e2) {
                                    e2.printStackTrace();
                                }
                                mBTSocket.connect();

                            } catch (IOException | IllegalStateException | FallbackException e3) {
                                //insert code to deal with this
                                e3.printStackTrace();
                                fail = true;
                                try {
                                    mBTSocket.close();
                                } catch (IOException e4) {
                                    e4.printStackTrace();
                                }
                                mHandler.obtainMessage(CONNECTING_STATUS, -1, -1)
                                        .sendToTarget();
                            }
                        }
                        if (fail == false) {
                            mConnectedThread = new ConnectedThread(mBTSocket);
                            mConnectedThread.start();

                            mHandler.obtainMessage(CONNECTING_STATUS, 1, -1, null)
                                    .sendToTarget();
                        }
                    }
                    else if (device.getBondState() == BluetoothDevice.BOND_NONE){
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                            device.createBond();
                        }
                    }
                    } else {
                    mBTAdapter.enable();
                }

            }}.start();
    }
    private class ConnectedThread extends Thread {
        private final BluetoothSocketWrapper mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocketWrapper socket) {
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the input and output streams, using temp objects because
            // member streams are final
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            byte[] buffer = new byte[1024];  // buffer store for the stream
            int bytes; // bytes returned from read()
            // Keep listening to the InputStream until an exception occurs
            while (true) {
                try {
                    // Read from the InputStream
                    bytes = mmInStream.available();
                    if (bytes != 0) {
                        buffer = new byte[1024];
                        SystemClock.sleep(100); //pause and wait for rest of data. Adjust this depending on your sending speed.
                        bytes = mmInStream.available(); // how many bytes are ready to be read?
                        bytes = mmInStream.read(buffer, 0, bytes); // record how many bytes we actually read
                        mHandler.obtainMessage(MESSAGE_READ, bytes, -1, buffer)
                                .sendToTarget(); // Send the obtained bytes to the UI activity
                    }
                } catch (IOException e) {
                    e.printStackTrace();

                    break;
                }
            }
        }

        /* Call this from the main activity to send data to the remote device */
        public void write(String input) {

            model.logMe("Commande envoyée : " + input, null, "/storage/emulated/0/" +
                            "SmartRobot/CommandeRobot/logCommandeRobot",
                    "logCommandeRobot", pathOfMyFile);

            byte[] bytes = input.getBytes();           //converts entered String into bytes
            try {
                mmOutStream.write(bytes);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        /* Call this from the main activity to shutdown the connection */
        public void cancel() {
            try {
                mmSocket.close();
                model.notifyObservers("RobotOff");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public class BluetoothBinder extends Binder {
        public BluetoothService getService() {
            return BluetoothService.this;
        }
    }

    final BroadcastReceiver blReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if(BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)){
                int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,-1);
                if (state == BluetoothAdapter.STATE_ON){
                    connectToDevice(model.getDeviceAddress());
                }
                if (state == BluetoothAdapter.STATE_OFF){
                    closeConnexion();
                    connectToDevice(model.getDeviceAddress());
                }

            }
            if(BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)){
            }

            if(BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)){
                BluetoothDevice device = intent
                        .getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if(device.getAddress().equals(model.getDeviceAddress())){
                    model.notifyObservers("RobotOff");
                    model.setEditor(RemoteControlUtil.CONNECTED_TO_APP, "false");
                    model.setRobotConnexion();
                    connectToDevice(model.getDeviceAddress());
                }
            }
            if(BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action)){
                int state = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, -1);
                if (state == BluetoothDevice.BOND_BONDED)
                    connectToDevice(model.getDeviceAddress());
            }
        }
    };

    public static interface BluetoothSocketWrapper {

        InputStream getInputStream() throws IOException;

        OutputStream getOutputStream() throws IOException;

        String getRemoteDeviceName();

        void connect() throws IOException;

        String getRemoteDeviceAddress();

        void close() throws IOException;

        BluetoothSocket getUnderlyingSocket();

    }


    public static class NativeBluetoothSocket implements BluetoothSocketWrapper {

        private BluetoothSocket socket;

        public NativeBluetoothSocket(BluetoothSocket tmp) {
            this.socket = tmp;
        }

        @Override
        public InputStream getInputStream() throws IOException {
            return socket.getInputStream();
        }

        @Override
        public OutputStream getOutputStream() throws IOException {
            return socket.getOutputStream();
        }

        @Override
        public String getRemoteDeviceName() {
            return socket.getRemoteDevice().getName();
        }

        @Override
        public void connect() throws IOException {
            socket.connect();
        }

        @Override
        public String getRemoteDeviceAddress() {
            return socket.getRemoteDevice().getAddress();
        }

        @Override
        public void close() throws IOException {
            socket.close();
        }

        @Override
        public BluetoothSocket getUnderlyingSocket() {
            return socket;
        }

    }

    public class FallbackBluetoothSocket extends NativeBluetoothSocket {

        private BluetoothSocket fallbackSocket;

        public FallbackBluetoothSocket(BluetoothSocket tmp) throws FallbackException {
            super(tmp);
            try
            {
                Class<?> clazz = tmp.getRemoteDevice().getClass();
                Class<?>[] paramTypes = new Class<?>[] {Integer.TYPE};
                Method m = clazz.getMethod("createRfcommSocket", paramTypes);
                Object[] params = new Object[] {Integer.valueOf(1)};
                fallbackSocket = (BluetoothSocket) m.invoke(tmp.getRemoteDevice(), params);
            }
            catch (Exception e)
            {
                throw new FallbackException(e);
            }
        }

        @Override
        public InputStream getInputStream() throws IOException {
            return fallbackSocket.getInputStream();
        }

        @Override
        public OutputStream getOutputStream() throws IOException {
            return fallbackSocket.getOutputStream();
        }


        @Override
        public void connect() throws IOException {
            fallbackSocket.connect();
        }


        @Override
        public void close() throws IOException {
            fallbackSocket.close();
        }

    }

    public static class FallbackException extends Exception {

        /**
         *
         */
        private static final long serialVersionUID = 1L;

        public FallbackException(Exception e) {
            super(e);
        }

    }
}

