package com.orrobotics.orsmartubo.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.util.RemoteControlUtil;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.UUID;

public class BluetoothConnect {
	private static final String TAG = "mbu";

	private static final String appName = "Server";

	private static final UUID MY_UUID_INSECURE = UUID
			.fromString("94f39d29-7d6d-437d-973b-fba39e49d4ee");

	private final BluetoothAdapter mBluetoothAdapter;
	Context mContext;

	private AcceptThread mInsecureAcceptThread;


	private ConnectedThread mConnectedThread;
	SmartRobotApplication application;

	public BluetoothConnect(Context context) {
		mContext = context;
		application = (SmartRobotApplication) context;
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		while (!mBluetoothAdapter.isEnabled()) {
			mBluetoothAdapter.enable();
		}

		startAccept();
	}
	public void makeVisible(){
		if (mBluetoothAdapter.getScanMode() != BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
			Intent discoverableIntent = new Intent(
					BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
			discoverableIntent.putExtra(
					BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 0);
			application.getActivity().startActivity(discoverableIntent);
		}
	}

	/**
	 * Ce thread reste en ecoute des connexion entrante. il se comporte comme un
	 * serveur. Il s'execute jusqu'a ce qu'une connexion est accept� (ou
	 * annul�).
	 */
	private class AcceptThread extends Thread {

		// server socket
		private final BluetoothServerSocket mmServerSocket;

		public AcceptThread() {
			BluetoothServerSocket tmp = null;

			try {
				tmp = mBluetoothAdapter
						.listenUsingInsecureRfcommWithServiceRecord(appName,
								MY_UUID_INSECURE);

				Log.i(TAG, "AcceptThread: Setting up Server using: "
						+ MY_UUID_INSECURE);
			} catch (IOException e) {
				e.printStackTrace();
				Log.e(TAG, "AcceptThread: IOException: " + e.getMessage());
			}

			mmServerSocket = tmp;
		}

		public void run() {
			Log.i(TAG, "run: AcceptThread Running.");

			BluetoothSocket socket = null;

			try {

				Log.i(TAG, "run: RFCOM server socket start.....");

				socket = mmServerSocket.accept();

				Log.i(TAG, "run: RFCOM server socket accepted connection.");

			} catch (IOException e) {
				e.printStackTrace();
				Log.e(TAG, "AcceptThread: IOException: " + e.getMessage());
			}

			Log.i(TAG, "socket =" + socket);
			if (socket != null) {
				connected(socket);
			}

			Log.i(TAG, "END mAcceptThread ");
		}

		public void cancel() {
			Log.i(TAG, "cancel: Canceling AcceptThread.");
			try {
				mmServerSocket.close();
			} catch (IOException e) {
				Log.e(TAG,
						"cancel: Close of AcceptThread ServerSocket failed. "
								+ e.getMessage());
			}
		}

	}

	public synchronized void startAccept() {
		Log.i(TAG, "start");
		makeVisible();
		if (mInsecureAcceptThread != null) {
			mInsecureAcceptThread.cancel();
			mInsecureAcceptThread = null;
		}
		mInsecureAcceptThread = new AcceptThread();
		mInsecureAcceptThread.start();

	}

	/**
	 * Ce thread est responsable de maintenir la conexion bluetooth,l'envoie et
	 * la reception des donn�es
	 **/
	private class ConnectedThread extends Thread {
		private final BluetoothSocket mmSocket;
		private final InputStream mmInStream;
		private final OutputStream mmOutStream;

		public ConnectedThread(BluetoothSocket socket) {
			Log.i(TAG, "----------ConnectedThread: Starting.");

			mmSocket = socket;
			InputStream tmpIn = null;
			OutputStream tmpOut = null;

			try {
				tmpIn = mmSocket.getInputStream();
				tmpOut = mmSocket.getOutputStream();
			} catch (IOException e) {
				e.printStackTrace();
			}

			mmInStream = tmpIn;
			mmOutStream = tmpOut;
		}

		public void run() {
			byte[] buffer = new byte[1024];

			int bytes;

			// Rester en ecoute des message entrant
			while (true) {
				try {
					bytes = mmInStream.read(buffer);
					String incomingMessage = new String(buffer, 0, bytes);
					Log.i(TAG, "--------InputStream incomingMessage: " + incomingMessage);
					sendRobotCommand(incomingMessage);
				} catch (IOException e) {
					Log.e(TAG,
							"write: Error reading Input Stream. "
									+ e.getMessage());
					startAccept();
					break;
				}
			}
		}

		// cette methode est utilis� pour l'envoie de commande
		public void write(byte[] bytes) {
			String text = new String(bytes, Charset.defaultCharset());
			Log.d(TAG, "write: Writing to outputstream: " + text);
			try {
				mmOutStream.write(bytes);
			} catch (IOException e) {
				Log.e(TAG,
						"write: Error writing to output stream. "
								+ e.getMessage());
			}
		}

		public void cancel() {
			try {
				mmSocket.close();
			} catch (IOException e) {
			}
		}
	}

	/**
	 * Ce methode permet d'envoyer les commandes au robot
	 * 
	 **/
	private void sendRobotCommand(String incomingMessage) {
		// TODO Auto-generated method stub
		Log.i(TAG, "----sendRobotCommand-----");
		if (incomingMessage.contains("speed")) {
			String speed = incomingMessage.split(";")[1];
			application.setRobotEditor(RemoteControlUtil.SPEED_LINEAIRE, speed);
			application.setRobotEditor(RemoteControlUtil.SPEED_LATERALE, speed);

		} else {
			application.notifyObservers(incomingMessage);

		}

	}

	private void connected(BluetoothSocket mmSocket) {
		Log.i(TAG, "connected: Starting.");
		if (mConnectedThread != null) {
			mConnectedThread.cancel();
			mConnectedThread = null;
		}
		mConnectedThread = new ConnectedThread(mmSocket);
		mConnectedThread.start();
	}

	public void write(byte[] out) {

		Log.i(TAG, "write: Write Called.");
		mConnectedThread.write(out);
	}

	public void destroy() {

		if (mConnectedThread != null) {
			mConnectedThread.cancel();
			mConnectedThread = null;
		}
		if (mInsecureAcceptThread != null) {
			mInsecureAcceptThread.cancel();
			mInsecureAcceptThread = null;
		}

		if (mBluetoothAdapter.isEnabled())
			mBluetoothAdapter.disable();

	}
}