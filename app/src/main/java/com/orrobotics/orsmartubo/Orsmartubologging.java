package com.orrobotics.orsmartubo;

import android.app.ActivityManager;
import android.content.Context;
import android.location.LocationManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartubo.control.ControlUboControllerImpl;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import static android.content.Context.ACTIVITY_SERVICE;
import static com.orrobotics.orsmartrobot.util.GestionDesFichiers.simpleFormat;
import static com.orrobotics.orsmartubo.util.SystemUtils.readSystemFileAsInt;

@Aspect
public class Orsmartubologging {
    // This code can be any value you want, its just a checksum.
    public static Boolean tts=false;
    public static Boolean fr_tts=false;
    public static Boolean en_tts=false;
    public static String default_engine_tts="";
    public static String voice_data="";
    public static List<TextToSpeech.EngineInfo> engine_tts=null;
    List<String> mLanguages= new ArrayList<String>();
    public static String supportedLanguage="";
    public static String defaultLanguague="";
    static File pathOfMyFile = Environment
            .getExternalStoragePublicDirectory("/SmartRobot/AutoControl");

    @Before("execution(* *..ControlUboControllerImpl.init(..))")
    public void logMetricsafter(final JoinPoint joinPoint) {
        System.out.println("Metrics Aspect orsmartubo checking in after");

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @After("execution(* *..CheckTTSService.onInit(..))")
    public void logMetricsbefore(final JoinPoint joinPoint) {
        int monitoring = 1;
        String data = SmartRobotApplication
                .getParamFromFile("Monitoring",
                        "Configuration.properties", null);
        if(data!=null)
            monitoring= Integer.parseInt(data);
        if (monitoring == 1) {

            System.out.println("Metrics Aspect orsmartubo checking in before");

            logMe("\n \n =============== Device Info ================");
            logMe("Manufacture                 : " + Build.MANUFACTURER);
            logMe("type: " + Build.TYPE);
            logMe("SDK                         : " + Build.VERSION.SDK);
            logMe("BRAND                       : " + Build.BRAND);
            logMe("TimeStamp                   : " + geTimeStamp());
            logMe("Version Code                : " + Build.VERSION.RELEASE);

            logMe("=============== CPU ================");
            logMe("device CPU Info              :\n " + getInfo());
            logMe("available CPU                : " + CPUUsage().availMem / 1000000 + "Mo");
            logMe("device CPU usage             : " + (CPUUsage().totalMem - CPUUsage().availMem) / 1000000 + "Mo");
            logMe("Total application            : " + CPUUsage().totalMem / 1000000 + "Mo");
            logMe("lowMemory CPU                : " + CPUUsage().lowMemory);
            try {
                logMe(" CPU Frequency               : " + Arrays.toString(this.getCPUFrequencyCurrent()));
            } catch (Exception e) {
                e.printStackTrace();
            }
            logMe("App  CPU usage               : " + getAppCPUUsage());

            logMe("=============== Storage ================");
            logMe("Storage available            : " + storageAvailable());
            logMe("Storage used by the app      : " + dirSize(new File("/storage/emulated/0/SmartRobot")) / 1048576 + " MB");

            logMe("=============== WIFI ================");
            String winfo = Wifiinfo();
            logMe("Mac Address                  : " + winfo.split(";")[0]);
            logMe("Wifi SSID                    : " + winfo.split(";")[1]);
            logMe("IPV4 Address                 : " + getIPAddress(true));
            logMe("IPV6 Address                 : " + getIPAddress(false));

            logMe("=============== Robot Info ================");
            logMe("Robot Type                   : " + ControlUboControllerImpl.application.getRobotType());
            logMe("account                      : " + ControlUboControllerImpl.application.getEmail());
            logMe("Robot connected              : " + ControlUboControllerImpl.application.getRobotConnexion());

            Double length = (new Long(new File("/storage/emulated/0/SmartRobot/AutoControl/logteambotSmart.txt").length()).doubleValue()) / (1024 * 1024);
            Double fileSize = 10d;
            String file = SmartRobotApplication
                    .getParamFromFile("FileSize",
                            "Configuration.properties", null);
            if(file!=null)
                fileSize= Double.parseDouble(file);
            if (length >= fileSize) {
                archifage();
            }

            logMe("=============== TTS Info ================");
            logMe("TTS Available                : " + this.tts);
            logMe("TTS English available        : " + this.en_tts);
            logMe("TTS French available         : " + this.fr_tts);
            logMe("TTS default engine           : " + this.default_engine_tts);
            String engines = "";
            for (int i = 0; i < Orsmartubologging.engine_tts.size(); i++) {
                engines = engines + " label : " + engine_tts.get(0).label + " name : " + engine_tts.get(0).name + " | ";
            }
            logMe("TTS  engines infos           : " + engines);

            logMe("=============== Speech Recongition ================");
            if (!SpeechRecognizer.isRecognitionAvailable(ControlUboControllerImpl.application)) {
                logMe("Speech recognizer available  : false ");
            } else {
                logMe("Speech recognizer available  : True ");

            }
            logMe("default languague            : " + defaultLanguague + " ");
            logMe("Supported languague          : " + supportedLanguage);

            logMe("=============== Localisation ================");
            LocationManager lm = (LocationManager) ControlUboControllerImpl.application.getSystemService(Context.LOCATION_SERVICE);
            logMe("Localisation enabled         : " + lm.isProviderEnabled(LocationManager.GPS_PROVIDER));

            logMe("=============== Services state ================");
            logMe("Number of Fail connection    : " + ControlUboControllerImpl.application.getNumFailConnection());
            logMe("Number of  SIP Fail          : " + ControlUboControllerImpl.application.getNumfailSIP());
            logMe("SIP is connected            : " + ControlUboControllerImpl.application.getConnectedToSip());
            logMe("OpenFire is Connected        : " + ControlUboControllerImpl.application.getConnectedToOpenFire());
            logMe("Number of openFire Fail      : " + ControlUboControllerImpl.application.getNumfailOpenFire());
        }
    }

    public static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress();
                        //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        boolean isIPv4 = sAddr.indexOf(':') < 0;

                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 zone suffix
                                return delim < 0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
        } // for now eat exceptions
        return "";
    }

    public String geTimeStamp() {
        Long tsLong = System.currentTimeMillis() / 1000;
        String ts = tsLong.toString();
        return ts;
    }

    public String Wifiinfo() {
        WifiManager wifiManager = (WifiManager) ControlUboControllerImpl.application.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wInfo = wifiManager.getConnectionInfo();
        return wInfo.getMacAddress() + ";" + wInfo.getSSID();
    }

    private String getInfo() {
        StringBuffer sb = new StringBuffer();
        sb.append("abi: ").append(Build.CPU_ABI).append("\n");
        if (new File("/proc/cpuinfo").exists()) {
            try {
                BufferedReader br = new BufferedReader(
                        new FileReader(new File("/proc/cpuinfo")));
                String aLine;
                while ((aLine = br.readLine()) != null) {
                    sb.append(aLine + "\n");
                }
                if (br != null) {
                    br.close();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    public static int[] getCPUFrequencyCurrent() throws Exception {
        int[] output = new int[getNumCores()];
        for (int i = 0; i < getNumCores(); i++) {
            output[i] = readSystemFileAsInt("/sys/devices/system/cpu/cpu" + String.valueOf(i) + "/cpufreq/scaling_cur_freq");
        }
        return output;
    }

    public static int getNumCores() {
        //Private Class to display only CPU devices in the directory listing
        class CpuFilter implements FileFilter {
            @Override
            public boolean accept(File pathname) {
                //Check if filename is "cpu", followed by a single digit number
                if (Pattern.matches("cpu[0-9]+", pathname.getName())) {
                    return true;
                }
                return false;
            }
        }

        try {
            //Get directory containing CPU info
            File dir = new File("/sys/devices/system/cpu/");
            //Filter to only list the devices we care about
            File[] files = dir.listFiles(new CpuFilter());
            //Return the number of cores (virtual CPU devices)
            return files.length;
        } catch (Exception e) {
            //Default to return 1 core
            return 1;
        }
    }

    public ActivityManager.MemoryInfo CPUUsage() {

        ActivityManager.MemoryInfo memoryInfo = ControlUboControllerImpl.application.getAvailableMemory();
        return memoryInfo;
    }



    public String getAppCPUUsage() {
        Process p;
        String app="";
        try {
            String cmd = "ps  om.teambotsmart ";
            p = Runtime.getRuntime().exec(cmd);
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(p.getInputStream()));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                app=app+"\n"+line;
            }
//            String[] cmd1 = {
//                    "sh",
//                    "-c",
//                    "top -m 1000 -d 1 -n 1 | grep \""+app.split(" ")[1]+"\" "};
//            line="";
//            p = Runtime.getRuntime().exec(cmd1);
//            bufferedReader = new BufferedReader(
//                    new InputStreamReader(p.getInputStream()));
//            while ((line = bufferedReader.readLine()) != null) {
//
//                    app=app+"\n"+line;
//
//            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return app;

    }
    public String storageAvailable(){
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
        long bytesAvailable;
        if (Build.VERSION.SDK_INT >=
                Build.VERSION_CODES.JELLY_BEAN_MR2) {
            bytesAvailable = stat.getBlockSizeLong() * stat.getAvailableBlocksLong();
        }
        else {
            bytesAvailable = (long)stat.getBlockSize() * (long)stat.getAvailableBlocks();
        }
        long megAvailable = bytesAvailable / (1024 * 1024);
        return megAvailable/1000+" GB";
    }

    private  double dirSize(File dir) {

        if (dir.exists()) {
            long result = 0;
            File[] fileList = dir.listFiles();
            for(int i = 0; i < fileList.length; i++) {
                // Recursive call if it's a directory
                if(fileList[i].isDirectory()) {
                    result += dirSize(fileList [i]);
                } else {
                    // Sum the file size in bytes
                    result += fileList[i].length();
                }
            }
            return  (result); // return the file size
        }
        return 0;
    }

    private void logMe(String msg) {
                try {
                String nomFichier = "logteambotSmart.txt";
                File file = new File(pathOfMyFile, nomFichier);
                if (!file.exists()) {
                    this.creerFichier(nomFichier);
                }
                FileWriter fw;
                try {
                    fw = new FileWriter(file.getAbsoluteFile(), true);
                    BufferedWriter bw = new BufferedWriter(fw);

                    bw.write("[" + simpleFormat.format(new Date()) + "] " + "INFO"
                            + " : " + msg + "\r\n");
                    bw.flush();
                    bw.close();
                } catch (FileNotFoundException e) {
                }
            } catch (Exception e) {
                e.printStackTrace();
            } catch (StackOverflowError e1) {
            }

    }
    public  void creerFichier(String nomFichier) {
        File file1 = new File(pathOfMyFile, "AutoControl");
        if (file1.exists() && file1.isDirectory()) {

            File file = new File(pathOfMyFile, nomFichier );
            if (!file.exists()) {
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public void archifage(){
        String archivage = "local";
        String data = SmartRobotApplication
                .getParamFromFile("LogsArchivage",
                        "Configuration.properties", null);
        if(data!=null)
            archivage=data;
        if (archivage.equals("local")) {
            File myFile = new File("/storage/emulated/0/SmartRobot/AutoControl/logteambotSmart.txt");
            myFile.renameTo(new File("/storage/emulated/0/SmartRobot/AutoControl/Cachedlogteambot" + now() + "" + ".txt"));
        }
    }
    public String now(){
        Date date=new Date();
        return date.getYear()+""+date.getMonth()+""+date.getDay()+""+date.getHours()+""+date.getMinutes()+""+date.getSeconds()+"";
    }

}