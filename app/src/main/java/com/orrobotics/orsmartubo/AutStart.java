package com.orrobotics.orsmartubo;

import com.orrobotics.orsmartrobot.util.RemoteControlUtil;
import com.orrobotics.orsmartubo.service.OrService;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.util.Log;

/**
 * 
 * @author Hajiba IFRAH
 * 
 * 
 * 
 */

@SuppressLint("NewApi")
public class AutStart extends BroadcastReceiver {

	String deviceAdresse;
	Handler mHandler;
	SharedPreferences sharedPref;
	Intent intent1;
	Context con;
	SharedPreferences.Editor editor;

	@Override
	public void onReceive(Context context, Intent intent) {
		con = context;

		sharedPref = context.getSharedPreferences("Teambot", Context.MODE_PRIVATE);
		mHandler = new Handler();

		if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
			// TODO Auto-generated method stub
			editor = sharedPref.edit();
			editor.putString(RemoteControlUtil.AUTOSTART, "true");
			editor.commit();
			Intent service = new Intent(context, OrService.class);
			con.startService(service);
			
		}

	}


}