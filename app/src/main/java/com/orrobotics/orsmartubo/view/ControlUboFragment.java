package com.orrobotics.orsmartubo.view;

import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.ui.BatteryView;
import com.orrobotics.orsmartrobot.ui.Setting_list_adapter;
import com.orrobotics.orsmartubo.R;
import com.orrobotics.orsmartubo.control.ControlUboController;
import com.orrobotics.orsmartubo.control.ControlUboControllerImpl;
import com.orrobotics.orsmartubo.model.ControlUboModel;
import com.orrobotics.orsmartubo.model.ControlUboModelImpl;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

public class ControlUboFragment extends Fragment {

	Context context;
	SmartRobotApplication application;
	ControlUboView mVieu;


	BatteryView battery;
	TextView textBatterie;
	ImageButton scenario;
	TextView textScenario;
	ImageButton haut;
	TextView textHaut;
	ImageButton bas;
	TextView textBas;
	ScrollView scroll;
	View layoutSettingAnim;
	ImageButton closePopAppAnim;
	ListView listOfAnims;
	ImageButton light;
	TextView textlight;

	public ControlUboFragment() {
	}

	@SuppressLint("ValidFragment")
	public ControlUboFragment(Context context) {
		this.context = context;
		application = (SmartRobotApplication) context;

	}

	// Warning useless because value is ignored and automatically set by new
	// APIs.
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.control, container, false);
		/**
		 * Construction du MVC
		 **/
		ControlUboModel model = new ControlUboModelImpl(getActivity()
				.getApplicationContext(), getActivity(), this);
		ControlUboController control = new ControlUboControllerImpl(model);

		mVieu = new ControlUboView(control, model);

		haut = (ImageButton) view.findViewById(R.id.haut);
		textHaut = (TextView) view.findViewById(R.id.textHaut);

		bas = (ImageButton) view.findViewById(R.id.bas);
		textBas = (TextView) view.findViewById(R.id.textBas);
		if (application == null)
			application = (SmartRobotApplication) getActivity().getApplicationContext();

		if (application.getModeControl().equals("nocontrol")) {
			haut.setVisibility(View.GONE);
			textHaut.setVisibility(View.GONE);
			bas.setVisibility(View.GONE);
			textBas.setVisibility(View.GONE);
		}

		battery = (BatteryView) view.findViewById(R.id.battery);
		textBatterie = (TextView) view.findViewById(R.id.textBatterie);

		scenario = (ImageButton) view.findViewById(R.id.scenario);
		textScenario = (TextView) view.findViewById(R.id.textScenario);


		layoutSettingAnim = inflater.inflate(R.layout.screen_popup_animation,
				(ViewGroup) getActivity().findViewById(R.id.popup_element));
		listOfAnims = (ListView) layoutSettingAnim.findViewById(R.id.animList);
		closePopAppAnim = (ImageButton) layoutSettingAnim
				.findViewById(R.id.fermerPopupAnim);
		light = (ImageButton) view.findViewById(R.id.light);
		textlight = (TextView) view.findViewById(R.id.textlight);

		mVieu.setWidget(battery,
				textBatterie, scenario, textScenario, layoutSettingAnim,
				listOfAnims, closePopAppAnim,haut, textHaut,bas,textBas,light,textlight);

		mVieu.setListners();

		return view;
	}

	@Override
	public void onResume() {
		super.onResume();

	}

	@Override
	public void onPause() {

		super.onPause();
	}

	@Override
	public void onDestroy() {
		Log.v("", "in onDestroy UboFragment ---------------");
		mVieu.destroy();
		super.onDestroy();

	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		context = activity;
	}

}
