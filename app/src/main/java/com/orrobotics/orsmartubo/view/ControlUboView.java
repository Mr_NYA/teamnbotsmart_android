package com.orrobotics.orsmartubo.view;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.TextView;
import android.widget.Toast;

import com.orrobotics.orsmartubo.R;
import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.controller.ControlRobotController;
import com.orrobotics.orsmartrobot.model.ControlRobotModel;
import com.orrobotics.orsmartrobot.observer.OrSmarRobotObserver;
import com.orrobotics.orsmartrobot.service.ServiceRobot;
import com.orrobotics.orsmartrobot.ui.BatteryView;
import com.orrobotics.orsmartrobot.util.GestionDesFichiers;
import com.orrobotics.orsmartrobot.util.RemoteControlUtil;
import com.orrobotics.orsmartrobot.view.ControlRobot;
import com.orrobotics.orsmartrobot.view.ControlRobotView;
import com.orrobotics.orsmartubo.bluetooth.BluetoothService;
import com.orrobotics.orsmartubo.control.ControlUboController;
import com.orrobotics.orsmartubo.control.ControlUboControllerImpl;
import com.orrobotics.orsmartubo.model.ControlUboModel;
import com.orrobotics.orsmartubo.service.OrService;
import com.orrobotics.orsmartubo.usb.Transfer;
import com.orrobotics.orsmartubo.usb.serial.UsbSerial;
import com.orrobotics.orsmartubo.util.CustumAnimationListAdapter;

/**
 *
 * @author Hajiba IFRAH
 *
 */
@SuppressLint({ "NewApi", "ClickableViewAccessibility" })
public class ControlUboView implements OrSmarRobotObserver,
		OnItemSelectedListener {

	ControlUboController vController;
	ControlRobotController vRobotController;
	ControlUboModel vModel;
	ControlRobotModel vRobotModel;
	SmartRobotApplication application;
	ImageButton haut;
	TextView textHaut;
	ImageButton bas;
	TextView textBas;	Handler mHandler = new Handler();
	boolean flagLightWasOn = false;
	BatteryView battery;
	TextView textBatterie;

	Handler handTTS = new Handler();
	Boolean start = false;
	int speedLineaireMax;
	int speedLateraleMax;
	ImageButton scenario;
	TextView textScenario;
	PopupWindow pwindo_settingAnim;
	View layoutSettingAnim;
	ListView listOfAnims;
	ImageButton closePopAppAnim;
	int scena = 1;
	int speed_lineaire;
	int speed_lateral;
	Handler handler = new Handler();
	int delai = 2000;
	ImageButton light;
	TextView textlight;
	String call;
	String communicationType;

	public ControlUboView(ControlUboController controller, ControlUboModel model) {

		vController = controller;
		vModel = model;
		vRobotModel = ControlRobotView.vModel;
		vRobotController = ControlRobotView.vController;
		application = (SmartRobotApplication) vModel.getContext();
		speed_lateral = vController.getSpeedLaterale();
		speed_lineaire = vController.getSpeedLineaire();

		communicationType = SmartRobotApplication.getParamFromFile(
				"robot.connexion.type", "Configuration.properties", null);
		Log.i("mbu","---communicationType1 ="+communicationType);
		if(communicationType == null)
			communicationType = "nordic";

		if (UsbSerial.isSerial && communicationType.equals("usb")) {
			new Handler().postDelayed(run, 5000);
		}

		call = SmartRobotApplication.getParamFromFile("call", "Configuration.properties",null);

		if(call==null){
			call ="webrtc";

		}

	}
	Runnable run = new Runnable() {
		@Override
		public void run() {


			UsbSerial.write(OrService.commandeAdresse
			);
		}
	};
	public void setWidget(
			BatteryView battery,
			TextView textBatterie, ImageButton scenario, TextView textScenario,
			View layoutSettingAnim, ListView listOfAnims,
			ImageButton closePopAppAnim,ImageButton haut,
			TextView textHaut,
			ImageButton bas,
			TextView textBas, ImageButton light, TextView textlight) {
		// TODO Auto-generated method stub
		this.battery = battery;
		this.textBatterie = textBatterie;
		this.scenario = scenario;
		this.textScenario = textScenario;
		this.haut=haut;
		this.textHaut=textHaut;
		this.bas=bas;
		this.textBas=textBas;

		this.layoutSettingAnim = layoutSettingAnim;
		this.listOfAnims = listOfAnims;
		this.closePopAppAnim = closePopAppAnim;

		battery.setPower(0);
		this.light=light;
		this.textlight=textlight;
		init();

	}


	private void init() {
		// TODO Auto-generated method stub
		application.registerObserver(this);

		application = (SmartRobotApplication) vModel.getContext();
		String dataSc = OrService
				.getParamFromFile("ScenarioVisibility");
		if(dataSc!=null)
			scena = Integer.parseInt(dataSc);

		String data = SmartRobotApplication.getParamFromFile(
				"commande.timout", "Configuration.properties", null);
		if(data!=null)
			delai = Integer
					.parseInt(SmartRobotApplication.getParamFromFile(
							"commande.timout", "Configuration.properties", null)
							.split(" ")[0]) * 1000;

		if (scena == 0 || application.getRemoteControl().equals("ON")) {

			scenario.setVisibility(View.GONE);
			textScenario.setVisibility(View.GONE);
		} else {
			scenario.setVisibility(View.VISIBLE);
			textScenario.setVisibility(View.VISIBLE);
		}

		if (application.getRobotConnexion().equals("false")) {
			haut.setVisibility(View.GONE);
			textHaut.setVisibility(View.GONE);
			bas.setVisibility(View.GONE);
			textBas.setVisibility(View.GONE);
		}
		else {
			if (application.getMacConnectedDevice().equals(
					application.getDeviceAddress())) {
				if (!application.getModeControl().equals("nocontrol"))
					haut.setVisibility(View.VISIBLE);
				textHaut.setVisibility(View.VISIBLE);
				bas.setVisibility(View.VISIBLE);
				textBas.setVisibility(View.VISIBLE);
			} else {
				haut.setVisibility(View.GONE);
				textHaut.setVisibility(View.GONE);
				bas.setVisibility(View.GONE);
				textBas.setVisibility(View.GONE);
			}
		}

		String lightValue = application.getParam(RemoteControlUtil.LIGHT);
		if(lightValue.equalsIgnoreCase("") || lightValue.equalsIgnoreCase("false")){
			light.setImageResource(R.drawable.lighton);
			textlight.setText(R.string.lightOn);
		}else {
			application.setEditor(RemoteControlUtil.LIGHT, "true");
			light.setImageResource(R.drawable.lightoff);
			vController.light(true);
			textlight.setText(R.string.lightOff);

		}


		initSetting();


	}

	public void initSetting() {





		if (application.getShowBatterie().equals("true")) {
			battery.setVisibility(View.VISIBLE);
			textBatterie.setVisibility(View.VISIBLE);
		} else {
			battery.setVisibility(View.GONE);
			textBatterie.setVisibility(View.GONE);
		}

		if (application.isOrientation() == false) {
			Intent intentControle = new Intent(vModel.getActivity(),
					ControlRobot.class);
			application.createNotification("Teambot",
					com.orrobotics.orsmartubo.R.drawable.ic_launcher,
					intentControle, vModel.getActivity());
		}

	}

	/**
	 * Gestion des évenements de clic
	 */
	public void setListners() {
		// TODO Auto-generated method stub

		light.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				UpdateLight();

			}

		});
		textlight.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				UpdateLight();

			}

		});
		closePopAppAnim.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				// TODO Auto-generated method stub
				pwindo_settingAnim.dismiss();
				application.notifyObservers("resume");
				application.notifyObservers("setControlVisible");
			}

		});

		scenario.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				// TODO Auto-generated method stub
				openAnim(scenario);
			}

		});
		textScenario.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				openAnim(scenario);
			}

		});

		haut.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				GestionDesFichiers.log("Erreur ",
						"---application.getRobotConnexion()="+application.getRobotConnexion(), "Log_teambot");
				if (application.getRobotConnexion().equals("true")) {

					switch (event.getAction()) {
						case MotionEvent.ACTION_DOWN:
							GestionDesFichiers.log("Erreur ",
									"---ACTION_DOWN---", "Log_teambot");
							vModel.setHeadMouve(true);
							vModel.setMovedLoop(true);
							vController.mouveHeadUp(false);
							break;
						case MotionEvent.ACTION_UP:
							GestionDesFichiers.log("Erreur ",
									"---ACTION_UP---", "Log_teambot");
							vModel.setHeadMouve(false);
							break;

						case MotionEvent.ACTION_CANCEL:
							GestionDesFichiers.log("Erreur ",
									"---ACTION_CANCEL---", "Log_teambot");
							vModel.setHeadMouve(false);
							break;
					}

				}

				return false;

			}

		});
		textHaut.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub

				if (application.getRobotConnexion().equals("true")) {

					switch (event.getAction()) {
						case MotionEvent.ACTION_DOWN:
							GestionDesFichiers.log("Erreur ",
									"---ACTION_DOWN---", "Log_teambot");
							vModel.setHeadMouve(true);
							vModel.setMovedLoop(true);
							vController.mouveHeadUp(false);
							break;
						case MotionEvent.ACTION_UP:
							GestionDesFichiers.log("Erreur ",
									"---ACTION_UP---", "Log_teambot");
							vModel.setHeadMouve(false);
							break;

						case MotionEvent.ACTION_CANCEL:
							vModel.setHeadMouve(false);
							break;
					}

				}

				return false;

			}

		});
		bas.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				if (application.getRobotConnexion().equals("true")) {

					switch (event.getAction()) {
						case MotionEvent.ACTION_DOWN:

							vModel.setHeadMouve(true);
							vModel.setMovedLoop(true);
							vController.mouveHeadDown(false);

							break;
						case MotionEvent.ACTION_UP:

							vModel.setHeadMouve(false);

							break;

						case MotionEvent.ACTION_CANCEL:
							vModel.setHeadMouve(false);
							break;


					}
				}
				return false;

			}

		});
		textBas.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				if (application.getRobotConnexion().equals("true")) {

					switch (event.getAction()) {
						case MotionEvent.ACTION_DOWN:

							vModel.setHeadMouve(true);
							vModel.setMovedLoop(true);
							vController.mouveHeadDown(false);
							break;
						case MotionEvent.ACTION_UP:

							vModel.setHeadMouve(false);

							break;

						case MotionEvent.ACTION_CANCEL:
							vModel.setHeadMouve(false);
							break;
					}

				}
				return false;

			}

		});

	}

	public void resume() {
		// TODO Auto-generated method stub
	}

	public void destroy() {
		// TODO Auto-generated method stub
		vController.destroy();
		application.removeObserver(this);
	}

	public void removeObserver() {
		application.removeObserver(this);
	}

	@Override
	public void update(final String message) {
		// TODO Auto-generated method stub
		//GestionDesFichiers.log("INFO", message, "Log_teambot");
		vModel.getActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {

				if (message.equals("lightOff")) {
					Boolean flagLight=vController.light(false);
					if(flagLight) {
						application.setEditor(RemoteControlUtil.LIGHT, "false");
						light.setImageResource(R.drawable.lighton);
						textlight.setText(R.string.lightOn);
					}


				}else if (message.equals("lightOn")) {
					Boolean flagLight=vController.light(true);
					if(flagLight) {
						application.setEditor(RemoteControlUtil.LIGHT, "true");
						light.setImageResource(R.drawable.lightoff);
						textlight.setText(R.string.lightOff);
					}

				}
				if (message.equals("showBatt")){
					battery.setVisibility(View.VISIBLE);
					textBatterie.setVisibility(View.VISIBLE);
				}
				if (message.equals("hideBatt")){
					battery.setVisibility(View.INVISIBLE);
					textBatterie.setVisibility(View.INVISIBLE);
				}
				if (message.contains("msg")) {
					mHandler.post(new Runnable() {
						public void run() {

						}
					});
				}

				if (message.contains("getBatterie")) {
					if(application.getRobotConnexion().equals("true")) {
						application.setIsBatteryRequest(true);
						vController.getBatterie();
					}
				}
				else if (message.contains("batterie;")) {
					try {
						float level = Float.parseFloat(message.split(";")[1]);
						battery.setPower(level);
						if (application.getIsBatteryRequest()) {
							if(call.equals("linphone")) {
								ServiceRobot.sendMessage(
										application.getPseudoOpenFireSender(),
										"batterie;" + level);
							}
							application.setIsBatteryRequest(false);
						}
					}catch(Exception e){

					}

				}

				if (message.contains("unregisterObserver")) {

					removeObserver();

				}
				if (message.equals("endtelepresence")) {
					vModel.getActivity().runOnUiThread(new Runnable() {

						@Override
						public void run() {
							if (scena == 1) {
								scenario.setVisibility(View.VISIBLE);
								textScenario.setVisibility(View.VISIBLE);
							}

							if (flagLightWasOn){
								Boolean flagLight=vController.light(true);
								if(flagLight) {
									application.setEditor(RemoteControlUtil.LIGHT, "true");
									light.setImageResource(R.drawable.lightoff);
									textlight.setText(R.string.lightOff);
									flagLightWasOn = false;
								}
							}

						}
					});
				}
				if (message.equals("istelepresence")) {
					if(application.getParam(RemoteControlUtil.LIGHT).equals("true")){
						Boolean flagLight=vController.light(false);

						if(flagLight) {
							flagLightWasOn = true;
							application.setEditor(RemoteControlUtil.LIGHT, "false");
							light.setImageResource(R.drawable.lighton);
							textlight.setText(R.string.lightOn);
						}
					}
					if (scena == 1) {
						application.setEndScenario(true);
						scenario.setVisibility(View.GONE);
						textScenario.setVisibility(View.GONE);
					}

				}

				if (message.equals("playAnimEnd")) {
					if (pwindo_settingAnim != null)
						pwindo_settingAnim.dismiss();
					application.notifyObservers("setControlVisible");
				}

				if (message.contains("getInitialInfo")) {
					if(call.equals("linphone")) {

						ServiceRobot.sendMessage(
								application.getPseudoOpenFireSender(), "tts"
										+ application.getTtsLangSelection() + ";" + application.getParam(RemoteControlUtil.LIGHT));
					}
				}

				if (message.equals("Uboconnected")) {

					if (application.getMacConnectedDevice().contains(":")) {
						if (application.getMacConnectedDevice().equals(
								application.getDeviceAddress())) {

							application.setEditor(
									RemoteControlUtil.CONNECTED_TO_APP, "true");
							application.notifyObservers("RobotOn");
							if (application.getMacConnectedDevice().equals(
									application.getDeviceAddress())) {
								if (!application.getModeControl().equals(
										"nocontrol"))
									haut.setVisibility(View.VISIBLE);
								textHaut.setVisibility(View.VISIBLE);
								bas.setVisibility(View.VISIBLE);
								textBas.setVisibility(View.VISIBLE);
							} else {
								haut.setVisibility(View.GONE);
								textHaut.setVisibility(View.GONE);
								bas.setVisibility(View.GONE);
								textBas.setVisibility(View.GONE);
							}
						} else {
							mHandler.post(new Runnable() {
								public void run() {
									Toast.makeText(vModel.getContext(),
											R.string.differentMacAddress,
											Toast.LENGTH_LONG).show();
								}
							});
						}
					}
				}

				if (message.equals("RobotOff")) {
					haut.setVisibility(View.GONE);
					textHaut.setVisibility(View.GONE);
					bas.setVisibility(View.GONE);
					textBas.setVisibility(View.GONE);
				}
				if (message.equals("RobotOn")) {
					if (application.getMacConnectedDevice().equals(
							application.getDeviceAddress())) {
						haut.setVisibility(View.VISIBLE);
						textHaut.setVisibility(View.VISIBLE);
						bas.setVisibility(View.VISIBLE);
						textBas.setVisibility(View.VISIBLE);
					} else {
						haut.setVisibility(View.GONE);
						textHaut.setVisibility(View.GONE);
						bas.setVisibility(View.GONE);
						textBas.setVisibility(View.GONE);
					}

				}

				if (message.contains("destroyControlRobotView")) {
					handTTS.removeCallbacks(run);
					application.cancelNotification(vModel.getContext(),
							"Teambot");
					if (communicationType.equals("bluetooth") || communicationType.equals("ble"))
						application.notifyObservers("disconnectBl");

					destroy();


				} else if (message.contains("joy")) {
					mHandler.post(new Runnable() {
						public void run() {

							String[] data = message.split(";");
							if (application.getRobotConnexion().equals("true")) {

								double x = Double.parseDouble(data[1]);
								double y = Double.parseDouble(data[2]);
								double Xmax = Double.parseDouble(data[3]);
								double Ymax = Double.parseDouble(data[4]);

								if(!communicationType.equals("nordic")) {
									speedLineaireMax = speed_lineaire * 10;
									speedLateraleMax = speed_lateral * 5;
								}
								else{
									speedLineaireMax = speed_lineaire;
									speedLateraleMax = speed_lateral;
								}

								x = x * speedLateraleMax / Xmax;
								y = y * speedLineaireMax / Ymax;

								int ypres1 = speedLineaireMax * 96 / 100;
								if (y >= ypres1)
									y = speedLineaireMax;

								if (y <= -ypres1)
									y = -speedLineaireMax;

								int xpres = speedLateraleMax * 46 / 100;

								if (x < xpres && x > (-1) * xpres) {
									vModel.setX(0);
									vModel.setY((int) y);

								} else {
									int ypre = speedLineaireMax * 30 / 100;
									if (y < ypre && y > (-1) * ypre)
										y = 0;
									if (application.getOptFaceToFace().equals(
											"false"))
										x = (-1) * x;

									vModel.setX((int) x);
									vModel.setY((int) y);

								}

								if (vModel.getMovedLoop() == false) {
									vModel.setMovedLoop(true);

									vController.move();

								}

							}
						}
					});
				} else if (message.contains("gyro")) {

					if (application.getRobotConnexion().equals("true")) {

						String[] data = message.split(";");

						if (message.contains("devantB")
								|| (message.contains("arriereB"))) {
							double speed = Double.parseDouble(message
									.split(";")[2]);
							if (data.length > 3) {
								speedLineaireMax = Integer.parseInt(data[3]);
								if(!communicationType.equals("nordic"))
								speedLateraleMax = Integer.parseInt(data[4]) * 5;
								else
									speedLateraleMax = Integer.parseInt(data[4]);
							} else {
								speedLineaireMax = vController
										.getSpeedLineaire();
								if(!communicationType.equals("nordic"))
								speedLateraleMax = vController
										.getSpeedLaterale() * 5;
								else
									speedLateraleMax = vController
											.getSpeedLaterale();
							}

							vModel.setX(0);
							vModel.setY((int) (speedLineaireMax * speed));
						} else {
							double angle = Double.parseDouble(data[4]);
							double speed = Double.parseDouble(data[3]);
							angle = angle * 2;

							speedLineaireMax = speed_lineaire;
							if(!communicationType.equals("nordic"))
							speedLateraleMax = speed_lateral * 5;
							else
								speedLateraleMax = speed_lateral;

							if ((angle < 15 && angle >= 0)
									|| (angle > -15 && angle < 0)) {
								vModel.setX(0);
								vModel.setY((int) (speedLineaireMax * speed));
							} else if (angle >= 60) {
								vModel.setX(speedLateraleMax);
								vModel.setY((int) (speedLineaireMax * speed));
							} else if (angle <= -60) {
								vModel.setX(-speedLateraleMax);
								vModel.setY((int) (speedLineaireMax * speed));
							} else {
								double speed_lat;
								if (angle < 0) {
									angle = (-1) * angle;
									angle = angle - 15;
									speed_lat = (angle * speedLateraleMax) / 45;
									speed_lat = (-1) * speed_lat;
								} else {
									angle = angle - 15;
									speed_lat = (angle * speedLateraleMax) / 45;
								}
								vModel.setX((int) speed_lat);
								vModel.setY((int) (speedLineaireMax * speed));
							}

						}

						if (vModel.getMovedLoop() == false) {
							vModel.setMovedLoop(true);
							vController.move();
						}

					}

				} else if (message.contains("lever")) {
					Log.v("Serial", "------------Lever---------------");
					vModel.setHeadMouve(true);
					vModel.setMovedLoop(true);
					vController.mouveHeadUp(true);
				} else if (message.contains("baisser")) {
					vModel.setHeadMouve(true);
					vModel.setMovedLoop(true);
					vController.mouveHeadDown(true);

				} else if (message.contains("devantB")) {
					if (application.getRobotConnexion().equals("true")) {

						if (vController != null) {
							int speed_line = 1;
							String speed_lineaire = application
									.getSpeedLinear();
							if (speed_lineaire.equals("lineaire")
									|| speed_lineaire.equals("")) {
								speed_line = 1;
							} else {
								speed_line = Integer.parseInt(speed_lineaire);
							}
							String[] data = message.split(";");
							vModel.setMovedLoop(true);
							if(!communicationType.equals("nordic"))
							speed_line = speed_line * 10;

							if (data.length > 1) {
								if (data[1].equals("VL*50%"))
									vModel.setY(speed_line / 2);
								else if (data[1].equals("VL*20%")) {
									vModel.setY(speed_line / 5);
								} else
									vModel.setY(speed_line);
								vModel.setX(0);
								vController.move();

							} else {
								vModel.setFlagAvancer(true);
								if (vModel.getFlagDroite() == true
										|| vModel.getFlagGauche() == true) {
									vModel.setY(speed_line);
								} else {
									vModel.setX(0);
									vModel.setY(speed_line);
									vController.move();
								}

							}
						}

					}
				}

				else if (message.contains("arriereB")) {
					if (application.getRobotConnexion().equals("true")) {

						if (vController != null) {
							vModel.setMovedLoop(true);

							int speed_line = 1;
							speed_line = vController.getSpeedLineaire();
							if(!communicationType.equals("nordic"))
							speed_line = (-1) * speed_line * 10;
							else
								speed_line = (-1) * speed_line;
							String[] data = message.split(";");
							if (data.length > 1) {
								if (data[1].equals("-VL*50%"))
									vModel.setY(speed_line / 2);
								else if (data[1].equals("-VL*20%")) {
									vModel.setY(speed_line / 5);
								} else
									vModel.setY(speed_line);
								vModel.setX(0);
								vController.move();

							} else {
								vModel.setFlagReculer(true);
								if (vModel.getFlagDroite() == true
										|| vModel.getFlagGauche() == true) {
									vModel.setY(speed_line);
								} else {
									vModel.setX(0);
									vModel.setY(speed_line);
									vController.move();
								}
							}
						}

					}
				} else if (message.contains("droiteB")) {
					if (application.getRobotConnexion().equals("true")) {

						if (vController != null) {

							String[] data = message.split(";");
							int speed_lat = 1;
							speed_lat = vController.getSpeedLaterale();
							if(!communicationType.equals("nordic"))
							speed_lat = (-1) * speed_lat * 5;
							else
								speed_lat = (-1)* speed_lat;

							vModel.setMovedLoop(true);
							if (data.length > 1) {
								if (data[2].equals("-VA*50%"))
									vModel.setX(speed_lat / 2);
								else if (data[2].equals("-VA*20%")) {
									vModel.setX(speed_lat / 5);
								} else
									vModel.setX(speed_lat);
								vModel.setY(0);
								vController.move();

							} else {
								vModel.setFlagDroite(true);
								if (vModel.getFlagAvancer() == true
										|| vModel.getFlagReculer() == true) {
									vModel.setX(speed_lat);
								} else {
									vModel.setX(speed_lat);
									vModel.setY(0);
									vController.move();
								}
							}

						}

					}
				} else if (message.contains("gaucheB")) {
					if (application.getRobotConnexion().equals("true")) {

						if (vController != null) {
							int speed_lat = 1;
							speed_lat = vController.getSpeedLaterale();
							if(!communicationType.equals("nordic"))
							speed_lat = speed_lat * 5;

							String[] data = message.split(";");
							vModel.setMovedLoop(true);
							if (data.length > 1) {
								if (data[2].equals("VA*50%"))
									vModel.setX(speed_lat / 2);
								else if (data[2].equals("VA*20%")) {
									vModel.setX(speed_lat / 5);
								} else
									vModel.setX(speed_lat);
								vModel.setY(0);
								vController.move();

							} else {
								vModel.setFlagGauche(true);
								if (vModel.getFlagAvancer() == true
										|| vModel.getFlagReculer() == true) {
									vModel.setX(speed_lat);
								} else {
									vModel.setX(speed_lat);
									vModel.setY(0);
									vController.move();
								}
							}
						}

					}
				} else if (message.contains("MOVE")) {

					try {
						handler.removeCallbacks(runStopMouvements);
						handler.removeCallbacks(runStopHead);
						if (delai != 0)
							handler.postDelayed(runStopMouvements, delai);
						String data = message.replace("MOVE;", "");
						String[] speeds = data.split(";");
						int x = (int) (Float.parseFloat(speeds[1]));
						int y = (int) (Float.parseFloat(speeds[0]));
						if(!communicationType.equals("nordic")) {
							 x = (int) (Float.parseFloat(speeds[1]) * 5);
							 y = (int) (Float.parseFloat(speeds[0]) * 10);
						}


						if (application.getRobotConnexion().equals("true")) {

							if (vController != null) {

								vModel.setMovedLoop(true);
								vController.move(x, y);


							}
						}
					}catch(Exception e){
						GestionDesFichiers.log("Error ", "message : " + message , "Log_teambot");
					}
				} else if (message.contains("arreter")) {
					if (message.contains("avancer")) {
						vModel.setY(0);
						vModel.setFlagAvancer(false);
					} else if (message.contains("reculer")) {
						vModel.setY(0);
						vModel.setFlagReculer(false);
					} else if (message.contains("droite")) {
						vModel.setX(0);
						vModel.setFlagDroite(false);
					} else if (message.contains("gauche")) {
						vModel.setX(0);
						vModel.setFlagGauche(false);
					}
					if ((vModel.getFlagAvancer() == false
							&& vModel.getFlagDroite() == false
							&& vModel.getFlagGauche() == false
							&& vModel.getFlagReculer() == false) || message.contains("all")) {
						vModel.setX((int) 0);
						vModel.setY((int) 0);
						start = false;
						vController.stopRobot();
					}

				} else if (message.contains("HEAD")) {
					if (application.getRobotConnexion().equals("true")) {
						handler.removeCallbacks(runStopMouvements);
						handler.removeCallbacks(runStopHead);
						handler.postDelayed(runStopHead, delai);
						String data[] = message.split(";");
						if (communicationType.equals("usb")) {
							if (data[1].equals("U")) {

								if (UsbSerial.isSerial) {
									UsbSerial.write((ControlUboControllerImpl.commandeHead
											+ " " + "U"));
								} else {
									Transfer.send(ControlUboControllerImpl.commandeHead
											+ " " + "U");
								}
							} else if (data[1].equals("D")) {
								if (UsbSerial.isSerial) {
									UsbSerial.write((ControlUboControllerImpl.commandeHead
											+ " " + "D"));
								} else {
									Transfer.send(ControlUboControllerImpl.commandeHead
											+ " " + "D");
								}
							} else if (data[1].equals("S")) {
								vController.stopHead();
							}

						}
						else{
							if (data[1].equals("U")) {
								BluetoothService.write((ControlUboControllerImpl.commandeHead
										+ " " + "U"));

							} else if (data[1].equals("D")) {

								BluetoothService.write((ControlUboControllerImpl.commandeHead
										+ " " + "D"));

							} else if (data[1].equals("S")) {
								vController.stopHead();
							}
						}
					}
				}

			}

		});

	}


	public PopupWindow initialpoup_setting(View layoutSetteing, View button,
										   PopupWindow popup) {

		popup = new PopupWindow(layoutSetteing,
				ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.MATCH_PARENT, true);

		popup.showAtLocation(button, 1, 1, 1);

		final PopupWindow pwindo_setting = popup;

		application.notifyObservers("setControlInvisible");


		return pwindo_setting;
	}


	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
							   long arg3) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub

	}

	public void openAnim(View Button) {
		application.notifyObservers("pause");
		String data = "";
		String senariosFile = OrService.getParamFromFile("ScenarioName");
		if(senariosFile!=null)
			data = senariosFile;
		String[] array_item = data.split(
				" ");
		List<String> list = Arrays.asList(array_item);
		Set<String> set = new HashSet<String>(list);
		for (String value : set) {
			if (value.equals(""))
				set.remove(value);
		}

		List<String> list1 = new ArrayList<String>(set);
		CustumAnimationListAdapter dataAdapter_animations = new CustumAnimationListAdapter(
				vModel.getContext(), list1, application, vController);

		listOfAnims.setAdapter(dataAdapter_animations);
		pwindo_settingAnim = initialpoup_setting(layoutSettingAnim, Button,
				pwindo_settingAnim);
	}

	Runnable runStopMouvements = new Runnable() {
		@Override
		public void run() {
			if(application.isEnableSuivi()==false) {
				vModel.setMovedLoop(false);
				vController.stopRobot();
			}

		}
	};

	Runnable runStopHead = new Runnable() {
		@Override
		public void run() {
			if(application.isEnableSuivi()==false) {
				vModel.setMovedLoop(false);
				vController.stopHead();
			}
		}
	};
	private void UpdateLight() {
		if(!application.isTelepresence()){
			String lightValue = application.getParam(RemoteControlUtil.LIGHT);
			if(lightValue.equalsIgnoreCase("") || lightValue.equalsIgnoreCase("true")){
				light.setImageResource(R.drawable.lighton);
				vController.light(false);
				application.setEditor(RemoteControlUtil.LIGHT,"false");
				textlight.setText(R.string.lightOn);
				if(application.getChatEnabled()==true){
					if(call.equals("linphone")) {

						ServiceRobot.sendMessage(application.getPseudoOpenFireSender(), "lightoff");
					}
				}
			}else {
				vController.light(true);
				application.setEditor(RemoteControlUtil.LIGHT, "true");
				light.setImageResource(R.drawable.lightoff);
				textlight.setText(R.string.lightOff);
				if(application.getChatEnabled()==true)
					ServiceRobot.sendMessage(application.getPseudoOpenFireSender(),"lighton");
			}

		}
		else {
			application.getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(application.getApplicationContext(), R.string.camUsed, Toast.LENGTH_SHORT).show();
				}
			});
		}

	}
}
