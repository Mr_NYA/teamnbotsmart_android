package com.orrobotics.orsmartubo.service;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Set;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.perf.FirebasePerformance;
import com.google.firebase.perf.metrics.Trace;
import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.observer.OrSmarRobotObserver;
import com.orrobotics.orsmartrobot.start.Start;
import com.orrobotics.orsmartrobot.util.GestionDesFichiers;
import com.orrobotics.orsmartrobot.util.RemoteControlUtil;
import com.orrobotics.orsmartubo.R;
import com.orrobotics.orsmartubo.bluetooth.BluetoothLeService;
import com.orrobotics.orsmartubo.bluetooth.BluetoothService;
import com.orrobotics.orsmartubo.usb.Transfer;
import com.orrobotics.orsmartubo.usb.serial.UsbSerial;
import com.orrobotics.orsmartubo.view.ControlUboFragment;
import com.orrobotics.webservice.util.ConfigurationFile;
import com.orrobotics.webservice.util.CustomProperties;

/**
 * @author HAJIBA IFRAH
 *
 */
@SuppressLint("NewApi")
public class OrService extends Service implements OrSmarRobotObserver {
	/*
	 * Notifications from UsbSerial will be received here.
	 */
	private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			switch (intent.getAction()) {

				case UsbSerial.ACTION_USB_PERMISSION_GRANTED: // USB PERMISSION

					break;
				case UsbSerial.ACTION_USB_PERMISSION_NOT_GRANTED: // USB PERMISSION
					// NOT GRANTED
					break;
				case UsbSerial.ACTION_NO_USB: // NO USB CONNECTED
					Log.i("serial", "No USB connected");
					application.setEditor(RemoteControlUtil.CONNECTED_TO_APP,
							"false");
					application.notifyObservers("RobotOff");
					application.setEditor(RemoteControlUtil.MAC_CONNECTED_DEVICE, "");
					break;
				case UsbSerial.ACTION_USB_DISCONNECTED: // USB DISCONNECTED
					application.setEditor(RemoteControlUtil.CONNECTED_TO_APP,
							"false");
					application.notifyObservers("RobotOff");
					application.setEditor(RemoteControlUtil.MAC_CONNECTED_DEVICE, "");
					UsbSerial.isSerial = false;
					break;
				case UsbSerial.ACTION_USB_NOT_SUPPORTED: // USB NOT SUPPORTED
					Log.i("serial", "USB device not supported");
					application.setEditor(RemoteControlUtil.CONNECTED_TO_APP,
							"false");
					application.notifyObservers("RobotOff");
					application.setEditor(RemoteControlUtil.MAC_CONNECTED_DEVICE, "");
					break;
			}
		}
	};
	private UsbSerial usbSerial;

	private final ServiceConnection usbConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName arg0, IBinder arg1) {
			usbSerial = ((UsbSerial.UsbBinder) arg1).getService();

			// usbSerial.setHandler(mHandler);

		}

		@Override
		public void onServiceDisconnected(ComponentName arg0) {
			usbSerial = null;
			application.setEditor(RemoteControlUtil.CONNECTED_TO_APP,
					"false");
			application.notifyObservers("RobotOff");
			application.setEditor(RemoteControlUtil.MAC_CONNECTED_DEVICE, "");
		}
	};

	private BluetoothService bluetooth;
	private final ServiceConnection bluetoothConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName arg0, IBinder arg1) {
			bluetooth = ((BluetoothService.BluetoothBinder) arg1).getService();

		}

		@Override
		public void onServiceDisconnected(ComponentName arg0) {
			bluetooth = null;
			application.setEditor(RemoteControlUtil.CONNECTED_TO_APP,
					"false");
			application.notifyObservers("RobotOff");
			application.setEditor(RemoteControlUtil.MAC_CONNECTED_DEVICE, "");
		}
	};

	private BluetoothLeService bluetoothLe;
	private final ServiceConnection bluetoothLeConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName arg0, IBinder arg1) {
			bluetoothLe = ((BluetoothLeService.LocalBinder) arg1).getService();

		}

		@Override
		public void onServiceDisconnected(ComponentName arg0) {
			bluetoothLe = null;
			application.setEditor(RemoteControlUtil.CONNECTED_TO_APP,
					"false");
			application.notifyObservers("RobotOff");
			application.setEditor(RemoteControlUtil.MAC_CONNECTED_DEVICE, "");
		}
	};
	/*
	 * ***************************************************************
	 */
	SmartRobotApplication application;
	ServerSocket serverSocket = null;
	final int SERVER_PORT = 10086;
	Socket socket;
	public static Boolean mainThreadFlag = true;
	Context con;
	Fragment controlFragment;
	ControlUboFragment controlUboFragment;
	Boolean flagSocket = false;
	static File pathOfMyFile = Environment
			.getExternalStoragePublicDirectory("SmartRobot");
	static File path1 = Environment.getExternalStorageDirectory();
	String deviceAdresse;
	public static String commandeAdresse="ADDRESS";
	Trace myTrace;
	String communicationType;




	Handler mHandler = new Handler();
	@Override
	public void onCreate() {
		super.onCreate();
		// ****************** *********************/
		myTrace = FirebasePerformance.getInstance().newTrace("TeambotSmart_trace");
		myTrace.start();
		application = (SmartRobotApplication) this.getApplicationContext();

		String adresse = getParamFromFile("commande.adresse");
		if(adresse!=null)
			commandeAdresse=adresse;
		setFilters();

		communicationType = SmartRobotApplication.getParamFromFile(
				"robot.connexion.type", "Configuration.properties", null);
		Log.i("mbu","---communicationType0="+communicationType);
		if(communicationType == null)
			communicationType = "nordic";

		if(communicationType.equals("usb"))
			startService(UsbSerial.class, usbConnection, null,"usb"); // Start
		// usbSerial(if it
		// was not started
		// before) and Bind
		// it
		// *****************************************

		application.registerObserver(OrService.this);
		con = getApplicationContext();
		/**
		 * Création de fichier de Configuration "teambotSmart.properties"
		 */
		createPropertiesFile();
		application.setSharedPref(this.getSharedPreferences("Teambot",
				Context.MODE_PRIVATE));
		application.setEditor(RemoteControlUtil.EXTRAS_ROBOT_TYPE, "5");
		application.setRobotType();
		//application.setEditor(RemoteControlUtil.EXTRAS_ROBOT_NAME, "Teambot");
		application.setEditor(RemoteControlUtil.MAC_CONNECTED_DEVICE, "");
		application.setEditor(RemoteControlUtil.CONNECTED_TO_APP, "false");
		deviceAdresse = application.getDeviceAddress();
		try {
			serverSocket = new ServerSocket();
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}


		if (!usb.isAlive())
			Log.i("usb", "start usb");
		usb.start();
		if (application.getParam(RemoteControlUtil.AUTOSTART).equals("true")) {
			if(usbSerial!=null)
				usbSerial.findSerialPortDevice();
			Intent intent = new Intent(getApplicationContext(), Start.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			getApplicationContext().startActivity(intent);

		}


	}



	/*
	 * ************************************************************************
	 */
	private void startService(Class<?> service,
							  ServiceConnection serviceConnection, Bundle extras,String type) {
		Boolean connected;
		if (type.equals("usb"))
			connected = UsbSerial.SERVICE_CONNECTED;
		else if(type.equals("bluetooth"))
			connected =BluetoothService.SERVICE_CONNECTED;
		else
			connected =BluetoothLeService.SERVICE_CONNECTED;

		if (!connected) {
			Intent startService = new Intent(getBaseContext(), service);
			if (extras != null && !extras.isEmpty()) {
				Set<String> keys = extras.keySet();
				for (String key : keys) {
					String extra = extras.getString(key);
					startService.putExtra(key, extra);
				}
			}
			startService(startService);
		}
		Intent bindingIntent = new Intent(getBaseContext(), service);
		bindService(bindingIntent, serviceConnection, Context.BIND_AUTO_CREATE);
	}

	private void setFilters() {
		IntentFilter filter = new IntentFilter();
		filter.addAction(UsbSerial.ACTION_USB_PERMISSION_GRANTED);
		filter.addAction(UsbSerial.ACTION_NO_USB);
		filter.addAction(UsbSerial.ACTION_USB_DISCONNECTED);
		filter.addAction(UsbSerial.ACTION_USB_NOT_SUPPORTED);
		filter.addAction(UsbSerial.ACTION_USB_PERMISSION_NOT_GRANTED);
		registerReceiver(mUsbReceiver, filter);
	}

	/*
	 * *****************************************************************************
	 */

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (application
				.isServiceRunning("com.orrobotics.orsmartubo.usb.serial.UsbSerial")) {
			Intent intent = new Intent(this, UsbSerial.class);
			stopService(intent);
		}

		flagSocket = true;
		mainThreadFlag = false;
		application.removeObserver(this);
		application.setEditor(RemoteControlUtil.MAC_CONNECTED_DEVICE, "");
		application.setEditor(RemoteControlUtil.CONNECTED_TO_APP, "false");
		application.removeObserver(OrService.this);
		if (socket != null) {
			try {
				socket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	Runnable run = new Runnable() {
		@Override
		public void run() {


			if(usbSerial!=null)
				usbSerial.findSerialPortDevice();
		}
	};

	@Override
	public void update(final String message) throws IOException {
		// TODO Auto-generated method stub
		// Log.v("observer", "message -------------" + message);

		if (message.equals("discoveringDevice")) {
			if (UsbSerial.isSerial) {
				if(usbSerial!=null)
					usbSerial
							.write(commandeAdresse);
			} else {
				Transfer.send(commandeAdresse);
			}
		}
		if(message.equals("listRobotStart")){
			if (application.getRobotConnexion().equals("false")||application.getRobotConnexion().equals("")) {
				if(usbSerial!=null)
					usbSerial.findSerialPortDevice();
			}
		}
		if (message.contains("close")) {
			application.setEditor(RemoteControlUtil.MAC_CONNECTED_DEVICE, "");
			mainThreadFlag = false;
			if (socket != null) {
				try {
					socket.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			usb = new Thread();
			if (usb.getState() == Thread.State.NEW) {
				mainThreadFlag = true;
				usb.start();
			} else {

				usb.interrupt();
				mainThreadFlag = true;
				usb.start();

			}

		}
		if(message.equals("disconnectBl")){
			Log.i("mbu","---disconnectBl----");
			if (communicationType.equals("bluetooth")) {
				Log.i("mbu","---bluetooth----");
				unbindService(bluetoothConnection);

			}
			else {
				Log.i("mbu","---ble----");
				unbindService(bluetoothLeConnection);
			}

			application.setEditor(RemoteControlUtil.MAC_CONNECTED_DEVICE, "");
			application.setEditor(RemoteControlUtil.CONNECTED_TO_APP, "false");
		}
		if (message.contains("fermer")) {
			myTrace.stop();
			stopSelf();

		}
		if (message.equals("controlRobot")) {


			controlFragment = new ControlUboFragment(getApplicationContext());
			controlUboFragment = (ControlUboFragment) controlFragment;

			controlFragment.setArguments(application.getActivity().getIntent()
					.getExtras());
			FragmentActivity fragment = (FragmentActivity) application
					.getActivity();
			fragment.getSupportFragmentManager()
					.beginTransaction()
					.add(com.orrobotics.orsmartrobot.R.id.fragment_robot,
							controlFragment).commitAllowingStateLoss();

			new Handler().postDelayed(run, 5000);

			if(communicationType.equals("bluetooth"))
			{
				Log.i("mbu", "---DeviceAddress ="+application.getDeviceAddress());
				if(!application.getDeviceAddress().equals(""))
					startService(BluetoothService.class, bluetoothConnection, null,"bluetooth");
			}
			else if(communicationType.equals("ble") || communicationType.equals("nordic"))
			{
				Log.i("mbu", "---DeviceAddressBle ="+application.getDeviceAddress());
				if(!application.getDeviceAddress().equals(""))
					startService(BluetoothLeService.class, bluetoothLeConnection, null,"bluetoothLe");
			}

		}
		if (message.contains("adresse")) {
			mHandler.post(new Runnable() {
				public void run() {
					if (message.split(";").length > 1) {
						GestionDesFichiers.log("INFO"," adresse mac de la carte "+message, "Log_SmartRobot");

						application.setEditor(RemoteControlUtil.MAC_CONNECTED_DEVICE,
								message.split(";")[1]);
						application.setMacConnectedDevice();
						application.setEditor(RemoteControlUtil.CONNECTED_TO_APP, "true");
						application.setRobotConnexion();
						application.notifyObservers("RobotOn");
					}

				}
			});


		}

	}

	private Thread usb = new Thread() {
		public void run() {
			if (serverSocket != null) {
				while (!serverSocket.isBound() && flagSocket == false) {
					try {
						serverSocket.setReuseAddress(true);
						serverSocket.bind(new InetSocketAddress(SERVER_PORT)); // <--

					} catch (IOException e) {
						// TODO Auto-generated catch block
						application
								.quitApplication("org.ros.android.android_sensors_driver");
						if (serverSocket != null) {
							try {
								serverSocket.close();
								serverSocket = null;

							} catch (IOException e2) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						try {
							serverSocket = new ServerSocket();
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}

					}
				}
			}
			while (mainThreadFlag) {
				Log.d("chl", "doListen() 4");
				try {
					socket = serverSocket.accept();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (application.getParam(RemoteControlUtil.AUTOSTART).equals(
						"true")) {
					if (application.getMacConnectedDevice().contains(":")) {
						if (application.getMacConnectedDevice().equals(
								application.getDeviceAddress())) {

							application.setEditor(
									RemoteControlUtil.CONNECTED_TO_APP, "true");

						} else {
							mHandler.post(new Runnable() {
								public void run() {
									Toast.makeText(getApplicationContext(),
											R.string.differentMacAddress,
											Toast.LENGTH_LONG).show();
								}
							});
						}
					}
				}
				application.notifyObservers("Uboconnected");

				Log.d("chl", "doListen() 3");
				new Thread(new Transfer(socket, getApplicationContext()))
						.start();
			}
		}

	};

	/**
	 * Création du fichier de configuration
	 *
	 *
	 */
	public static void createPropertiesFile() {
		File file = new File(pathOfMyFile, "teambotSmart.properties");

		File file1 = new File(path1, "SmartRobot");
		if (!file.exists()) {
			ConfigurationFile.WritePropertiesFile_ubo(file1, file);

		}
	}


	public static String getParamFromFile(String param) {
		Log.v("", "-----getParamFromFile------");
		File file1 = new File(path1, "SmartRobot");
		String paramValue = null;
		CustomProperties props = new CustomProperties();
		CustomProperties newProps = new CustomProperties();

		props = ConfigurationFile.uboProps;

		newProps = ConfigurationFile.loadproperties(file1,
				"teambotSmart.properties", props);
		paramValue = newProps.getProperty(param);
		return paramValue;

	}

}