package com.orrobotics.orsmartubo.service;

import android.app.Activity;
import android.app.IntentService;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.util.Log;

import com.orrobotics.orsmartubo.Orsmartubologging;

import java.util.Locale;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class CheckTTSService extends Service implements TextToSpeech.OnInitListener{
    private TextToSpeech tts;
    private Handler handler;

    @Override
    public void onCreate() {
        super.onCreate();
        tts = new TextToSpeech(getApplicationContext(), this);
        handler = new Handler();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Intent detailsIntent =  new Intent(RecognizerIntent.ACTION_GET_LANGUAGE_DETAILS);
        sendOrderedBroadcast(
                detailsIntent, null, new LanguageDetailsChecker(), null, Activity.RESULT_OK, null, null);
        detailsIntent =  new Intent(RecognizerIntent.EXTRA_PREFER_OFFLINE);
        sendOrderedBroadcast(
                detailsIntent, null, new LanguageDetailsChecker(), null, Activity.RESULT_OK, null, null);

        handler.removeCallbacksAndMessages(null);
        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
                stopSelf();
            }
        }, 15*1000);

        return CheckTTSService.START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        super.onDestroy();
    }

    @Override
    public void onInit(int status) {
        Orsmartubologging.engine_tts=tts.getEngines();
        Orsmartubologging.default_engine_tts=tts.getDefaultEngine();
        if (status == TextToSpeech.SUCCESS) {

            Orsmartubologging.tts=true;
            int result = tts.setLanguage(Locale.ENGLISH);
            if (result != TextToSpeech.LANG_MISSING_DATA && result != TextToSpeech.LANG_NOT_SUPPORTED) {
                Orsmartubologging.en_tts=true;
            }else{
                Orsmartubologging.en_tts=false;
            }
            result = tts.setLanguage(Locale.FRANCE);
            if (result != TextToSpeech.LANG_MISSING_DATA && result != TextToSpeech.LANG_NOT_SUPPORTED) {
                Orsmartubologging.fr_tts=true;
            }else{
                Orsmartubologging.fr_tts=false;
            }
        }else{
            Orsmartubologging.tts=false;
        }
        this.stopSelf();
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }
}

class LanguageDetailsChecker extends BroadcastReceiver
{


    @Override
    public void onReceive(Context context, Intent intent)
    {
        Bundle results = getResultExtras(true);
        if (results.containsKey(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE))
        {
            Orsmartubologging.defaultLanguague =
                    results.getString(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE);
        }
        if (results.containsKey(RecognizerIntent.EXTRA_SUPPORTED_LANGUAGES))
        {
            Orsmartubologging.supportedLanguage =
                    String.valueOf(results.getStringArrayList(
                            RecognizerIntent.EXTRA_SUPPORTED_LANGUAGES));
        }


    }
}
