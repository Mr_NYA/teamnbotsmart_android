package com.orrobotics.orsmartubo.model;

import com.orrobotics.orsmartubo.control.ControlUboController;
import com.orrobotics.orsmartubo.control.ControlUboControllerImpl;
import com.orrobotics.orsmartubo.service.OrService;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;

public class ControlUboModelImpl implements ControlUboModel {

	Context context;
	Activity activity;
	Fragment fragement;
	Boolean moved = false;
	int x=0;
	int y=0;
	String commande;
	Boolean flagAvancer=false;
	Boolean flagReculer=false;
	Boolean flagDroite=false;
	Boolean flagGauche=false;
	boolean ismouved = false;
	



	public ControlUboModelImpl(Context con, Activity acti, Fragment fragement) {

		this.context = con;
		this.activity = acti;
		this.fragement = fragement;
		commande = ControlUboControllerImpl.commandemouve + " "
				+ 0 + " " + 0;

	}

	@Override
	public Context getContext() {
		// TODO Auto-generated method stub
		return context;
	}

	@Override
	public Activity getActivity() {
		// TODO Auto-generated method stub
		return activity;
	}

	@Override
	public Fragment getFragment() {
		// TODO Auto-generated method stub
		return fragement;
	}
	@Override
	public Boolean getMovedLoop() {
		return moved;
	}
	@Override
	public void setMovedLoop(Boolean moved) {
		this.moved = moved;
	}

	@Override
	public void setX(int x) {
		// TODO Auto-generated method stub
		this.x=x;
	}

	@Override
	public void setY(int y) {
		// TODO Auto-generated method stub
		this.y=y;
	}

	@Override
	public int getX() {
		// TODO Auto-generated method stub
		return x;
	}

	@Override
	public int getY() {
		// TODO Auto-generated method stub
		return y;
	}

	@Override
	public void setCommande(String commande) {
		// TODO Auto-generated method stub
		this.commande=commande;
	}
	@Override
	public String getCommande() {
		return commande;
	}
	
	public Boolean getFlagAvancer() {
		return flagAvancer;
	}

	public void setFlagAvancer(Boolean flagAvancer) {
		this.flagAvancer = flagAvancer;
	}

	public Boolean getFlagReculer() {
		return flagReculer;
	}

	public void setFlagReculer(Boolean flagReculer) {
		this.flagReculer = flagReculer;
	}

	public Boolean getFlagDroite() {
		return flagDroite;
	}

	public void setFlagDroite(Boolean flagDroite) {
		this.flagDroite = flagDroite;
	}

	public Boolean getFlagGauche() {
		return flagGauche;
	}

	public void setFlagGauche(Boolean flagGauche) {
		this.flagGauche = flagGauche;
	}

	@Override
	public void setHeadMouve(boolean ismouved) {
		// TODO Auto-generated method stub
		this.ismouved=ismouved;
	}
	@Override
	public Boolean isHeadMouve() {
		return ismouved;
		
	}

}
