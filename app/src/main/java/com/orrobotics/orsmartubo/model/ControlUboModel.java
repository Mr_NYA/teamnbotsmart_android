package com.orrobotics.orsmartubo.model;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;

public interface ControlUboModel {

	public Context getContext();

	public Activity getActivity();
	
	public Fragment getFragment();

	public Boolean getMovedLoop();
	
	public void setMovedLoop(Boolean moved);

	public void setX(int x);

	public void setY(int y);

	public int getX();

	public int getY();

	public void setCommande(String commande);
	
	public String getCommande();
	
	public void setFlagAvancer(Boolean flagAvancer);
	
	public Boolean getFlagAvancer();
	
	public Boolean getFlagReculer();
	
	public void setFlagReculer(Boolean flagReculer);
	
	public Boolean getFlagDroite();
	
	public void setFlagDroite(Boolean flagDroite);
	
	public Boolean getFlagGauche();
	
	public void setFlagGauche(Boolean flagGauche);

	public void setHeadMouve(boolean ismouved);

	Boolean isHeadMouve();

}
