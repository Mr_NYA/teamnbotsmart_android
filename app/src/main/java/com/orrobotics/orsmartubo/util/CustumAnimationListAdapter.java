package com.orrobotics.orsmartubo.util;

import java.util.List;
import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartubo.R;
import com.orrobotics.orsmartubo.control.ControlUboController;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

@SuppressLint("InlinedApi") public class CustumAnimationListAdapter extends BaseAdapter{
	
	private Context cntxt;
	List<String> setting_list;
	SmartRobotApplication model;
	ControlUboController vController;

	public CustumAnimationListAdapter(Context cntxt,
			List<String> list,
			SmartRobotApplication model, ControlUboController vController) {
		this.cntxt = cntxt;
		this.setting_list = list;
		this.model = model;
		this.vController=vController;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return setting_list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return setting_list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View row = null;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) cntxt
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(R.layout.custom_row_animation, parent,
					false);
		} else {
			row = convertView;
		}
		final String chosenanim=setting_list.get(position);
		TextView titleRow = (TextView) row.findViewById(R.id.title_anim);
		titleRow.setText(chosenanim);
		ImageButton play = (ImageButton) row
				.findViewById(R.id.play);

		play.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				model.notifyObservers("playAnimEnd");
				Handler handler = new Handler();
				handler.postDelayed(new Runnable() {
				    public void run() {
				    	model.getActivity().setRequestedOrientation(
								ActivityInfo.SCREEN_ORIENTATION_LOCKED);
				    	
				    	XmlScenriosCreator.ExecuterScenario(chosenanim +".xml",
						vController.getSpeedLineaire(), vController.getSpeedLaterale(), cntxt);
						
				    }
				}, 800);
		
			}
		});
		
		return row;
	}
	}
