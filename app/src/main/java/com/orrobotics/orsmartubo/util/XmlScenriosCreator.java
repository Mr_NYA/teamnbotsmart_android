package com.orrobotics.orsmartubo.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipOutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartubo.control.ControlUboController;
import com.orrobotics.orsmartubo.model.ControlUboModel;
import com.orrobotics.orsmartubo.usb.Transfer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Environment;
import android.os.Handler;

/**
 * 
 * @author Soufiane Zahir Cette classe sert à gérer le fichier XML des scénarios
 * @version 2.1.0
 * @date 04/05/2017
 * 
 */
@SuppressLint("SdCardPath")
public class XmlScenriosCreator {

	static File absolutPath = Environment.getExternalStorageDirectory();
	static File pathOfMyFile = Environment
			.getExternalStoragePublicDirectory("/SmartRobot");
	static SimpleDateFormat simpleFormat = new SimpleDateFormat(
			"dd/MM/yyyy HH:mm:ss", java.util.Locale.getDefault());
	static Calendar c = Calendar.getInstance();
	static String FilePath = pathOfMyFile + "/SmartTeambotScenarios";
	static FileOutputStream dest;
	static ZipOutputStream out;
	static final int BUFFER = 2048;
	public static String[][] Action_Liste;
	public static String[] RequeteListe;
	public static Handler mHandler = new Handler();
	public static String data;
	static SmartRobotApplication application;
	static int speedLineaire;
	static int speedLaterale;
	static ControlUboController vController;
	static String tempo = "2";
	static int screenWidth;
	static int screenHight;
	static String urlvideo = null;
	static boolean flagvideoplay = false;
	static ControlUboModel vModel;
	static int l = -1;
	static boolean flagIsVideo = false;
	static boolean flag = false;
	static int delayx = 0;

	public static String[][] ReadScnariosFile(String Filename) {
		try {
			File file = new File("/sdcard/SmartRobot/SmartTeambotScenarios/"
					+ Filename);

			InputStream is = new FileInputStream(file.getPath());
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(new InputSource(is));
			doc.getDocumentElement().normalize();
			Element element = doc.getDocumentElement();
			element.normalize();

			NodeList BloquenodeList = doc.getElementsByTagName("Bloque");
			NodeList nodeRequteList = doc.getElementsByTagName("Requete");
			NodeList nodeActionList = doc.getElementsByTagName("Action");
			NodeList nodeTempoList = doc.getElementsByTagName("tempo");
			String requeteName = null;
			Action_Liste = new String[BloquenodeList.getLength()][BloquenodeList
					.getLength()];
			RequeteListe = new String[nodeRequteList.getLength()];

			for (int i = 0; i < nodeActionList.getLength(); i++) {

				Node node = nodeActionList.item(i);
				Element fstElmnt = (Element) node;
				Node node2 = nodeTempoList.item(i);

				Node nodeR = nodeRequteList.item(i);
				Element elmR = (Element) nodeR;
				Element fstElmnt2 = (Element) node2;

				requeteName = elmR.getTextContent();

				// SPECIFIER L'ACTION SPEAK
				if (requeteName.equals("SPEAK")) {
					Action_Liste[i][0] = "sp " + fstElmnt.getTextContent();
				}
				// SP�CIFIER L'ACTION WRITE
				else if (requeteName.equals("WRITE")) {
					Action_Liste[i][0] = "wr " + fstElmnt.getTextContent();
				}
				// SPECIFIER L'ACTION FILE
				else if (requeteName.equals("SHOW_FILE")) {
					Action_Liste[i][0] = "fl " + fstElmnt.getTextContent();
				}
				// SPECIFIER MOVE ET HEAD
				else {
					Action_Liste[i][0] = fstElmnt.getTextContent();
				}

				// TEMPO
				Action_Liste[i][1] = fstElmnt2.getTextContent();

			} // END LOUP FOR
		} catch (Exception e) {
		}

		return Action_Liste;
	}// END FUNCTION READSCNARIOSFILE

	// POUR EXECECUTER LES COMMANDE ENREGISTRER
	public static void ExecuterScenario(final String filename,
			final int speedLineaire, final int speedLaterale,
			final Context context) {
		application = (SmartRobotApplication) context;
		String[][] table = ReadScnariosFile(filename);

		int j = 0;
		
		for (int i = l + 1; i < table.length; i++) {

			if (application.getEnd() == false) {

				data = table[i][0];
				delayx = (int) (Float.parseFloat(table[i][j + 1]) * 1000);

				if (data.startsWith("fl") || data.contains("wr")) {
					if (data.startsWith("fl")) {
						application.notifyObservers("openSnapFile;"
								+ data.substring(3) + ";" + delayx);
						flagIsVideo = true;
					} else
						application.notifyObservers("ecrire;"
								+ data.substring(3)+";"+delayx);

					l = i;
					flag = true;
					j = 0;
					break;

				} else
					execute(data, speedLineaire, speedLaterale, delayx);

				j = 0;

			} else {
				application.setEndScenario(false);
				if (flag == true) {
					if (flagIsVideo == true) {
						application.notifyObservers("Noplaying");
						flagIsVideo = false;
					}
					application.notifyObservers("hideTexts");
					flag = false;
				}
				break;

			}
		}
		if (flag == true) {
			Handler hand = new Handler();
			hand.postDelayed(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub

					if (flagIsVideo == true) {
						application.notifyObservers("Noplaying");
						flagIsVideo = false;
					}
					application.notifyObservers("hideTexts");
					if (application.getEnd() == false)
						ExecuterScenario(filename, speedLineaire,
								speedLaterale, context);

					else
						application.setEndScenario(false);

					flag = false;
				}
			}, delayx);
		} else {
			l = -1;
			application.getActivity().setRequestedOrientation(
					ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
			application.notifyObservers("resume");
		}
	}

	private static void execute(String data2, int speedLineairex,
			int speedLateralex, int delay) {
		// TODO Auto-generated method stub
		String datasplit = "";
		String datasubstring = "";
		// int delayx=Integer.parseInt(delay)*1000;

		speedLineaire = speedLineairex * 10;
		speedLaterale = speedLateralex * 5;
		datasubstring = data2.substring(0, 4);
		datasplit = data2.substring(4);
		// CONDITON POUR SEPARER LES MOVE
		if (datasubstring.equals("MOVE")) {

			String[] listdata = data2.split(";");

			if (listdata[1].contains("VL")) {

				if (listdata[1].contains("50")) {
					speedLineaire = speedLineaire / 2;

				} else if (listdata[1].contains("25")) {
					speedLineaire = speedLineaire / 4;
				}
			} else
				speedLineaire = 0;

			if (listdata[1].contains("-VL"))
				speedLineaire = -speedLineaire;

			if (listdata[2].contains("VA")) {
				if (listdata[2].contains("50")) {
					speedLaterale = speedLaterale / 2;

				} else if (listdata[2].contains("25")) {
					speedLaterale = speedLaterale / 4;
				}
			} else
				speedLaterale = 0;

			if (listdata[2].contains("-VA"))
				speedLaterale = -speedLaterale;

			data2 = "MOVE " + speedLineaire + " " + speedLaterale;

			Transfer.send(data2);

		} // END ELSE IF CONDITION MOVE
		else if (data2.equals("STOP")) {
			data = "MOVE 0 0";
			Transfer.send(data);

		} // END LAST ELSE IF
		else if (datasubstring.equals("HEAD")) {
			data2 = datasubstring + datasplit;
			Transfer.send(data2);

		} else if (data2.startsWith("sp")) {
			application.notifyObservers("dire;" + data2.substring(3));
		}
		long start = new Date().getTime();
		while (new Date().getTime() - start < delay) {
			if (application.getEnd() == true)
				break;
		}

	}

	public static String getfilename() {
		return urlvideo;
	}

	public static List<String> getScenarioName() throws IOException {
		File f = new File("/sdcard/SmartRobot/teambotSmart.properties");
		BufferedReader in = new BufferedReader(new FileReader(f));
		String line;
		String datasub = "";
		List<String> listsplit = null;

		while ((line = in.readLine()) != null) {
			if (line.startsWith("liste_scenarios")) {
				datasub = line.substring(16);
				listsplit = new ArrayList<String>(Arrays.asList(datasub
						.split(",")));

			}
		}
		in.close();

		return listsplit;
	}

	public static String getBtnVisibility() throws IOException {
		File f = new File("/sdcard/SmartRobot/teambotSmart.properties");
		BufferedReader in = new BufferedReader(new FileReader(f));
		String line;
		String datasub = "";
		while ((line = in.readLine()) != null) {
			if (line.startsWith("bouton.scenario")) {
				datasub = line.substring(15);
			}
		}
		in.close();

		return datasub;
	}

	// METHODE POUR CREER LE DOSSIER ORSMARTTEAMBOTSCENARIO
	public static void CreerScenariosDossier() {
		File scd = new File(FilePath);
		if (!scd.exists()) {
			scd.mkdir();
		}
	}

	// CREER LES FICHIER XML DES SCENARIOS
	public static void WriteScenario1File() {
		CreerScenariosDossier();
		File f = new File(
				"/sdcard/SmartRobot/SmartTeambotScenarios/Scenario1.xml");
		if (!f.exists()) {
			try {
				String d = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
						+ "<RobotScenario>"
						+ "<ScenarioName>Scenario1</ScenarioName>"
						+ "<Bloque><Requete>Baisser</Requete><Action>HEAD 1</Action><tempo>2</tempo></Bloque>"
						+ "<Bloque><Requete>Monter</Requete><Action>HEAD -1</Action><tempo>2</tempo></Bloque>"
						+ "<Bloque><Requete>STOP</Requete><Action>MOVE;0;0</Action><tempo>2</tempo></Bloque>"
						+ "<Bloque><Requete>Gauche</Requete><Action>MOVE;-VL*50%;0</Action><tempo>2</tempo></Bloque>"
						+ "<Bloque><Requete>Droite</Requete><Action>MOVE;0;VA*50%</Action><tempo>2</tempo></Bloque>"
						+ "<Bloque><Requete>STOP</Requete><Action>MOVE;0;0</Action><tempo>2</tempo></Bloque>"
						+ "<Bloque><Requete>SPEAK</Requete><Action>Bonjour</Action><tempo>2</tempo></Bloque>"
						+ "<Bloque><Requete>WRITE</Requete><Action>Salut</Action><tempo>2</tempo></Bloque>"
						+ "<Bloque><Requete>SHOW_FILE</Requete><Action>image.gif</Action><tempo>2</tempo></Bloque>"
						+ "</RobotScenario>";

				f.createNewFile();
				FileOutputStream fos;
				fos = new FileOutputStream(f);
				fos.write(d.getBytes());
				fos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}

	public static void WriteScenario2File() {
		CreerScenariosDossier();
		File f = new File(
				"/sdcard/SmartRobot/SmartTeambotScenarios/Scenario2.xml");
		if (!f.exists()) {
			try {
				String d = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
						+ "<RobotScenario>"
						+ "<ScenarioName>Scenario2</ScenarioName>"
						+ "<Bloque><Requete>Baisser</Requete><Action>HEAD 1</Action><tempo>2</tempo></Bloque>"
						+ "<Bloque><Requete>Monter</Requete><Action>HEAD -1</Action><tempo>2</tempo></Bloque>"
						+ "<Bloque><Requete>STOP</Requete><Action>MOVE;0;0</Action><tempo>2</tempo></Bloque>"
						+ "<Bloque><Requete>SHOW_FILE</Requete><Action>image.jpg</Action><tempo>2</tempo></Bloque>"
						+ "<Bloque><Requete>Gauche</Requete><Action>MOVE;-VL*25%;0</Action><tempo>2</tempo></Bloque>"
						+ "<Bloque><Requete>Droite</Requete><Action>MOVE;0;VA*25%</Action><tempo>2</tempo></Bloque>"
						+ "<Bloque><Requete>SPEAK</Requete><Action>Bonjour je suis UBO</Action><tempo>2</tempo></Bloque>"
						+ "<Bloque><Requete>WRITE</Requete><Action>Salut</Action><tempo>2</tempo></Bloque>"
						+ "<Bloque><Requete>SHOW_FILE</Requete><Action>74.mp3</Action><tempo>2</tempo></Bloque>"
						+ "</RobotScenario>";

				f.createNewFile();
				FileOutputStream fos;
				fos = new FileOutputStream(f);
				fos.write(d.getBytes());
				fos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}

	public static void WriteScenario3File() {
		CreerScenariosDossier();
		File f = new File(
				"/sdcard/SmartRobot/SmartTeambotScenarios/Scenario.xml");
		if (!f.exists()) {
			try {
				String d = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
						+ "<RobotScenario>"
						+ "<ScenarioName>Scenario3</ScenarioName>"
						+ "<Bloque><Requete>Baisser</Requete><Action>HEAD 1</Action><tempo>2</tempo></Bloque>"
						+ "<Bloque><Requete>Monter</Requete><Action>HEAD -1</Action><tempo>2</tempo></Bloque>"
						+ "<Bloque><Requete>STOP</Requete><Action>MOVE;0;0</Action><tempo>2</tempo></Bloque>"
						+ "<Bloque><Requete>Gauche</Requete><Action>MOVE;-VL*50%;0</Action><tempo>2</tempo></Bloque>"
						+ "<Bloque><Requete>Droite</Requete><Action>MOVE;0;VA*50%</Action><tempo>2</tempo></Bloque>"
						+ "<Bloque><Requete>SPEAK</Requete><Action>Bonjour</Action><tempo>2</tempo></Bloque>"
						+ "<Bloque><Requete>WRITE</Requete><Action>Salut</Action><tempo>2</tempo></Bloque>"
						+ "<Bloque><Requete>SHOW_FILE</Requete><Action>kid.mp4</Action><tempo>2</tempo></Bloque>"
						+ "</RobotScenario>";

				f.createNewFile();
				FileOutputStream fos;
				fos = new FileOutputStream(f);
				fos.write(d.getBytes());
				fos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}

}
