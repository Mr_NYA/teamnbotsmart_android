package com.orrobotics.orsmartubo;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import java.io.File;

import com.orrobotics.orsmartubo.R;
import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.start.Start;
import com.orrobotics.orsmartrobot.util.GestionDesFichiers;
import com.orrobotics.orsmartrobot.util.RemoteControlUtil;
import com.orrobotics.orsmartubo.service.OrService;
import com.orrobotics.orsmartubo.util.XmlScenriosCreator;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.util.Base64;
import android.util.Log;

/**
 * @author Hajiba IFRAH
 *
 */

public class OrSmartUbo extends Activity {

	Activity acti;
	SmartRobotApplication application;
	static File pathOfMyFile = Environment
			.getExternalStoragePublicDirectory("/SmartRobot");
	static File pathOfUBOMyFile = Environment
			.getExternalStoragePublicDirectory("/SmartRobot/SmartTeambotScenarios");

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Fabric.with(this, new Crashlytics());
		acti = this;
		application = (SmartRobotApplication) this.getApplicationContext();
		getPermissions();

	}
	public void init(){
		application.stricMode();
		application.setSharedPref(this.getSharedPreferences("Teambot",
				Context.MODE_PRIVATE));
		application.setEditor(RemoteControlUtil.AUTOSTART, "false");
		String bmp = Base64.encodeToString(
				application.parseDrawable(R.drawable.ubo), Base64.DEFAULT);
		application.setEditor(RemoteControlUtil.ICON, bmp);
		application.setEditor(RemoteControlUtil.EXTRAS_ROBOT_TYPE, "5");
		//application.setEditor(RemoteControlUtil.EXTRAS_ROBOT_NAME, "Teambot");
		application.StartAct(null, null, Start.class, acti);
		File file1 = new File(pathOfMyFile, "SmartTeambotScenarios");
		if (!file1.exists()) {
			XmlScenriosCreator.CreerScenariosDossier();
			XmlScenriosCreator.WriteScenario1File();
			XmlScenriosCreator.WriteScenario2File();
			XmlScenriosCreator.WriteScenario3File();
		}

		Intent serviceUbo = new Intent(acti, OrService.class);
		if (application
				.isServiceRunning("com.orrobotics.orsmartatti.service.OrService")) {
			stopService(serviceUbo);
			startService(serviceUbo);
		} else
			startService(serviceUbo);

		File file = new File(pathOfMyFile, "SmartRobot");
		if (!file.exists()) {
			GestionDesFichiers.creerFichier("Log_teambot");
		}
		finish();
	}

    public void getPermissions(){
        Log.i("mbu", "-------in getPermissions ---------");
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M){
            try {
                PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_PERMISSIONS);

                if (info.requestedPermissions != null) {

                    ActivityCompat.requestPermissions(this,
                            info.requestedPermissions,
                            1);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }}
        else init();

    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[],
                                           int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // La permission est garantie
                    init();
                } else {
                }
                return;
            }
        }
    }

}
