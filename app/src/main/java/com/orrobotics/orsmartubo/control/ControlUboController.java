package com.orrobotics.orsmartubo.control;


public interface ControlUboController {

	void move();
	
	void init();

	void destroy();

	int getSpeedLaterale();

	int getSpeedLineaire();

	void mouveHeadUp(boolean isVocale);
	
	void mouveHeadDown(boolean isVocale);

	void move(int x, int y);

	void stopRobot();

	void stopHead();

	void getBatterie();

    boolean light(boolean b);
}
