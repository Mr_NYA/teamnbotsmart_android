package com.orrobotics.orsmartubo.control;

import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.LocationManager;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.util.GestionDesFichiers;
import com.orrobotics.orsmartrobot.util.RemoteControlUtil;
import com.orrobotics.orsmartubo.R;
import com.orrobotics.orsmartubo.bluetooth.BluetoothLeService;
import com.orrobotics.orsmartubo.bluetooth.BluetoothService;
import com.orrobotics.orsmartubo.model.ControlUboModel;
import com.orrobotics.orsmartubo.sensor.FluidPressurePublisher;
import com.orrobotics.orsmartubo.sensor.IlluminancePublisher;
import com.orrobotics.orsmartubo.sensor.ImuPublisher;
import com.orrobotics.orsmartubo.sensor.MagneticFieldPublisher;
import com.orrobotics.orsmartubo.sensor.NavSatFixPublisher;
import com.orrobotics.orsmartubo.sensor.TemperaturePublisher;
import com.orrobotics.orsmartubo.service.CheckTTSService;
import com.orrobotics.orsmartubo.service.OrService;
import com.orrobotics.orsmartubo.usb.Transfer;
import com.orrobotics.orsmartubo.usb.serial.UsbSerial;
import com.orrobotics.orsmartubo.util.ProtocolCom;

/**
 * @author Hajiba IFRAH
 *
 */
@SuppressLint("NewApi")
public class ControlUboControllerImpl implements ControlUboController {
	ControlUboModel model;
	public static SmartRobotApplication application;
	Handler mHandler = new Handler();
	LocationManager mLocationManager;
	SensorManager mSensorManager;
	NavSatFixPublisher fix_pub;
	ImuPublisher imu_pub;
	MagneticFieldPublisher magnetic_field_pub;
	FluidPressurePublisher fluid_pressure_pub;
	IlluminancePublisher illuminance_pub;
	TemperaturePublisher temperature_pub;
	long i = 0;
	int sensorDelay;
	int delai = 500;
	Timer remoteTimer;
	Timer remoteTimerSensor;
	Timer remoteTimerHead;

	int headUp = 0;
	int headDown = 0;
	int headValueUP=4;
	int headValueDOWN=4;
	String enable = "oui";
	int BatterieDelay = 10000;
	Handler hand = new Handler();
	int tempsPropagartion=100;
	public static String commandemouve ="MOVE";
	public static String commandeHead = "HEAD";
	public static String commandBat = "BATTERRIE";
	public static String commandeSensor = "SENSOR";

	long LogFrequencyPerDay = 1;
	long interval;
	Timer remoteTimerLog;
	private Camera camera;
	Camera.Parameters p;
	String communicationType;
	Boolean flag = false;

	final FirebaseDatabase database = FirebaseDatabase.getInstance();
	DatabaseReference myRef = database.getReference().child("alexa");

	public ControlUboControllerImpl(ControlUboModel model) {

		this.model = model;

		application = (SmartRobotApplication) model.getContext();

		mLocationManager = (LocationManager) model.getContext()
				.getSystemService(Context.LOCATION_SERVICE);
		mSensorManager = (SensorManager) model.getContext().getSystemService(
				Context.SENSOR_SERVICE);
		if (remoteTimer != null)
			remoteTimer.cancel();

		if (remoteTimerSensor != null)
			remoteTimerSensor.cancel();
		if (remoteTimerHead != null)
			remoteTimerHead.cancel();
		model.setX(0);
		model.setY(0);
		String data = SmartRobotApplication.getParamFromFile("frequence",
				"Configuration.properties", null);
		if (data != null) {
			delai = 1000 / Integer.parseInt(data.split(" ")[0]);
		}

		model.setHeadMouve(false);

		model.setMovedLoop(false);

		String temps = OrService.getParamFromFile(
				"temps.propagation");
		if(temps!=null){
			tempsPropagartion= Integer.parseInt(temps.split(" ")[0]);
		}

		String move = OrService.getParamFromFile("commande.mouve");

		if(move!=null)
			commandemouve=move;

		String head = OrService
				.getParamFromFile("commande.head");
		if(head!=null)
			commandeHead=head;

		String bat = OrService.getParamFromFile("commande.batterie");

		if(bat!=null)
			commandBat = bat;

		String sensor = OrService
				.getParamFromFile("commande.sensor");
		if(sensor!=null)
			commandeSensor = sensor;
		if (remoteTimerLog!= null) {
			remoteTimerLog.cancel();
			remoteTimerLog=null;
		}

		String frequence = SmartRobotApplication.getParamFromFile("LogFrequencyPerDay",
				"Configuration.properties", null);
		if(frequence!=null){
			interval = (1000L*60L*60L*24L)/Long.parseLong(frequence);
		}else{
			interval = (1000L*60L*60L*24L)/LogFrequencyPerDay;
		}

		remoteTimerLog = new Timer();
		remoteTimerLog.scheduleAtFixedRate(new RemoteLog(), 1,
				interval);

		myRef.addValueEventListener(
				new ValueEventListener() {
					@Override
					public void onDataChange(DataSnapshot dataSnapshot) {
						int i=0;


						for (DataSnapshot myChild : dataSnapshot.getChildren()) {
							i=i+1;
							if(flag==false) {
								if (myChild.child("email").getValue() != null) {
									if (myChild.child("email").getValue().equals(application.getEmail()) &&
											myChild.child("nom").getValue().equals(application.getRobotName())) { flag = true;
									}
								}
							}

							else if (flag == true && i == dataSnapshot.getChildrenCount()-1) {
								String data = myChild.child("commande").getValue().toString();

								if(data.equalsIgnoreCase("avancer"))
									application.notifyObservers("devantB");
								else if(data.equalsIgnoreCase("reculer"))
									application.notifyObservers("arriereB");
								else if(data.equalsIgnoreCase("tourner à droite"))
									application.notifyObservers("droiteB");
								else if(data.equalsIgnoreCase("tourner à gauche"))
									application.notifyObservers("gaucheB");
								else {
									application.notifyObservers("arreter;droite");
									move(0, 0);
								}
								myChild.child("commande").getRef().removeValue();


							}


						}


					}

					@Override
					public void onCancelled(DatabaseError databaseError) {

					}
				});


		init();
	}

	private class RemoteLog extends TimerTask {

		public RemoteLog() {
			// TODO Auto-generated constructor stub
		}

		@Override
		public void run() {
			Intent speechIntent = new Intent(model.getContext(), CheckTTSService.class);
			if(application.isServiceRunning("com.orrobotics.orsmartubo.service.CheckTTSService")){
				model.getContext().stopService(new Intent(model.getContext(), CheckTTSService.class));
			}
			model.getContext().startService(speechIntent);
		}
	}

	@Override
	public void init() {
		int currentapiVersion = android.os.Build.VERSION.SDK_INT;
		String filedata = OrService.getParamFromFile("imu.send");

		if(filedata!=null)
			enable = filedata ;

		sensorDelay = 1000
				/ (delai - tempsPropagartion);

		int tempSensor = Sensor.TYPE_TEMPERATURE;
		if (currentapiVersion <= android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
			tempSensor = Sensor.TYPE_AMBIENT_TEMPERATURE; // Use newer
			// temperature if
			// possible
		}
		communicationType = SmartRobotApplication.getParamFromFile(
				"robot.connexion.type", "Configuration.properties", null);
		Log.i("mbu","---communicationType2 ="+communicationType);
		if(communicationType == null)
			communicationType = "nordic";

		if (currentapiVersion >= android.os.Build.VERSION_CODES.GINGERBREAD) {
			this.imu_pub = new ImuPublisher(mSensorManager, sensorDelay);
			this.magnetic_field_pub = new MagneticFieldPublisher(
					mSensorManager, sensorDelay);
			if (mLocationManager
					.isProviderEnabled(LocationManager.GPS_PROVIDER))
				this.fix_pub = new NavSatFixPublisher(mLocationManager);
			this.imu_pub = new ImuPublisher(mSensorManager, sensorDelay);
			this.fluid_pressure_pub = new FluidPressurePublisher(
					mSensorManager, sensorDelay);
			this.illuminance_pub = new IlluminancePublisher(mSensorManager,
					sensorDelay);
			this.temperature_pub = new TemperaturePublisher(mSensorManager,
					sensorDelay, tempSensor);
		}
		if (remoteTimerSensor == null) {
			remoteTimerSensor = new Timer();
		}


		String headUP = SmartRobotApplication.getParamFromFile(
				"head.up", "Configuration.properties", null);
		if(headUP!=null){
			headValueUP = Integer.parseInt(headUP);
		}

		String headDOWN = SmartRobotApplication.getParamFromFile(
				"head.down", "Configuration.properties", null);
		if(headDOWN!=null){
			headValueDOWN = Integer
					.parseInt(headDOWN);
		}

		if(communicationType.equals("usb"))
			remoteTimerSensor.scheduleAtFixedRate(new SensorRemote(), 1,
					sensorDelay);



		if (application.getShowBatterie().equals("true") && application.getRobotConnexion().equals("true") && application.getIsControl()==true) {

			hand.postDelayed(run, getBatteryDelay());
		}


	}

	@Override
	public void move() {
		// TODO Auto-generated method stub
     Log.i("mbu","---move()---");
		if (remoteTimer == null) {
			remoteTimer = new Timer();

		} else {
			remoteTimer.cancel();
			remoteTimer = null;
			remoteTimer = new Timer();
		}

			remoteTimer.scheduleAtFixedRate(new MyRemoteTaskSimple(), 1, delai);

	}

	private class MyRemoteTaskSimple extends TimerTask {

		public MyRemoteTaskSimple() {
			// TODO Auto-generated constructor stub
		}

		@Override
		public void run() {

			if (model.getMovedLoop() == false) {
				if (remoteTimer != null) {
					remoteTimer.cancel();
					remoteTimer = null;
				}
			} else if (model.getX() != 0 || model.getY() != 0) {

				controlRobot(model.getX(), model.getY());

			}
			else if (model.getX() == 0 && model.getY() == 0) {
				stopRobot();
			}

		}

	}

	private void controlRobot(int x, int y) {
		// TODO Auto-generated method stub
		if (communicationType.equals("usb")) {
			if (UsbSerial.isSerial) {
				UsbSerial.write((commandemouve + " "
						+ y + " " + x));
			} else {
				Transfer.send(commandemouve + " "
						+ y + " " + x);
			}
		}
		else if (communicationType.equals("bluetooth")) {
			BluetoothService.write((commandemouve + " "
					+ y + " " + x));
		}
		else if (communicationType.equals("ble") || communicationType.equals("nordic") ) {

			application.notifyObservers("sendBle;"+commandemouve + " "
					+ y + " " + x);
		}

		model.setCommande(commandemouve + " "
				+ y + " " + x);
	}

	public void stopRobot() {
		// TODO Auto-generated method stub
		if (!model.getCommande().equals(
				commandemouve + " " + 0 + " "
						+ 0)) {
			if (remoteTimer != null) {
				remoteTimer.cancel();
				remoteTimer = null;
			}
			if (communicationType.equals("usb")) {
				if (UsbSerial.isSerial) {
					UsbSerial.write((commandemouve
							+ " " + 0 + " " + 0));
				} else {
					Transfer.send(commandemouve
							+ " " + 0 + " " + 0);
				}
			} else if(communicationType.equals("bluetooth")) {
				BluetoothService.write((commandemouve
						+ " " + 0 + " " + 0));
			}
			else if (communicationType.equals("ble") || communicationType.equals("nordic")) {
				application.notifyObservers("sendBle;"+commandemouve + " "
						+ 0 + " " + 0);
			}

			model.setCommande(commandemouve
					+ " " + 0 + " " + 0);
			model.setMovedLoop(false);
		}
	}

	private class SensorRemote extends TimerTask {

		public SensorRemote() {
			// TODO Auto-generated constructor stub
		}

		@Override
		public void run() {

			if (application.getRobotConnexion().equals("true")) {
				if (application.getDeviceAddress().equals(
						application.getMacConnectedDevice())) {
					if (enable.equals("oui") || enable.equals("Oui")
							|| enable.equals("OUI")) {
						long time_delta_millis = System.currentTimeMillis()
								- SystemClock.uptimeMillis();
						String seq = application.getParam(ProtocolCom.SEQUENCE);
						if (seq.equals(""))
							i = 0;
						else
							i = Long.parseLong(seq);
						String data = commandeSensor
								+ " "
								+ i
								+ " "
								+ time_delta_millis
								+ " "
								+ System.nanoTime()
								+ " "
								+ ImuPublisher.frameId
								+ " "
								+ ImuPublisher.orienX
								+ " "
								+ ImuPublisher.orienY
								+ " "
								+ ImuPublisher.orienZ
								+ " "
								+ ImuPublisher.orienW
								+ " "
								+ Arrays.toString(
								ImuPublisher.orientationCovariance)
								.replace(" ", "")
								+ " "
								+ ImuPublisher.gyroX
								+ " "
								+ ImuPublisher.gyroY
								+ " "
								+ ImuPublisher.gyroZ
								+ " "
								+ Arrays.toString(
								ImuPublisher.angularVelocityCovariance)
								.replace(" ", "")
								+ " "
								+ ImuPublisher.accX
								+ " "
								+ ImuPublisher.accY
								+ " "
								+ ImuPublisher.accZ
								+ " "
								+ Arrays.toString(
								ImuPublisher.linearAccelerationCovariance)
								.replace(" ", "") + " ";
						if (FluidPressurePublisher.frameId != null)
							data = data + FluidPressurePublisher.frameId + " "
									+ FluidPressurePublisher.fluidPressure
									+ " " + FluidPressurePublisher.variance
									+ " ";
						else
							data = data + "null null null ";
						if (IlluminancePublisher.frameId != null)
							data = data + IlluminancePublisher.frameId + " "
									+ IlluminancePublisher.illuminance + " "
									+ IlluminancePublisher.variance + " ";
						else
							data = data + "null null null ";
						if (MagneticFieldPublisher.frameId != null)
							data = data
									+ MagneticFieldPublisher.frameId
									+ " "
									+ MagneticFieldPublisher.x
									+ " "
									+ MagneticFieldPublisher.y
									+ " "
									+ MagneticFieldPublisher.z
									+ " "
									+ Arrays.toString(
									MagneticFieldPublisher.magneticFieldCovariance)
									.replace(" ", "") + " ";
						else
							data = data + "null null null null null ";
						if (mLocationManager
								.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
							if (NavSatFixPublisher.frameId != null)
								data = data
										+ NavSatFixPublisher.frameId
										+ " "
										+ NavSatFixPublisher.currentStatus
										+ " "
										+ NavSatFixPublisher.latitude
										+ " "
										+ NavSatFixPublisher.longitude
										+ " "
										+ NavSatFixPublisher.altitude
										+ " "
										+ Arrays.toString(
										NavSatFixPublisher.positionCovariance)
										.replace(" ", "") + " ";
							else
								data = data + "null null null null null null ";
						}
						if (TemperaturePublisher.frameId != null)
							data = data + TemperaturePublisher.frameId + " "
									+ TemperaturePublisher.temperature + " "
									+ TemperaturePublisher.variance;
						else
							data = data + "null null null ";
						if (communicationType.equals("usb")) {
							if (UsbSerial.isSerial) {
								UsbSerial.write(data);
							} else {
								Transfer.send(data);
							}
						}
						else if(communicationType.equals("bluetooth")){
							BluetoothService.write(data);
						}
						else if (communicationType.equals("ble") || communicationType.equals("nordic")) {
							application.notifyObservers("sendBle;"+data);
						}

						long j = i + 1;
						application.setEditor(ProtocolCom.SEQUENCE,
								Long.toString(j));
					}
				}
			}

		}
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		if (remoteTimer != null) {
			remoteTimer.cancel();
			remoteTimer = null;
		}

		if (remoteTimerSensor != null) {
			remoteTimerSensor.cancel();
			remoteTimerSensor = null;
		}

		if (remoteTimerHead != null) {
			remoteTimerHead.cancel();
			remoteTimerHead = null;
		}
		hand.removeCallbacks(run);

		if (camera != null) {
			camera.release();
		}

	}

	@Override
	public int getSpeedLaterale() {
		// TODO Auto-generated method stub
		String speed_lateral = application.getSpeedLateral();
		int speed_lat = 1;
		if (speed_lateral.equals("laterale") || speed_lateral.equals("")) {
			speed_lat = 1;
		} else {
			speed_lat = Integer.parseInt(speed_lateral);
		}
		return speed_lat;
	}

	@Override
	public int getSpeedLineaire() {
		// TODO Auto-generated method stub
		int speed_line = 1;

		String speed_lineaire = application.getSpeedLinear();
		if (speed_lineaire.equals("lineaire") || speed_lineaire.equals("")) {
			speed_line = 1;
		} else {
			speed_line = Integer.parseInt(speed_lineaire);
		}
		return speed_line;
	}

	@Override
	public void mouveHeadUp(boolean isVocale) {
		// TODO Auto-generated method stub


		if (remoteTimerHead == null) {
			remoteTimerHead = new Timer();

		} else {
			remoteTimerHead.cancel();
			remoteTimerHead = null;
			remoteTimerHead = new Timer();
		}
		headUp = 0;
		remoteTimerHead.scheduleAtFixedRate(new MyRemoteTaskSimpleHeadUP(
				isVocale), 1, delai);
	}

	@Override
	public void mouveHeadDown(boolean isVocale) {
		// TODO Auto-generated method stub

		if (remoteTimerHead == null) {
			remoteTimerHead = new Timer();

		} else {
			remoteTimerHead.cancel();
			remoteTimerHead = null;
			remoteTimerHead = new Timer();
		}
		headDown = 0;
		remoteTimerHead.scheduleAtFixedRate(new MyRemoteTaskSimpleHeadDOWN(
				isVocale), 1, delai);
	}

	private class MyRemoteTaskSimpleHeadUP extends TimerTask {
		boolean isVocale = false;

		public MyRemoteTaskSimpleHeadUP(Boolean isVocale) {
			// TODO Auto-generated constructor stub
			this.isVocale = isVocale;
		}

		@Override
		public void run() {

			if (model.isHeadMouve() == true) {
				if (isVocale == true) {
					headUp = headUp + 1;
					if (headUp <= headValueUP)
					/***********************************/
						if (communicationType.equals("usb")) {
							if (UsbSerial.isSerial) {
								UsbSerial
										.write((commandeHead
												+ " " + "U"));
							} else {
								Transfer.send(commandeHead
										+ " "
										+ "U");
							}
						}
						else
							BluetoothService
									.write((commandeHead
											+ " " + "U"));
					/***********************************/

					else {
						/***********************************/
						if (communicationType.equals("usb")) {
							if (UsbSerial.isSerial) {
								UsbSerial
										.write((commandeHead
												+ " " + "S"));
							} else {
								Transfer.send(commandeHead
										+ " "
										+ "S");
							}
						}
						else
							BluetoothService
									.write((commandeHead
											+ " " + "S"));
						/***********************************/
						remoteTimerHead.cancel();
						remoteTimerHead = null;
						remoteTimerHead = new Timer();
						model.setMovedLoop(false);
					}
				} else {
					if (communicationType.equals("usb")) {
						if (UsbSerial.isSerial) {
							UsbSerial.write((commandeHead + " " + "U")
							);
						} else {
							Transfer.send(commandeHead
									+ " " + "U");
						}
					}
					else
						BluetoothService.write((commandeHead + " " + "U")
						);
				}
			} else {
				if (communicationType.equals("usb")) {
					if (UsbSerial.isSerial) {
						UsbSerial.write((commandeHead + " " + "S")
						);
					} else {
						Transfer.send(commandeHead
								+ " " + "S");
					}
				}
				else
					BluetoothService.write((commandeHead + " " + "S")
					);
				remoteTimerHead.cancel();
				remoteTimerHead = null;
				remoteTimerHead = new Timer();
				model.setMovedLoop(false);
			}

		}
	}

	private class MyRemoteTaskSimpleHeadDOWN extends TimerTask {
		boolean isVocale = false;

		public MyRemoteTaskSimpleHeadDOWN(Boolean isVocale) {
			// TODO Auto-generated constructor stub
			this.isVocale = isVocale;
		}

		@Override
		public void run() {
			if (model.isHeadMouve() == true) {
				Log.i("Serial", "down , is vocale : " + isVocale);
				if (isVocale == true) {

					headDown = headDown + 1;
					if (headDown <= headValueDOWN) {
						if (communicationType.equals("usb")) {
							if (UsbSerial.isSerial) {
								UsbSerial
										.write((commandeHead
												+ " " + "D"));
							} else {
								Transfer.send(commandeHead
										+ " "
										+ "D");
							}
						}
						else
							BluetoothService
									.write((commandeHead
											+ " " + "D"));
					} else {
						if (communicationType.equals("usb")) {
							if (UsbSerial.isSerial) {
								UsbSerial
										.write((commandeHead
												+ " " + "S"));
							} else {
								Transfer.send(commandeHead
										+ " "
										+ "S");
							}
						}
						else

							remoteTimerHead.cancel();
						remoteTimerHead = null;
						remoteTimerHead = new Timer();
						model.setMovedLoop(false);
					}

				} else
					Log.i("Serial", "down , is serial : " + UsbSerial.isSerial);
				if (communicationType.equals("usb")) {
					if (UsbSerial.isSerial) {
						UsbSerial.write((commandeHead + " " + "D")
						);
					} else {
						Transfer.send(commandeHead
								+ " " + "D");
					}
				}
				else
					BluetoothService.write((commandeHead + " " + "D")
					);
			} else {
				if (communicationType.equals("usb")) {
					if (UsbSerial.isSerial) {
						UsbSerial.write((commandeHead + " " + "S")
						);
					} else {
						Transfer.send(commandeHead
								+ " " + "S");
					}
				}
				else

					remoteTimerHead.cancel();
				remoteTimerHead = null;
				remoteTimerHead = new Timer();
				model.setMovedLoop(false);
			}

		}
	}


	@Override
	public void move(int x, int y) {
		// TODO Auto-generated method stub
		if (remoteTimer != null) {
			remoteTimer.cancel();
			remoteTimer = null;
		}
		if (model.getMovedLoop() == false) {
			GestionDesFichiers.log("INFO",
					"contrôle à distance " + model.getMovedLoop(),
					"Log_teambot");

		} else if (x != 0 || y != 0) {

			controlRobot(x, y);

		} else if (x == 0 && y == 0) {
			stopRobot();
		}

	}

	public void stopHead() {
		// TODO Auto-generated method stub
		if (communicationType.equals("usb")) {
			if (UsbSerial.isSerial) {
				UsbSerial
						.write((commandeHead + " " + "S")
						);
			} else {
				Transfer.send(commandeHead + " " + "S");
			}
		}
		else
			BluetoothService
					.write((commandeHead + " " + "S")
					);
	}

	@Override
	public void getBatterie() {
		// TODO Auto-generated method stub
		if(communicationType.equals("usb"))
			Transfer.send(commandBat);

	}
	public boolean hasFlash() {
		if (camera == null) {
			return false;
		}

		Camera.Parameters parameters = camera.getParameters();

		if(!model.getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH))
			return false;

		if (parameters.getFlashMode() == null) {
			return false;
		}

		List<String> supportedFlashModes = parameters.getSupportedFlashModes();
		if (supportedFlashModes == null || supportedFlashModes.isEmpty() || supportedFlashModes.size() == 1 && supportedFlashModes.get(0).equals(Camera.Parameters.FLASH_MODE_OFF)) {
			return false;
		}

		return true;
	}
	@Override
	public boolean light(boolean lighOn) {
		if(!application.isTelepresence() && !application.getBackgroundType().equals("camera")){
			if (camera == null) {
				camera = Camera.open();
				p = camera.getParameters();
			}
			if (hasFlash()) {
				Log.i("info", "has flash !");
				if (!lighOn) {

					Log.i("info", "turn off torch !");

					p.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
					camera.setParameters(p);
					camera.stopPreview();
					if (camera != null) {
						camera.release();
						camera = null;
					}

				} else {

					Log.i("info", "turn on torch !");

					p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
					try {
						camera.setParameters(p);
						camera.startPreview();
					} catch (RuntimeException e) {

					}

				}
				return true;
			} else {
				Log.i("info", "no flash !");
				if (camera != null) {
					camera.release();
					camera = null;
				}
				application.setEditor(RemoteControlUtil.LIGHT,"false");
				model.getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						Toast.makeText(model.getContext(), com.orrobotics.orsmartubo.R.string.noFlash, Toast.LENGTH_SHORT).show();
					}
				});
				return false;
			}
		}
		else{
			application.getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(application.getApplicationContext(), R.string.camUsed,Toast.LENGTH_SHORT).show();
				}
			});
			return false;
		}
	}


	Runnable run = new Runnable() {
		@Override
		public void run() {
			getBatterie();


			if (application.getShowBatterie().equals("true") && application.getRobotConnexion().equals("true") && application.getIsControl()==true) {
				hand.postDelayed(this, getBatteryDelay());
			}
		}
	};

	public int getBatteryDelay() {

		String[] values = application.getBatteryDelay().split(" ");
		if (values[1].equals("min"))
			BatterieDelay = Integer.parseInt(values[0]) * 1000 * 60;
		else
			BatterieDelay = Integer.parseInt(values[0]) * 1000;
		return BatterieDelay;
	}

}
