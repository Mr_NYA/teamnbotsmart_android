package com.orrobotics.orsmartubo.usb;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.Socket;

import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.orrobotics.orsmartrobot.application.SmartRobotApplication;
import com.orrobotics.orsmartrobot.util.GestionDesFichiers;
import com.orrobotics.orsmartrobot.util.RemoteControlUtil;
import com.orrobotics.orsmartubo.control.ControlUboControllerImpl;
import com.orrobotics.orsmartubo.service.OrService;

import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.util.Log;

public class Transfer implements Runnable {

	private static Socket socket;
	Context context;
	static SmartRobotApplication model;
	BufferedInputStream in;
	Boolean flagTransfert = true;
	static String commande = null;
	static File pathOfMyFile = Environment
			.getExternalStoragePublicDirectory("/SmartRobot/CommandeRobot");
	public Transfer(Socket client, Context context) {
		socket = client;
		this.context = context;
		model = (SmartRobotApplication) context;
	}

	@Override
	public void run() {

		try {
			String currCMD = "";
			in = new BufferedInputStream(socket.getInputStream());
			while (flagTransfert) {
				try {
					if (socket == null)
						break;
					if (!socket.isConnected())
						break;


                        if (model.getMacConnectedDevice().equals("") || (!model.getMacConnectedDevice().equals(
								model.getDeviceAddress() )&& model.getIsControl())){
							Transfer.send(OrService.commandeAdresse);

                        }

					if (socket.isConnected())
						currCMD = readCMDFromSocket(in);

					if (currCMD.contains("mac")) {
						model.notifyObservers("adresse" + currCMD);
					}
					if (currCMD.contains(ControlUboControllerImpl.commandBat)) {
						model.notifyObservers("batterie;"
								+ currCMD.split(";")[1]);

					}

				} catch (Exception e) {
					GestionDesFichiers.log("Erreur ",
							"error réception socket ", "Log_teambot");

				}
			}
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
			GestionDesFichiers.log("Erreur ", "out in close ", "Log_teambot");

		}
	}

	public String readCMDFromSocket(InputStream in)
			throws java.net.SocketException {
		int MAX_BUFFER_BYTES = 4048;
		String msg = "";
		byte[] tempbuffer = new byte[MAX_BUFFER_BYTES];
		try {
			String connexion = model.getRobotConnexion();
			if (!socket.isClosed()) {
				int numReadedBytes = in.read(tempbuffer, 0, tempbuffer.length);
				if (numReadedBytes < 0) {

					flagTransfert = false;
					if (connexion.equals("true")) {
						model.setEditor(RemoteControlUtil.CONNECTED_TO_APP,
								"false");
						model.setRobotConnexion();
						model.notifyObservers("RobotOff");
						model.notifyObservers("close");
					}
				} else {

					msg = new String(tempbuffer, 0, numReadedBytes, "utf-8");
					model.logMe( "Commande reçue : "+msg,null,"/storage/emulated/0/" +
									"SmartRobot/CommandeRobot/logCommandeRobot",
							"logCommandeRobot",pathOfMyFile);
					if (connexion.equals("") || connexion.equals("false")) {
						model.setEditor(RemoteControlUtil.CONNECTED_TO_APP,
								"true");
						model.setRobotConnexion();
						model.notifyObservers("RobotOn");

					}

				}

			}
			tempbuffer = null;
		} catch (Exception e) {
			Intent serviceUbo = new Intent(model.getApplicationContext(), OrService.class);
			model.stopService(serviceUbo);
			model.startService(serviceUbo);
			e.printStackTrace();
			GestionDesFichiers.log("Erreur ", "error de lecture socket ",
					"Log_teambot");

		}
		return msg;
	}

	public static void send(String message) {
		try {
			if (message != null) {
				if (!message.equals("")) {
					model.logMe( "Commande envoyée : "+message,null,"/storage/emulated/0/" +
									"SmartRobot/CommandeRobot/logCommandeRobot",
							"logCommandeRobot",pathOfMyFile);
					commande = message;
					if (socket != null) {
						if (!socket.isClosed()) {
							Iterable<String> result = Splitter
									.fixedLength(4096).split(message);
							String[] parts = Iterables.toArray(result,
									String.class);
							for (int i = 0; i < parts.length; i++) {
								if (i == 0)
									commande = parts[0];
								else
									commande = ControlUboControllerImpl.commandeSensor
											+ " " + parts[i];

								PrintWriter out = new PrintWriter(
										socket.getOutputStream(), true);
								if (commande != null) {
									if (!commande.equals("")
											|| !commande.equals("null")) {
										out.print(commande);
										out.print("\n");
										out.flush();
									}
								}

								commande = null;

							}

						}

					} else {
						GestionDesFichiers.log("Erreur ", "error d'envoie ",
								"Log_teambot");
					}
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e("Erreur ", e.getStackTrace().toString());
			GestionDesFichiers.log("Erreur ", "error d'envoie ", "Log_teambot");

		}

	}

}
