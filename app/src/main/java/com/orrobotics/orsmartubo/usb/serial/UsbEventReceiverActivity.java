package com.orrobotics.orsmartubo.usb.serial;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Parcelable;

import com.orrobotics.orsmartrobot.util.GestionDesFichiers;

public class UsbEventReceiverActivity extends Activity
{
    public static final String ACTION_USB_DEVICE_ATTACHED  = "com.orrobotics.orsmartubo.ACTION_USB_DEVICE_ATTACHED";
    PendingIntent mPermissionIntent = null;
    UsbManager mUsbManager = null;

    @SuppressLint("NewApi")
    private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {

        @SuppressLint("NewApi")
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            UsbDevice usbDevice = (UsbDevice)intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
            if (ACTION_USB_DEVICE_ATTACHED.equals(action)) {
                // Permission requested
                synchronized (this) {
                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        // User has granted permission
                        // ... Setup your UsbDeviceConnection via mUsbManager.openDevice(usbDevice) ...
                    } else {
                        // User has denied permission
                    }
                }
            }
            if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {
                // Device removed
                synchronized (this) {
                    // ... Check to see if usbDevice is yours and cleanup ...
                }
            }
            if (UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(action)) {
                // Device attached
                synchronized (this) {
                    // Qualify the new device to suit your needs and request permission
                    GestionDesFichiers.log("INFO"," read arcuino vendor-id "+usbDevice.getVendorId()+" usbDevice.getProductId() "+usbDevice.getProductId(), "Log_SmartRobot");
                    if ((usbDevice.getVendorId() == 1256) && (usbDevice.getProductId() == 26720)) {
                        mUsbManager.requestPermission(usbDevice, mPermissionIntent);
                    }
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // ... App Specific Setup Here ...

        mUsbManager = (UsbManager)getSystemService(Context.USB_SERVICE);

        // Register an intent filter so we can get permission to connect
        // to the device and get device attached/removed messages
        mPermissionIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_DEVICE_ATTACHED), 0);
        IntentFilter filter = new IntentFilter(ACTION_USB_DEVICE_ATTACHED);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        registerReceiver(mUsbReceiver, filter);

        // ... More App Specific Setup ...
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        Intent intent = getIntent();
        if (intent != null)
        {
            if (intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_ATTACHED))
            {
                Parcelable usbDevice = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);

                // Create a new intent and put the usb device in as an extra
                Intent broadcastIntent = new Intent(ACTION_USB_DEVICE_ATTACHED);
                broadcastIntent.putExtra(UsbManager.EXTRA_DEVICE, usbDevice);

                // Broadcast this event so we can receive it
                sendBroadcast(broadcastIntent);
            }
        }

        // Close the activity
        finish();
    }
}